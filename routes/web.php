<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group([ 'prefix' => 'admin','middleware' => 'auth.hrs'], function () {
    Route::get('/', 'AdminController@index');
});
Route::group([ 'prefix' => 'admin','middleware' => 'auth.admin'], function () {
    Route::get('/', 'AdminController@index');


	Route::get('/site/home', 'SiteController@get_home');
	Route::post('/site/home', 'SiteController@home');



	Route::get('/changepassword', 'AdminController@get_changepassword');
	Route::post('/changepassword', 'AdminController@changepassword');

	Route::get('/faqs/new', 'FaqsController@get_newfaq');
	Route::post('/faqs/new', 'FaqsController@newfaq');
	Route::get('/faqs/{id}/update', 'FaqsController@get_updatefaq');
	Route::post('/faqs/{id}/update', 'FaqsController@updatefaq');
	Route::get('/faqs', 'FaqsController@get_faqs');
	Route::get('/faqs/{id}/delete', 'FaqsController@destroy_faqs');


	Route::get('/slider/new', 'SliderController@get_newpage');
	Route::post('/slider/new', 'SliderController@newpage');
	Route::get('/slider/{id}/update', 'SliderController@get_updatepage');
	Route::get('/slider/{id}/delete', 'SliderController@destroy_page');
	Route::post('/slider/{id}/update', 'SliderController@updatepage');
	Route::get('/slider', 'SliderController@get_admin_pages');

	Route::get('/courses/new', 'CoursesController@view_new');
	Route::post('/courses/new', 'CoursesController@store');
	Route::get('/courses/{id}/update', 'CoursesController@view_update');
	Route::post('/courses/{id}/update', 'CoursesController@update');
	Route::get('/courses', 'CoursesController@Items');
	Route::get('/courses/report', 'CoursesController@courses_report');
	Route::get('/courses/{id}/delete', 'CoursesController@destroy_Course');
    Route::get('/courses/import', 'CoursesController@GetImportCoursers');
    Route::post('/courses/import', 'CoursesController@importCoursers');
    Route::get('/courses/{id}/wait', 'CoursesController@wait');
    Route::get('/courses/{id}/unwait', 'CoursesController@unwait');

	Route::post('/ajax/category', 'CoursesController@ajax_get_category');

	Route::get('/customers', 'CustomerController@customers');
	Route::get('/customers/{id}/update', 'CustomerController@getCustomerUpdate');
	Route::post('/customers/{id}/update', 'CustomerController@customerUpdate');
    Route::get('/customers/{id}/delete', 'CustomerController@Delete');
	Route::get('/customers/report', 'CustomerController@customers_report');
    Route::post('/customers/multiDelete', 'CustomerController@multiDelete');
    Route::get('/customers/uncourse', 'CustomerController@uncourse');

    Route::post('/allcustomers', 'CustomerController@allcustomers');
    Route::post('/search_customer', 'CustomerController@search_customer');
    Route::post('/search_orders_ev', 'Evaluation_timeController@search_orders_ev');


    Route::get('/orders', 'CoursesController@orders');
    Route::get('/orders_unpaid', 'CoursesController@orders_unpaid');
    Route::get('/orders_new', 'CoursesController@orders_new');
    Route::get('/orders/{id}/update', 'CoursesController@GetorderUpdate');
	Route::post('/orders/{id}/update', 'CoursesController@OrderUpdate');
	Route::get('/orders/report', 'CoursesController@order_report');
    Route::get('/orders/{id}/delete', 'CoursesController@DeleteOrder');
    Route::post('/orders/multiDelete', 'CoursesController@multiDelete');
    Route::post('/orders/multiDelete_unpaid', 'CoursesController@multiDelete_unpaid');
    Route::post('/orders/multiDelete_paid', 'CoursesController@multiDelete_paid');

    Route::get('/orders/report_unpaid', 'CoursesController@order_report_unpaid');
    Route::post('/orders/multiDelete_course', 'CoursesController@multiDelete_course');
    Route::get('/orders/{id}/delete_order', 'CoursesController@delete_order');
    Route::get('/orders/report_new', 'CoursesController@order_report_new');
    Route::get('/orders_new_course', 'CoursesController@orders_new_course');
    Route::post('/import_orders', 'CoursesController@import_orders');
    Route::post('/allorders_new', 'CoursesController@allorders_new');
    Route::post('/searchO', 'CoursesController@searchO');
    Route::get('/orders_school', 'CoursesController@orders_school');
    Route::post('/import_orders_school', 'CoursesController@import_orders_school');
    Route::get('/orders/report_new_1/{i}', 'CoursesController@order_report_new_1');
    Route::get('/orders/putdata/{i}', 'CoursesController@putdata');


    Route::post('/orders_unpaid_all', 'CoursesController@orders_unpaid_all');
    Route::post('/search_orders_unpaid', 'CoursesController@search_orders_unpaid');
    Route::post('/orders_paid_all', 'CoursesController@orders_paid_all');
    Route::post('/search_orders_paid', 'CoursesController@search_orders_paid');

	Route::get('/news/new', 'NewsController@view_new');
	Route::post('/news/new', 'NewsController@store');
	Route::get('/news/{id}/update', 'NewsController@view_update');
	Route::post('/news/{id}/update', 'NewsController@update');
	Route::get('/news', 'NewsController@news');
	Route::get('/news/{id}/delete', 'NewsController@destroy');


	Route::get('/pages/new', 'PagesController@view_new');
	Route::post('/pages/new', 'PagesController@store');
	Route::get('/pages/{id}/update', 'PagesController@view_update');
	Route::post('/pages/{id}/update', 'PagesController@update');
	Route::get('/pages', 'PagesController@pages');


	Route::get('/branches', 'BranchesController@get_all');
	Route::get('/branches/new', 'BranchesController@view_new');
	Route::post('/branches/new', 'BranchesController@store');
	Route::get('/branches/{id}/update', 'BranchesController@view_update');
	Route::post('/branches/{id}/update', 'BranchesController@update');
	Route::get('/branches/{id}/delete', 'BranchesController@destroy');


	Route::get('/photos', 'SliderController@get_admin_photos');
	Route::get('/photos/new', 'SliderController@get_newphoto');
	Route::post('/photos/new', 'SliderController@newphoto');
	Route::get('/photos/{id}/delete', 'SliderController@destroy_photo');


	Route::get('/applications', 'CoursesController@get_Apps');
	Route::get('/applications/new', 'CoursesController@get_newApp');
	Route::post('/applications/new', 'CoursesController@newApp');
	Route::get('/applications/{id}/update', 'CoursesController@get_updateApp');
	Route::post('/applications/{id}/update', 'CoursesController@updateApp');
	Route::get('/applications/{id}/delete', 'CoursesController@destroy_App');


	Route::get('/category/new', 'CoursesController@get_newCategory');
	Route::post('/category/new', 'CoursesController@newCategory');
	Route::get('/category/{id}/update', 'CoursesController@get_updateCategory');
	Route::post('/category/{id}/update', 'CoursesController@updateCategory');
	Route::get('/category/{id}/delete', 'CoursesController@destroy_Category');
	Route::get('/category/{id}', 'CoursesController@get_Category');

	Route::get('/period', 'CoursesController@get_Period');
	Route::get('/period/new', 'CoursesController@get_newPeriod');
	Route::post('/period/new', 'CoursesController@newPeriod');
	Route::get('/period/{id}/update', 'CoursesController@get_updatePeriod');
	Route::post('/period/{id}/update', 'CoursesController@updatePeriod');
	Route::get('/period/{id}/delete', 'CoursesController@destroy_Period');

    Route::get('/eva_period', 'Evaluation_periodController@get_Period');
    Route::get('/eva_period/new', 'Evaluation_periodController@get_newPeriod');
    Route::post('/eva_period/new', 'Evaluation_periodController@newPeriod');
    Route::get('/eva_period/{id}/update', 'Evaluation_periodController@get_updatePeriod');
    Route::post('/eva_period/{id}/update', 'Evaluation_periodController@updatePeriod');
    Route::get('/eva_period/{id}/delete', 'Evaluation_periodController@destroy_Period');

    Route::post('/orders/multiDelete_orders', 'Evaluation_timeController@multiDelete');


	/*

	Route::get('/coupons/new', 'CopounController@GetAdd');
	Route::post('/coupons/new', 'CopounController@Store');
	Route::get('/coupons/{id}/update', 'CopounController@getUpdate');
	Route::post('/coupons/{id}/update', 'CopounController@Update');
	Route::get('/coupons/{id}/delete', 'CopounController@Destroy');
	Route::get('/coupons', 'CopounController@GetItems');

	Route::get('/doctors/new', 'DoctorController@GetAdd');
	Route::post('/doctors/new', 'DoctorController@Store');
	Route::get('/doctors/{id}/update', 'DoctorController@getUpdate');
	Route::post('/doctors/{id}/update', 'DoctorController@Update');
	Route::get('/doctors', 'DoctorController@GetItems');
	Route::get('/doctors/{id}/delete', 'DoctorController@Destroy');


	Route::get('/categories', 'CategoryController@get_all');
	Route::get('/categories/new', 'CategoryController@view_new');
	Route::post('/categories/new', 'CategoryController@store');
	Route::get('/categories/{id}/update', 'CategoryController@view_update');
	Route::post('/categories/{id}/update', 'CategoryController@update');
	Route::get('/categories/{id}/delete', 'CategoryController@destroy');

	Route::get('/products', 'ProductController@get_all');
	Route::get('/products/new', 'ProductController@view_new');
	Route::post('/products/new', 'ProductController@store');
	Route::get('/products/{id}/update', 'ProductController@view_update');
	Route::post('/products/{id}/update', 'ProductController@update');
	Route::get('/products/{id}/delete', 'ProductController@destroy');

	Route::post('/photos/upload', 'ProductController@addPhotos');
	Route::post('/photos/delete', 'ProductController@delete_photo');





	Route::get('/site/about', 'SiteController@get_about');
	Route::post('/site/about', 'SiteController@about');





	Route::get('/settings', 'SettingsController@get_settings');
	Route::post('/settings', 'SettingsController@update_settings');








	Route::get('/clients/new', 'SliderController@get_new');
	Route::post('/clients/new', 'SliderController@store');
	Route::get('/clients/{id}/update', 'SliderController@get_update');
	Route::get('/clients/{id}/delete', 'SliderController@destroy_page');
	Route::post('/clients/{id}/update', 'SliderController@update');
	Route::get('/clients', 'SliderController@get_admin_client');

*/
// Mai works /////


    Route::get('/evaluation_time', 'Evaluation_timeController@get_Period');
    Route::get('/evaluation_time/new', 'Evaluation_timeController@get_newPeriod');
    Route::post('/evaluation_time/new', 'Evaluation_timeController@newPeriod');
    Route::get('/evaluation_time/{id}/update', 'Evaluation_timeController@get_updatePeriod');
    Route::post('/evaluation_time/{id}/update', 'Evaluation_timeController@updatePeriod');
    Route::get('/evaluation_time/{id}/delete', 'Evaluation_timeController@destroy_Period');
    Route::get('/evaluation_time/{id}/active', 'Evaluation_timeController@active');
    Route::get('/evaluation_time/{id}/inactive', 'Evaluation_timeController@inactive');
    Route::get('/evaluation_time/orders', 'Evaluation_timeController@orders');
    Route::get('/evaluation_time/export_excel', 'Evaluation_timeController@export_excel');
    Route::post('/orders_evaluation', 'Evaluation_timeController@orders_evaluation');

    Route::get('/evaluation_time/{id}/delete_reserve', 'Evaluation_timeController@delete_reserve');
    Route::get('/evaluation_time/{id}/attend', 'Evaluation_timeController@attend');
    Route::get('/evaluation_time/{id}/inattend', 'Evaluation_timeController@inattend');

    Route::get('/evaluation_time/result/{id}', 'Evaluation_timeController@result');
    Route::post('/evaluation_time/import_result', 'Evaluation_timeController@import_result');
    Route::get('/evaluation_time/viewresult/{id}', 'Evaluation_timeController@viewresult');

    Route::post('/evaluation_time/ques', 'Evaluation_timeController@ques');


    Route::get('/video/new', 'VideoController@get_newPeriod');
    Route::post('/video/new', 'VideoController@new');
    Route::get('/video', 'VideoController@videos');

    Route::get('/video/{id}/update', 'VideoController@get_updatePeriod');
    Route::post('/video/{id}/update', 'VideoController@updatePeriod');
    Route::get('/video/{id}/delete', 'VideoController@destroy_Period');

    Route::get('/getschools', 'SchoolController@getschools');
    Route::get('/school/{id}/active', 'SchoolController@active');
    Route::get('/school/{id}/inactive', 'SchoolController@inactive');
    Route::get('/report', 'SchoolController@report');
    Route::get('/school/report', 'SchoolController@exportreport');
    Route::get('/school/{id}/update', 'SchoolController@view_update');
    Route::post('/school/{id}/update', 'SchoolController@update');

    //End Mai///

});




Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
	Route::get('/login', 'Auth\LoginController@showLoginForm');
	//Route::get('/newuser', 'Auth\LoginController@newuser');
    Route::post('login', 'Auth\LoginController@authenticate');
    Route::get('logout', 'Auth\LoginController@getlogout');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});









Route::get('/HijriToJD/{s}/{p}/{k}', 'CustomerController@HijriToJD');

Route::get('/login', 'CustomerController@get_login');
Route::get('/signup', 'CustomerController@get_signup');
Route::get('/resetpassword', 'CustomerController@get_ressetpassword');
Route::post('ajax/resetpassword', 'CustomerController@ressetpassword_confirm');
Route::get('/resetlink/{token}', 'CustomerController@setNewPassword');
Route::get('/map', 'MapController@view_map');
Route::get('/test_send', 'CustomerController@test_send');




Route::post('ajax/login', 'CustomerController@login');
Route::post('ajax/register', 'CustomerController@signup');
Route::get('ajax/contact', 'SiteController@contactform_ar');
Route::get('/cron/orders', 'CoursesController@exportOrders');
Route::get('/cron/ordersunpaid', 'CoursesController@exportOrdersunpaid');
Route::get('/cron/school', 'SchoolController@exportreport_drive');
Route::get('/cron/customer', 'CustomerController@customers_drive');
Route::get('/cron/exportOrders_new', 'CoursesController@exportOrders_new');


Route::group([ 'middleware' => 'auth.doctor'], function () {
    Route::get('profile', 'CustomerController@profile');
    Route::post('profile', 'CustomerController@profileUpdate');

    Route::get('mycourses', 'CoursesController@mycourses');
    Route::get('mycourses/{id}', 'CoursesController@mycourse');

    Route::get('logout', 'CustomerController@logout');

    /// MAi New//

    Route::get('evaluation', 'Evaluation_timeController@evaluation');
    Route::get('get_time', 'Evaluation_timeController@get_time');
    Route::post('reserve', 'Evaluation_timeController@reserve');
    Route::get('/course_start', 'CoursesController@course_start');
    Route::get('/videos', 'CoursesController@videos');

    Route::get('/course_start_wait', 'CoursesController@course_start_wait');
    Route::get('/course_start_new', 'CoursesController@course_start_new');
    Route::get('/add_no', 'CoursesController@add_no');


    //End Mai New//
});
Route::get('payment_course/{id}', 'CoursesController@payment_course');
Route::get('payment/{id}/{r}', 'CoursesController@payment');
Route::get('retry_pay/{id}', 'CoursesController@retry_pay');

Route::get('course/payment/{id}', 'CoursesController@payment');

	Route::post('ajax/subsCours', 'CoursesController@subsCours');

	Route::post('ajax/testing', 'SiteController@testing');
	Route::post('ajax/wrongAnswers', 'SiteController@checkAnswers');

	Route::get('/', 'SiteController@site_home');
	//Route::get('/courses', 'CoursesController@getCourses');
	Route::get('/courses/s_mail', 'CoursesController@s_mail');

	Route::get('/about', 'SiteController@aboutpage');
	Route::get('/services', 'SiteController@servicespage');
	Route::get('/times', 'SiteController@timespage');
	Route::get('/branches', 'SiteController@branchespage');
	Route::get('/gallery', 'SiteController@gallerypage');
	Route::get('/faq', 'SiteController@faqpage');
	Route::get('/contact', 'SiteController@contactpage');
	Route::get('/testing', 'SiteController@testingpage');
Route::post('/testing_lang', 'SiteController@testingpage_lang');

	Route::get('/page/{id}', 'SiteController@page');

	Route::get('/news', 'SiteController@news');
	Route::get('/news/{id}', 'SiteController@news_item');

	/**************english ***********/
	Route::group(['prefix' => 'en'], function () {
		if(Request::segment(1) == 'en' ){
			App::setLocale('en');
		}



		Route::get('/', 'SiteController@site_home');
		Route::get('/courses', 'CoursesController@getCourses');
		Route::get('/about', 'SiteController@aboutpage');
		Route::get('/services', 'SiteController@servicespage');
		Route::get('/times', 'SiteController@timespage');
		Route::get('/branches', 'SiteController@branchespage');
		Route::get('/gallery', 'SiteController@gallerypage');
		Route::get('/faq', 'SiteController@faqpage');
		Route::get('/contact', 'SiteController@contactpage');
		Route::get('/testing', 'SiteController@testingpage');
		Route::get('/page/{id}', 'SiteController@page');
		Route::get('/news', 'SiteController@news');
		Route::get('/news/{id}', 'SiteController@news_item');

		Route::get('/resetpassword', 'CustomerController@get_ressetpassword_en');

	});
        Route::get('/courses/getpay/{id}/{r}', 'CoursesController@getpay');
                Route::get('/courses/testpay/{id}', 'CoursesController@testpay');

        Route::get('/courses/updatepay/{id}', 'CoursesController@updatepay');

        Route::get('/courses/result', 'CoursesController@result');

	Route::group([ 'prefix' => 'en','middleware' => 'auth.basic'], function () {
		Route::get('member', 'Auth\LoginController@member');

		Route::get('mycourses', 'CoursesController@mycourses');
		Route::get('mycourses/{id}', 'CoursesController@mycourse');
		Route::get('profile', 'Auth\LoginController@profile');
		Route::post('profile', 'Auth\LoginController@profileUpdate');


		Route::get('logout', 'Auth\LoginController@logout');



		/// MAi New//

        Route::get('evaluation', 'Evaluation_timeController@evaluation');
        Route::get('get_time', 'Evaluation_timeController@get_time');
        Route::get('reserve/{id}', 'Evaluation_timeController@reserve');

        //End Mai New//
	});

Route::get('test_mail', 'CoursesController@test_mail');
Route::get('sendsmstest', 'CoursesController@sendsmstest');

Route::get('createFolder/{id}', 'DriveController@createFolder');
Route::get('/new_dir', 'CustomerController@new_dir');


		Route::get('/condate', 'CustomerController@condate');
		// Route::get('/runquery', 'CustomerController@runquery');
Route::get('/checkPayment', 'CoursesController@checkPayment');

Route::get('/checkPayment2', 'CoursesController@checkPayment2');

Route::get('/condate', 'CustomerController@condate');
Route::get('/remind_sms', 'CustomerController@remind_sms');

Route::get('/export_drive', 'Evaluation_timeController@export_drive');
Route::get('/remind_pay', 'CustomerController@remind_pay');

Route::get('/special_send', 'CustomerController@special_send');

Route::get('/idtest', 'CustomerController@idtest');
/*
Route::post('en/contactform', 'SiteController@contactform');
	Route::get('en/order', 'SiteController@site_order');
	Route::post('en/orderform', 'SiteController@orderform');
	Route::get('en/projects', 'SiteController@projects');
	Route::get('en/projects/{id}', 'SiteController@project');
Route::get('/contact', 'SiteController@site_contactus_ar');
	Route::post('/contactform', 'SiteController@contactform_ar');
	Route::get('/order', 'SiteController@site_order_ar');
	Route::post('/orderform', 'SiteController@orderform_ar');

 */