<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/nationality','Api\CustomerController@Nationality');
Route::post('user/signup','Api\CustomerController@signup');
Route::post('user/login','Api\CustomerController@login');
Route::post('user/resetpassword','Api\CustomerController@resetpassword');
Route::post('user/checknewpasscode','Api\CustomerController@checknewpass');
Route::post('user/changepass','Api\CustomerController@changepass');
Route::get('course/ispaid/{id}/{user}','Api\CoursesController@ispaid');
Route::get('course/pay_exit/{msg}','Api\CoursesController@pay_exit');
	//Route::get('course/mycourses','Api\CoursesController@mycourses');

Route::group([ 'middleware' => 'auth:api'], function () {
    Route::get('user/profile','Api\CustomerController@profile');
	Route::post('user/changepassword','Api\CustomerController@changepassword');
	Route::post('user/updateprofile','Api\CustomerController@profileUpdate');
	Route::post('course/sub','Api\CoursesController@subsCours');
    Route::get('payment/{id}', 'Api\CoursesController@payment');
    Route::get('course/payment/{id}/{user}','Api\CoursesController@payment');
    Route::get('course/payment_online/{id}/{user}','Api\CoursesController@payment_online');

	Route::get('mycourses','Api\CoursesController@mycourses');
	Route::get('mycourses/{id}','Api\CoursesController@mycourse');
    Route::get('/get_time','Api\CoursesController@get_time');
    Route::get('/videos','Api\CoursesController@videos');
    Route::get('/testing', 'Api\CoursesController@testingpage');
    Route::post('/testing_lang', 'Api\CoursesController@testingpage_lang');
    Route::post('/reserve', 'Api\CoursesController@reserve');
    Route::post('/checkAnswers', 'Api\CoursesController@checkAnswers');
    Route::post('/testing', 'Api\CoursesController@testing');
    Route::get('/course_start_new','Api\CoursesController@course_start_new');
    Route::get('/evaluation','Api\CoursesController@evaluation');



});
Route::get('payment_course/{id}', 'Api\CoursesController@payment_course');

Route::get('/evaluation_check','Api\CoursesController@evaluation_check');



Route::get('/applications','Api\CoursesController@applications');
Route::get('/schools','Api\CoursesController@schools');
Route::get('/courses','Api\CoursesController@items');
Route::get('/page/{id}','Api\SettingsController@Page');
Route::get('/slider','Api\SettingsController@slider');

Route::get('/news','Api\CoursesController@news');
Route::get('/news/{id}','Api\CoursesController@news_item');

Route::get('/branches','Api\SettingsController@branches');
Route::get('/gallery','Api\SettingsController@gallery');
Route::get('/faqs','Api\SettingsController@faqs');
Route::post('/contact','Api\SettingsController@contact');
Route::get('/schools','Api\SettingsController@get_schools');
Route::get('/course_start','Api\CoursesController@course_start');
Route::get('/course_start_wait','Api\CoursesController@course_start_wait');

Route::get('/required',function(){
   
		$optios = DB::table('site_option')->where('varname','hm_bl4_required')->select('value')->first();
			$res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $optios
		);
		return response()->json($res);

	
});
