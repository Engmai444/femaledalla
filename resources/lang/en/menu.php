<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'menu'       	=> 'Menu',
	'home'       	=> 'Home',
	'about'       	=> 'About',
	'services'      => 'Services',
	'times'       	=> 'Work time',
	'testing'       => 'Questions',
	'news'       	=> 'News',
	'courses'       => 'Courses',
	'branches'      => 'Branches',
	'gallery'       => 'Gallery',
	'faq'       	=> 'FAQs',
	'contact'       => 'Contactus',
	'mailsubs'       => 'Subscribe to the email service',
	'sitename'       => 'Dallah Driving Learning Company',
];
