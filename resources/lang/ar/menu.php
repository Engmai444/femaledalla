<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'menu'       	=> 'القائمة',
	'home'       	=> 'اﻟﺮﺋﻴﺴﻴﺔ',
	'about'       	=> 'من نحن',
	'testing'       => 'الاختبار النظري',
	'services'      => 'خدماتنا',
	'times'       	=> 'اﻭﻗﺎﺕ الدوام',
	'news'       	=> 'اﻷﺧﺒﺎﺭ',
	'courses'       => 'الدورات',
	'branches'      => 'فروعنا',
	'gallery'       => 'ﻣﻌﺮﺽ اﻟﺼﻮﺭ',
	'faq'       	=> 'اﻻﺳﺌﻠﻪ اﻟﺸﺎﺋﻌﺔ',
	'contact'       => 'اتصل بنا',
	'mailsubs'       => 'سجل فى القائمة البريدية',
	'sitename'       => 'شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ',
];
