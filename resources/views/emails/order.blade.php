<html>
                        <head>
                         <style>
                        .body{
                        direction: rtl;
						background:#F4F4F4;
                        }
                        .head{
                       
                        padding:30px 40px 0;
                        overflow: hidden;
                      
                        }
						.title2{
						
                        padding: 5px;
						color:#666;
							
						}
                        .logo{
                   
                        color:#F5913A;
                        font-weight:bold;
                        font-size:24px;
                        padding:0 40px;
                        }
                        .head img{
                        height: 50px;
                        float: right;
                        }
                        .footer{
                      
                        padding: 12px;
                        overflow: hidden;
                        color:#fff;	
                        }
                        .footer .copyright{
                        float:right;
						color:#666;
                        }
                        .footer .shareicon{
                        float:left;
                        }
                        .desc{
                        color:#333;
                        padding:0 40px;
                        font-weight:bold;
                        }
                        .desc p{
                        color:#ccc;
                        }
                        .title{
                        color:#b2b2b2;
                        width:25%;
                        }
                        .table{
                            font-weight:bold;
                        padding:30px;
						color:#333;
						background:#fff;
						border: 1px solid #ccc;
						border-radius:5px;
						margin:50px;
                        }
                        table {
							
                        width:80%;
                          border-collapse: collapse;
						  margin: 0 auto;
                        }
                        .shareicon{
                        list-style:none;
                        
                        }
                        .shareicon li{
                            float:left;
                        }
                        .shareicon li a{
                            color:#fff;
                        }
                        table, th, td {
                          
                        }
                        table td {
                          padding:5px;
                        }
                        </style>
                        </head>
                        <body>
                       
                        <div class="body">
                        <div class="head"><img src="https://dallahdrivingschool.sa/resources/views/site/images/logo.png"/></div>
                        <div class="logo">تأكيد عملية الدفع</div>
                        <div class="desc">
							مرحباً ,{{$FullName}}
                        </div>
                        <div class="table">
                            <table>
								<tr>
                                    <td class="title">رقم العملية:</td>
                                    <td>{{$order_ref}}</td>
                                </tr>
                                <tr>
                                    <td class="title">نوع العملية:</td>
                                    <td>{{$transactionType}}</td>
                                </tr>
                                <tr>
                                    <td class="title">Authorisation Code:</td>
                                    <td>{{$transactionCode}}</td>
                                </tr>
                                <tr>
                                    <td class="title">المبلغ:</td>
                                    <td>{{$price}} ر.س</td>
                                </tr>
                                <tr>
                                    <td class="title">تاريخ العملية</td>
                                    <td>{{$PaymentDate}}</td>
                                </tr>
								<tr>
                                    <td class="title">الكورس</td>
                                    <td>{{$CourseName}}</td>
                                </tr>
							
								
                            </table>
                        </div>
                        <div class="footer">
                            
                            <div class="copyright">
                                Copyright &copy; Dalla Driving School 2019 . All Right Reserved
                            </div>
                        </div>
                        </div>
                        </body>
                        </html>