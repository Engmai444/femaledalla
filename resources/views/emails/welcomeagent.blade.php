<h1><img src='http://kazzeem.com/resources/views/site/assets/img/logo.png' /></h1>
<h4>Welcome <b>{{$fullname_en}}</b> to KAZZEEM family</h4>
<p>We are proud for joining us our business and hope to you a fruitful career path in your life with KAZZEEM.</p>
<p>You have granted to access our application with the following credentials:</p>
<p>Account ID 	: {{$agentid}} </p>
<p>Password 	: {{$password}}</p>
<p><small>Please, don’t communicate your password or ID with any one, if issue happens you may be asked legal according to the situation assessment. </small></p>

<br />
<br />
<br />

<div style="direction: rtl; text-align: right;">
<h4>أهلا بكم <b>{{$fullname_ar}}</b> إلى عائلة كازيم</h4>
<p>ونحن فخورون بإنضمامك إلينا ونأمل لكم مسار وظيفي مثمر في حياتك مع كازيم.</p>
<p>الان تملك الصلاحية لدخول التطبيق من خلال البيانات التالية:</p>
<p>معرف الحساب 	: {{$agentid}} </p>
<p>كلمة المرور 	: {{$password}}</p>
<p><small>من فضلك، لا تقم بتوصيل كلمة المرور أو الهوية الخاصة بك مع أي واحد، إذا حدث يحدث قد يطلب منك القانونية وفقا لتقييم الوضع. </small></p>
</div>