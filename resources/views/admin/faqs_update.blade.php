@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
						<li>
				<a href="{{url('admin')}}"><i class="entypo-home"></i>Dashboard</a>
			</li>
					<li>

							<a href="{{url('admin/faqs')}}">FAQS</a>
					</li>
				<li class="active">

							<strong>Update</strong>
					</li>
					</ol>






<div class="row">
	<div class="col-md-12">

		<div class="panel panel-primary" data-collapsed="0">

			<div class="panel-heading">
				<div class="panel-title">
					Update FAQS
				</div>


			</div>

			<div class="panel-body">
					@if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif

					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif

						{{ Form::open(array('url' => '/admin/faqs/'.$faqs->id.'/update','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered','files'=> true)) }}
					<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
					<li class="active">
						<a href="#home" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-home"></i></span>
							<span class="hidden-xs">English</span>
						</a>
					</li>
					<li>
						<a href="#ar1" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-user"></i></span>
							<span class="hidden-xs">Arabic</span>
						</a>
					</li>
				</ul>
				
				<div class="tab-content">
					<div class="tab-pane active" id="home">	
					
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Question</label>
						
						<div class="col-sm-5">
							{{ Form::text('question',$faqs->question, array('class' => 'form-control','placeholder'=>'Question')) }} 
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Answer</label>
						
						<div class="col-sm-9">
							
							{{ Form::textarea('answer',$faqs->answer, array('class' => 'form-control ','placeholder'=>'Answer')) }} 
							<script type="text/javascript">
							CKEDITOR.replace( 'answer');
							</script>
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Ordered</label>
						
						<div class="col-sm-5">
							{{ Form::text('ordered',$faqs->ordered, array('class' => 'form-control','placeholder'=>'Ordered')) }} 
						</div>
					</div>
					</div>
					<div class="tab-pane" id="ar1">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">السؤال</label>
					
					<div class="col-sm-5">
						{{ Form::text('question_ar',$faqs->question_ar, array('class' => 'form-control','placeholder'=>'السؤال')) }} 
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">الاجابة</label>
					
					<div class="col-sm-9">
						
						{{ Form::textarea('answer_ar',$faqs->answer_ar, array('class' => 'form-control ','placeholder'=>'الاجابة')) }}  
						<script type="text/javascript">
							CKEDITOR.replace( 'answer_ar',{
							language: 'ar',
						});
							</script>
					</div>
				</div>
				
			</div>
					
					</div>
					<div class="form-group col-sm-6">
						<a href="javascript:;" onclick="jQuery('#modal-1').modal('show');" class="btn btn-green btn-icon btn-icon btn-lg">Update <i class="entypo-check"></i></a>

					</div>
				{{ Form::close() }}

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">

jQuery(document).ready(function($)
{
	$(".textarea").wysihtml5();
});
</script>


@stop
