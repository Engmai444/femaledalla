@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
						<li>
				<a href="{{url('admin')}}"><i class="entypo-home"></i>Dashboard</a>
			</li>
					<li>

							<a href="{{url('admin/pages')}}">Pages</a>
					</li>
				<li class="active">

							<strong>Update</strong>
					</li>
					</ol>






<div class="row">
	<div class="col-md-12">

		<div class="panel panel-primary" data-collapsed="0">

			<div class="panel-heading">
				<div class="panel-title">
					Update Page
				</div>


			</div>

			<div class="panel-body">
					@if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif

					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif

						{{ Form::open(array('url' => '/admin/pages/'.$page->pageId.'/update','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered','files'=> true)) }}
					<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
					<li class="active">
						<a href="#home" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-home"></i></span>
							<span class="hidden-xs">English</span>
						</a>
					</li>
					<li>
						<a href="#ar1" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-user"></i></span>
							<span class="hidden-xs">Arabic</span>
						</a>
					</li>
				</ul>
				
				<div class="tab-content">
					<div class="tab-pane active" id="home">	
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Title</label>
						
						<div class="col-sm-5">
							{{ Form::text('title',$page->title, array('class' => 'form-control','placeholder'=>'Title')) }} 
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"></label>
						
						<div class="col-sm-9">
							
							{{ Form::textarea('content',$page->content, array('class' => 'form-control ','placeholder'=>'Content')) }} 
							<script type="text/javascript">
							CKEDITOR.replace( 'content');
							</script>
						</div>

					</div>
					<h3>SEO</h3>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">SEO Title</label>
					<div class="col-sm-5">
						{{ Form::text('seo_title',$page->seo_title, array('class' => 'form-control','placeholder'=>'Title')) }} 
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">SEO Description</label>
					<div class="col-sm-9">
						{{ Form::textarea('seo_desc',$page->seo_desc, array('class' => 'form-control ','placeholder'=>'','data-stylesheet-url'=> url('/resources/views/admin').'/assets/css/wysihtml5-color.css')) }} 
					</div>
				</div>
					</div>
					<div class="tab-pane" id="ar1">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">عنوان الصفحة</label>
					
					<div class="col-sm-5">
						{{ Form::text('title_ar',$page->title_ar, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label"></label>
					
					<div class="col-sm-9">
						{{ Form::textarea('content_ar',$page->content_ar, array('class' => 'form-control','placeholder'=>'')) }}  
						<script type="text/javascript">
						CKEDITOR.replace( 'content_ar',{
							language: 'ar',
						});
						</script>
					</div>
				</div>
				<h3>SEO</h3>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">SEO Title AR</label>
					<div class="col-sm-5">
						{{ Form::text('seo_title_ar',$page->seo_title_ar, array('class' => 'form-control','placeholder'=>'Title')) }} 
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">SEO Description AR</label>
					<div class="col-sm-9">
						{{ Form::textarea('seo_desc_ar',$page->seo_desc_ar, array('class' => 'form-control ','placeholder'=>'','data-stylesheet-url'=> url('/resources/views/admin').'/assets/css/wysihtml5-color.css')) }} 
					</div>
				</div>
			</div>
					
					</div>
					<div class="form-group col-sm-6">
						<a href="javascript:;" onclick="jQuery('#modal-1').modal('show');" class="btn btn-green btn-icon btn-icon btn-lg">Update <i class="entypo-check"></i></a>

					</div>
				{{ Form::close() }}

			</div>

		</div>

	</div>
</div>




<script type="text/javascript">

jQuery(document).ready(function($)
{
	$(".textarea").wysihtml5();
});
</script>

@stop
