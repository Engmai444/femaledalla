@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
	</li>

	<li class="active">

		<strong>طلبات اختبار التقييم</strong>
	</li>
</ol>



<h3>طلبات اختبار التقييم</h3>
@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif

<a href="{{url('/admin/evaluation_time/export_excel')}}" class="btn btn-danger">تصدير Exel</a>
<br />
<table class="table table-bordered "  >
	<thead>
		<tr>
			<th>ID</th>
			<th>الاسم</th>
			<th>رقم الهوية</th>
			<th width="10%">البريد الالكترونى</th>
			<th>الموبايل</th>
			<th>الجنسية</th>
			<th>DoB</th>
			<th>اليوم</th>
			<th>الفترة</th>
			<th>المدرسة</th>
			<th>الجنس</th>


			<th width="17%">الادوات</th>
		</tr>
	</thead>
	<tbody>
		@foreach($items as $item)
		<tr class="odd gradeX">
			<td class="center">{{$item->id}}</td>
			<td>{{$item->FullName}}</td>
			<td>{{$item->IDNumber}}</td>
			<td>{{$item->email}}</td>
			<td>{{$item->mobile}}</td>
			<?php $nat=DB::table('nationality')->where('Id',$item->NationalityId)->first();?>
			<td>{{$nat->Name}}</td>
			<td>{{$item->DoB}}</td>
			<td class="center">{{$item->day}}</td>
			<td>{{$item->from_time}} الي {{$item->to_time}}</td>
			<td class="center">{{$item->Name}}</td>
			<td class="center">{{$item->gender}}</td>
			<td>
                 <?php if($item->attend==0){?>
				<a href="{{url('admin/evaluation_time/'.$item->id.'/attend')}}" class="btn btn-red btn-sm btn-icon icon-left">
					<i class="entypo-cancel-circled"></i>
					حضور
				</a>
					 <?php }else{ ?>
					 <a href="{{url('admin/evaluation_time/'.$item->id.'/inattend')}}" class="btn btn-green btn-sm btn-icon icon-left">
						 <i class="entypo-check"></i>
						 إلغاء الحضور
					 </a>
					 <?php }?>
&nbsp;&nbsp;
				<a href="{{url('admin/evaluation_time/'.$item->id.'/delete_reserve')}}" class="btn btn-red btn-sm btn-icon icon-left" onclick="return ConfirmDelete()">
					<i class="entypo-cancel"></i>
					حذف
				</a>
				
			</td>
		</tr>
		@endforeach

	</tbody>
	<tfoot>

	</tfoot>
</table>


@stop
