<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title>موقع دالة | لوحة التحكم</title>
	

	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/bootstrap.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/neon-core.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/neon-theme.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/neon-forms.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/custom.css">

	<script src="{{url('/resources/views/admin')}}/assets/js/jquery-1.11.0.min.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}">
	<!--[if lt IE 9]><script src="{{url('/resources/views/admin')}}/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="icon" type="image/png" href="{{url('/resources/views/admin')}}/assets/images/en_logo.png" />
	
</head>

<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<!-- This is needed when you send requests via Ajax --><script type="text/javascript">
var baseurl = "{{url('/')}}";

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>

<div class="login-container">
	
	<div class="login-header login-caret">
		
		<div class="login-content">
			
			<a href="index.html" class="logo">
				<img src="{{url('resources/views/site/images/logo.png')}}" width="120" alt="" />
			</a>
			
			<p class="description">Dear user, log in to access the admin area!</p>
			
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>
	
	<div class="login-form">
		
		<div class="login-content">
			
			
			@if ($errors)
               
									<div class="form-login-error">
				<h3>Invalid login</h3>
				<p>{{ $errors->first() }}</p>
			</div>
                                @endif
			
			<form role="form" method="POST" action="{{ url('/admin/login') }}" id="form_login">
				{{ csrf_field() }}
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" />
					</div>
					
				</div>
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						
						<input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
					</div>
				
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Login In
					</button>
				</div>
				
		
				
				<!-- 
				
				You can also use other social network buttons
				<div class="form-group">
				
					<button type="button" class="btn btn-default btn-lg btn-block btn-icon icon-left twitter-button">
						Login with Twitter
						<i class="entypo-twitter"></i>
					</button>
					
				</div>
				
				<div class="form-group">
				
					<button type="button" class="btn btn-default btn-lg btn-block btn-icon icon-left google-button">
						Login with Google+
						<i class="entypo-gplus"></i>
					</button>
					
				</div> -->				
			</form>
			
			
			<!--<div class="login-bottom-links">
				
				<a href="{{ url('/password/reset') }}" class="link">Forgot your password?</a>
				
				<br />
				
				<a href="#">ToS</a>  - <a href="#">Privacy Policy</a>
				
			</div>-->
			
		</div>
		
	</div>
	
</div>

	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/rickshaw/rickshaw.min.css">

	<!-- Bottom Scripts -->
	<script src="{{url('/resources/views/admin')}}/assets/js/gsap/main-gsap.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/bootstrap.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/joinable.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/resizeable.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/neon-api.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jquery.sparkline.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/rickshaw/vendor/d3.v3.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/rickshaw/rickshaw.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/raphael-min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/morris.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/toastr.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/neon-chat.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/neon-custom.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/neon-demo.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jquery.validate.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/neon-login.js"></script>


</body>
</html>