<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title>موقع دالة | لوحة التحكم</title>
	

	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/bootstrap.css">
	<link href="{{url('/resources/views/admin')}}/assets/css/bootstrap-modal.css" rel="stylesheet" />
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/neon-core.css">
	
	
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/neon-forms.css">
	
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/wysihtml5/bootstrap-wysihtml5.css">
	<script src="{{url('/resources/views/admin')}}/assets/js/jquery-1.11.0.min.js"></script>
	
	
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/datatables/responsive/css/datatables.responsive.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/select2/select2.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/daterangepicker/daterangepicker-bs3.css">
	
	<!-- <link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/neon-rtl.css"> -->
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/css/custom.css">
	<link rel="icon" type="image/png" href="{{url('/resources/views/admin')}}/assets/images/en_logo.png" />
	<!--[if lt IE 9]><script src="{{url('/resources/views/admin')}}/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<script src="{{url('/resources/views/admin')}}/assets/js/ckeditor2/ckeditor.js"></script>

	<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
var baseurl = "{{url('/')}}";

$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
</script>
</head>
<body class="page-body loaded" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->	
	
	<div class="sidebar-menu">
		
			
		<header class="logo-env">
			
			<!-- logo -->
			<div class="logo">
				<a href="{{url('/admin')}}">
					 <img src="{{url('resources/views/site/images/logo.png')}}" width="120" alt="" />
				</a>
			</div>
			
						<!-- logo collapse icon -->
						
			<div class="sidebar-collapse">
				<a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
					<i class="entypo-menu"></i>
				</a>
			</div>
			
									
			
			<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
			<div class="sidebar-mobile-menu visible-xs">
				<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
					<i class="entypo-menu"></i>
				</a>
			</div>
			
		</header>
				
		
		
				
		
				
		<ul id="main-menu" class="auto-inherit-active-class multiple-expanded">
			<!-- add class "multiple-expanded" to allow multiple submenus to open -->
			<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
			<!-- Search Bar -->
			<li id="search">
				<form method="get" action="">
					<input type="text" name="q" class="search-input" placeholder="Search something..."/>
					<button type="submit">
						<i class="entypo-search"></i>
					</button>
				</form>
			</li>
			<li class="active opened">
				<a href="{{url('/admin')}}">
					<i class="entypo-home"></i>
					<span>لوحة التحكم</span>
				</a>
			</li>

			<li>
				<a href="{{url('admin/customers')}}">
				    <i class="entypo-user"></i>
					<span>المشتركين</span>
				</a>
			</li>

			
			
			
			
		
			
		</ul>
				
	</div>	
	<div class="main-content">
		
<div class="row">
	
	<!-- Profile Info and Notifications -->
	<div class="col-md-6 col-sm-8 clearfix">
		
		<ul class="user-info pull-left pull-none-xsm">
		
						<!-- Profile Info -->
			<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
				
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<img src="{{url('/resources/views/site/favicon.png')}}" alt="" class="img-circle" width="44" />
						{{Auth::guard('admins')->user()->name}}
				</a>
				
				<ul class="dropdown-menu">
					
					<!-- Reverse Caret -->
					<li class="caret"></li>
					
					<!-- Profile sub-links -->
					<li>
						<a href="{{url('/admin/changepassword')}}">
							<i class="entypo-user"></i>
							تغيير كلمة المرور
						</a>
					</li>
					
					<li>
						<a href="{{url('/admin/logout')}}">
							<i class="entypo-mail"></i>
							خروج
						</a>
					</li>
					

				</ul>
			</li>
		
		</ul>
				
		
	
	</div>
	
	
	<!-- Raw Links -->
	<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
		<ul class="list-inline links-list pull-right">
			
			<!-- Language Selector -->			<!--<li class="dropdown language-selector">
				
				Language: &nbsp;
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
					<img src="{{url('/resources/views/admin')}}/assets/images/flag-uk.png" />
				</a>
				
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="#">
							<img src="{{url('/resources/views/admin')}}/assets/images/flag-de.png" />
							<span>Deutsch</span>
						</a>
					</li>
					<li class="active">
						<a href="#">
							<img src="{{url('/resources/views/admin')}}/assets/images/flag-uk.png" />
							<span>English</span>
						</a>
					</li>
					<li>
						<a href="#">
							<img src="{{url('/resources/views/admin')}}/assets/images/flag-fr.png" />
							<span>François</span>
						</a>
					</li>
					<li>
						<a href="#">
							<img src="{{url('/resources/views/admin')}}/assets/images/flag-al.png" />
							<span>Shqip</span>
						</a>
					</li>
					<li>
						<a href="#">
							<img src="{{url('/resources/views/admin')}}/assets/images/flag-es.png" />
							<span>Español</span>
						</a>
					</li>
				</ul>
				
			</li>
			
			<li class="sep"></li>
			
						
			<li>
				<a href="#" data-toggle="chat" data-animate="1" data-collapse-sidebar="1">
					<i class="entypo-chat"></i>
					Chat
					
					<span class="badge badge-success chat-notifications-badge is-hidden">0</span>
				</a>
			</li>
			-->
			<li class="sep"></li>
			
			<li>
				<a href="{{url('/admin/logout')}}">
					خروج <i class="entypo-logout right"></i>
				</a>
			</li>
		</ul>
		
	</div>
	
</div>

<hr />



<!-- content -->
@yield('content')
<!-- /content -->


<!-- Footer -->
<footer class="main">
	
		
	&copy; 2018 <strong><a href="https://is.net.sa/" target="_blank">INTERNeT SOLUTIONS Co.</a></strong>
	
</footer>	</div>
	

		<div class="modal fade" id="sendpass" data-backdrop="static"  tabindex="-1" >
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Done</h4>
					</div>
					
					<div class="modal-body">
						Password sent successfully
					</div>
				</div>
			</div>
		</div>		
	
		<div class="modal fade" id="modal-1" data-backdrop="static"  tabindex="-1" >
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Confirm</h4>
					</div>
					
					<div class="modal-body">
						Are you sure you want to save ?
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
						<button type="button" class="btn btn-info" id="SubForm">Yes</button>
					</div>
				</div>
			</div>
		</div>

	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					حذف
				</div>
				<div class="modal-body">
					هل انت متأكد من الحذف ؟
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">الغاء</button>
					<a class="btn btn-danger btn-ok">حذف</a>
				</div>
			</div>
		</div>
	</div>
		
		
	</div>

	
	<script src="{{url('/resources/views/admin')}}/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jquery.sparkline.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/rickshaw/vendor/d3.v3.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/rickshaw/rickshaw.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/raphael-min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/morris.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/toastr.js"></script>
	
	
	<script src="{{url('/resources/views/admin')}}/assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/bootstrap-datepicker.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/bootstrap-timepicker.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/bootstrap-colorpicker.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/daterangepicker/moment.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/daterangepicker/daterangepicker.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jquery.multi-select.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/icheck/icheck.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jcrop/jquery.Jcrop.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/fileinput.js"></script>
	<script src="http://jhollingworth.github.com/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/wysihtml5/bootstrap-wysihtml5.js"></script>
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/rickshaw/rickshaw.min.css">
	
	
	<script src="{{url('/resources/views/admin')}}/assets/js/gsap/main-gsap.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/bootstrap.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/joinable.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/resizeable.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/neon-api.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/jquery.dataTables.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/datatables/TableTools.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/dataTables.bootstrap.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/datatables/lodash.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/datatables/responsive/js/datatables.responsive.js"></script>
	

	<script src="{{url('/resources/views/admin')}}/assets/js/select2/select2.min.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/neon-custom.js"></script>
	<script src="{{url('/resources/views/admin')}}/assets/js/neon-demo.js"></script>
	
	
	<script src="{{url('/resources/views/admin')}}/assets/js/dropzone/dropzone.js"></script>
	<link rel="stylesheet" href="{{url('/resources/views/admin')}}/assets/js/dropzone/dropzone.css">
	<!-- Bottom Scripts -->
	

	<script src="{{url('/resources/views/admin')}}/assets/js/script.js"></script>
	
	
	
	
	
	
	
	
	
	
	
	<script src="{{url('/resources/views/admin')}}/assets/js/neon-chat.js"></script>
	
	
	<script>
		function ConfirmDelete()
		{
			var x = confirm("هل انت متأكد من الحذف ؟");
			if (x)
				return true;
			else
				return false;
		}
	</script>
	
	

	
	
	

</body>
</html>