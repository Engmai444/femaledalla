@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
						<li>
				<a href="{{url('admin')}}"><i class="entypo-home"></i>لوحة التحكم</a>
			</li>
					<li>

							<a href="{{url('admin/orders')}}">طلبات الاشتراك</a>
					</li>
				<li class="active">

							<strong>تعديل</strong>
					</li>
					</ol>






<div class="row">
	<div class="col-md-12">

		<div class="panel panel-primary" data-collapsed="0">

			<div class="panel-heading">
				<div class="panel-title">
					تأكيد الحضور
				</div>


			</div>

			<div class="panel-body">
					@if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif

					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> خطأ!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif
						{{ Form::open(array('url' => '/admin/orders/'.$item->OrderId.'/update','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered')) }}
					
				
				<div class="tab-content">
					<div class="tab-pane active" id="home">	
					
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">تأكيد الدفع والحضور ؟</label>
						
						<div class="col-sm-5">
							{{ Form::select('IsPaid',['0'=>'لا','1'=>'نعم'],$item->IsPaid, array('class' => 'form-control')) }} 
						</div>
					</div>
					
					</div>
					
					
					</div>
					<div class="form-group col-sm-6">
						<a href="javascript:;" onclick="jQuery('#modal-1').modal('show');" class="btn btn-green btn-icon btn-icon btn-lg">حفظ <i class="entypo-check"></i></a>

					</div>
				{{ Form::close() }}

			</div>

		</div>

	</div>
</div>


@stop
