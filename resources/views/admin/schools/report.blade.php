@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
	</li>

	<li class="active">

		<strong>المدارس</strong>
	</li>
</ol>



<h3>تقرير</h3>
<form action="{{url('/admin/school/report')}}" method="get">
	<div class="row search">
		<div class="col-md-12">
			<h5>تصدير</h5>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-12">
			<input type="submit" class="btn btn-primary" id="" value="تصدير Exel">
		</div>
	</div>
</form>
@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif
<table class="table table-bordered "  >
	<thead>
		<tr>
			<th>ID</th>
			<th>المدرسة</th>

			<th>عدد المشتركين فى المدرسة</th>
			<th>إجمالى المبلغ المسدد</th>
			<th>عدد المشتركين المسددين فى دورات</th>
			<th>عدد المشتركين الغير المسددين فى دورات</th>
			<th>عدد المشتركين الغير مشتركين فى دورات</th>
		</tr>
	</thead>
	<tbody>
	<?php $i=1;?>
		@foreach($items as $item)
		<tr class="odd gradeX">
			<td class="center">{{$i}}</td>
			<td class="center">{{$item->Name}}</td>
			<td class="center">{{$item->total}}</td>
			<?php $tot=DB::table('orders')->join('users','users.StudentId','=','orders.StudentId')
					->join('course','course.id','=','orders.CourseId')
					->where('users.school',$item->Id)
					->where('IsPaid','1')
					->select(DB::raw('Sum(price+tax) as tot'))->first();?>
			<td>@if($tot->tot>0){{$tot->tot}}@else{{'0'}} @endif</td>
			<?php $cot=DB::table('orders')->join('users','users.StudentId','=','orders.StudentId')
			                              ->where('users.school',$item->Id)
			                               ->where('IsPaid','1')
			                               ->count();?>
			<td>{{$cot}}</td>
			<?php $uncot=DB::table('orders')->join('users','users.StudentId','=','orders.StudentId')
					->where('users.school',$item->Id)
					->where('IsPaid','0')
					->count();?>
			<td>{{$uncot}}</td>
			<td>{{$item->total-($cot+$uncot)}}</td>

		</tr>
			<?php $i++;?>
		@endforeach

	</tbody>
	<tfoot>

	</tfoot>
</table>


@stop
