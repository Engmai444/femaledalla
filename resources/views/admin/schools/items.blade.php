@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
	</li>

	<li class="active">

		<strong>المدارس</strong>
	</li>
</ol>



<h3>المدارس</h3>
@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif
<table class="table table-bordered "  >
	<thead>
		<tr>
			<th>ID</th>
			<th>المدرسة</th>

			<th>الادوات</th>
		</tr>
	</thead>
	<tbody>
	<?php $i=1;?>
		@foreach($items as $item)
		<tr class="odd gradeX">
			<td class="center">{{$i}}</td>
			<td class="center">{{$item->Name}}</td>

			<td>
                 <?php if($item->active==1){?>
				<a href="{{url('admin/school/'.$item->Id.'/active')}}" class="btn btn-red btn-sm btn-icon icon-left">
					<i class="entypo-cancel-circled"></i>
					تفعيل
				</a>
					 <?php }else{ ?>
					 <a href="{{url('admin/school/'.$item->Id.'/inactive')}}" class="btn btn-green btn-sm btn-icon icon-left">
						 <i class="entypo-check"></i>
						 إلغاء التفعيل
					 </a>
					 <?php }?>

					 <a href="{{url('admin/school/'.$item->Id.'/update')}}" class="btn btn-info btn-sm btn-icon icon-left">
						 <i class="entypo-pencil"></i>
						 تعديل
					 </a>
			</td>
		</tr>
			<?php $i++;?>
		@endforeach

	</tbody>
	<tfoot>

	</tfoot>
</table>


@stop
