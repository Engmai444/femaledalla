@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
						<li>
				<a href="{{url('admin')}}"><i class="entypo-home"></i>Dashboard</a>
			</li>
					<li>

							<a href="{{url('admin/slider')}}">Slider</a>
					</li>
				<li class="active">

							<strong>Update</strong>
					</li>
					</ol>






<div class="row">
	<div class="col-md-12">

		<div class="panel panel-primary" data-collapsed="0">

			<div class="panel-heading">
				<div class="panel-title">
					Update Slider
				</div>


			</div>

			<div class="panel-body">
					@if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif

					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif

						{{ Form::open(array('url' => '/admin/slider/'.$page->id.'/update','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered','files'=> true)) }}
					<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
					<li class="active">
						<a href="#home" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-home"></i></span>
							<span class="hidden-xs">English</span>
						</a>
					</li>
					<li>
						<a href="#ar1" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-user"></i></span>
							<span class="hidden-xs">Arabic</span>
						</a>
					</li>
				</ul>
				
				<div class="tab-content">
					<div class="tab-pane active" id="home">	
					
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Title</label>
						
						<div class="col-sm-5">
							{{ Form::text('title',$page->title, array('class' => 'form-control','placeholder'=>'Title')) }} 
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Title 2</label>
						
						<div class="col-sm-5">
							{{ Form::textarea('content',$page->content, array('class' => 'form-control','placeholder'=>'Title')) }} 
						</div>
					</div>
					
					<hr />
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Photo</label>
							<div class="col-sm-5">
								{{ Form::file('photo') }} 
								<br />
								@if($page->photo != '')
									<img src="{{url('/')}}/public/photos/{{$page->photo}}"  height="200" />
								@endif
							</div>
						</div>
					</div>
					<div class="tab-pane" id="ar1">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">العنوان</label>
					
					<div class="col-sm-5">
						{{ Form::text('title_ar',$page->title_ar, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">العنوان 2</label>
					
					<div class="col-sm-5">
						{{ Form::textarea('content_ar',$page->content_ar, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				
			</div>
					
					</div>
					<div class="form-group col-sm-6">
						<a href="javascript:;" onclick="jQuery('#modal-1').modal('show');" class="btn btn-green btn-icon btn-icon btn-lg">Update <i class="entypo-check"></i></a>

					</div>
				{{ Form::close() }}

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($)
{
	// Example 1 - Simple case
	$("#jcrop-1").Jcrop({}, function()
	{
		this.setSelect([580, 310, 320, 140]);
	});

	// Example 2 - Coordinates Fetch
	$("#jcrop-2").Jcrop({
		onSelect: showCoords,
		onChange: showCoords
	});

	function showCoords(c)
	{
		$('#jc2-x1').val(c.x);
		$('#jc2-y1').val(c.y);
		$('#jc2-x2').val(c.x2);
		$('#jc2-y2').val(c.y2);
		$('#jc2-w').val(c.w);
		$('#jc2-h').val(c.h);
	}


	// Example 3 - crop the thumbnail
	var img_3_w = $("#jcrop-3").width(),
		img_3_h = $("#jcrop-3").height();

	$('#jcrop-3').Jcrop({
		onChange: showPreview,
		onSelect: showPreview,
		aspectRatio: 1
	}, function()
	{
		this.setSelect([200, 200, 120, 40]);
	});
	function showPreview(coords)
	{
		var rx = 150 / coords.w;
		var ry = 150 / coords.h;

		$('#preview').css({
			width: Math.round(rx * img_3_w) + 'px',
			height: Math.round(ry * img_3_h) + 'px',
			marginLeft: '-' + Math.round(rx * coords.x) + 'px',
			marginTop: '-' + Math.round(ry * coords.y) + 'px'
		});
	}

	// Example 4 - Live Crop
	$('#jcrop-4').Jcrop({
      aspectRatio: 1,
      onSelect: updateCoords,
      boxWidth: 1080,
      boxHeight: 450
    });

	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
	};
});
</script>

@stop
