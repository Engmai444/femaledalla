@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>Home</a>
	</li>

	<li class="active">

		<strong>Pages</strong>
	</li>
</ol>



<h3>Pages</h3>
@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif

<table class="table table-bordered "  >
	<thead>
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>Title ar</th>
			<th>Link</th>
			<th>Link ar</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		@foreach($pages as $item)
		<tr class="odd gradeX">
			<td class="center">{{$item->pageId}}</td>
			<td>{{$item->title}}</td>
			<td>{{$item->title_ar}}</td>
			<td><a class="btn btn-default btn-sm" href="{{url('/en/page/'.$item->pageId)}}" target="_blank">View</a></td>
			<td><a class="btn btn-default btn-sm" href="{{url('/page/'.$item->pageId)}}" target="_blank">View</a></td>
			<td>
			<a href="{{url('admin/pages/'.$item->pageId.'/update')}}" class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					Edit
				</a>
				<br/>
			</td>
		</tr>
		@endforeach
		
	</tbody>
	<tfoot>

	</tfoot>
</table>
{{$pages->links()}}

<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		var table = $("#table-4").dataTable({
			//"sPaginationType": "bootstrap",
			// "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
			"sDom": '<"H"Tfr>t<"F"i>',
			// "bJQueryUI": true,
                // "sPaginationType": "full_numbers",
                // "bStateSave": true,
                // "iDisplayLength": -1,
			"oTableTools": {
				"sSwfPath": "{{url('/resources/views/admin')}}/assets/js/datatables/copy_csv_xls_pdf.swf",

			},

		});
	});

</script>

@stop
