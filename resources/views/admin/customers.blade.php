@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>Home</a>
	</li>

	<li class="active">

		<strong>Customers</strong>
	</li>
</ol>



<h3>Customers</h3>

<form action="{{url('/admin/customers/report')}}" method="get">
<div class="row search">
	<div class="col-md-12">
		<h5>تصدير</h5>
	</div>
	<div class="col-md-3">
		<label for="">من تاريخ</label>
		<input type="text" class="form-control datepicker2" id="" name="from" required >
	</div>
	<div class="col-md-3">
		<label for="">الي تاريخ</label>
		<input type="text" class="form-control datepicker2" id="" name="to" required>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-12">
		<input type="submit" class="btn btn-primary" id="" value="تصدير Exel">
	</div>
</div>
</form>



@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif
<table class="table table-bordered"  >
	<thead>
		<tr>
			<th>ID</th>
			<th>User Name</th>
			<th>ID Number</th>
			<th>email</th>
			<th>mobile</th>
			<th>Nationality</th>
			<th>DoB</th>
			<th>created_at</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		@foreach($customers as $item)
		<tr class="">
			<td class="center">{{$item->StudentId}}</td>
			<td>{{$item->FullName}}</td>
			<td>{{$item->IDNumber}}</td>
			<td>{{$item->email}}</td>
			<td>{{$item->mobile}}</td>
			<td>{{$item->Nationality}}</td>
			<td>{{$item->DoB}}</td>
			<td>{{$item->created_at}}</td>
			<td>
				<!-- <a href="{{url('admin/branches/'.$item->order_id.'/delete')}}" class="btn btn-red btn-sm btn-icon icon-left"> -->
					<!-- <i class="entypo-cancel"></i> -->
					<!-- Delete -->
				<!-- </a> -->
			</td>
		</tr>
		@endforeach
		
	</tbody>
	<tfoot>
{{$customers->links()}}
	</tfoot>
</table>
@stop
