@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
						<li>
				<a href="{{url('/admin')}}"><i class="entypo-home"></i>Home</a>
			</li>
					<li>
			
							<a href="{{url('/admin/branches')}}">Branches</a>
					</li>
				<li class="active">
			
							<strong>Add</strong>
					</li>
					</ol>
					
					
					
			


<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Add
				</div>
				
				
			</div>
			
			<div class="panel-body">
			
					@if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif
                   
					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif
					
						{{ Form::open(array('url' => '/admin/branches/new','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered','files'=> true)) }}
						
		<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
			<li class="active">
				<a href="#home" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-home"></i></span>
					<span class="hidden-xs">English</span>
				</a>
			</li>
			<li>
				<a href="#arabic" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-home"></i></span>
					<span class="hidden-xs">Arabic</span>
				</a>
			</li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="home">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">title</label>
					<div class="col-sm-5">
						{{ Form::text('title',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Address</label>
					<div class="col-sm-5">
						{{ Form::text('address',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Phone</label>
					<div class="col-sm-5">
						{{ Form::text('phone',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Location (LAT)</label>
					<div class="col-sm-5">
						{{ Form::text('lat',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Location (LNG)</label>
					<div class="col-sm-5">
						{{ Form::text('lng',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
			</div>
			<div class="tab-pane " id="arabic">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Title Ar</label>
					<div class="col-sm-5">
						{{ Form::text('title_ar',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Address Ar</label>
					<div class="col-sm-5">
						{{ Form::text('address_ar',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
			</div>
		</div>
		
					<div class="form-group col-sm-6">
						<a href="javascript:;" onclick="jQuery('#modal-1').modal('show');" class="btn btn-green btn-icon btn-icon btn-lg">Add <i class="entypo-check"></i></a>
						<button type="reset" class="btn btn-danger btn-icon icon-left btn-lg">Reset <i class="entypo-cancel"></i></button>
					</div>	
				{{ Form::close() }} 
						
			</div>
		
		</div>
	
	</div>
</div>
<script type="text/javascript">

jQuery(document).ready(function($)
{
	$(".textarea").wysihtml5();
});
</script>


@stop