@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
						<li>
				<a href="{{url('admin')}}"><i class="entypo-home"></i>Dashboard</a>
			</li>
					
				<li class="active">

							<strong>Settings</strong>
					</li>
					</ol>






<div class="row">
	<div class="col-md-12">

		<div class="panel panel-primary" data-collapsed="0">

			<div class="panel-heading">
				<div class="panel-title">
					Home
				</div>


			</div>

			<div class="panel-body">
					@if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif

					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif

						{{ Form::open(array('url' => '/admin/site/home','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered','files'=> true)) }}
					<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
					<!-- <li class="active"> -->
						<!-- <a href="#home" data-toggle="tab"> -->
							<!-- <span class="visible-xs"><i class="entypo-home"></i></span> -->
							<!-- <span class="hidden-xs">About</span> -->
						<!-- </a> -->
					<!-- </li> -->
					<!-- <li> -->
						<!-- <a href="#Features" data-toggle="tab"> -->
							<!-- <span class="visible-xs"><i class="entypo-home"></i></span> -->
							<!-- <span class="hidden-xs">Home Block Titles</span> -->
						<!-- </a> -->
					<!-- </li> -->
					<!-- <li> -->
						<!-- <a href="#ar1" data-toggle="tab"> -->
							<!-- <span class="visible-xs"><i class="entypo-user"></i></span> -->
							<!-- <span class="hidden-xs">Contact Block</span> -->
						<!-- </a> -->
					<!-- </li> -->
					<!-- <li > -->
						<!-- <a href="#ar2" data-toggle="tab"> -->
							<!-- <span class="visible-xs"><i class="entypo-user"></i></span> -->
							<!-- <span class="hidden-xs">Settings</span> -->
						<!-- </a> -->
					<!-- </li> -->
					<li class="active">
						<a href="#ar3" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-user"></i></span>
							<span class="hidden-xs">Social links</span>
						</a>
					</li>
					<li >
						<a href="#Features" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-user"></i></span>
							<span class="hidden-xs">home</span>
						</a>
					</li>
				</ul>
				
				<div class="tab-content">
					<div class="tab-pane " id="home">	
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Title</label>
							<div class="col-sm-5">
								{{ Form::text('hm_bl1_title',$items['hm_bl1_title'], array('class' => 'form-control','placeholder'=>'Title')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Desc</label>
							<div class="col-sm-5">
								{{ Form::textarea('hm_bl1_desc',$items['hm_bl1_desc'], array('class' => 'form-control','placeholder'=>'Desc')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Title AR</label>
							<div class="col-sm-5">
								{{ Form::text('hm_bl1_title_ar',$items['hm_bl1_title_ar'], array('class' => 'form-control','placeholder'=>'Title')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Desc AR</label>
							<div class="col-sm-5">
								{{ Form::textarea('hm_bl1_desc_ar',$items['hm_bl1_desc_ar'], array('class' => 'form-control','placeholder'=>'Desc')) }} 
							</div>
						</div>
						
						
						
					</div>
					<div class="tab-pane" id="Features">
						
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">عدد الرخص</label>
							<div class="col-sm-5">
								{{ Form::text('hm_Features_box1_title',$items['hm_Features_box1_title'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">عدد المتدربين</label>
							<div class="col-sm-5">
								{{ Form::text('hm_Features_box1_desc',$items['hm_Features_box1_desc'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">عدد ساعات العمل</label>
							<div class="col-sm-5">
								{{ Form::text('hm_Features_box1_title_ar',$items['hm_Features_box1_title_ar'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">ﺳﻨﻴﻦ اﻟﺘﻮاﺟﺪ ﻓﻲ اﻟﺴﻮﻕ</label>
							<div class="col-sm-5">
								{{ Form::text('hm_Features_box1_desc_ar',$items['hm_Features_box1_desc_ar'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
							<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">مطلوب _للموبايل_</label>
							<div class="col-sm-5">
								<select name="hm_bl4_required" class="form-control">
								    <option value="true" {{ $items['hm_bl4_required'] == 'true' ?'selected':''}}>نعم</option>
								    <option value="false" {{ $items['hm_bl4_required'] == 'false' ?'selected':''}}>لا</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label"> رسالة قبول الإناث فى الموقع</label>
							<div class="col-sm-5">
								<select name="msg_female" class="form-control">
									<option value="0" {{ $items['msg_female'] == '0' ?'selected':''}}>نعم</option>
									<option value="1" {{ $items['msg_female'] == '1' ?'selected':''}}>لا</option>
								</select>
							</div>
						</div>


						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">نص اسفل دوراتي</label>
							<div class="col-sm-5">
								<textarea class="form-control" name="hm_mycoursrs_text">{{$items["hm_mycoursrs_text"]}}</textarea>
							</div>
						</div>

						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label"> %نسبة الضريبة</label>
							<div class="col-sm-5">
								{{ Form::text('tax',$items['tax'], array('class' => 'form-control','placeholder'=>'')) }}
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">النوع فى الموقع</label>
							<div class="col-sm-5">
								<select name="gender" class="form-control">
									<option value="0" {{ $items['gender'] == '0' ?'selected':''}}>الاناث</option>
									<option value="1" {{ $items['gender'] == '1' ?'selected':''}}>الذكور</option>
									<option value="2" {{ $items['gender'] == '2' ?'selected':''}}>كلا معا</option>

								</select>
							</div>
						</div>

						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label"> تفعيل تحديد المستوى</label>
							<div class="col-sm-5">
								<select name="evaluation_time" class="form-control">
									<option value="0" {{ $items['evaluation_time'] == '0' ?'selected':''}}>نعم</option>
									<option value="1" {{ $items['evaluation_time'] == '1' ?'selected':''}}>لا</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label"> تفعيل الجنسيات</label>
							<div class="col-sm-5">
								<select name="nationality_all" class="form-control">
									<option value="0" {{ $items['nationality_all'] == '0' ?'selected':''}}>سعوديات فقط</option>
									<option value="1" {{ $items['nationality_all'] == '1' ?'selected':''}}>جميع الجنسيات</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">إيقاف تفعيل  تسجيل المقيمين في اختبار طلب تحديد المستوي</label>
							<div class="col-sm-5">
								<select name="id_setting" class="form-control">
									<option value="0" {{ $items['id_setting'] == '0' ?'selected':''}}>نعم</option>
									<option value="1" {{ $items['id_setting'] == '1' ?'selected':''}}>لا</option>
								</select>
							</div>
						</div>

					</div>
					<div class="tab-pane" id="ar1">
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Address</label>
							<div class="col-sm-5">
								{{ Form::text('hm_bl4_address',$items['hm_bl4_address'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Address Ar</label>
							<div class="col-sm-5">
								{{ Form::textarea('hm_bl4_address_ar',$items['hm_bl4_address_ar'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Email</label>
							<div class="col-sm-5">
								{{ Form::text('hm_bl4_email',$items['hm_bl4_email'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Phone</label>
							<div class="col-sm-5">
								{{ Form::text('hm_bl4_phone',$items['hm_bl4_phone'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Whatsapp</label>
							<div class="col-sm-5">
								{{ Form::text('hm_bl4_whatsapp',$items['hm_bl4_whatsapp'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<h3>Map</h3>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">location (lat)</label>
							<div class="col-sm-5">
								{{ Form::text('hm_map_lat',$items['hm_map_lat'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">location (lng)</label>
							<div class="col-sm-5">
								{{ Form::text('hm_map_lng',$items['hm_map_lng'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
					</div>
					<div class="tab-pane" id="ar2">
						<h3>Settings</h3>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Site Title</label>
							<div class="col-sm-5">
								{{ Form::text('site_title',$items['site_title'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Site Desc</label>
							<div class="col-sm-5">
								{{ Form::textarea('site_desc',$items['site_desc'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Site Title Ar</label>
							<div class="col-sm-5">
								{{ Form::text('site_title_ar',$items['site_title_ar'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">Site Desc Ar</label>
							<div class="col-sm-5">
								{{ Form::textarea('site_desc_ar',$items['site_desc_ar'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">content form email</label>
							<div class="col-sm-5">
								{{ Form::text('hm_bl4_contactform',$items['hm_bl4_contactform'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						
					</div>
					<div class="tab-pane active" id="ar3">
						<h3>Social links</h3>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">facebook</label>
							<div class="col-sm-5">
								{{ Form::text('social_facebook',$items['social_facebook'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">twitter</label>
							<div class="col-sm-5">
								{{ Form::text('social_twitter',$items['social_twitter'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">youtube</label>
							<div class="col-sm-5">
								{{ Form::text('social_youtube',$items['social_youtube'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">instagram</label>
							<div class="col-sm-5">
								{{ Form::text('social_instagram',$items['social_instagram'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">linkedin</label>
							<div class="col-sm-5">
								{{ Form::text('social_linkedin',$items['social_linkedin'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">googleplus</label>
							<div class="col-sm-5">
								{{ Form::text('social_googleplus',$items['social_googleplus'], array('class' => 'form-control','placeholder'=>'')) }} 
							</div>
						</div>

						
					</div>
					
					</div>
					<div class="form-group col-sm-6">
						<a href="javascript:;" onclick="jQuery('#modal-1').modal('show');" class="btn btn-green btn-icon btn-icon btn-lg">Update <i class="entypo-check"></i></a>

					</div>
				{{ Form::close() }}

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($)
{
	// Example 1 - Simple case
	
});
</script>

@stop
