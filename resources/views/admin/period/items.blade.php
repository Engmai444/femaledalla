@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
	</li>

	<li class="active">

		<strong>الفترات</strong>
	</li>
</ol>



<h3>الفترات</h3>
@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif
<table class="table table-bordered "  >
	<thead>
		<tr>
			<th>ID</th>
			<th>الفترة</th>
			<th>الادوات</th>
		</tr>
	</thead>
	<tbody>
		@foreach($items as $item)
		<tr class="odd gradeX">
			<td class="center">{{$item->PeriodId}}</td>
			
			<td>{{$item->fromH}} الي {{$item->toH}}</td>
			<td>
			<a href="{{url('admin/period/'.$item->PeriodId.'/update')}}" class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					تعديل
				</a>
				<a href="{{url('admin/period/'.$item->PeriodId.'/delete')}}" class="btn btn-red btn-sm btn-icon icon-left">
					<i class="entypo-cancel"></i>
					حذف
				</a>
				
			</td>
		</tr>
		@endforeach

	</tbody>
	<tfoot>

	</tfoot>
</table>


@stop
