@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>Home</a>
	</li>

	<li class="active">

		<strong>Slider</strong>
	</li>
</ol>



<h3>Slider</h3>

<a href="{{url('admin/slider/new')}}" class="btn btn-green btn-lg btn-icon icon-left">
					<i class="entypo-plus"></i>
					New
				</a>
				<br /><br />
				
				
@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif
<table class="table table-bordered "  >
	<thead>
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>Photo</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		@foreach($pages as $item)
		<tr class="odd gradeX">
			<td class="center">{{$item->id}}</td>
			<td>{{$item->title}}</td>
			<td>@if($item->photo != '')
				<img src="{{url('/')}}/public/photos/{{$item->photo}}"  height="100px" />
			@endif
			</td>
			<td>
			<a href="{{url('admin/slider/'.$item->id.'/update')}}" class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					Edit
				</a>
				<a href="{{url('admin/slider/'.$item->id.'/delete')}}" class="btn btn-red btn-sm btn-icon icon-left">
					<i class="entypo-cancel"></i>
					Delete
				</a>
				<br/>
			</td>
		</tr>
		@endforeach
		
	</tbody>
	<tfoot>

	</tfoot>
</table>

@stop
