@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
	</li>

	<li class="active">

		<strong>الدورات</strong>
	</li>
</ol>



<h3>الدورات</h3>
<a href="{{url('/admin/courses/report')}}" class="btn btn-danger">تصدير Exel</a>
<br />
<br />
<form action="" method="get">
<div class="row search">
	<div class="col-md-12">
		<h5>بحث</h5>
	</div>

	<div class="col-md-3">
		<label for="">المدرسة</label>
	<select name="SchoolId" class="form-control select2">
		<option value="">اختر</option>
		@foreach($schools as $sc)
			<option value="{{$sc->Id}}">{{$sc->Name}}</option>

			@endforeach
	</select>
	</div>
	<div class="col-md-3">
		<label for="">الرقم التعريفى للدورة</label>

		{{ Form::text('CourseRefNo',null, array('class' => 'form-control ')) }}

	</div>
	<div class="clearfix"></div>
	<div class="col-md-12">
	    
		<input type="submit" class="btn btn-primary" id="" value="بحث">
	</div>
</div>
</form>


@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif
{{ Form::open(array('url' => '/admin/orders/multiDelete_course')) }}
<button class="btn btn-danger" type="submit">حذف متعدد</button>
<br /><br />
<table class="display"  id="example2" style="width:100%;" >
	<thead>
	<tr>
		<th>#</th>
								<th class="orange-bg" width="10%">
                                    <p>الرقم التعريفي</p>
                                </th>
                                <th class="orange-bg" width="10%">
                                    <p>اﺳﻢ اﻟﺪﻭﺭﺓ</p>
                                </th>

                                <th class="grey-bg">
                                    <p>المدرسة</p>
                                </th>
                                <th class="orange-bg"  width="8%">
                                    <p>بداية الدورة</p>
                                </th>
								<th class="orange-bg"  width="8%">
                                    <p>نهاية الدورة</p>
                                </th>
        <th class="orange-bg" width="10%">
            <p>مواعيد الدورة</p>
        </th>
								<th class="orange-bg" width="5%">
                                    <p>اﻟﻤﺪﺓ</p>
                                </th>
								<th class="orange-bg">
                                    <p>الاماكن المتاحة</p>
                                </th>
								<th class="orange-bg">
                                    <p>المحجوز</p>
                                </th>
                                <th class="grey-bg" width="7%">
                                    <p>السعر</p>
                                </th>
								<th class="grey-bg" width="7%">
                                    <p>الضريبة</p>
                                </th>
                                <th class="orange-bg" width="8%">
                                    <p>اﻟﺴﻌﺮ اﻻﺟﻤﺎﻟﻲ</p>
                                </th>
								<th class="orange-bg">
                                    <p>الادوات</p>
                                </th>
        <th class="orange-bg" >
            <p> الجنس</p>
        </th>
								
                            </tr>
	</thead>
	<tbody>
							@foreach($courses as $item)
								
					
									<tr>
										<td><input type="checkbox" value="{{$item->id}}" name="orderID[]"></td>

										<td>
										<p>{{$item->CourseRefNo}}</p>
									</td>
									<td>
										<p>{{$item->AppDefName}} - {{$item->categoryName}}</p>
									</td>

									<td>
										<p>{{$item->schoolName}}</p>
									</td>
									<td>
										<p>{{$item->CourseStartIn}}</p>
									</td>
									<td>
										<p>{{$item->CourseEndIn}}</p>
									</td>
									<td>
										<p>{{$item->fromH}} <br />to <br />{{$item->toH}}  </p>
									</td>

									<td>
										<p>{{$item->days}} اﻳﺎﻡ</p>
									</td>
								   <?php  $cou=DB::table('orders')->where('orders.CourseId',$item->id)->count();?>
									<td>
										@if(($item->seats - $cou )>0)
										<p>{{$item->seats - $cou}}</p>
										@else
										<p>0</p>
										@endif
									</td>
									<td>
										<p>{{$cou}}</p>
									</td>
									<td>
										<p>{{$item->price}} ريال</p>
									</td>
									<td>
										<p>{{$item->tax}} ريال</p>
									</td>
									<td>
										<p>{{ $item->price + $item->tax}} ريال</p>
									</td>
									<td>
										@if($item->waiting_list=='0')
										<p><a href="{{url('admin/courses/'.$item->id.'/wait')}}" class="btn btn-info">قائمة انتظار</a></p>
										@else
											<p><a href="{{url('admin/courses/'.$item->id.'/unwait')}}" class="btn btn-danger">إلغاء من قائمة انتظار</a></p>

										@endif
										<p><a href="{{url('admin/courses/'.$item->id.'/update')}}" class="btn btn-primary">تعديل</a></p>
										<p>
										<a data-href="{{url('admin/courses/'.$item->id.'/delete')}}" href="#"  data-toggle="modal" data-target="#confirm-delete" class="btn btn-danger">حذف</a></p>
									</td>
									<td>
										<p>{{$item->gender=='male' ? 'ذكر':'انثى' }}</p>
									</td>
								</tr>
							
						
                           @endforeach
	</tbody>
	<tfoot><tr>
		<th>#</th>
		<th class="orange-bg" width="10%">
			<p>الرقم التعريفي</p>
		</th>
		<th class="orange-bg" width="10%">
			<p>اﺳﻢ اﻟﺪﻭﺭﺓ</p>
		</th>

		<th class="grey-bg">
			<p>المدرسة</p>
		</th>
		<th class="orange-bg"  width="8%">
			<p>بداية الدورة</p>
		</th>
		<th class="orange-bg"  width="8%">
			<p>نهاية الدورة</p>
		</th>
		<th class="orange-bg" width="10%">
			<p>مواعيد الدورة</p>
		</th>
		<th class="orange-bg" width="5%">
			<p>اﻟﻤﺪﺓ</p>
		</th>
		<th class="orange-bg">
			<p>الاماكن المتاحة</p>
		</th>
		<th class="orange-bg">
			<p>المحجوز</p>
		</th>
		<th class="grey-bg" width="7%">
			<p>السعر</p>
		</th>
		<th class="grey-bg" width="7%">
			<p>الضريبة</p>
		</th>
		<th class="orange-bg" width="8%">
			<p>اﻟﺴﻌﺮ اﻻﺟﻤﺎﻟﻲ</p>
		</th>
		<th class="orange-bg">
			<p>الادوات</p>
		</th>
		<th class="orange-bg" >
			<p> الجنس</p>
		</th>

	</tr>
	</tfoot>
</table>
{{ Form::close() }}
@stop