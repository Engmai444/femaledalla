@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
						<li>
				<a href="{{url('admin')}}"><i class="entypo-home"></i>لوحة التحكم</a>
			</li>
					<li>
			
							<a href="{{url('admin/courses')}}">الدورات</a>
					</li>
				<li class="active">
			
							<strong>اضافة</strong>
					</li>
					</ol>
					
					
					
			


<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					اضافة دورة
				</div>
				
				
			</div>
			
			<div class="panel-body">
			
					@if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif
                   
					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> خطأ!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif
					
						{{ Form::open(array('url' => '/admin/courses/new','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered','files'=> true)) }}
						
		<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
			<!-- <li > -->
				<!-- <a href="#home" data-toggle="tab"> -->
					<!-- <span class="visible-xs"><i class="entypo-home"></i></span> -->
					<!-- <span class="hidden-xs">English</span> -->
				<!-- </a> -->
			<!-- </li> -->
			<!-- <li class="active"> -->
				<!-- <a href="#ar1" data-toggle="tab"> -->
					<!-- <span class="visible-xs"><i class="entypo-user"></i></span> -->
					<!-- <span class="hidden-xs">Arabic</span> -->
				<!-- </a> -->
			<!-- </li> -->
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane " id="home">	
					
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">المدرسة<span class="required">*</span></label>
						<div class="col-sm-5">
						<select class="form-control" name="SchoolId" id="schools" style="direction: rtl;">
							<option value="">اختر</option>
							@foreach($schools as $item)
								<option value="{{$item->Id}}">{{$item->Name}}</option>
							@endforeach
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">النموذج<span class="required">*</span></label>
						<div class="col-sm-5">
						<select class="form-control" name="applicationdef" style="direction: rtl;" id="applicationdef">
							<option value="">اختر النموذج</option>
							@foreach($applicationdef as $item)
								<option value="{{$item->Id}}">{{$item->AppDefName}}</option>
							@endforeach
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">الفئة<span class="required">*</span></label>
						<div class="col-sm-5">
						<select class="form-control" name="category" id="category" style="direction: rtl;">
							
						</select>
						</div>
					</div>
					<div class="form-group" id="gearShiftLever">
						<label for="field-1" class="col-sm-3 control-label">ناقل الحركة<span class="required">*</span></label>
						<div class="col-sm-5">
						<select class="form-control" name="gearShiftLever" style="direction: rtl;">
							<option value="Automatic">اتوماتيك</option>
							<option value="Manuel">عادي</option>
						</select>
						</div>
					</div>
					
					<div class="form-group" id="car">
						<label for="field-1" class="col-sm-3 control-label">هل السيارة صغيرة</label>
						<div class="col-sm-5">
						<input class="form-control" type="checkbox"  name="small_car" >
						</div>
					</div>
					
					<div class="form-group" id="car2">
						<label for="field-1" class="col-sm-3 control-label">هل السيارة كبيرة</label>
						<div class="col-sm-5">
						<input class="form-control" type="checkbox"  name="big_car" >
						</div>
					</div>
					
					<div class="form-group" >
						<label for="field-1" class="col-sm-3 control-label">رقم تعريف الدورة<span class="required">*</span></label>
						<div class="col-sm-5">
						
						{{ Form::text('CourseRefNo',null, array('class' => 'form-control','placeholder'=>'')) }} 
						</div>
					</div>
					
					<div class="form-group" >
						<label for="field-1" class="col-sm-3 control-label">اسم الدورة</label>
						<div class="col-sm-5">
						
						{{ Form::text('CourseName',null, array('class' => 'form-control','placeholder'=>'')) }} 
						</div>
					</div>
					<div class="form-group" >
						<label for="field-1" class="col-sm-3 control-label">سعر الدورة<span class="required">*</span></label>
						<div class="col-sm-5">
						
						{{ Form::number('price',null, array('class' => 'form-control','placeholder'=>'')) }} 
						</div>
					</div>
					<div class="form-group" >
						<label for="field-1" class="col-sm-3 control-label">الاماكن المتاحة للحجز <span class="required">*</span></label>
						<div class="col-sm-5">
						
						{{ Form::number('seats',null, array('class' => 'form-control','placeholder'=>'')) }} 
						</div>
					</div>
					
					<div class="form-group" >
						<label for="field-1" class="col-sm-3 control-label">مدة الدورة بالايام<span class="required">*</span></label>
						<div class="col-sm-5">
						
						{{ Form::number('days',null, array('class' => 'form-control','placeholder'=>'')) }} 
						</div>
					</div>
					<div class="form-group" >
						<label for="field-1" class="col-sm-3 control-label">تاريخ بداية الدورة<span class="required">*</span></label>
						<div class="col-sm-5">
						{{ Form::text('CourseStartIn',null, array('class' => 'form-control datepicker1','placeholder'=>'0000-00-00')) }} 
						</div>
					</div>
					<div class="form-group" >
						<label for="field-1" class="col-sm-3 control-label">تاريخ نهاية الدورة<span class="required">*</span></label>
						<div class="col-sm-5">
						
						{{ Form::text('CourseEndIn',null, array('class' => 'form-control datepicker2','placeholder'=>'0000-00-00')) }} 
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">الفترة<span class="required">*</span></label>
						<div class="col-sm-5">
						<select class="form-control" name="Period" style="direction: rtl;">
							<option value="0">اختر</option>
							@foreach($periods as $item)
								<option value="{{$item->PeriodId}}">{{$item->fromH}} : {{$item->toH}}</option>
							@endforeach
						</select>
						</div>
					</div>
					
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">النوع</label>
						<div class="col-sm-5">
						
						{{Form::select('gender',['male'=>'ذكور','female'=>'اناث'],'',['class'=>'form-control'])}}
						</div>
					</div>
					
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">حالة الدورة</label>
						<div class="col-sm-5">
						
						{{Form::select('enable',['0'=>'غير مفعلة','1'=>'مفعلة'],'',['class'=>'form-control'])}}
						</div>
					</div>
					
					
					
				
				</div>
			
		</div>
		
					<div class="form-group col-sm-6">
						<a href="javascript:;" onclick="jQuery('#modal-1').modal('show');" class="btn btn-green btn-icon btn-icon btn-lg">اضافة <i class="entypo-check"></i></a>
						<button type="reset" class="btn btn-danger btn-icon icon-left btn-lg">الغاء <i class="entypo-cancel"></i></button>
					</div>	
				{{ Form::close() }} 
						
			</div>
		
		</div>
	
	</div>
</div>


<script type="text/javascript">

jQuery(document).ready(function($)
{
	$("#gearShiftLever").hide();
	$("#car").hide();
	$("#car2").hide();
	
	$('#applicationdef').change(function(){
        var photo = $(this);
        $.ajax({
            url: baseurl+"/admin/ajax/category",
            data: { id: $(this).val()},
            type: 'POST',
			dataType: 'json',
            success: function (data) {
				var sess = '';
                $.each(data.items, function (i, value) {
					sess += '<option value="' + value.Id+'">' + value.Name +'</option>';//$('<option>').text(value.name).attr('value', value.session_id);
				});
				$("#category").html(sess);
				sess ='';
				
				if(data.applicationdef.gearShiftLever == 1){
					$("#gearShiftLever").show();
				}else{
					$("#gearShiftLever").hide();
				}
				
				if(data.applicationdef.car == 1){
					$("#car").hide();
					$("#car2").show();
				}else if(data.applicationdef.car == 2){
					$("#car").show();
					$("#car2").hide();
				}else{
					$("#car2").hide();
					$("#car").hide();
				}
				
            },
            error: function (data) {
                
            }
        });
    });
});
</script>

@stop