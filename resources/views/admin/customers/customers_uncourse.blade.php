@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
	</li>

	<li class="active">

		<strong>المشتركين</strong>
	</li>
</ol>



<h3>الغير مشتركين فى دورات</h3>



@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif

{{ Form::open(array('url' => '/admin/customers/multiDelete')) }}
<button class="btn btn-danger" type="submit">حذف متعدد</button>
<br /> <br/>
<table class="display"  id="example" style="width:100%;">
	<thead>
		<tr>
			<th><input type="checkbox" id="checkAll"></th>
			<th>ID</th>
			<th>الاسم</th>
			<th>رقم الهوية</th>

			<th>البريد الالكتروني</th>
			<th>الجوال</th>
			<th>الجنسية</th>
			<th>تاريخ الميلاد</th>
			<th>المدرسة</th>
			<th>تاريخ الاشتراك</th>
			<th>الادوات</th>
			<th>الجنس</th>
		</tr>
	</thead>
	<tbody>
		@foreach($customers as $item)
			<?php $order=DB::table('orders')->where('StudentId',$item->StudentId)
			                                ->where('deleted_at','0000-00-00 00:00:00')->get();?>
		@if(count($order)=='0')
			<tr class="">
			<td><input type="checkbox" value="{{$item->StudentId}}" name="StudentId[]"></td>
			<td class="center">{{$item->StudentId}}</td>
			<td>{{$item->FullName}}</td>
			<td>{{$item->IDNumber}}</td>

			<td>{{$item->email}}</td>
			<td>{{$item->mobile}}</td>
			<td>{{$item->Nationality}}</td>
			<td><?php if($item->date_type == 1){	\GeniusTS\HijriDate\Date::setToStringFormat('Y-m-d');  $date = date('Y-m-d', strtotime($item->DoB. ' - 1 days')); echo(\GeniusTS\HijriDate\Hijri::convertToHijri($date));  }else{ echo $item->DoB ; } ?></td>
			<td>{{$item->sname}}</td>
				<td>{{$item->created_at}}</td>
			<td>
				 <a href="{{url('admin/customers/'.$item->StudentId.'/update')}}" class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					تعديل
				</a>
				<p>
									<a data-href="{{url('admin/customers/'.$item->StudentId.'/delete')}}" href="#"  data-toggle="modal" data-target="#confirm-delete" class="btn btn-danger">حذف</a></p>
			</td>
			<td>{{$item->gender}}</td>
		</tr>
		@endif
		@endforeach
		
	</tbody>
	<tfoot>
	<tr>
		<th><input type="checkbox" id="checkAll"></th>
		<th>ID</th>
		<th>الاسم</th>
		<th>رقم الهوية</th>

		<th>البريد الالكتروني</th>
		<th>الجوال</th>
		<th>الجنسية</th>
		<th>تاريخ الميلاد</th>
		<th>المدرسة</th>
		<th>تاريخ الاشتراك</th>
		<th>الادوات</th>
		<th>الجنس</th>
	</tr>
	</tfoot>
</table>
{{ Form::close() }}
    <script>
        $("#checkAll").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    </script>
@stop
