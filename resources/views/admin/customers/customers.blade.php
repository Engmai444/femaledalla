@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
	</li>

	<li class="active">

		<strong>المشتركين</strong>
	</li>
</ol>



<h3>المشتركين</h3>
<form action="{{url('/admin/customers/report')}}" method="get">
<div class="row search">
	<div class="col-md-12">
		<h5>تصدير</h5>
	</div>
	<div class="col-md-3">
		<label for="">من تاريخ</label>
		<input type="date" class="form-control datepicker2" id="" name="from" required >
	</div>
	<div class="col-md-3">
		<label for="">الي تاريخ</label>
		<input type="date" class="form-control datepicker2" id="" name="to" required>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-12">
		<input type="submit" class="btn btn-primary" id="" value="تصدير Exel">
	</div>
</div>
</form>


@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif
<form action="{{url('/admin/search_customer/')}}" method="post">
    {{ csrf_field() }}
<div class="row search">
	<div class="col-md-12">
		<h5>بحث</h5>
	</div>
	
	<div class="col-md-3">
		<label for="">الاسم</label>
		<input type="text" class="form-control" id="" name="FullName">
	</div>
	<div class="col-md-3">
		<label for="">رقم الهوية</label>
		<input type="text" class="form-control" id="" name="IDNumber">
	</div>
	<div class="col-md-3">
		<label for="">البريد الالكتروني</label>
		<input type="text" class="form-control" id="" name="email">
	</div>
	<div class="col-md-3">
		<label for="">الجوال</label>
		<input type="text" class="form-control" id="" name="mobile">
	</div>
	<div class="col-md-3">
		<label for="">الجنسية</label>
		<select name="nationality" class="form-control select2">
			<option value="">--اختر--</option>
			@if($nationalitys)
				@foreach($nationalitys as $s)

					<option value="{{$s->Id}}">{{$s->Name}}</option>
				@endforeach
			@endif
		</select>

	</div>
	<div class="col-md-3">
		<label for="">المدرسة</label>
		<select name="school_id" class="form-control select2">
			<option value="">--اختر--</option>
			@if($schools)
				@foreach($schools as $s)

					<option value="{{$s->Id}}">{{$s->Name}}</option>
				@endforeach
			@endif
		</select>

	</div>
	<div class="clearfix"></div>
	<div class="col-md-12">
		<input type="submit" class="btn btn-primary" id="" value="بحث">
	</div>
</div>
</form>
{{ Form::open(array('url' => '/admin/customers/multiDelete')) }}
<button class="btn btn-danger" type="submit">حذف متعدد</button>
<a href="{{url('/admin/customers/uncourse')}}" class="btn btn-success" >الغير مشتركين فى الكورسات</a>

<br /><br/>
<table class="display"  id="posts" style="width:100%;">
	<thead>
		<tr>

            <th>#</th>
			<th>ID</th>
			<th>الاسم</th>
			<th>رقم الهوية</th>

			<th>البريد الالكتروني</th>
			<th>الجوال</th>
			<th>الجنسية</th>
			<th>تاريخ الميلاد</th>
			<th>المدرسة</th>
			<th>تاريخ الاشتراك</th>
			<th>الجنس</th>
			<th>الادوات</th>

		</tr>
	</thead>
	<!--<tbody>
		@foreach($customers as $item)
		<tr class="">
			<td><input type="checkbox" value="{{$item->StudentId}}" name="StudentId[]"></td>
			<td class="center">{{$item->StudentId}}</td>
			<td>{{$item->FullName}}</td>
			<td>{{$item->IDNumber}}</td>

			<td>{{$item->email}}</td>
			<td>{{$item->mobile}}</td>
			<td>{{$item->Nationality}}</td>
			<td><?php if($item->date_type == 1){	\GeniusTS\HijriDate\Date::setToStringFormat('Y-m-d');  $date = date('Y-m-d', strtotime($item->DoB. ' - 1 days')); echo(\GeniusTS\HijriDate\Hijri::convertToHijri($date));  }else{ echo $item->DoB ; } ?></td>
			<td>{{$item->sname}}</td>
			<td>{{$item->created_at}}</td>
			<td>
				 <a href="{{url('admin/customers/'.$item->StudentId.'/update')}}" class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					تعديل
				</a>
				<p>
									<a data-href="{{url('admin/customers/'.$item->StudentId.'/delete')}}" href="#"  data-toggle="modal" data-target="#confirm-delete" class="btn btn-danger">حذف</a></p>
			</td>
			<td>{{$item->gender}}</td>
		</tr>
		@endforeach
		
	</tbody>-->
	<tfoot>
	<tr>
       <th>#</th>
		<th>ID</th>
		<th>الاسم</th>
		<th>رقم الهوية</th>

		<th>البريد الالكتروني</th>
		<th>الجوال</th>
		<th>الجنسية</th>
		<th>تاريخ الميلاد</th>
		<th>المدرسة</th>
		<th>تاريخ الاشتراك</th>
		<th>الجنس</th>
		<th>الادوات</th>

	</tr>
	</tfoot>
</table>

{{ Form::close() }}
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
<script>
	$(document).ready(function () {
		$('#posts').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax":{
				"url": "{{ url('admin/allcustomers') }}",
				"dataType": "json",
				"type": "POST",
				"data":{ _token: "{{csrf_token()}}"}
			},
			"columns": [
                { "data": "#" },
				{ "data": "id" },
				{ "data": "name" },
				{ "data": "id_no" },
				{ "data": "email" },
				{ "data": "mobile" },
				{ "data": "nationality" },
				{ "data": "dob" },
				{ "data": "school" },
				{ "data": "created_at" },
				{ "data": "gender" },
				{ "data": "options" }
			]

		});
	});
</script>
@stop
