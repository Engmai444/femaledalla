@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
						<li>
				<a href="{{url('admin')}}"><i class="entypo-home"></i>لوحة التحكم</a>
			</li>
					<li>

							<a href="{{url('admin/customers')}}">المشتركين</a>
					</li>
				<li class="active">

							<strong>تعديل</strong>
					</li>
					</ol>






<div class="row">
	<div class="col-md-12">

		<div class="panel panel-primary" data-collapsed="0">

			<div class="panel-heading">
				<div class="panel-title">
					تعديل 
				</div>


			</div>

			<div class="panel-body">
					@if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif

					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif

						{{ Form::open(array('url' => '/admin/customers/'.$customer->StudentId.'/update','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered','files'=> true)) }}
					
				
				<div class="tab-content">
					<div class="tab-pane active" id="home">	
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">الاسم</label>
						<div class="col-sm-5">
							{{ Form::text('FullName',$customer->FullName, array('class' => 'form-control','placeholder'=>'')) }} 
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">البريد الالكتروني</label>
						
						<div class="col-sm-5">
							{{ Form::text('email',$customer->email, array('class' => 'form-control','placeholder'=>'')) }} 
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">رقم الجوال</label>
						
						<div class="col-sm-5">
							{{ Form::text('mobile',$customer->mobile, array('class' => 'form-control','placeholder'=>'')) }} 
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">رقم الهوية / الاقامة</label>
						
						<div class="col-sm-5">
							{{ Form::text('IDNumber',$customer->IDNumber, array('class' => 'form-control','placeholder'=>'')) }} 
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"></label>
						
						<div class="col-sm-5">
							<label><input type="checkbox" value="1" name="d_type" id="d_type"  onchange="return validate();" class="swidate" <?php if(isset($customer->date_type) && $customer->date_type== 1 ){ echo 'checked';} ?>>هجري؟</label>
						</div>
					</div>					
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">تاريخ الميلاد</label>
						
						<div class="col-sm-5">
						<?php
						if($customer->date_type == 1){
							\GeniusTS\HijriDate\Date::setToStringFormat('Y-m-d');
							$date = date('Y-m-d', strtotime($customer->DoB. ' - 1 days')); 
							$date = \GeniusTS\HijriDate\Hijri::convertToHijri($date);
							$time=explode("-",$date);
							$day=$time['2'];
							$month=$time['1'];
							$year=$time['0'];							
						}else{
							$date =  $customer->DoB;
							$time=explode("-",$date);
							$day=$time['2'];
							$month=$time['1'];
							$year=$time['0'];


							
							
						}							
						?>
							<div class="datebox" id="georgian" style="<?php if($customer->date_type == 1){ echo 'display:none;';  }else{ echo 'display:flex'; } ?>">
                                <select name="d_day"  id="d_day" class="form-control" required>
								<option value="">اليوم</option>
								<?php
									for($i = 1; $i<32; $i++){ ?>
									
										<option value="{{$i}}" <?php if($day == $i){ echo 'selected';} ?>>{{$i}}</option>
										<?php
										
									}
									?>
									
									
								</select>
								<select name="d_month" id="d_month" class="form-control" required>
									<option value="">الشهر</option>
									<option value='01' <?php if($month == '01'){ echo 'selected';} ?>>يناير</option>
									<option value='02' <?php if($month == '02'){ echo 'selected';} ?>>فبراير</option>
									<option value='03' <?php if($month == '03'){ echo 'selected';} ?>>مارس</option>
									<option value='04' <?php if($month == '04'){ echo 'selected';} ?>>ابريل</option>
									<option value='05' <?php if($month == '05'){ echo 'selected';} ?>>مايو</option>
									<option value='06' <?php if($month == '06'){ echo 'selected';} ?>>يونيو</option>
									<option value='07' <?php if($month == '07'){ echo 'selected';} ?>>يوليو</option>
									<option value='08' <?php if($month == '08'){ echo 'selected';} ?>>اغسطس</option>
									<option value='09' <?php if($month == '09'){ echo 'selected';} ?>>سبتمبر</option>
									<option value='10' <?php if($month == '10'){ echo 'selected';} ?>>اكتوبر</option>
									<option value='11' <?php if($month == '11'){ echo 'selected';} ?>>نوفمبر</option>
									<option value='12' <?php if($month == '12'){ echo 'selected';} ?>>ديسمبر</option>
								</select>
								<select name="d_year" id="d_year" class="form-control" required>
									<option value="">السنة</option>
								<?php
									for($i = 2006; $i>1939; $i--){ ?>
									
										<option value="{{$i}}" <?php if($year == $i){ echo 'selected';} ?>>{{$i}}</option>
										<?php
										
									}
									?>									
									
								</select>
							
							</div>							
							
						
							<div class="datebox" id="hijri" style="<?php if($customer->date_type == 0){ echo 'display:none;';  }else{ echo 'display:flex'; } ?>">
                                <select name="d_day2"  id="d_day" class="form-control" required>
								<option value="">اليوم</option>
								<?php
									for($i = 1; $i<31; $i++){ ?>
									
										<option value="{{$i}}" <?php if($day == $i){ echo 'selected';} ?>>{{$i}}</option>
										<?php
										
									}
									?>
									
									
								</select>
								<select name="d_month2" id="d_month" class="form-control" required>
									<option value="">الشهر</option>
									<option value='01' <?php if($month == '01'){ echo 'selected';} ?>>محرم </option>
									<option value='02' <?php if($month == '02'){ echo 'selected';} ?>>صفر</option>
									<option value='03' <?php if($month == '03'){ echo 'selected';} ?>>ربيع الأول</option>
									<option value='04' <?php if($month == '04'){ echo 'selected';} ?>>ربيع الثاني</option>
									<option value='05' <?php if($month == '05'){ echo 'selected';} ?>>جمادي الأول</option>
									<option value='06' <?php if($month == '06'){ echo 'selected';} ?>>جمادي الثاني</option>
									<option value='07' <?php if($month == '07'){ echo 'selected';} ?>>رجب</option>
									<option value='08' <?php if($month == '08'){ echo 'selected';} ?>>شعبان</option>
									<option value='09' <?php if($month == '09'){ echo 'selected';} ?>>رمضان</option>
									<option value='10' <?php if($month == '10'){ echo 'selected';} ?>>شوال</option>
									<option value='11' <?php if($month == '11'){ echo 'selected';} ?>>ذو القعدة</option>
									<option value='12' <?php if($month == '12'){ echo 'selected';} ?>>ذو الحجة</option>
								</select>
								<select name="d_year2" id="d_year" class="form-control" required>
									<option value="">السنة</option>
								<?php
									for($i = 1435; $i>1373; $i--){ ?>
									
										<option value="{{$i}}" <?php if($year == $i){ echo 'selected';} ?>>{{$i}}</option>
										<?php
										
									}
									?>									
									
								</select>
							
							</div>							
												
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">الجنسية</label>
						
						<div class="col-sm-5">
							{{ Form::select('NationalityId',$nationality,$customer->NationalityId, array('class' => 'form-control','placeholder'=>'')) }} 
						</div>
					</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">المدرسة</label>
							<div class="col-sm-5">
							<select name="school" class=" form-control"  >

									@foreach($schools as $sc)
										<option value="{{$sc->Id}}" @if($sc->Id==$customer->school) {{'selected'}} @endif>{{$sc->Name}}</option>
									@endforeach
								</select>
							</div>
							</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">الصورة</label>
						<div class="col-sm-5">
							{{ Form::file('photo') }} 
							<br />
							@if($customer->photo != '')
								<img src="{{url('/')}}/public/photos/{{$customer->photo}}"  height="60" />
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">كلمة المرور الجديدة</label>
						
						<div class="col-sm-5">
							{{ Form::text('newpassword',null, array('class' => 'form-control','placeholder'=>'اتركه فارغ اذا كنت لا تريد تغيير كلمة المرور للمشترك')) }} 
						</div>
					</div>
					
					</div>
					
					
					</div>
					<div class="form-group col-sm-6">
						<a href="javascript:;" onclick="jQuery('#modal-1').modal('show');" class="btn btn-green btn-icon btn-icon btn-lg">تعديل <i class="entypo-check"></i></a>

					</div>
				{{ Form::close() }}

			</div>

		</div>

	</div>
</div>




<script type="text/javascript">
   function validate(){
		if($('#d_type').prop("checked") == true){
			   $('#hijri').css('display','flex');
			   $('#georgian').css('display','none'); 
		
			   
		}else{
			   $('#hijri').css('display','none');
			   $('#georgian').css('display','flex'); 
			   
		}
   }
jQuery(document).ready(function($)
{
	$(".textarea").wysihtml5();
});
</script>

@stop
