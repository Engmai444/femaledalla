@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
	</li>

	<li class="active">

		<strong>طلبات الاشتراك الغير مسددين</strong>
	</li>
</ol>



<h3>طلبات الاشتراك الغير مسددين</h3>

<form action="{{url('/admin/orders/report_unpaid')}}" method="get">
<div class="row search">
	<div class="col-md-12">
		<h5>تصدير</h5>
	</div>
	<div class="col-md-3">
		<label for="">من تاريخ</label>
		<input type="date" class="form-control datepicker2" id="" name="from" required >
	</div>
	<div class="col-md-3">
		<label for="">الي تاريخ</label>
		<input type="date" class="form-control datepicker2" id="" name="to" required>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-12">
		<input type="submit" class="btn btn-primary" id="" value="تصدير Exel">
	</div>
</div>
</form>



<form action="{{url('/admin/search_orders_unpaid/')}}" method="post">
	{{ csrf_field() }}
<div class="row search">
	<div class="col-md-12">
		<h5>بحث</h5>
	</div>
	<div class="col-md-3">
		<label for="">الرقم المرجعي</label>
		<input type="text" class="form-control" id="" name="OrderId">
	</div>
	<div class="col-md-3">
		<label for="">الاسم</label>
		<input type="text" class="form-control" id="" name="FullName">
	</div>
	<div class="col-md-3">
		<label for="">رقم الهوية</label>
		<input type="text" class="form-control" id="" maxlength="10"  name="IDNumber">
	</div>
	<div class="col-md-3">
		<label for="">الجوال</label>
		<input type="text" class="form-control" id="" name="mobile">
	</div>
	<div class="col-md-3">
		<label for="">من تاريخ</label>
		<input type="text" class="form-control datepicker2" id="" name="from">
	</div>
	<div class="col-md-3">
		<label for="">الي تاريخ</label>
		<input type="text" class="form-control datepicker2" id="" name="to">
	</div>

	<div class="col-md-3">
		<label for="">رقم الدورة </label>
		<input type="text" class="form-control" id="" name="courseref">
	</div>
	<div class="col-md-3">
		<label for="">الدورة </label>
		<input type="text" class="form-control" id="" name="course_name">
	</div>
	<div class="col-md-3">
		<label for="">رقم العملية </label>
		<input type="text" class="form-control" id="" name="order_ref">
	</div>
	<div class="col-md-3">
		<label for="">البريد الالكترونى </label>
		<input type="text" class="form-control" id="" name="email">
	</div>
	<div class="col-md-3">
		<label for="">المدرسة</label>
		<select name="SchoolId" class="'form-control select2">
			<option value="">--اختر--</option>
			@if($schools)
				@foreach($schools as $s)

					<option value="{{$s->Id}}">{{$s->Name}}</option>
				@endforeach
			@endif
		</select>

	</div>
	<div class="clearfix"></div>
	<div class="col-md-12">
		<input type="submit" class="btn btn-primary" id="" value="بحث">
	</div>
</div>
</form>



@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> خطأ!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif
{{ Form::open(array('url' => '/admin/orders/multiDelete_unpaid')) }}
<button class="btn btn-danger" type="submit">حذف متعدد</button>
<br /><br/>
<table class="display"  id="example" style="width:100%;">
	<thead>
		<tr>
			<th>#</th>
			<th>ID</th>
			<th>الاسم</th>
			<th>رقم الهوية</th>
			<th>رقم الجوال</th>
			<th>البريد الالكترونى</th>

			<th>تاريخ التسجيل</th>

			<th width="10%">رقم الدورة</th>
			<th width="15%">الدورة</th>
			<th>المدرسة</th>
			<th>تاريخ بداية</th>
			<th>الفترة</th>
			<th>تأكيد الدفع والحضور</th>
			<th>رقم عملية الدفع</th>
			<th>الجنس</th>
			<th width="10%">الادوات</th>
		</tr>
	</thead>
<tbody>

		@foreach($items as $item)
		<tr class="odd gradeX">
			<td><input type="checkbox" value="{{$item->OrderId}}" name="StudentId[]"></td>
			<td class="center">{{$item->OrderId}}</td>
			<td>{{$item->FullName}}</td>
			<td>{{ $item->IDNumber }}</td>
			<td>{{ $item->mobile }}</td>
			<td>{{ $item->email }}</td>
			<td>{{date('Y-m-d h:i A',strtotime($item->created_at))}}</td>
			<td >{{$item->CourseRefNo}}</td>
			<td >{{$item->CourseName}}</td>
			<td>{{$item->SchoolName}}</td>
			<td>{{$item->CourseStartIn}}</td>
			<td>{{$item->fromH}} : {{$item->toH}}</td>

			<td align="center">@if($item->IsPaid == 1)<span style="color:green;">نعم</span>@else<span style="color:red;">لا</span>@endif</td>
			<td align="center">{{ $item->trans_no }}</td>
			<td>
				
				 <a href="{{url('admin/orders/'.$item->OrderId.'/update')}}" class="btn btn-success btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					تأكيد الحضور
				</a>
				<a data-href="{{url('admin/orders/'.$item->OrderId.'/delete')}}" href="#"  data-toggle="modal" data-target="#confirm-delete" class="btn btn-danger">حذف</a>
			</td>
			<td>{{$item->gender}}</td>
		</tr>
		@endforeach

	</tbody>
	<tfoot>
	<tr>
		<th>#</th>
		<th>ID</th>
		<th>الاسم</th>
		<th>رقم الهوية</th>
		<th>رقم الجوال</th>
		<th>البريد الالكترونى</th>
		<th>تاريخ التسجيل</th>
		<th width="10%">رقم الدورة</th>
		<th width="15%">الدورة</th>
		<th>المدرسة</th>
		<th>تاريخ بداية</th>
		<th>الفترة</th>
		<th>تأكيد الدفع والحضور</th>
		<th>رقم عملية الدفع</th>
		<th>الجنس</th>
		<th width="10%">الادوات</th>
	</tr>
	</tfoot>
</table>
{{ Form::close() }}

@stop
