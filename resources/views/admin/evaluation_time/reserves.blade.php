@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
	</li>

	<li class="active">

		<strong>طلبات اختبار التقييم</strong>
	</li>
</ol>



<h3>طلبات اختبار التقييم</h3>



<form action="{{url('/admin/search_orders_ev/')}}" method="post">
	{{ csrf_field() }}
	<div class="row search">
		<div class="col-md-12">
			<h5>بحث</h5>
		</div>
		<div class="col-md-2">
			<label for="">من تاريخ</label>
			<input type="text" class="form-control datepicker2" id="" name="from"  >
		</div>
		<div class="col-md-2">
			<label for="">الي تاريخ</label>
			<input type="text" class="form-control datepicker2" id="" name="to" >
		</div>
		<div class="col-md-2">
			<label for="">المدرسة</label>
			<select name="SchoolId" class="'form-control select2">
				<option value="">--اختر--</option>
				@if($schools)
					@foreach($schools as $s)

						<option value="{{$s->Id}}">{{$s->Name}}</option>
					@endforeach
				@endif
			</select>

		</div>

		<div class="col-md-3">
			<label for="">الفترة</label>
			<select name="period_id" class="'form-control select2">
				<option value="">--اختر--</option>
				@if($periods)
					@foreach($periods as $s)

						<option value="{{$s->id}}">{{$s->fromH.' To '.$s->toH}}</option>
					@endforeach
				@endif
			</select>

		</div>
		<div class="col-md-2">
			<label for="">الجنس</label>
			<select name="gender" class="'form-control select2">
				<option value="">--اختر--</option>
				<option value="male">Male</option>
				<option value="female">Female</option>
			</select>

		</div>
		<div class="col-md-2">
			<label for="">الاسم</label>
			<input type="text" class="form-control " id="" name="name" >
		</div>
		<div class="col-md-2">
			<label for="">رقم الهوية</label>
			<input type="text" class="form-control " id="" name="id_no" >
		</div>

		<div class="col-md-2">
			<label for="">البريد الالكترونى</label>
			<input type="text" class="form-control " id="" name="email" >
		</div>


		<div class="col-md-2">
			<label for="">الجوال</label>
			<input type="text" class="form-control " id="" name="mobile" >
		</div>
		<div class="clearfix"></div>
		<div class="col-md-12">
			<input type="submit" class="btn btn-primary" id="" value="بحث">
		</div>
	</div>
</form>
@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif

<a href="{{url('/admin/evaluation_time/export_excel')}}" class="btn btn-danger">تصدير Exel</a>
<br />
<table class="display"  id="posts" style="width:100%;"  >
	<thead>
		<tr>
			<th>ID</th>
			<th>الاسم</th>
			<th>رقم الهوية</th>
			<th width="10%">البريد الالكترونى</th>
			<th>الموبايل</th>
			<th>الجنسية</th>
			<th>DoB</th>
			<th>اليوم</th>
			<th>الفترة</th>
			<th>المدرسة</th>
			<th>الجنس</th>
			<th>تاريخ التسجيل</th>

			<th width="17%">الادوات</th>
		</tr>
	</thead>
	<!--<tbody>
		@foreach($items as $item)
		<tr class="odd gradeX">
			<td class="center">{{$item->id}}</td>
			<td>{{$item->FullName}}</td>
			<td>{{$item->IDNumber}}</td>
			<td>{{$item->email}}</td>
			<td>{{$item->mobile}}</td>
			<?php $nat=DB::table('nationality')->where('Id',$item->NationalityId)->first();?>
			<td>{{@$nat->Name}}</td>
			<td>{{$item->DoB}}</td>
			<td class="center">{{$item->day}}</td>
			<td width="140px">{{$item->fromH}} To {{$item->toH}}</td>
			<td class="center">{{$item->Name}}</td>
			<td class="center">{{$item->gender}}</td>
			<td>
                 <?php if($item->attend==0){?>
				<a href="{{url('admin/evaluation_time/'.$item->id.'/attend')}}" class="btn btn-red btn-sm btn-icon icon-left">
					<i class="entypo-cancel-circled"></i>
					حضور
				</a>
					 <?php }else{ ?>
					 <a href="{{url('admin/evaluation_time/'.$item->id.'/inattend')}}" class="btn btn-green btn-sm btn-icon icon-left">
						 <i class="entypo-check"></i>
						 إلغاء الحضور
					 </a>
					 <?php }?>
&nbsp;&nbsp;
				<a href="{{url('admin/evaluation_time/'.$item->id.'/delete_reserve')}}" class="btn btn-red btn-sm btn-icon icon-left" onclick="return ConfirmDelete()">
					<i class="entypo-cancel"></i>
					حذف
				</a>
				
			</td>
		</tr>
		@endforeach

	</tbody>-->
	<tfoot>
	<tr>
		<th>ID</th>
		<th>الاسم</th>
		<th>رقم الهوية</th>
		<th width="10%">البريد الالكترونى</th>
		<th>الموبايل</th>
		<th>الجنسية</th>
		<th>DoB</th>
		<th>اليوم</th>
		<th>الفترة</th>
		<th>المدرسة</th>
		<th>الجنس</th>
		<th>تاريخ التسجيل</th>

		<th width="17%">الادوات</th>
	</tr>
	</tfoot>
</table>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
<script>
	$(document).ready(function () {
		$('#posts').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax":{
				"url": "{{ url('admin/orders_evaluation') }}",
				"dataType": "json",
				"type": "POST",
				"data":{ _token: "{{csrf_token()}}"}
			},
			"columns": [
				{ "data": "id" },
				{ "data": "name" },
				{ "data": "id_no" },
				{ "data": "email" },
				{ "data": "mobile" },
				{ "data": "nationality" },
				{ "data": "dob" },
				{ "data": "day" },
				{ "data": "period" },
				{ "data": "school" },
				{ "data": "gender" },
				{"data":"created_at"},
				{ "data": "options" }
			]

		});
	});
</script>
@stop
