@extends('admin.master')

@section('content')


	<ol class="breadcrumb bc-3">
		<li>
			<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
		</li>

		<li class="active">

			<strong>نتائج اختبار التقييم</strong>
		</li>
	</ol>



	<h3>نتائج اختبار التقييم</h3>

	@if(Session::get('success') != '')
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="fa fa-info-circle"></i>
			{{Session::get('success')}}
			{{Session::forget('success')}}
		</div>
	@endif

	@if (isset($errors) && count($errors) > 0)
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa fa-ban"></i> Error!</h4>
			@foreach ($errors->all() as $error)
				- {{ $error }} <br />
			@endforeach
		</div>
	@endif

	<table class="table table-bordered "  >
		<thead>
		<tr>
			<th>ID</th>
			<th>الاسم</th>
			<th>رقم الهوية</th>
			<th>DoB</th>
			<th>الجنسية</th>
			<th>المدرسة</th>
			<th>الموبايل</th>
			<th>ApplicationdefId</th>
			<th>AssignGrade</th>
			<th>الرقم التعريفى للدورة</th>
			<th>بداية الدورة</th>
            <th>نهاية الدورة</th>
			<th>الجنس</th>

		</tr>
		</thead>
		<tbody>
<?php $i=1;?>
		@foreach($orders as $item)
			<tr class="odd gradeX">
				<td class="center">{{$i}}</td>
				<td>{{$item->fullname}}</td>
				<td>{{$item->idnumber}}</td>
				<td>{{$item->dob}}</td>
				<?php $nat=DB::table('nationality')->where('Id',$item->nationality_id)->first();?>
				<td>{{@$nat->Name}}</td>
				<td>{{$item->Name}}</td>

				<td>{{$item->mobile}}</td>
				<td >{{$item->ApplicationDefId}}</td>
				<td >{{$item->assign_grade}}</td>

				<td>{{$item->CourseRefNo}}</td>
				<td>{{$item->course_start}}</td>
				<td>{{$item->course_end}}</td>
				<td>{{$item->gender}}</td>
			</tr>
			<?php $i++?>
		@endforeach

		</tbody>
		<tfoot>

		</tfoot>
	</table>


@stop
