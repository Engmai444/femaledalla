@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
						<li>
				<a href="{{url('admin')}}"><i class="entypo-home"></i>لوحة التحكم</a>
			</li>
					<li>

							<a href="{{url('admin/evaluation_time')}}">إعدادات التقييم</a>
					</li>
				<li class="active">

							<strong>تعديل</strong>
					</li>
					</ol>






<div class="row">
	<div class="col-md-12">

		<div class="panel panel-primary" data-collapsed="0">

			<div class="panel-heading">
				<div class="panel-title">
					تعديل
				</div>


			</div>

			<div class="panel-body">
					@if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif

					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif

						{{ Form::open(array('url' => '/admin/evaluation_time/'.$item->id.'/update','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered','files'=> true)) }}

				
				<div class="tab-content">

					<div class="tab-pane active" >
							{{ Form::hidden('id',$item->id, array('class' => 'form-control','placeholder'=>'')) }}
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">المدرسة</label>

							<div class="col-sm-5">
								<select name="school_id" class="form-control">
									@foreach($schools as $sc):
									<option  value="{{$sc->Id}}" @if($item->school_id==$sc->Id){{'selected'}} @endif>{{$sc->Name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">الفترة</label>

							<div class="col-sm-5">
								<select name="period_id" class="form-control">
									@foreach($periods as $sc):
									<option  value="{{$sc->id}}" @if($item->period_id==$sc->id){{'selected'}} @endif>{{$sc->fromH.' to '.$sc->toH}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label"> عدد اماكن ذكور</label>

							<div class="col-sm-5">
								{{ Form::text('slots_male',$item->slots_male, array('class' => 'form-control','placeholder'=>'')) }}
							</div>
						</div>
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label"> عدد اماكن إناث</label>

							<div class="col-sm-5">
								{{ Form::text('slots_female',$item->slots_female, array('class' => 'form-control','placeholder'=>'')) }}
							</div>
						</div>
						
					</div>
					
				</div>
					<div class="form-group col-sm-6">
						<a href="javascript:;" onclick="jQuery('#modal-1').modal('show');" class="btn btn-green btn-icon btn-icon btn-lg">حفظ <i class="entypo-check"></i></a>

					</div>
				{{ Form::close() }}

			</div>

		</div>

	</div>
</div>



@stop
