@extends('admin.master')

@section('content')


<ol class="breadcrumb bc-3">
	<li>
	<a href="{{url('admin/')}}"><i class="entypo-home"></i>لوحة التحكم</a>
	</li>

	<li class="active">

		<strong>إعدادات التقييم</strong>
	</li>
</ol>



<h3>إعدادات التقييم</h3>
@if(Session::get('success') != '')
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="fa fa-info-circle"></i>
	{{Session::get('success')}}
	{{Session::forget('success')}}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-ban"></i> Error!</h4>
  @foreach ($errors->all() as $error)
		- {{ $error }} <br />
  @endforeach
  </div>
@endif
<table class="table table-bordered "  >
	<thead>
		<tr>
			<th>ID</th>
			<th>المدرسة</th>
			<th>الفترة</th>
			<th>الاماكن  للاناث</th>
			<th>الاماكن  للذكور</th>
			<th>الادوات</th>
		</tr>
	</thead>
	<tbody>
	<?php $i=1;?>
		@foreach($items as $item)
		<tr class="odd gradeX">
			<td class="center">{{$i}}</td>
			<td class="center">{{$item->Name}}</td>

			<td>{{$item->fromH}} to {{$item->toH}}</td>
			<td>@if($item->slots_female<0){{'0'}}@else{{$item->slots_female}}@endif</td>
			<td>@if($item->slots_male<0){{'0'}}@else{{$item->slots_male}}@endif</td>
			<td>
                 <?php if($item->status==0){?>
				<a href="{{url('admin/evaluation_time/'.$item->id.'/active')}}" class="btn btn-red btn-sm btn-icon icon-left">
					<i class="entypo-cancel-circled"></i>
					تفعيل
				</a>
					 <?php }else{ ?>
					 <a href="{{url('admin/evaluation_time/'.$item->id.'/inactive')}}" class="btn btn-green btn-sm btn-icon icon-left">
						 <i class="entypo-check"></i>
						 إلغاء التفعيل
					 </a>
					 <?php }?>
			<a href="{{url('admin/evaluation_time/'.$item->id.'/update')}}" class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					تعديل
				</a>
				<a href="{{url('admin/evaluation_time/'.$item->id.'/delete')}}" class="btn btn-red btn-sm btn-icon icon-left" onclick="return ConfirmDelete()">
					<i class="entypo-cancel"></i>
					حذف
				</a>
				
			</td>
		</tr>
			<?php $i++;?>
		@endforeach

	</tbody>
	<tfoot>
	{{$items->links()}}
	</tfoot>
</table>


@stop
