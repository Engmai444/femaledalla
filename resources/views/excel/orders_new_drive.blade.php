<!DOCTYPE html>
<html dir="rtl">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style>
        * {
            font-family: DejaVu Sans !important;
        }

        table {
            direction: rtl;
            text-align: right;
        }

        body {
            direction: rtl;
            text-align: right;
        }
    </style>

</head>

<body>
<table>
    <thead>
    <tr>
        <th>Order No</th>
        <th>User Name</th>
        <th>ID Number</th>

<th>Mobile</th>
        <th>Email</th>
        <th>Nationality</th>
        <th>DoB</th>
        <th>SchoolId</th>
        <th>Created_at</th>
        <th>gender</th>
        <!--<th>AssignGrade</th>-->
    </tr>
    </thead>

    <tbody>
    @foreach ($items as $item)
        <tr>
            <td>{{ $item->order_no }}</td>
            <td>{{ $item->FullName }}</td>
            <td>{{ $item->IDNumber }}</td>
            <td>{{ $item->mobile }}</td>
            <td>{{ $item->email }}</td>
            <td>{{ $item->nationality }}</td>
            <td>{{ $item->DoB }}</td>
            <td>{{ $item->school }}</td>

            <td>{{ $item->created_at }}</td>

            <td>@if($item->gender == 'male')

                    0
                @elseif($item->gender=='female')
                    1
                @endif
            </td>
           <!-- <td>{{ $item->assign_grade }}</td>-->
        </tr>
    @endforeach
    </tbody>
</table>
</body>

</html>