<!DOCTYPE html>
<html dir="rtl">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <style>
            *{ font-family: DejaVu Sans !important;}

            table{
                direction: rtl;
                text-align: right;
            }

            body{
                direction: rtl;
                text-align: right;
            }
        </style>

    </head>

    <body>
        <table>
            <thead>
		<tr>
			<th>User Name</th>
			<th>ID Number</th>

			<th>Email</th>
			<th>Mobile</th>
			<th>Nationality</th>
			<th>DoB</th>
			<th>created_at</th>
			<th>Actions</th>
			<th>gender</th>
			<th>School</th>
		</tr>
	</thead>
	<tbody>
		@foreach($customers as $item)
		<tr class="">
			<td>{{$item->FullName}}</td>
			<td>{{$item->IDNumber}}</td>

			<td>{{$item->email}}</td>
			<td>{{$item->mobile}}</td>
			<td>{{$item->Nationality}}</td>
			<td>{{$item->DoB}}</td>
			<td>{{$item->created_at}}</td>
			<td>
				<!-- <a href="{{url('admin/branches/'.$item->order_id.'/delete')}}" class="btn btn-red btn-sm btn-icon icon-left"> -->
					<!-- <i class="entypo-cancel"></i> -->
					<!-- Delete -->
				<!-- </a> -->
			</td>
			<td>@if($item->gender == 'male')

					0
				@elseif($item->gender=='female')
					1
				@endif
			</td>
			<td>{{$item->sname}}</td>
		</tr>
		@endforeach
		
	</tbody>
	</table>

</html>