<!DOCTYPE html>
<html dir="rtl">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <style>
            *{ font-family: DejaVu Sans !important;}

            table{
                direction: rtl;
                text-align: right;
            }

            body{
                direction: rtl;
                text-align: right;
            }
        </style>

    </head>

    <body>
        <table>
    <tr>
		<th class="orange-bg">
			<p>CourseRefNo</p>
		</th>
		<th class="orange-bg">
			<p>CourseName</p>
		</th>

		<th class="grey-bg">
			<p>categoryName</p>
		</th>
		<th class="orange-bg">
			<p>CourseStartIn</p>
		</th>
		<th class="orange-bg">
			<p>days</p>
		</th>
		<th class="grey-bg">
			<p>Price</p>
		</th>
		<th class="orange-bg">
			<p>Total price</p>
		</th>
		<th class="orange-bg">
			<p>النوع</p>
		</th>
		
	</tr>
	@foreach($courses as $item)
	<tr>
		<td>
			<p>{{$item->CourseRefNo}}</p>
		</td>
		<td>
			<p>{{$item->CourseName}}</p>
		</td>

		<td>
			<p>{{$item->categoryName}}</p>
		</td>
		<td>
			<p>{{$item->CourseStartIn}}</p>
		</td>
		<td>
			<p>{{$item->days}} اﻳﺎﻡ</p>
		</td>
		<td>
			<p>{{$item->price}} ريال</p>
		</td>
		<td>
			<p>{{ $item->price + $item->tax}} ريال</p>
		</td>
		<td>@if($item->gender == 'male')

				0
			@elseif($item->gender=='female')
				1
			@endif
		</td>
	</tr>
   @endforeach
	</table>

</html>