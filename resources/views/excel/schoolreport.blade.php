<!DOCTYPE html>
<html dir="rtl">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <style>
            *{ font-family: DejaVu Sans !important;}

            table{
                direction: rtl;
                text-align: right;
            }

            body{
                direction: rtl;
                text-align: right;
            }
        </style>

    </head>

    <body>
        <table>
			<thead>
			<tr>
				<th>ID</th>
				<th>المدرسة</th>

				<th>عدد المشتركين فى المدرسة</th>
				<th>إجمالى المبلغ المسدد</th>
				<th>عدد المشتركين المسددين فى دورات</th>
				<th>عدد المشتركين الغير المسددين فى دورات</th>
				<th>عدد المشتركين الغير مشتركين فى دورات</th>
			</tr>
			</thead>
			<tbody>
			<?php $i=1;?>
			@foreach($items as $item)
				<tr class="odd gradeX">
					<td class="center">{{$i}}</td>
					<td class="center">{{$item->Name}}</td>
					<td class="center">{{$item->total}}</td>
					<?php $tot=DB::table('orders')->join('users','users.StudentId','=','orders.StudentId')
							->join('course','course.id','=','orders.CourseId')
							->where('users.school',$item->Id)
							->where('IsPaid','1')
							->select(DB::raw('Sum(price+tax) as tot'))->first();?>
					<td>@if($tot->tot>0){{$tot->tot}}@else{{'0'}} @endif</td>
					<?php $cot=DB::table('orders')->join('users','users.StudentId','=','orders.StudentId')
							->where('users.school',$item->Id)
							->where('IsPaid','1')
							->count();?>
					<td>{{$cot}}</td>
					<?php $uncot=DB::table('orders')->join('users','users.StudentId','=','orders.StudentId')
							->where('users.school',$item->Id)
							->where('IsPaid','0')
							->count();?>
					<td>{{$uncot}}</td>
					<td>{{$item->total-($cot+$uncot)}}</td>

				</tr>
				<?php $i++;?>
			@endforeach

			</tbody>
	</table>
	</body>
</html>