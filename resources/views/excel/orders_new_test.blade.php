<!DOCTYPE html>
<html dir="rtl">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style>
        * {
            font-family: DejaVu Sans !important;
        }

        table {
            direction: rtl;
            text-align: right;
        }

        body {
            direction: rtl;
            text-align: right;
        }
    </style>

</head>

<body>
<table>
    <thead>
    <tr>
        <th>OrderNo</th>
        <th>FullName</th>
        <th>IDNumber</th>

<th>Mobile</th>
        <th>Email</th>
        <th>DoB</th>
        <th>SponserType</th>
        <th>NationalityId</th>
        <th>SchoolId</th>
        <th>ApplicationDefId</th>
        <th>NewApplicationDate</th>
        <th>CourseRefNo</th>
        <th>IsPaid</th>
        <th>PayRefNo</th>
        <th>gender</th>
        <!--<th>AssignGrade</th>-->
    </tr>
    </thead>

    <tbody>
    @foreach ($items as $item)
        <tr>
            <td>{{ $item['order_no'] }}</td>
            <td>{{ $item['FullName'] }}</td>
            <td>{{ $item['IDNumber'] }}</td>
            <td>{{ $item['mobile'] }}</td>
            <td>{{ $item['email'] }}</td>

            <td>{{ $item['DoB'] }}</td>
            <td>1</td>
            <td>{{ $item['NationalityId'] }}</td>
            <td>{{ $item['school'] }}</td>
            <td></td>
            <td>{{ $item['created_at'] }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td>@if($item['gender'] == 'male')

                    0
                @elseif($item['gender']=='female')
                    1
                @endif
            </td>

        </tr>
    @endforeach
    </tbody>
</table>
</body>

</html>