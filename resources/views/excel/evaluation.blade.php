<!DOCTYPE html>
<html dir="rtl">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style>
        * {
            font-family: DejaVu Sans !important;
        }

        table {
            direction: rtl;
            text-align: right;
        }

        body {
            direction: rtl;
            text-align: right;
        }
    </style>

</head>

<body>
<table >
    <thead>
    <tr>
        <th>CheckNo</th>
        <th>FullName</th>
        <th>IDNumber</th>
        <th width="10%">Email</th>
        <th>Mobile</th>
        <th>Nationality</th>
        <th>DoB</th>
        <th>CheckDate</th>
        <th>From-To</th>
        <th>SchoolId</th>
        <th>Gender</th>
        <th>Created_at</th>



    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr class="odd gradeX">
            <td class="center">{{$item->id}}</td>
            <td>{{$item->FullName}}</td>
            <td>{{$item->IDNumber}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->mobile}}</td>
            <td>{{$item->NationalityId}}</td>
            <td>{{$item->DoB}}</td>
            <td class="center">{{$item->day}}</td>
            <td>{{$item->fromH}} to {{$item->toH}}</td>
            <td class="center">{{$item->sid}}</td>
            <td class="center">@if($item->gender=='male'){{'0'}}@else{{'1'}} @endif</td>
            <td class="center">{{$item->created_at}}</td>
        </tr>
    @endforeach

    </tbody>
    <tfoot>

    </tfoot>
</table>
</body>

</html>