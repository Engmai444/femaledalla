
@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | من نحن</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
   <div class="clearfix"></div>
    <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-1.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">عن شركة داله</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 text-center">
                    <div class="about-box">
                        <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>ﻧﺒﺬﺓ ﻣﺨﺘﺼﺮﺓ ﻋﻦ اﻟﺸﺮﻛﺔ  </h2>
                        <div class="content">

                            <ul class="circle">
                                <li>ﺗﻌﺘﺒﺮ ﺷﺮﻛﺔ ﺩﻟﻪ ﻟﺘﻌﻠﻴﻢ اﻟﻘﻴﺎﺩﺓ ﻣﻦ اﻟﺸﺮﻛﺎﺕ اﻟﺮاﺋﺪﺓ ﻓﻲ ﻣﺠﺎﻝ ﺗﻌﻠﻴﻢ ﻣﻬﺎﺭاﺕ اﻟﻘﻴﺎﺩﺓ ﺑﺎﻟﻤﻤﻠﻜﺔ اﻟﻌﺮﺑﻴﺔ اﻟﺴﻌﻮﺩﻳﺔ. </li>
                                <li>ﺗﺴﺘﺤﻮﺫ اﻟﺸﺮﻛﺔ ﻋﻠﻲ اﻟﺤﺼﺔ اﻟﺴﻮﻗﻴﺔ اﻷﻛﺒﺮ ﻓﻲ ﺳﻮﻕ ﻧﺸﺎﻁ ﺗﻌﻠﻴﻢ ﻣﻬﺎﺭاﺕ اﻟﻘﻴﺎﺩﺓ ﻛﻤﺎ ﺳﻴﺮﺩ ﻻﺣﻘﺎ ﺑﺪﺭاﺳﺔ اﻟﺴﻮﻕ. </li>
                                <li>ﺗﻘﻮﻡ اﻟﺸﺮﻛﺔ ﺣﺎﻟﻴﺎ ﺑﺈﻋﺪاﺩ اﻟﻜﻮاﺩﺭ اﻟﺴﻌﻮﺩﻳﺔ ﻋﺎﻟﻴﺔ اﻟﻤﻬﺎﺭﺓ ﻭاﻟﺘﺨﺼﺺ ﻭاﻟﺘﻲ ﺗﻤﺜﻞ اﻟﻨﻮاﻩ اﻟﺤﻘﻴﻘﻴﺔ ﻟﺘﺰﻭﻳﺪ ﺳﻮﻕ اﻟﻌﻤﻞ
                                    <br /> ﺑﺎﻟﻤﻤﻠﻜﺔ ﺑﺎﻟﻔﻨﻴﻴﻦ ﻭاﻟﻤﺪﺭﺑﻴﻦ ﻭاﻟﻘﻴﺎﺩاﺕ اﻟﻤﺪﺭﻳﺔ ﻓﻲ ﻫﺬا اﻟﻤﺠﺎﻝ. ﻫﺬا اﻟﻤﺠﺎﻝ.</li>
                            </ul>
                            <br />
                          <p dir="rtl">ﺗﺄﺳﺴﺖ ﺷﺮﻛﺔ ﺩﻟﻪ ﻟﺘﻌﻠﻴﻢ اﻟﻘﻴﺎﺩﺓ ﻋﺎﻡ 1975 ﻓﻲ  اﻟﻤﻤﻠﻜﻪ اﻟﻌﺮﺑﻴﻪ اﻟﺴﻌﻮﺩﻳﻪ ﻭﻓﻰ ﻣﺪﻳﻨﻪ  ﺟﺪﻩ ،ﻭﺗﻌﺘﺒﺮ   اﻷﻛﺜﺮ ﻧﺠﺎﺣﺎً ﻟﺘﻌﻠﻴﻢ ﻗﻴﺎﺩﺓ اﻟﺴﻴﺎﺭاﺕ ﻓﻲ اﻟﻤﻤﻠﻜﻪ  اﻟﻌﺮﺑﻴﻪ اﻟﺴﻌﻮﺩﻳﻪ ﺣﻴﺚ اﻥ ﻋﺪﺩ اﻟﻤﻠﺘﺤﻘﻴﻦ ﺑﻬﺎ ﺳﻨﻮﻳﺎ ﻳﺘﻌﺪﻯ 250 اﻟﻒ ﻃﺎﻟﺐ<br /><br />
  ﺑﺪﺃﺕ ﺷﺮﻛﺔ ﺩﻟﻪ ﻟﺘﻌﻠﻴﻢ ﻗﻴﺎﺩﺓ اﻟﺴﻴﺎﺭاﺕ  اﻟﺘﺪﺭﻳﺐ ﺑﻔﺮﻉ ﻭاﺣﺪ ﺳﻨﻪ 1975  ﻭاﺳﺘﻤﺮﺕ  ﺑﺎﻟﺘﻮﺳﻊ ﻭاﻹﻧﺘﺸﺎﺭ ﻓﻲ اﻟﻤﻤﻠﻜﻪ  <br />ﺣﺘﻰ ﻭﺻﻞ ﻋﺪﺩ اﻻﻓﺮﻉ ﺑﻬﺎ اﻟﻰ 10 اﻓﺮﻉ ﻭﻣﻮﺯﻋﻪ  ﻛﺎﻟﺘﺎﻟﻰ  :</p>

<ul class="circle">
  <li>ﻋﺪﺩ 1 ﻓﺮﻉ ﺑﻤﺪﻳﻨﻪ ﺟﺪﻩ </li>
  <li>ﻋﺪﺩ 2 ﻓﺮﻉ ﺑﻤﺪﻳﻨﻪ اﻟﺮﻳﺎﺽ ( اﻟﺘﺨﺼﺼﻰ – اﻟﺴﻠﻰ )</li>
  <li>ﻋﺪﺩ 1 ﻓﺮﻉ ﺑﻤﺪﻳﻨﻪ اﻟﺨﺮﺝ</li>
  <li>ﻋﺪﺩ 1 ﻓﺮﻉ ﺑﻤﺪﻳﻨﻪ اﻟﻄﺎﻳﻒ</li>
  <li>ﻋﺪﺩ 1 ﻓﺮﻉ ﺑﻤﺪﻳﻨﻪ ﺟﻴﺰاﻥ</li>
</ul>

<p>
ﻳﺘﺠﺎﻭﺯ ﻋﺪﺩ اﻟﻤﺘﻘﺪﻣﻴﻦ ﺳﻨﻮﻳﺎ 250 ﺃﻟﻒ ﻃﺎﻟﺐ 
<br /><br />
ﺗﻘﺪﻡ ﺷﺮﻛﺔ ﺩﻟﻪ ﺩﻭﺭاﺕ ﺗﺪﺭﻳﺒﻴﺔ ﻟﺘﺄﻫﻴﻞ اﻟﻤﺘﺪﺭﺑﻴﻦ ﻋﻠﻰ اﻟﻘﻴﺎﺩﺓ اﻻﻣﻨﻪ ﻭﺭﻓﻊ اﻟﻤﺴﺘﻮﻯ اﻟﺜﻘﺎﻓﻲ ﻓﻲ اﻟﺴﻼﻣﺔ اﻟﻤﺮﻭﺭﻳﺔ ﻟﻠﻤﺘﺪﺭﺏ ﻣﻦ ﺧﻼﻝ اﻟﺘﺪﺭﻳﺐ اﻟﻌﻤﻠﻲ ﻭاﻟﻨﻈﺮﻱ ﻣﻦ ﺧﻼﻝ ﺃﺳﻄﻮﻝ ﻣﻦ اﻟﻤﺮﻛﺒﺎﺕ ﻳﻀﻢ ﺃﻛﺜﺮ ﻣﻦ 300  ﺳﻴﺎﺭﺓ ﻣﻦ اﻟﻤﺮﻛﺒﺎﺕ اﻟﺨﺼﻮﺻﻲ ﻭاﻟﻌﻤﻮﻣﻲ ( اﻟﺨﻔﻴﻒ – اﻟﺜﻘﻴﻞ ) ﻭاﻟﺤﺎﻓﻼﺕ  ﻭاﻟﺪﺭﺟﺎﺕ اﻟﻨﺎﺭﻳﺔ 
<br><br />
. ﻟﺪﻳﻬﺎ ﺃﻛﺜﺮ ﻣﻦ 200 ﻣﺪﺭﺏ ﻣﻦ ﻋﺪﺓ ﺟﻨﺴﻴﺎﺕ ﻣﺨﺘﻠﻔﺔ ﻭﺫﻟﻚ ﻣﻦ اﺟﻞ ﺳﻬﻮﻟﺔ ﺇﻋﻄﺎء اﻟﻤﻌﻠﻮﻣﺎﺕ ﻭﺳﻬﻮﻟﻪ ﺇﻳﺼﺎﻟﻬﺎ ﻟﻠﻄﺎﻟﺐ ﻧﻈﺮا ﻟﺘﻌﺪﺩ اﻟﻮاﻓﺪﻳﻦ ﻓﻲ ﺑﻼﺩﻧﺎ ﻣﻦ ﺟﻨﺴﻴﺎﺕ ﻣﺨﺘﻠﻔﺔ
ﺗﻌﻤﻞ ﺷﺮﻛﺔ ﺩﻟﻪ ﻟﺘﻌﻠﻴﻢ ﻗﻴﺎﺩﺓ اﻟﺴﻴﺎﺭاﺕ ﺗﺤﺖ ﺃﺷﺮاﻑ اﻹﺩاﺭﺓ اﻟﻌﺎﻣﺔ ﻟﻠﻤﺮﻭﺭ ,<br><br />
ﻭﺗﺤﺮﺹ ﺩاﺋﻤﺎ ﺷﺮﻛﺔ ﺩﻟﻪ ﻟﺘﻌﻠﻴﻢ ﻗﻴﺎﺩﺓ اﻟﺴﻴﺎﺭاﺕ ﻋﻠﻰ ﺭﻓﻊ اﻟﻤﺴﺘﻮﻯ اﻟﺘﻌﻠﻴﻤﻲ ﻭاﻟﺘﺪﺭﻳﺒﻲ ﻟﻘﺎﺋﺪﻱ اﻟﻤﺮﻛﺒﺎﺕ ﺑﺄﻧﻮاﻋﻬﺎ .
</p><br><br />



                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12 col-md-12 text-center">
                    <a class="pdf" href="#"><img src="{{url('/resources/views/site')}}/images/pdf.png" alt=""> </a>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12 col-md-12 orange-box text-center">
                    <h2>اﻟﻤﻬﻤﻪ</h2>
                    <p>ﺗﺪﺭﻳﺐ ﻋﻤﻠﻴﺎ ﻭﻧﻈﺮﻳﺎ ﻭﺗﺄﻫﻴﻞ ﻗﺎﺋﺪﻱ اﻟﻤﺮﻛﺒﺎﺕ ﺑﺄﻧﻮاﻋﻬﺎ ﻣﻦ اﺟﻞ ﺇﻋﺪاﺩ ﺳﺎﺋﻖ ﻣﺎﻫﺮ ﻟﻠﻤﺤﺎﻓﻈﺔ ﻋﻠﻰ اﻷﺭﻭاﺡ ﻭاﻟﻤﻤﺘﻠﻜﺎﺕ</p>

                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12 col-md-12 grey-box text-center">
                    <h2>ﻫﺪﻑ اﻟﻤﺪﺭﺳﺔ</h2>
<ul style="list-style: none;">
  <li>ﺗﻮﻓﻴﺮ اﻓﻀﻞ اﻟﺨﺪﻣﺎﺕ ﻟﻠﻤﺘﺪﺭﺑﻴﻦ  ﻓﻲ ﺑﻴﺌﺔ ﺗﻌﻠﻴﻤﻴﺔ ﻣﻨﺎﺳﺒﺔ</li>
  <li> ﺭﻓﻊ ﺟﻮﺩﺓ اﻟﺘﺪﺭﻳﺐ  ﻭﺫﻟﻚ ﺗﻤﺎﺷﻴﺎ ﻣﻊ ﺗﻄﻠﻌﺎﺕ اﻟﻤﻤﻠﻜﺔ اﻟﻌﺮﺑﻴﺔ اﻟﺴﻌﻮﺩﻳﺔ ﻓﻰ ﺭﺅﻳﺔ 2030</li>
  <li>ﺿﻤﺎﻥ ﺗﻘﺪﻳﻢ ﻛﺎﻓﺔ  اﻟﺨﺪﻣﺎﺕ ﻭﻓﻖ ﻣﻌﺎﻳﻴﺮ اﻟﺠﻮﺩﺓ ﻭاﻟﻜﻔءﺔ ﻭاﻟﺸﻔﺎﻓﻴﺔ</li>
  <li>ﺧﺪﻣﺔ اﻟﻤﺠﺘﻤﻊ ﻋﻠﻰ  اﻟﻤﺴﺘﻮﻯ اﻟﻤﺤﻠﻰ ﻭاﻟﺪﻭﻟﻰ</li>
</ul>

                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12 col-md-12 text-center">
                    <div class="about-box">
                        <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>ﻃﺒﻴﻌﺔ اﻟﻨﺸﺎﻁ</h2>
                        <div class="content">

                            <ul class="circle">
                                <li>ﺗﺪﺭﻳﺐ ﻭﺗﺄﻫﻴﻞ ﻗﺎﺋﺪﻱ اﻟﻤﺮﻛﺒﺎﺕ ﺑﺄﻧﻮاﻋﻬﺎ ( ﺧﺼﻮﺻﻲ - ﻋﻤﻮﻣﻲ ﺛﻘﻴﻞ - ﻋﻤﻮﻣﻲ ﺧﻔﻴﻒ ﺣﺎﻓﻼﺕ – ﺩﺭﺟﺎﺕ ﻧﺎﺭﻳﻪ ) ﻣﻦ اﺟﻞ ﺃﻥ ﻳﻜﻮﻥ ﻗﺎﺋﺪ اﻟﻤﺮﻛﺒﺔ ﻣﻠﻢ ﺑﻜﺎﻓﺔ ﺗﻌﻠﻴﻤﺎﺕ اﻟﻘﻴﺎﺩﺓ اﻻﻣﻨﻪ ﻭﻳﺠﺘﺎﺯ اﻻﺧﺘﺒﺎﺭ ﻓﻲ اﻟﺤﺼﻮﻝ ﻋﻠﻰ اﻟﺮﺧﺼﺔ ﻓﻲ اﻻﺧﺘﺒﺎﺭ اﻟﻨﻈﺮﻱ ﻭاﻟﻌﻤﻠﻲ 
                                </li>

                            </ul>

                        </div>

                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{url('/resources/views/site')}}/images/cr1.jpg" alt="">
                </div>
                <!-- end col -->
                <div class="col-12 col-md-6">
                    <img src="{{url('/resources/views/site')}}/images/cr1.jpg" alt="">
                </div>
                <!-- end col -->

            </div>
            <!-- end row -->

        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->
    
@stop