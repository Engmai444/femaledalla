@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | الدورات المشترك بها |  {{$course->CourseName}}</title>
<meta name="Description" content="" />
@stop
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">
                <div class="row">
				<br />
                    <h2 class="page_title">{{$course->CourseName}}</h2>
				<br />	
                </div>
            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 side-profile">
                            <div class="profile-sidebar">
								<div class="profile-userpic text-center">
									@if(Auth::guard('doctor')->user()->img_national_id != '')
										<img src="{{url('/public/photos/thumb/'.Auth::guard('doctor')->user()->IDNumber.'/')}}/{{Auth::guard('doctor')->user()->img_national_id}}">
									@else
										<img src="{{url('/public/photos')}}/avatar.jpg">
									@endif
								</div>
								<div class="profile-usertitle text-center">

									<div class="profile-usertitle-name">
										{{Auth::guard('doctor')->user()->IDNumber}}
										<br />
										<?php

										$sch=DB::table('schools')->where('Id',Auth::guard('doctor')->user()->school)->first();
										echo $sch->Name?>
									</div>
								</div>
								<!-- END SIDEBAR USER TITLE -->
								<!-- SIDEBAR BUTTONS -->
								<div class="aside user-menu-block">
									<!--<div class="profile-usermenu">
										<a href="{{url('/course_start')}}" class="btn btn-primary ">
											<i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة </a>
									</div>-->
										<div class="profile-usermenu">
											<a href="{{url('/course_start_new')}}" class="btn btn-primary ">
												<i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار</a>
										</div>
									<div class="profile-usermenu">
										<a href="{{url('/evaluation')}}" class="btn btn-primary ">
											<i class="fa fa-calendar" aria-hidden="true"></i>  حجز موعد تحديد مستوى </a>
									</div>
									<!--<div class="profile-usermenu">
										<a href="{{url('/mycourses')}}" class="btn btn-primary ">
											<i class="fa fa-pencil-square" aria-hidden="true"></i>  الدورات المشترك بها </a>
									</div>-->
								<!-- <div class="profile-usermenu">
                                               <a href="{{url('/profile')}}" class="btn btn-primary ">
                                                            <i class="fa fa-user" aria-hidden="true"></i> تعديل الملف الشخصي </a>
                                            </div>

                                            <div class="profile-usermenu">
                                               <a href="{{url('/mycourses')}}" class="btn btn-primary">
                                                 <i class="fa fa-user" aria-hidden="true"></i> دورات مشترك بها </a>
                                            </div>

                                            <div class="profile-userbuttons">
                                                <a href="{{url('/logout')}}" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> ﺗﺴﺠﻴﻞ اﻟﺨﺮﻭﺝ </a>
                                            </div>-->
									<!-- END SIDEBAR BUTTONS -->
								</div>
                                <!-- END SIDEBAR USER TITLE -->
                                <!-- SIDEBAR BUTTONS -->

                            </div>
                        </div>
                        <!-- end col -->
                        <!-- Default form register -->
					
						
                       <div class="text-right border border-light col-md-8 p-5">
                           	@if(session('msg'))
						
						<div class="notealert">{{session('msg')}}</div>
						
						@endif
					   <span class="print">
                                            <a href="javascript:printDiv('couseinfo')"  class="btn btn-link"><i class="fa fa-lg fa-print"></i> طباعة</a>
                                        </span>
                                        @if($course->IsPaid=='0')
                                          <span class="pay">
                                            <a href="{{url('payment_course/'.$course->OrderId)}}"  class="btn btn-link" style="background:green"><i class="fa fa-lg fa-paypal"></i>  الدفع أونلاين بواسطة بطاقة مدي </a>
                                        </span>
                                        @endif
										<div id="couseinfo">
							<table class="table table-borderd" >
							    <tr>
									<td>اسم الفرع</td>
									<td>{{$course->schoolName}}</td>
								</tr>
                                <tr>
									<td>رقم الهوية</td>
									<td>{{$course->IDNumber}}</td>
								</tr>
                                <tr>
									<td>اسم المشترك</td>
									<td>{{$course->FullName}}</td>
								</tr>
                                <tr>
									<td>الجنسية</td>
									<td>{{$course->nationality}}</td>
								</tr>
                                <tr>
									<td>رقم الجوال</td>
									<td>{{$course->mobile}}</td>
								</tr>
                                <tr>
									<td>رقم تعريف الدورة</td>
									<td>{{$course->CourseRefNo}}</td>
								</tr>
								<tr>
									<td>الرقم المرجعي</td>
									<td>{{$course->OrderId}}</td>
								</tr>

								@if($course->IsPaid == 1)
									<tr>
										<td>حالة السداد</td>
										<td>تم الدفع عبر الانترنت</td>
									</tr>
									<tr>
										<td>رقم عملية السداد</td>
										<td>{{$course->trans_no}}</td>
									</tr>
								@else
									<tr>
										<td>حالة السداد</td>
										<td>لم يتم السداد</td>
									</tr>
								@endif
								<tr>
									<td>اسم الدورة</td>
									<td>{{$course->CourseName}}</td>
								</tr>
								<tr>
									<td>تاريخ التسجيل</td>
									<td>{{date('d-m-Y',strtotime($course->created_at))}}</td>
								</tr>
								<tr>
									<td>تاريخ بداية الدورة</td>
									<td>{{date('d-m-Y',strtotime($course->CourseStartIn))}}</td>
								</tr>
								<tr>
									<td>المدة</td>
									<td>{{$course->days}} أيام</td>
								</tr>
								<tr>
									<td>الفترة</td>
									<td>{{$course->fromH}} الي {{$course->toH}}</td>
								</tr>	
							</table>
								<div>
								</div>
							<?php
							$option = DB::table('site_option')->where('varname','hm_mycoursrs_text')->first();
							?>
							<div class="notealert">{{$option->value}}</div>
						
							</div>
					   </div>

                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->

            </div>
            <!--/.about-block  -->

        </div>
        <!-- /.content -->
		<iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>
@stop