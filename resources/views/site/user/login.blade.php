@extends('site.master')
@section('content')


<div class="content container-fluid read-bg">
  <div class="container">
  <div class="row justify-content-center">
    <div class="page_title">دخول</div>
  </div>
</div>
</div>
<!-- End slider -->
<div class="content container-fluid login-content">
  <div class="reada-block">
  <div class="container">
    <div class="row justify-content-center">
 <!-- Default form register -->

<form class="text-center border border-light col-md-5 p-5" id="login">
 <div class="msg"></div>
 <div class="form-group">
    <!-- E-mail -->
    <input type="text" name="email" class="form-control mb-4" required placeholder="رقم الهوية">
</div>
<div class="form-group">
	<input type="password" name="password" class="form-control mb-4" required placeholder="كلمة المرور">
  </div>
  <div class="form-group">
    <!-- Sign up button -->
    <button class="btn btn-info my-4 btn-block" type="submit">دخول</button> 
    </div>
    <div class="form-group">
    <a href="{{url('/resetpassword')}}">نسيت كلمة المرور؟</a>
    </div>
</form>

<!-- Default form register -->

    </div><!-- /.serv -->
</div><!-- /.container -->

</div><!--/.about-block  -->

</div><!-- /.content -->
@stop