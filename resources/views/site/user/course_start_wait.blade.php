@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | الدورات المشترك بها </title>
<meta name="Description" content="" />
@stop
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">

                   <center> <h2 class="page_title">الدورات 30 ساعة قائمة الانتظار</h2></center>

            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container-fluid">
                    <div class="row">
                         <!-- <div class="col-md-2 side-profile">
                            <div class="profile-sidebar">
                                <div class="profile-userpic text-center">
                                    @if(Auth::guard('doctor')->user()->img_national_id != '')
                                        <img src="{{url('/public/photos/thumb/'.Auth::guard('doctor')->user()->IDNumber.'/')}}/{{Auth::guard('doctor')->user()->img_national_id}}">
                                    @else
                                        <img src="{{url('/public/photos')}}/avatar.jpg">
                                    @endif
                                </div>
    
                                <div class="profile-usertitle text-center">

                                    <div class="profile-usertitle-name">
                                        {{Auth::guard('doctor')->user()->IDNumber}}
                                        <br />
                                        <?php

                                        $sch=DB::table('schools')->where('Id',Auth::guard('doctor')->user()->school)->first();
                                        echo $sch->Name?>
                                    </div>
                                </div>




                            </div>

                        </div> -->
                        <!-- end col -->
                        <!-- Default form register -->
                        <div class="col-md-12 left-col">
                            <div class="aside user-menu-block mt-5 ">
                               <!-- <div class="profile-usermenu">
                                    <a href="{{url('/course_start')}}" class="btn btn-primary ">
                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة </a>
                                </div>
                                <div class="profile-usermenu">
                                    <a href="{{url('/course_start_wait')}}" class="btn btn-primary ">
                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار</a>
                                </div>-->
                                   <div class="profile-usermenu">
                                       <a href="{{url('/course_start_new')}}" class="btn btn-primary ">
                                           <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار</a>
                                   </div>
                                <div class="profile-usermenu">
                                    <a href="{{url('/evaluation')}}" class="btn btn-primary ">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>  حجز موعد تحديد مستوى </a>
                                </div>

                               <!-- <div class="profile-usermenu">
                                    <a href="{{url('/mycourses')}}" class="btn btn-primary ">
                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>  الدورات المشترك بها </a>
                                </div>-->
                            <!-- <div class="profile-usermenu">
                                       <a href="{{url('/profile')}}" class="btn btn-primary ">
                                          <i class="fa fa-user" aria-hidden="true"></i> تعديل الملف الشخصي </a>
                                    </div>

                                    <div class="profile-usermenu">
                                       <a href="{{url('/mycourses')}}" class="btn btn-primary">
                                         <i class="fa fa-user" aria-hidden="true"></i> دورات مشترك بها </a>
                                    </div>

                                    <div class="profile-userbuttons">
                                        <a href="{{url('/logout')}}" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> ﺗﺴﺠﻴﻞ اﻟﺨﺮﻭﺝ </a>
                                    </div>-->
                                <!-- END SIDEBAR BUTTONS -->
                            </div>

                       <div class="text-right border border-light  p-5">
                           @if(count($courses)>0)
                                       <table class="table table-borderd">
                                           <tr>
                                               <th class="orange-bg">
                                                   <p>اﺳﻢ اﻟﺪﻭﺭﺓ</p>
                                               </th>

                                               <th class="orange-bg">
                                                   <p>بداية الدورة</p>
                                               </th>
                                               <th class="orange-bg">
                                                   <p>نهاية الدورة</p>
                                               </th>
                                               <th class="orange-bg">
                                                   <p>الفترة</p>
                                               </th>
                                               <th class="orange-bg">
                                                   <p>المدرسة</p>
                                               </th>
                                               <th class="orange-bg">
                                                   <p>السعر</p>
                                               </th>
                                               <th class="orange-bg">
                                                   <p>الضريبة</p>
                                               </th>
                                               <th class="orange-bg">
                                                   <p>اﻟﺴﻌﺮ اﻻﺟﻤﺎﻟﻲ</p>
                                               </th>
                                               <th class="orange-bg">
                                                   <p>الاشتراك</p>
                                               </th>
                                           </tr>

                                               @foreach($courses as $item)
                                                   <?php $c=DB::table('orders')->where('CourseId',$item->id)->where('orders.IsPaid','1')->count(); ?>

                                                   @if($c < $item->seats)
                                                       <tr>

                                                           <td>
                                                               <p>{{$item->CourseName}}
                                                               <h6>{{$item->CourseRefNo}}</h6>
                                                               </p>
                                                           </td>
                                                           <!-- <td> -->
                                                       <!-- <p>{{$item->categoryName}}</p> -->
                                                           <!-- </td> -->
                                                           <td>
                                                               <p>{{$item->CourseStartIn}}</p>
                                                           </td>
                                                           <td>
                                                               <p>{{$item->CourseEndIn}}</p>
                                                           </td>
                                                           <td>
                                                               <p>{{$item->fromH}} الى {{$item->toH}}</p>
                                                           </td>
                                                           <td>
                                                               <p>{{$item->SchoolName}}</p>
                                                           </td>
                                                           <td>
                                                               <p>{{$item->price}} ريال</p>
                                                           </td>
                                                           <td>
                                                               <p>{{$item->tax}} ريال</p>
                                                           </td>
                                                           <td>
                                                               <p>{{ $item->price + $item->tax}} ريال</p>
                                                           </td>
                                                           <td>
                                                               <p><button class="btn  btn-large subscours" id="submitbutton{{$item->id}}" onclick="document.getElementById('submitbutton{{$item->id}}').disabled = true;document.getElementById('submitbutton{{$item->id}}').style.opacity='0.5';"  course-id="{{$item->id}}" style="font-weight: 500;">تسجيل</button></p>
                                                           </td>
                                                       </tr>
                                                   @endif
                                               @endforeach


                                       </table>

                           @else

                               <div class="alert alert-danger alert-dismissable">
                                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                   <i class="icon fa fa-ban"></i>
                                 لا توجد دورات متاحة الان
                               </div>
                           @endif
					   </div>
                       </div>
                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->
            </div>
            </div>
            <!--/.about-block  -->



        <!-- /.content -->
@stop