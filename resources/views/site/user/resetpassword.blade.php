@extends('site.master')
@section('content')


<div class="content container-fluid read-bg">
  <div class="container">
  <div class="row">
    <div class="page_title">استعادة كلمة المرور</div>
  </div>
</div>
</div>
<!-- End slider -->
<div class="content container-fluid">
  <div class="reada-block">
  <div class="container">
    <div class="row">
 <!-- Default form register -->
<div class="text-center border border-light col-md-8 p-5">
<div class="msg"></div>
<form class="" id="resetpassword">
 
    <!-- E-mail -->
    <input type="email" name="email" class="form-control mb-4" required placeholder="البريد الالكتروني">

    <!-- Sign up button -->
    <button class="btn btn-info my-4 btn-block resetpasswordbutton" type="submit">ارسال</button> 

</form>
<!-- Default form register -->
</div>
    </div><!-- /.serv -->
</div><!-- /.container -->

</div><!--/.about-block  -->

</div><!-- /.content -->
@stop