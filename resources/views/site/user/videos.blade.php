@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | الفيديوهات </title>
<meta name="Description" content="" />
@stop
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">
                <div class="row">
				<br />
                    <h2 class="page_title">الفيديوهات</h2>
				<br />	
                </div>
            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 side-profile">
                            <div class="profile-sidebar">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic text-center">
                                    @if(Auth::guard('doctor')->user()->img_national_id != '')
                                        <img src="{{url('/public/photos/thumb/'.Auth::guard('doctor')->user()->IDNumber.'/')}}/{{Auth::guard('doctor')->user()->img_national_id}}">
                                    @else
                                        <img src="{{url('/public/photos')}}/avatar.jpg">
                                    @endif
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle text-center">

                                    <div class="profile-usertitle-name">
                                        {{Auth::guard('doctor')->user()->IDNumber}}
                                        <br />
                                        <?php

                                        $sch=DB::table('schools')->where('Id',Auth::guard('doctor')->user()->school)->first();
                                        echo $sch->Name?>
                                    </div>
                                </div>
                                <!-- END SIDEBAR USER TITLE -->
                                <!-- SIDEBAR BUTTONS -->
                                <div class="aside user-menu-block">
                                   <!-- <div class="profile-usermenu">
                                        <a href="{{url('/course_start')}}" class="btn btn-primary ">
                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة </a>
                                    </div>
                                    <div class="profile-usermenu">
                                        <a href="{{url('/course_start_wait')}}" class="btn btn-primary ">
                                            <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار </a>
                                    </div>-->
                                       <div class="profile-usermenu">
                                           <a href="{{url('/course_start_new')}}" class="btn btn-primary ">
                                               <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار</a>
                                       </div>
                                    <div class="profile-usermenu">
                                        <a href="{{url('/evaluation')}}" class="btn btn-primary ">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>  حجز موعد تحديد مستوى </a>
                                    </div>
                                    <div class="profile-usermenu">
                                        <a href="{{url('/testing')}}" class="btn btn-primary ">
                                        <i class="fa fa-file-text" aria-hidden="true"></i> الاختبار النظرى </a>
                                    </div>
                                    <div class="profile-usermenu">
                                        <a href="{{url('/videos')}}" class="btn btn-primary ">
                                        <i class="fa fa-video-camera" aria-hidden="true"></i>  الفيديو </a>
                                    </div>
                                    <!--<div class="profile-usermenu">
                                        <a href="{{url('/mycourses')}}" class="btn btn-primary ">
                                            <i class="fa fa-pencil-square" aria-hidden="true"></i>  الدورات المشترك بها </a>
                                    </div>-->
                                       <!-- <div class="profile-usermenu">
                                           <a href="{{url('/profile')}}" class="btn btn-primary ">
                                              <i class="fa fa-user" aria-hidden="true"></i> تعديل الملف الشخصي </a>
                                        </div>
                                        
                                        <div class="profile-usermenu">
                                           <a href="{{url('/mycourses')}}" class="btn btn-primary">
                                             <i class="fa fa-user" aria-hidden="true"></i> دورات مشترك بها </a>
                                        </div>
                                    
                                        <div class="profile-userbuttons">
                                            <a href="{{url('/logout')}}" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> ﺗﺴﺠﻴﻞ اﻟﺨﺮﻭﺝ </a>
                                        </div>-->
                                        <!-- END SIDEBAR BUTTONS -->
                                    </div> 
                                
                            </div>
                        </div>
                        <!-- end col -->
                        <!-- Default form register -->
						<div class="col-md-10 left-col">
   
                       <div class="text-right border border-light  p-5">

                                     @if(count($orders)>0)
                                        @foreach($orders as $or)
                                            @if($or->link_video!='')
                                   @if(file_exists(public_path().'/videos/'.$or->link_video))
                                   <iframe width="400" height="215" src="{{url('/public/videos/'.$or->link_video)}}" frameborder="0" data-autoplay="false" allow=" encrypted-media" allowfullscreen></iframe>

                                       @endif
                                   @else
                                       <div class="alert alert-danger alert-dismissable">
                                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                           <i class="icon fa fa-ban"></i>
                                           لا توجد فيديوهات متاحة الان
                                       </div>
                                   @endif

                               @endforeach

                           @else
                               <div class="alert alert-danger alert-dismissable">
                                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                  <i class="icon fa fa-ban"></i>
                                  أنت غير مسجل فى دورات الان
                               </div>

                           @endif

					   </div>
                       </div>
                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->

            </div>
            <!--/.about-block  -->

        </div>

        <!-- /.content -->
@stop