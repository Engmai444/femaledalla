@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | الدورات المشترك بها </title>
<meta name="Description" content="" />
@stop
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">

                <center> <h2 class="page_title">الدورات المشترك بها</h2></center>
            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 left-col">
                            <div class="aside user-menu-block mt-5 ">
                               <!-- <div class="profile-usermenu">
                                    <a href="{{url('/course_start')}}" class="btn btn-primary ">
                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة </a>
                                </div>
                                <div class="profile-usermenu">
                                    <a href="{{url('/course_start_wait')}}" class="btn btn-primary ">
                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار</a>
                                </div>-->
                                   <div class="profile-usermenu">
                                       <a href="{{url('/course_start_new')}}" class="btn btn-primary ">
                                           <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار</a>
                                   </div>
                                <div class="profile-usermenu">
                                    <a href="{{url('/evaluation')}}" class="btn btn-primary ">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>  حجز موعد تحديد مستوى </a>
                                </div>
                                <div class="profile-usermenu">
                                    <a href="{{url('/testing')}}" class="btn btn-primary ">
                                        <i class="fa fa-file-text" aria-hidden="true"></i> الاختبار النظرى </a>
                                </div>
                                <div class="profile-usermenu">
                                    <a href="{{url('/videos')}}" class="btn btn-primary ">
                                        <i class="fa fa-video-camera" aria-hidden="true"></i>  الفيديو </a>
                                </div>
                                <!--<div class="profile-usermenu">
                                    <a href="{{url('/mycourses')}}" class="btn btn-primary ">
                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>  الدورات المشترك بها </a>
                                </div>-->
                            <!-- <div class="profile-usermenu">
                                       <a href="{{url('/profile')}}" class="btn btn-primary ">
                                          <i class="fa fa-user" aria-hidden="true"></i> تعديل الملف الشخصي </a>
                                    </div>

                                    <div class="profile-usermenu">
                                       <a href="{{url('/mycourses')}}" class="btn btn-primary">
                                         <i class="fa fa-user" aria-hidden="true"></i> دورات مشترك بها </a>
                                    </div>

                                    <div class="profile-userbuttons">
                                        <a href="{{url('/logout')}}" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> ﺗﺴﺠﻴﻞ اﻟﺨﺮﻭﺝ </a>
                                    </div>-->
                                <!-- END SIDEBAR BUTTONS -->
                            </div>

                            <div class="text-right border border-light  p-5">
					   
							<table class="table table-borderd">
								<tr>
								    <td>اسم الفرع</td>
									<td>اسم الدورة</td>
									<td>تاريخ بداية الدورة</td>
									<td>رقم تعريف الدورة</td>
									<td>التفاصيل</td>
								</tr>
								@foreach($courses as $course)
								<tr>
								    <td>{{$course->schoolName}}</td>
									<td>{{$course->CourseName}}</td>
									<td>{{date('d-m-Y',strtotime($course->CourseStartIn))}}</td>
									<td>{{$course->CourseRefNo}}</td>
									<td><a href="{{url('/mycourses/'.$course->OrderId)}}">التفاصيل</a></td>
									
								</tr>
								@endforeach
							</table>
							
					   </div>
                       </div>
                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->

            </div>
            <!--/.about-block  -->

        </div>
        <!-- /.content -->
@stop