@extends('site.master')
@section('content')

<!--slideer-->

<div class="content container-fluid read-bg">
  <div class="container">
  <div class="row">
    <div class="page_title">أشترك</div>
  </div>
</div>
</div>
<!-- End slider -->
<div class="content container-fluid">
  <div class="reada-block">
  <div class="container">
    <div class="row">
 <!-- Default form register -->

<form class="text-center border border-light col-md-8 p-5" id="signup">
 <div class="msg"></div>
    <div class="form-row mb-4">
        <div class="col">
            <!-- First name -->
            <input type="text" name="FullName" id="FullName" class="form-control" required placeholder="الاسم">
        </div>

    </div>

	<input type="text" class="form-control mb-4" name="IDNumber" required placeholder="رقم الهوية / الاقامة">
	<input type="text" class="form-control mb-4" name="DoB" required placeholder="تاريخ الميلاد">
	<select class="form-control mb-4 required" name="NationalityId" required>
		<option value="">أختر الجنسية</option>
	</select>
    <!-- E-mail -->
    <input type="email" name="email" class="form-control mb-4" required placeholder="البريد الالكتروني">

    <!-- Phone number -->
    <input type="text" name="mobile" class="form-control mb-4" required placeholder="رقم الهاتف" aria-describedby="">



	<input type="password" name="password" class="form-control mb-4" required placeholder="كلمة المرور">

    <!-- Sign up button -->
    <button class="btn btn-info my-4 btn-block" type="submit">أشترك</button>

</form>
<!-- Default form register -->

    </div><!-- /.serv -->
</div><!-- /.container -->

</div><!--/.about-block  -->

</div><!-- /.content -->
@stop