@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | مواعيد اختبار التقييم </title>
<meta name="Description" content="" />
@stop
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">
                <div class="row">
				<br />
                    <h2 class="page_title">مواعيد اختبار التقييم</h2>
				<br />	
                </div>
            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 side-profile">
                            <div class="profile-sidebar">
                                <div class="profile-userpic text-center">
                                    @if(Auth::guard('doctor')->user()->img_national_id != '')
                                        <img src="{{url('/public/photos/thumb/'.Auth::guard('doctor')->user()->IDNumber.'/')}}/{{Auth::guard('doctor')->user()->img_national_id}}">
                                    @else
                                        <img src="{{url('/public/photos')}}/avatar.jpg">
                                    @endif
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle text-center">

                                    <div class="profile-usertitle-name">
                                        {{Auth::guard('doctor')->user()->IDNumber}}
                                        <br />
                                        <?php

                                        $sch=DB::table('schools')->where('Id',Auth::guard('doctor')->user()->school)->first();
                                        echo $sch->Name?>
                                    </div>
                                </div>
                                <!-- END SIDEBAR USER TITLE -->
                                <!-- SIDEBAR BUTTONS -->

                                <div class="aside user-menu-block">
                                   <!-- <div class="profile-usermenu">
                                            <a href="{{url('/course_start')}}" class="btn btn-primary ">
                                            <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة </a>
                                        </div>
                                    <div class="profile-usermenu">
                                        <a href="{{url('/course_start_wait')}}" class="btn btn-primary ">
                                            <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار </a>
                                    </div>-->
                                       <div class="profile-usermenu">
                                           <a href="{{url('/course_start_new')}}" class="btn btn-primary ">
                                               <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار</a>
                                       </div>
                                        <div class="profile-usermenu">
                                            <a href="{{url('/evaluation')}}" class="btn btn-primary ">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>  حجز موعد تحديد مستوى </a>
                                        </div>
                                   <!-- <div class="profile-usermenu">
                                        <a href="{{url('/mycourses')}}" class="btn btn-primary ">
                                            <i class="fa fa-pencil-square" aria-hidden="true"></i>  الدورات المشترك بها </a>
                                    </div>-->
                                           <!-- <div class="profile-usermenu">
                                               <a href="{{url('/profile')}}" class="btn btn-primary ">
                                                            <i class="fa fa-user" aria-hidden="true"></i> تعديل الملف الشخصي </a>
                                            </div>
                                            
                                            <div class="profile-usermenu">
                                               <a href="{{url('/mycourses')}}" class="btn btn-primary">
                                                 <i class="fa fa-user" aria-hidden="true"></i> دورات مشترك بها </a>
                                            </div>
                                        
                                            <div class="profile-userbuttons">
                                                <a href="{{url('/logout')}}" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> ﺗﺴﺠﻴﻞ اﻟﺨﺮﻭﺝ </a>
                                            </div>-->
                                            <!-- END SIDEBAR BUTTONS -->
                                        </div>   
                            </div>
                        </div>
                        <!-- end col -->
                        <!-- Default form register -->
						<div class="col-md-6 left-col">
 
                       <div class="text-right border border-light  p-5">
                           @if(Session::get('success') != '')
                               <div class="alert alert-success">
                                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                   <i class="fa fa-info-circle"></i>
                                   {{Session::get('success')}}
                                   {{Session::forget('success')}}
                               </div>
                           @endif
                               @if(Session::get('error') != '')
                                   <div class="alert alert-danger alert-dismissable">
                                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                       <i class="fa fa-info-circle"></i>
                                       {{Session::get('error')}}
                                       {{Session::forget('error')}}
                                   </div>
                               @endif

                               @if($setting->value=='0')
                                   @if($setting_id->value=='0')
                                       @if($idno[0]!='2')
                               <form method="post" action="{{url('/reserve')}}">
                                   <div class="form-row">
                                       <div class="form-group col-md-12">
                                   <p style="color: red">ملحوظة الايام التى بها ظل باللون الازرق لا يوجد بها أماكن متاحة للحجز</p>
                                       </div>
                                   </div>
                                   <div class="form-row">
                               <div class="form-group col-md-5">
                               <label>اليوم</label>
                               <input type="text" class="form-control" readonly name="day" autocomplete="off"  id="day"  onchange="return get_time(this.value)" required>
    </div>
    <div class="form-group col-md-5">
    <label>المواعيد</label>
                           <select class="form-control" id="time" name="time" required>

                           </select>
                                   {{ csrf_field() }}
    </div>
    <div class="form-group col-md-2 d-flex align-items-center btn-col">
    <input type="submit" value="احجز" class="btn btn-primary ">
    </div>
  </div>



                                   <div class="form-row">
                                       <div class="form-group col-md-5">
                                       </div>
                                       <div class="form-group col-md-5">
                                           <span id="msg" style="display: none;color: red;">لا توجد مواعيد متاحة</span>

                                       </div>
                                       <div class="form-group col-md-2 d-flex align-items-center btn-col">
                                       </div>
                                   </div>
      
                                   <br />
                               


                               </form>
                                   @else
                                       <div class="alert alert-danger alert-dismissable">
                                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                           <i class="fa fa-info-circle"></i>
                                           هذه الخدمة غير مفعله الان
                                       </div>
                                       @endif
                                   @else
                                       <form method="post" action="{{url('/reserve')}}">
                                           <div class="form-row">
                                               <div class="form-group col-md-12">
                                                   <p style="color: red">ملحوظة الايام التى بها ظل باللون الازرق لا يوجد بها أماكن متاحة للحجز</p>
                                               </div>
                                           </div>                                           <div class="form-row">
                                               <div class="form-group col-md-5">
                                                   <label>اليوم</label>
                                                   <input type="text" class="form-control" readonly name="day" autocomplete="off"  id="day"  onchange="return get_time(this.value)" required>
                                               </div>
                                               <div class="form-group col-md-5">
                                                   <label>المواعيد</label>
                                                   <select class="form-control" id="time" name="time" required>

                                                   </select>
                                                   {{ csrf_field() }}
                                               </div>
                                               <div class="form-group col-md-2 d-flex align-items-center btn-col">
                                                   <input type="submit" value="احجز" class="btn btn-primary ">
                                               </div>
                                           </div>



                                           <div class="form-row">
                                               <div class="form-group col-md-5">
                                               </div>
                                               <div class="form-group col-md-5">
                                                   <span id="msg" style="display: none;color: red;">لا توجد مواعيد متاحة</span>

                                               </div>
                                               <div class="form-group col-md-2 d-flex align-items-center btn-col">
                                               </div>
                                           </div>

                                           <br />



                                       </form>

                                   @endif
                               @else
                                   <div class="alert alert-danger alert-dismissable">
                                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                       <i class="fa fa-info-circle"></i>
                                      هذه الخدمة غير مفعله الان
                                   </div>

                                   @endif
					   </div>
                       </div>
                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->

            </div>
            <!--/.about-block  -->

        </div>
        <!-- /.content -->
        <style>
            .highlight-green {
                /* Cell */
                background-color: lightblue;
            }

            .datepicker table tr td.disabled, .datepicker table tr td.disabled:hover{
                background-color: lightblue;

            }
            .highlight-green a.ui-state-default {
                /* Link */
            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script>
        function get_time(id) {
            $('#table4 tbody').empty();
            $.ajax({
                url: '{{url('get_time')}}',
                type: 'GET',
                dataType: 'json',
                success: 'success',
                data: {'id': id},
                success: function (result) {
                    if(!$.trim(result.result)) {
                       // alert('لا توجد أماكن حجز متاحة الان فى هذا اليوم وهذا الميعاد');
                        $('#msg').fadeIn();
                        $('#time').empty();

                    }else{
                        $('#msg').fadeOut();
                        $('#time').empty();

                        $.each(result.result, function (i, index) {

                            $('#time').append('<option value="' + index.id + '">' + index.fromH + ' إلى ' + index.toH + '</option>');
                        });
                    }
                }
            })
        }
        var disabledDays = '<?= $days?>';



        function noWeekendsOrHolidays(date) {
            dmy =date.getFullYear()  + "-" + (date.getMonth() + 1) + "-" + date.getDate();
            var d=new Date();
            var now=d.getFullYear()  + "-" + (d.getMonth() + 1) + "-" + d.getDate();

            if(disabledDays.indexOf(dmy) != -1 ){

                    return false;
            }else if( Date.parse(dmy) <= Date.parse(now)) {
                return false;
            }
            else{
                return true;
            }
        }
        function enableAllTheseDays(date) {
            dmy =date.getFullYear()  + "-" + (date.getMonth() + 1) + "-" + date.getDate();

            if(disabledDays.indexOf(dmy) != -1 ){
               var  res= (false,"highlight-green");
               return  res;
            }
            else{
                return true;
            }
        }

        $(document).ready(function() {

            $('#day').datepicker({
                startDate: new Date(2021,8,19),
               // startDate: dmy,
               // endDate:new Date(2022,11,31),


                format: 'yyyy-mm-dd',
                daysOfWeekDisabled: [5,6],
                autoclose: true,
                beforeShowDay: noWeekendsOrHolidays

            });

        });
    </script>
        <style>
            .datepicker table tr td, .datepicker table tr th {
                font-weight: bold;
            }
            .datepicker table tr td.new, .datepicker table tr td.old {
                height: 0;
                line-height: 0;
                visibility: hidden;
            }
        </style>
@stop