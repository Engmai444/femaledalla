@extends('site.master')
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">
                <div class="row">
				<br />
                    <h2 class="page_title">ملف الشخصي</h2>
				<br />	
                </div>
            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 side-profile">
                            <div class="profile-sidebar">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic text-center">

                                @if(Auth::guard('doctor')->user()->img_national_id != '')
                                        @if(file_exists(public_path().'/photos/thumb/'.md5(Auth::guard('doctor')->user()->IDNumber).'/'.Auth::guard('doctor')->user()->img_national_id ))
									<img src="{{url('/public/photos/thumb/'.md5(Auth::guard('doctor')->user()->IDNumber).'/')}}/{{Auth::guard('doctor')->user()->img_national_id}}">
									   @elseif(file_exists(public_path().'/photos/thumb/'.Auth::guard('doctor')->user()->IDNumber.'/'.Auth::guard('doctor')->user()->img_national_id))
									<img src="{{url('/public/photos/thumb/'.Auth::guard('doctor')->user()->IDNumber.'/')}}/{{Auth::guard('doctor')->user()->img_national_id}}">
                                        @elseif(file_exists(public_path().'/photos/thumb/'.md5(Auth::guard('doctor')->user()->StudentId).'/'.Auth::guard('doctor')->user()->img_national_id))
                                            <img src="{{url('/public/photos/thumb/'.md5(Auth::guard('doctor')->user()->StudentId).'/')}}/{{Auth::guard('doctor')->user()->img_national_id}}">

                                        @endif
									@else
                                    <img src="{{url('/public/photos')}}/avatar.jpg">
									@endif
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle text-center">

                                    <div class="profile-usertitle-name">
									{{Auth::guard('doctor')->user()->IDNumber}}
<br />
                                        <?php

                                        $sch=DB::table('schools')->where('Id',Auth::guard('doctor')->user()->school)->first();
                                              echo $sch->Name?>
                                    </div>
                                </div>
                                <!-- END SIDEBAR USER TITLE -->
                                <!-- SIDEBAR BUTTONS -->
                                <div class="aside user-menu-block">
                                   <!-- <div class="profile-usermenu">
                                        <a href="{{url('/course_start')}}" class="btn btn-primary ">
                                            <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة </a>
                                    </div>
                                    <div class="profile-usermenu">
                                        <a href="{{url('/course_start_wait')}}" class="btn btn-primary ">
                                            <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار </a>
                                    </div>-->
                                       <div class="profile-usermenu">
                                           <a href="{{url('/course_start_new')}}" class="btn btn-primary ">
                                               <i class="fa fa-pencil-square" aria-hidden="true"></i>  اشتراك دورة 30 ساعة قائمة الانتظار</a>
                                       </div>
                                    <div class="profile-usermenu">
                                        <a href="{{url('/evaluation')}}" class="btn btn-primary ">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>  حجز موعد تحديد مستوى </a>
                                    </div>
                                   <!-- <div class="profile-usermenu">
                                        <a href="{{url('/mycourses')}}" class="btn btn-primary ">
                                            <i class="fa fa-pencil-square" aria-hidden="true"></i>  الدورات المشترك بها </a>
                                    </div>-->
                                <!-- <div class="profile-usermenu">
                                               <a href="{{url('/profile')}}" class="btn btn-primary ">
                                                            <i class="fa fa-user" aria-hidden="true"></i> تعديل الملف الشخصي </a>
                                            </div>

                                            <div class="profile-usermenu">
                                               <a href="{{url('/mycourses')}}" class="btn btn-primary">
                                                 <i class="fa fa-user" aria-hidden="true"></i> دورات مشترك بها </a>
                                            </div>

                                            <div class="profile-userbuttons">
                                                <a href="{{url('/logout')}}" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> ﺗﺴﺠﻴﻞ اﻟﺨﺮﻭﺝ </a>
                                            </div>-->
                                    <!-- END SIDEBAR BUTTONS -->
                                </div>

                            </div>
                        </div>
                        <!-- end col -->
                        <!-- Default form register -->
                        <div class="col-md-10 left-col">

					<!-- SIDEBAR MENU -->

                                <!-- END MENU -->	
                       
{{ Form::open(array('url' => '/profile','role' => 'form','id' => 'sendform','class' => 'text-right form-two-col border border-light  p-5','files'=> true)) }}
                            @if(!empty($orders))
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                                   عفوا لا يمكنك التعديل على بياناتك لانك مشترك فى دورات من قبل
                                </div>


                                @endif
                            
                        @if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif
                   
					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif   
                            <div class="form-row mb-4">
        <div class="col">
            <label>الاسم كامل</label>
            <!-- First name -->
            <input type="text" name="FullName" id="FullName" class="form-control" required placeholder="الاسم كامل" value="{{Auth::guard('doctor')->user()->FullName}}" autocomplete="off" onkeypress="return CheckArabicCharactersOnly(event);"  onkeypress="return CheckArabicCharactersOnly(event);">
        </div>
      
    </div>
	<div class="form-row mb-4">
        
        <div class="col">
            <label>رقم الهوية</label>
            <!-- Last name -->
            <input type="text" name="IDNumber" class="form-control" maxlength="10"  onkeypress="return numbersonly(event);"  placeholder="رقم الهوية" value="{{Auth::guard('doctor')->user()->IDNumber}}">
        </div>
    </div>
	
	<div class="form-row mb-4">
		<div class="col">
            <label>الجنسية</label>
		{{Form::select('NationalityId',$Nationality,Auth::guard('doctor')->user()->NationalityId,['class'=>'form-control','placeholder' => 'الجنسية'])}}
		</div>
	</div>

                            <div class="form-row mb-4">
                                <div class="col">
                                    <label>المدرسة</label>
                                    <select name="school" class="form-control"  disabled >

                                        @foreach($schools as $sc)
                                            <option value="{{$sc->Id}}" @if($sc->Id==Auth::guard('doctor')->user()->school) {{'selected'}} @endif>{{$sc->Name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
	 <!-- photos -->
     <div class="form-row mb-4">
		<div class="col">
	<label>الصورة الشخصية</label>
	<input type="file" id="photos" name="file" class="form-control"  onkeypress="return numbersonly(event);" placeholder="" aria-describedby="photos">
</div>
</div>
    <!-- E-mail -->
    <div class="form-row mb-4">
		<div class="col">
    <label>البريد الالكتروني</label>
    <input type="email" name="email"  value="{{Auth::guard('doctor')->user()->email}}" id="email" class="form-control mb-4" required placeholder="البريد الالكتروني" >
</div>
</div>
    <!-- Phone number -->
    <div class="form-row mb-4">
		<div class="col">
    <label>رقم الهاتف</label>
    <input type="text" name="mobile"  value="{{Auth::guard('doctor')->user()->mobile}}" class="form-control mb-4" required placeholder="رقم الهاتف" aria-describedby="">
	
</div>
</div>
<div class="pass-group">
<h3>تغيير كلمة المرور</h3>
<div class="form-row mb-4 pass">
    <div class="col">
	<input type="password" name="oldpassword" value="" class="form-control mb-4 pass" required placeholder="كلمة المرور القديمة" aria-describedby="">
</div>
</div>
<div class="form-row mb-4 pass">
    <div class="col">
    <input type="password" name="newpassword" value="" class="form-control mb-4 pass" required placeholder="كلمة المرور الجديدة" aria-describedby="">
</div>
</div>
<div class="form-row mb-4 pass">
    <div class="col">
	<input type="password" name="renewpassword" value="" class="form-control mb-4 pass" required placeholder="تأكيد كلمة المرور الجديدة" aria-describedby="">
</div>
</div>    
</div> 
<div class="form-row mb-4 btn-col-full">
    <div class="col">
              <!-- Sign up button -->
              <button class="btn btn-info my-4 btn-block" type="submit" @if(!empty($orders)) disabled @endif>تعديل</button>
    </div>
</div> 
                           

                            <!-- end col-md-10  -->
              

                       {{ Form::close() }} 
                        <!-- Default form register -->
                        </div>
                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->

            </div>
            <!--/.about-block  -->

        </div>
        <!-- /.content -->
		    <script type="text/javascript">
			
     function CheckArabicCharactersOnly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8) {
                if ( unicode >= 65 && unicode <= 90 || unicode >= 97 && unicode <= 122)
                    return false;
                else {
                    if ((unicode < 48 || unicode > 57) && (unicode < 0x0600 || unicode > 0x06FF)) //if not a number or arabicalert(unicode);


                        return true;
                }
            }


        }
		
		</script>
@stop