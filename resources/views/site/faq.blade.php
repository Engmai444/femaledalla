
@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | الأسئلة الشائعة</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
   <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-3.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">اﻻﺳﺌﻠﻪ اﻟﺸﺎﺋﻌﺔ</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container mt-5">

            <div class="row">
                <div class="col-12">
             <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		@foreach($items as $item)
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{{$item->id}}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collaps{{$item->id}}" aria-expanded="true" aria-controls="collapseOne">
                       <i class="more-less fa fa-plus" aria-hidden="true"></i>
                       {{$item->question_ar}}
                    </a>
                </h4>
            </div>
            <div id="collaps{{$item->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$item->id}}">
                <div class="panel-body">
					{!! $item->answer_ar !!}
                </div>
            </div>
        </div>
		@endforeach




    </div><!-- panel-group -->
               </div>
              <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->
@stop