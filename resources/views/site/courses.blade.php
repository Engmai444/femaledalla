@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | الدورات</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('header')


@stop
@section('footer')

@stop
@section('content')
   <div class="clearfix"></div>
    <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-3.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">الدورات</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 text-center pb-3 pt-5">
					<form method="get">
							<div class="row">
								<div class="col-md-3">
									{{Form::select('applicationdef',$applicationdefs,$applicationdef,['class'=>'form-control','placeholder'=> 'نوع الدورة'])}}
								</div>
								<div class="col-md-3">
									{{Form::select('SchoolId',$schools,$SchoolId,['class'=>'form-control','placeholder'=> 'المكان'])}}
								</div>
								<div class="col-md-1">
									بداية الدورة
								</div>
								<div class="col-md-2">
									<input type="text" name="date_from" value="{{$date_from}}" class="form-control datepicker" placeholder="من" />
								</div>
								<div class="col-md-2">
									<input type="text"  name="date_to"  value="{{$date_to}}" class="form-control datepicker" placeholder="الي" />
								</div>
								<input type="hidden" name="filter" value="1" />
								<div class="col-md-1">
									<input type="submit" value="بحث" class="btn btn-primary" />
								</div>
							</div>
						</form>
						<br/>
						
					<div class="row">
						<div class="col-md-12 text-center">
						    

						            <h2 style="color:#d95621;padding:10px;">شرح طريقة التسجيل في الدورات</h2>
						    </div>
        						<div style="width:1200px;min-height:500px;clear:both; margin-bottom:25px;">
        						    
        						    <iframe width="100%" height="600" src="https://www.youtube.com/embed/cKhhyLAwCFQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        
        					
						</div>
						</div>
						<br/>
						<br/>
						<form method="get" action="ajax/subsCours"> 
                    <div class="table-responsive">
						@if($filter == 1)
                         <div class="alert alert-danger">عدد ساعات الدورة قابل للتغيير بناءا علي نتيجة اختبار تحديد المستوي </div>
                        <table class="table table-st1">
                            <tr>
                                <th class="orange-bg">
                                    <p>اﺳﻢ اﻟﺪﻭﺭﺓ</p>
                                </th>
                                
                                <th class="orange-bg">
                                    <p>بداية الدورة</p>
                                </th>
								<th class="orange-bg">
                                    <p>نهاية الدورة</p>
                                </th>
                                <th class="orange-bg">
                                    <p>الفترة</p>
                                </th>
                                <th class="orange-bg">
                                    <p>المدرسة</p>
                                </th>
                                <th class="grey-bg">
                                    <p>السعر</p>
                                </th>
								<th class="grey-bg">
                                    <p>الضريبة</p>
                                </th>
                                <th class="orange-bg">
                                    <p>اﻟﺴﻌﺮ اﻻﺟﻤﺎﻟﻲ</p>
                                </th>
								<th class="orange-bg">
                                    <p>الاشتراك</p>
                                </th>
                            </tr>
							@if(count($courses))
							@foreach($courses as $item)
                                <?php $c=DB::table('orders')->where('CourseId',$item->id)->where('orders.IsPaid','1')->count(); ?>

                                @if($c < $item->seats)
                            <tr>

                                <td>
                                    <p>{{$item->CourseName}}
									<h6>{{$item->CourseRefNo}}</h6>
									</p>
                                </td>
                                <!-- <td> -->
                                    <!-- <p>{{$item->categoryName}}</p> -->
                                <!-- </td> -->
								<td>
                                    <p>{{$item->CourseStartIn}}</p>
                                </td>
                                <td>
                                    <p>{{$item->CourseEndIn}}</p>
                                </td>
                                <td>
                                    <p>{{$item->fromH}} الى {{$item->toH}}</p>
                                </td>
                                <td>
                                    <p>{{$item->SchoolName}}</p>
                                </td>
                                <td>
                                    <p>{{$item->price}} ريال</p>
                                </td>
								<td>
                                    <p>{{$item->tax}} ريال</p>
                                </td>
                                <td>
                                    <p>{{ $item->price + $item->tax}} ريال</p>
                                </td>
								<td>
                                    <p><button class="btn btn-orange btn-large subscours" id="submitbutton{{$item->id}}" onclick="document.getElementById('submitbutton{{$item->id}}').disabled = true;document.getElementById('submitbutton{{$item->id}}').style.opacity='0.5';"  course-id="{{$item->id}}" style="font-weight: 500;">تسجيل</button></p>
                                </td>
                            </tr>
                                    @endif
                           @endforeach
                            @else
							<tr >
						<td colspan='9' class="text-center">لا توجد نتائج تطابق بحثك</td>
							
							</tr>
							@endif

                        </table>
						@endif
                    </div>
                    </form>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
               <div class="row">
                 <div class="col-12 text-center">
            
                </div>
               <!-- end col -->
               </div>
               <!-- end row -->

        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->

@stop