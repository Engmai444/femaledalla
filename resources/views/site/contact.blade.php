
@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | اتصل بنا</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
    <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-3.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">اتصل بنا</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">

            <div class="row m-top-25">
               
                <div class="col-md-6 offset-md-3">
                    <h3><strong>تواصل معنا</strong></h3>
                    <form id="contact">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="" placeholder="الاسم">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" value="" placeholder="البريد الالكترونى">
                        </div>
                        <div class="form-group">
                            <input type="tel" class="form-control" name="phone" value="" placeholder="التليفون">
                        </div>
                        
                        <div class="form-group">
                            <textarea class="form-control" name="msg" rows="8" placeholder="الرسالة"></textarea>
                        </div>
                        <div class="g-recaptcha" data-sitekey="6LeamI4UAAAAAOqqe4c0i-78P1cheg7ehT6MxLm3"></div>
                        <button class="btn btn-default" type="submit" name="button"> <i class="fa fa-paper-plane-o" aria-hidden="true"></i> ارسال </button>
                    </form>
                </div>
            </div>
            <!-- end row -->

        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->

    
@stop