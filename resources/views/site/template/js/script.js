(function($) { 
  
  "use strict";

    jQuery(window).load(function() {
        
        $(".loader-imsg").delay(500).fadeOut();
        $("#page-loader").delay(1000).fadeOut("slow");


setTimeout("$('.logo').addClass('animated bounceInRight ').removeClass('hidden1')",1100);


        });

    $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
    // ===== Scroll to Top ==== 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});


       $('.venobox').venobox(); 


})(jQuery);






            var tpj=jQuery;
      
      var revapi471;
      tpj(document).ready(function() {
        if(tpj("#rev_slider_471_1").revolution == undefined){
          revslider_showDoubleJqueryError("#rev_slider_471_1");
        }else{
          revapi471 = tpj("#rev_slider_471_1").show().revolution({
            sliderType:"carousel",
jsFileLocation:"revolution/js/",
            sliderLayout:"fullwidth",
            dottedOverlay:"none",
            delay:6000,
            navigation: {
              keyboardNavigation:"off",
              keyboard_direction: "horizontal",
              mouseScrollNavigation:"off",
              mouseScrollReverse:"default",
              onHoverStop:"off",
              touch:{
                touchenabled:"on",
                swipe_threshold: 75,
                swipe_min_touches: 1,
                swipe_direction: "horizontal",
                drag_block_vertical: false
              }
              ,
              arrows: {
                style:"gyges",
                enable:true,
                hide_onmobile:false,
                hide_onleave:false,
                tmp:'',
                left: {
                  h_align:"left",
                  v_align:"center",
                  h_offset:20,
                  v_offset:0
                },
                right: {
                  h_align:"right",
                  v_align:"center",
                  h_offset:20,
                  v_offset:0
                }
              }
              ,
              tabs: {
                style:"gyges",
                enable:true,
                width:180,
                height:80,
                min_width:250,
                wrapper_padding:30,
                wrapper_color:"#26292b",
                wrapper_opacity:"1",
                tmp:'<div class="tp-tab-content">  <span class="tp-tab-date">{{param1}}</span>  <span class="tp-tab-title">{{title}}</span></div><div class="tp-tab-image"></div>',
                visibleAmount: 3,
                hide_onmobile: false,
                hide_onleave:false,
                hide_delay:200,
                direction:"horizontal",
                span:true,
                position:"outer-bottom",
                space:0,
                h_align:"left",
                v_align:"bottom",
                h_offset:0,
                v_offset:0
              }
            },
            carousel: {
              horizontal_align: "center",
              vertical_align: "center",
              fadeout: "on",
              vary_fade: "on",
              maxVisibleItems:1,
              infinity: "on",
              space: 0,
              stretch: "on"
            },
            visibilityLevels:[1240,1024,778,480],
            gridwidth:720,
            gridheight:405,
            lazyType:"none",
            shadow:0,
            spinner:"off",
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",
            autoHeight:"off",
            disableProgressBar:"on",
            hideThumbsOnMobile:"off",
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            debugMode:false,
            fallbacks: {
              simplifyAll:"off",
              nextSlideOnWindowFocus:"off",
              disableFocusListener:false,
            }
          });
        }


                if(tpj("#rev_slider_471_2").revolution == undefined){
          revslider_showDoubleJqueryError("#rev_slider_471_2");
        }else{
          revapi471 = tpj("#rev_slider_471_2").show().revolution({
            sliderType:"carousel",
jsFileLocation:"revolution/js/",
            sliderLayout:"fullwidth",
            dottedOverlay:"none",
            delay:9000,
            navigation: {
              keyboardNavigation:"off",
              keyboard_direction: "horizontal",
              mouseScrollNavigation:"off",
              mouseScrollReverse:"default",
              onHoverStop:"off",
              touch:{
                touchenabled:"on",
                swipe_threshold: 75,
                swipe_min_touches: 1,
                swipe_direction: "horizontal",
                drag_block_vertical: false
              }
              ,
              arrows: {
                style:"gyges",
                enable:true,
                hide_onmobile:false,
                hide_onleave:false,
                tmp:'',
                left: {
                  h_align:"left",
                  v_align:"center",
                  h_offset:20,
                  v_offset:0
                },
                right: {
                  h_align:"right",
                  v_align:"center",
                  h_offset:20,
                  v_offset:0
                }
              }
              ,
              tabs: {
                style:"gyges",
                enable:true,
                width:178,
                height:113,
                min_width:250,
                wrapper_padding:30,
                wrapper_color:"#26292b",
                wrapper_opacity:"1",
     
                visibleAmount: 5,
                hide_onmobile: true,
                hide_onleave:false,
                hide_delay:200,
                direction:"horizontal",
                span:true,
                position:"outer-bottom",
                space:0,
                h_align:"center",
                v_align:"bottom",
                h_offset:0,
                v_offset:0
              }
            },
            carousel: {
              horizontal_align: "center",
              vertical_align: "center",
              fadeout: "on",
              vary_fade: "on",
              maxVisibleItems: 0,
              infinity: "on",
              space: 0,
              stretch: "on"
            },
            visibilityLevels:[1240,1024,778,480],
            gridwidth:720,
            gridheight:405,
            lazyType:"none",
            shadow:0,
            spinner:"off",
            stopLoop:"on",
            stopAfterLoops:0,
            stopAtSlide:0,
            shuffle:"off",
            autoHeight:"off",
            disableProgressBar:"on",
            hideThumbsOnMobile:"off",
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            debugMode:false,
            fallbacks: {
              simplifyAll:"off",
              nextSlideOnWindowFocus:"off",
              disableFocusListener:false,
            }
          });
        }


      }); /*ready*/



        var tpj=jQuery;         
          var revapi74;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_74_1").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_74_1");
            }else{
              revapi74 = tpj("#rev_slider_74_1").show().revolution({
                sliderType:"standard",
                jsFileLocation:"../../revolution/js/",
                sliderLayout:"auto",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                  keyboardNavigation:"off",
                  keyboard_direction: "horizontal",
                  mouseScrollNavigation:"off",
                  onHoverStop:"off",
                  arrows: {
                    style:"uranus",
                    enable:true,
                    hide_onmobile:true,
                    hide_under:778,
                    hide_onleave:true,
                    hide_delay:200,
                    hide_delay_mobile:1200,
                    tmp:'',
                    left: {
                      h_align:"left",
                      v_align:"center",
                      h_offset:20,
                      v_offset:0
                    },
                    right: {
                      h_align:"right",
                      v_align:"center",
                      h_offset:20,
                      v_offset:0
                    }
                  }
                  ,
                  thumbnails: {
                    style:"erinyen",
                    enable:true,
                    width:200,
                    height:113,
                    min_width:170,
                    wrapper_padding:30,
                    wrapper_color:"#333333",
                    wrapper_opacity:"1",
                    tmp:'<span class="tp-thumb-over"></span><span class="tp-thumb-image"></span><span class="tp-thumb-title">{{title}}</span><span class="tp-thumb-more"></span>',
                    visibleAmount:10,
                    hide_onmobile:false,
                    hide_onleave:false,
                    direction:"horizontal",
                    span:true,
                    position:"outer-bottom",
                    space:20,
                    h_align:"center",
                    v_align:"bottom",
                    h_offset:0,
                    v_offset:0
                  }
                },
                gridwidth:1230,
                gridheight:392,
                lazyType:"none",
                shadow:0,
                spinner:"spinner2",
                stopLoop:"on",
                stopAfterLoops:0,
                stopAtSlide:1,
                shuffle:"off",
                autoHeight:"off",
                disableProgressBar:"on",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                  simplifyAll:"off",
                  nextSlideOnWindowFocus:"off",
                  disableFocusListener:false,
                }
              });
            }
          }); /*ready*/





