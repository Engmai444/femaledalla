
@extends('site.master'
@section('seo')
<title>شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ | خدماتنا</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
   <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-3.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">ﺧﺪﻣﺎﺕ اﻟﻤﺪﺭﺳﺔ</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container mt-5">
               <div class="row">
                   <div class="col-12 tab-pills">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">اﺟﺮاءاﺕ اﺻﺪاﺭ ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#two" role="tab" aria-controls="pills-profile" aria-selected="false">ﺭﺧﺺ اﻟﻘﻴﺎﺩﺓ ﻭاﻧﻮاﻋﻬﺎ</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#three" role="tab" aria-controls="pills-profile" aria-selected="false">ﺗﻌﻠﻴﻤﺎﺕ اﺩاﺭﺓ اﻟﻤﺮﻭﺭ ﻟﺤﻀﻮﺭ اﻟﻄﻼﺏ</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#four" role="tab" aria-controls="pills-profile" aria-selected="false">التدريب</a>
  </li>


</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
   <div class="row">
       <div class="col-12 col-md-12">
                <div class="sr-box">
                        <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>اﺟﺮاءاﺕ اﺻﺪاﺭ ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ ﺧﺎﺻﺔ</h2>
                        <div class="content">
                          <ol>
                            <li>اﻥ ﻳﻜﻮﻥ اﻟﻤﺘﻘﺪﻡ اﺗﻢ ﻣﻦ اﻟﻌﻤﺮ 18 ﻋﺎﻣﺎ</li>
                            <li>اﻟﻜﺸﻒ اﻟﻄﺒﻰ ( ﻛﺸﻒ ﻧﻈﺮ ﻭﻓﺼﻴﻠﺔ اﻟﺪﻡ ﻣﻊ ﻣﺴﺘﻮﺻﻒ اﻭ  ﻣﺴﺘﺸﻔﻰ ﻣﺮﺗﺒﻄﻪ ﺑﻨﻈﺎﻡ اﻟﻤﺮﻭﺭ)</li>
                          </ol>
                          <p dir="RTL"><strong class="orange">اﻟﻤﻮاﻃﻨﻮﻥ :</strong> ﺻﻮﺭﺓ ﻣﻦ ﺑﻄﺎﻗﺔ اﻻﺣﻮاﻝ ﻣﻊ اﻻﺻﻞ <br>
                           <strong class="orange"> اﻟﻤﻘﻴﻤﻮﻥ :</strong>  ﺻﻮﺭﺓ ﻣﻦ اﻻﻗﺎﻣﺔ ﻣﻊ اﻻﺻﻞ </p>
                          <ol>
                            <li>عدد 4 صور شخصية مقاس 4*6</li>
                            <li>اﺳﺘﻜﻤﺎﻝ ﺑﻴﺎﻧﺎﺕ اﻟﻨﻤﻮﺫﺝ ( ﻣﻦ ﻣﻮﻗﻊ ﺩﻟﻪ – ﻣﻮﻗﻊ  ﻭﺯاﺭﺓ اﻟﺪاﺧﻠﻴﺔ )</li>
                            <li>ﻣﻠﻒ ﻟﺤﻔﻆ اﻻﻭﺭاﻕ </li>
                            <li>ﺩﻓﻊ اﻟﺮﺳﻮﻡ اﻟﻤﻘﺮﺭﺓ + ﺿﺮﻳﺒﻪ اﻟﻘﻴﻤﻪ اﻟﻤﻀﺎﻓﺔ</li>
                          </ol>
                        </div>
                    </div>
      </div>
     <!-- end col -->
       <div class="col-12 col-md-12">
                <div class="sr-box">
                        <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>اﺟﺮاءاﺕ اﺻﺪاﺭ ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ ﻣﺆﻗﺘﺔ</h2>
                        <div class="content">
                          <ol>
                            <li>اﻥ ﻳﻜﻮﻥ اﻟﻤﺘﻘﺪﻡ اﺗﻢ ﻣﻦ اﻟﻌﻤﺮ 17 ﻋﺎﻣﺎ</li>
                            <li>اﻟﻜﺸﻒ اﻟﻄﺒﻰ ( ﻛﺸﻒ ﻧﻈﺮ ﻭﻓﺼﻴﻠﺔ اﻟﺪﻡ ﻣﻊ ﻣﺴﺘﻮﺻﻒ اﻭ  ﻣﺴﺘﺸﻔﻰ ﻣﺮﺗﺒﻄﻪ ﺑﻨﻈﺎﻡ اﻟﻤﺮﻭﺭ)</li>
                          </ol>
                          <p dir="RTL"><strong class="orange">اﻟﻤﻮاﻃﻨﻮﻥ :</strong> ﺻﻮﺭﺓ ﻣﻦ ﺑﻄﺎﻗﺔ اﻻﺣﻮاﻝ ﻣﻊ اﻻﺻﻞ <br>
                            <strong class="orange"> ﻣﻮاﻃﻨﻮﻥ ﺩﻭﻝ ﻣﺠﻠﺲ اﻟﺘﻌﺎﻭﻥ اﻟﺨﻠﻴﺠﻰ :</strong>  ﺻﻮﺭﺓ ﻣﻦ اﻟﺠﻮاﺯ ﻭﻳﻜﻮﻥ ﻣﻀﻰ ﻋﻠﻰ اﻟﺪﺧﻮﻝ 3 اﺷﻬﺮ ﻣﺘﻮاﺻﻠﻪ ﺩﻭﻥ ﺳﻔﺮ <br>
                           <strong class="orange"> اﻟﻤﻘﻴﻤﻮﻥ :</strong>  ﺻﻮﺭﺓ ﻣﻦ اﻻﻗﺎﻣﺔ ﻣﻊ اﻻﺻﻞ 

                          </p>

                          <ol>
                            <li>عدد 4 صور شخصية مقاس 4*6</li>
                            <li>اﺳﺘﻜﻤﺎﻝ ﺑﻴﺎﻧﺎﺕ اﻟﻨﻤﻮﺫﺝ ( ﻣﻦ ﻣﻮﻗﻊ ﺩﻟﻪ – ﻣﻮﻗﻊ  ﻭﺯاﺭﺓ اﻟﺪاﺧﻠﻴﺔ )</li>
                            <li>ﻣﻠﻒ ﻟﺤﻔﻆ اﻻﻭﺭاﻕ </li>
                            <li>ﺩﻓﻊ اﻟﺮﺳﻮﻡ اﻟﻤﻘﺮﺭﺓ + ﺿﺮﻳﺒﻪ اﻟﻘﻴﻤﻪ اﻟﻤﻀﺎﻓﺔ</li>
                          </ol>
                        </div>
                    </div>
      </div>

     <!-- end col -->

            <div class="col-12 col-md-12">
                <div class="sr-box">
                        <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>اﺟﺮاءاﺕ اﺻﺪاﺭ ﺭﺧﺼﺔ ﺩﺭاﺟﺔ ﻧﺎﺭﻳﺔ</h2>
                        <div class="content">
                          <ol>
                            <li>اﻥ ﻳﻜﻮﻥ اﻟﻤﺘﻘﺪﻡ اﺗﻢ ﻣﻦ اﻟﻌﻤﺮ 18 ﻋﺎﻣﺎ</li>
                            <li>اﻟﻜﺸﻒ اﻟﻄﺒﻰ ( ﻛﺸﻒ ﻧﻈﺮ ﻭﻓﺼﻴﻠﺔ اﻟﺪﻡ ﻣﻊ ﻣﺴﺘﻮﺻﻒ اﻭ  ﻣﺴﺘﺸﻔﻰ ﻣﺮﺗﺒﻄﻪ ﺑﻨﻈﺎﻡ اﻟﻤﺮﻭﺭ)</li>
                            <li>ﺷﻬﺎﺩﺓ ﺧﻠﻮ ﺳﻮاﺑﻖ </li>
                          </ol>
                          <p dir="RTL"><strong class="orange">اﻟﻤﻮاﻃﻨﻮﻥ :</strong>  ﺻﻮﺭﺓ ﻣﻦ ﺑﻄﺎﻗﺔ اﻻﺣﻮاﻝ ﻣﻊ اﻻﺻﻞ ﺻﻮﺭﺓ ﻣﻦ اﻻﻗﺎﻣﺔ ﻣﻊ اﻻﺻﻞ ﻭﻳﺠﺐ اﻥ ﻳﻜﻮﻥ ﻣﻬﻨﺔ اﻟﻤﺘﻘﺪﻡ ( ﻣﺘﺴﺒﺐ اﻭ ﻣﻮﻇﻒ اﻫﻠﻰ اﻭ ﻃﺎﻟﺐ اﻭ ﻣﻮﻇﻒ ﺣﻜﻮﻣﻰ ﻣﻦ اﻟﻠﺬﻳﻦ ﻳﺴﺘﻠﺰﻡ ﻭﻇﺎﺋﻔﻬﻢ اﻟﺤﺼﻮﻝ ﻋﻠﻰ ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ ﻋﺎﻣﺔ ﻭﻳﺘﻄﻠﺐ اﺣﻀﺎﺭ ﺧﻄﺎﺏ ﻣﻦ اﻟﻤﺮﺟﻊ )<br>
                           <strong class="orange"> اﻟﻤﻘﻴﻤﻮﻥ :</strong> ﺻﻮﺭﺓ ﻣﻦ اﻻﻗﺎﻣﺔ ﻣﻊ اﻻﺻﻞ ﻭﻳﺠﺐ اﻥ ﺗﻜﻮﻥ اﻟﻤﻬﻨﺔ ﺳﺎﺋﻘﺎ ﻋﺎﻣﺎ اﻭ ﺳﺎﺋﻖ ﻧﻘﻞ اﻭ ﺳﺎﺋﻖ ﻣﻌﺪاﺕ اﻭ ﺳﺎﺋﻖ ﺣﺎﻓﻠﺔ اﻭ ﺳﺎﺋﻖ اﺟﺮﺓ ﻭﻟﺪﻳﻪ ﺭﺧﺼﺔ ﻋﺎﻣﺔ ﻣﻦ ﺑﻠﺪﻩ  </p>
                          <ol>
                            <li>عدد 4 صور شخصية مقاس 4*6</li>
                            <li>اﺳﺘﻜﻤﺎﻝ ﺑﻴﺎﻧﺎﺕ اﻟﻨﻤﻮﺫﺝ ( ﻣﻦ ﻣﻮﻗﻊ ﺩﻟﻪ – ﻣﻮﻗﻊ  ﻭﺯاﺭﺓ اﻟﺪاﺧﻠﻴﺔ )</li>
                            <li>ﻣﻠﻒ ﻟﺤﻔﻆ اﻻﻭﺭاﻕ </li>
                            <li>ﺩﻓﻊ اﻟﺮﺳﻮﻡ اﻟﻤﻘﺮﺭﺓ + ﺿﺮﻳﺒﻪ اﻟﻘﻴﻤﻪ اﻟﻤﻀﺎﻓﺔ</li>
                          </ol>
                        </div>
                    </div>
      </div>

     <!-- end col -->
            <div class="col-12 col-md-12">
                <div class="sr-box">
                        <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>اﺟﺮاءاﺕ اﺻﺪاﺭ ﺭﺧﺼﺔ اﻟﻤﻌﺪاﺕ اﻟﺜﻘﻴﻠﺔ ﻭاﻵﻟﻴﺎﺕ</h2>
                        <div class="content">
                          <ol>
                            <li>اﻥ ﻳﻜﻮﻥ اﻟﻤﺘﻘﺪﻡ اﺗﻢ ﻣﻦ اﻟﻌﻤﺮ 20 ﻋﺎﻣﺎ</li>
                            <li>  اﻟﻜﺸﻒ اﻟﻄﺒﻰ ( ﻛﺸﻒ ﻧﻈﺮ ﻭﻓﺼﻴﻠﺔ اﻟﺪﻡ ﻣﻊ ﻣﺴﺘﻮﺻﻒ اﻭ ﻣﺴﺘﺸﻔﻰ ﻣﺮﺑﻮﻃﻪ ﻣﻊ اﻟﻤﺮﻭﺭ)</li>
                            <li> ﺷﻬﺎﺩﺓ ﺧﻠﻮ ﺳﻮاﺑﻖ </li>
                            <li> اﻥ ﺗﻜﻮﻥ اﻟﻤﻬﻨﻪ ﺣﺎﻝ اﻟﺤﺼﻮﻝ ﻋﻠﻰ اﻟﺮﺧﺼﺔ ﻣﻦ اﻟﻤﻬﻦ اﻟﺘﻰ ﺗﺨﻮﻝ ﻟﻘﻴﺎﺩﺓ ﻫﺬﺓ اﻟﻔﺌﺔ </li>
                          </ol>
                          <p dir="RTL"><strong class="orange">اﻟﻤﻮاﻃﻨﻮﻥ :</strong> ﺻﻮﺭﺓ ﻣﻦ ﺑﻄﺎﻗﺔ اﻻﺣﻮاﻝ ﻣﻊ اﻻﺻﻞ ﺻﻮﺭﺓ ﻣﻦ اﻻﻗﺎﻣﺔ ﻣﻊ اﻻﺻﻞ ﻭﻳﺠﺐ اﻥ ﻳﻜﻮﻥ ﻣﻬﻨﺔ اﻟﻤﺘﻘﺪﻡ ( ﻣﺘﺴﺒﺐ اﻭ ﻣﻮﻇﻒ اﻫﻠﻰ اﻭ ﻃﺎﻟﺐ اﻭ ﻣﻮﻇﻒ ﺣﻜﻮﻣﻰ ﻣﻦ اﻟﻠﺬﻳﻦ ﻳﺴﺘﻠﺰﻡ ﻭﻇﺎﺋﻔﻬﻢ اﻟﺤﺼﻮﻝ ﻋﻠﻰ ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ ﻋﺎﻣﺔ ﻭﻳﺘﻄﻠﺐ اﺣﻀﺎﺭ ﺧﻄﺎﺏ ﻣﻦ اﻟﻤﺮﺟﻊ )<br>
                           <strong class="orange"> اﻟﻤﻘﻴﻤﻮﻥ :</strong>  ﺻﻮﺭﺓ ﻣﻦ اﻻﻗﺎﻣﺔ ﻣﻊ اﻻﺻﻞ ﻭﻳﺠﺐ اﻥ ﺗﻜﻮﻥ اﻟﻤﻬﻨﺔ ﺳﺎﺋﻘﺎ ﻋﺎﻣﺎ اﻭ ﺳﺎﺋﻖ ﻧﻘﻞ اﻭ ﺳﺎﺋﻖ ﻣﻌﺪاﺕ اﻭ ﺳﺎﺋﻖ ﺣﺎﻓﻠﺔ اﻭ ﺳﺎﺋﻖ اﺟﺮﺓ ﻭﻟﺪﻳﻪ ﺭﺧﺼﺔ ﻋﺎﻣﺔ ﻣﻦ ﺑﻠﺪﻩ </p>
                          <ol>
                            <li>عدد 4 صور شخصية مقاس 4*6</li>
                            <li>اﺳﺘﻜﻤﺎﻝ ﺑﻴﺎﻧﺎﺕ اﻟﻨﻤﻮﺫﺝ ( ﻣﻦ ﻣﻮﻗﻊ ﺩﻟﻪ – ﻣﻮﻗﻊ  ﻭﺯاﺭﺓ اﻟﺪاﺧﻠﻴﺔ )</li>
                            <li>ﻣﻠﻒ ﻟﺤﻔﻆ اﻻﻭﺭاﻕ </li>
                            <li>ﺩﻓﻊ اﻟﺮﺳﻮﻡ اﻟﻤﻘﺮﺭﺓ + ﺿﺮﻳﺒﻪ اﻟﻘﻴﻤﻪ اﻟﻤﻀﺎﻓﺔ</li>
                          </ol>
        
                        </div>
                    </div>
      </div>

     <!-- end col -->


     
   </div>
   <!-- end row -->

  </div>
  <div class="tab-pane fade" id="two" role="tabpanel" aria-labelledby="pills-profile-tab">
<div class="sr-box">
    <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>ﺗﻨﻘﺴﻢ ﺭﺧﺺ اﻟﻘﻴﺎﺩﺓ اﻟﻰ</h2>
    <div class="content">


<strong>ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ ﺧﺎﺻﺔ: </strong>

<p dir="rtl">ﻭﺗﻤﻨﺢ ﻟﻤﻦ ﻳﻘﻮﺩ اﻟﺴﻴﺎﺭاﺕ اﻟﺨﺎﺻﺔ اﻟﺘﻰ ﻻﺗﺰﻳﺪ ﺳﻌﺘﻬﺎ  ﻋﻦ 9 اﺷﺨﺎﺹ ﺑﻤﺎ ﻓﻴﻬﻢ اﻟﺴﺎﺋﻖ ﻭﻣﺮﻛﺒﺎﺕ اﻟﻨﻘﻞ اﻟﺨﺎﺹ اﻟﺘﻰ ﻻﻳﺘﺠﺎﻭﺯ ﻭﺯﻧﻬﺎ اﻻﺟﻤﺎﻟﻰ  (3500) ﻛﺠﻢ</p>
<ol>
  <li>ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ ﻋﺎﻣﺔ: ﻭﺗﻨﻘﺴﻢ اﻟﻰ 3 ﻓﺌﺎﺕ </li>
</ol>
<strong class="orange">اﻟﻔﺌﺔ اﻻﻭﻟﻰ: ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ اﺟﺮﺓ ﻋﺎﻣﺔ :</strong> <br />
<p>  ﺗﺠﻴﺰ ﻟﺤﺎﻣﻠﻬﺎ ﻗﻴﺎﺩﺓ ﺳﻴﺎﺭاﺕ اﻻﺟﺮﺓ اﻟﻌﺎﻣﺔ ﻭﻻﺗﺰﻳﺪ  ﺳﻌﺘﻬﺎ ﻋﻦ ﺛﻤﺎﻧﻴﻪ اﺷﺨﺎﺹ</p>
<strong class="orange">اﻟﻔﺌﺔ اﻟﺜﺎﻧﻴﺔ: ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ اﻟﻨﻘﻞ ﻭﺗﻨﻘﺴﻢ اﻟﻰ :</strong>
<ol>
  <li>ﺭﺧﺼﺔ اﻟﻨﻘﻞ اﻟﺨﻔﻴﻒ ﻭﺗﺠﻴﺰ ﻟﺤﺎﻣﻠﻬﺎ ﻗﻴﺎﺩﺓ ﻣﺮﻛﺒﺎﺕ  اﻟﻨﻘﻞ اﻟﻌﺎﻡ اﻟﺼﻐﻴﺮﺓ اﻟﺘﻰ ﻻﻳﺰﻳﺪ ﻭﺯﻧﻬﺎ اﻻﺟﻤﺎﻟﻰ ﻋﻦ )3500) ﻛﺠﻢ</li>
  <li>ﺭﺧﺼﺔ اﻟﻨﻘﻞ اﻟﺜﻘﻴﻞ ﻭﺗﺠﻴﺰ ﻟﺤﺎﻣﻠﻬﺎ ﻗﻴﺎﺩﺓ ﻣﺮﻛﺒﺎﺕ  اﻟﻨﻘﻞ اﻟﻌﺎﻡ اﻭ اﻟﺨﺎﺹ اﻟﻜﺒﻴﺮﺓ اﻟﺘﻰ ﻳﺰﻳﺪ ﻭﺯﻧﻬﺎ اﻻﺟﻤﺎﻟﻰ ﻋﻦ (3500) ﻛﺠﻢ</li>
</ol>
<strong class="orange">اﻟﻔﺌﺔ اﻟﺜﺎﻟﺜﻪ: ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ اﻟﺤﺎﻓﻼﺕ ﻭﺗﻨﻘﺴﻢ اﻟﻰ :</strong>
<ol>
  <li>ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ اﻟﺤﺎﻓﻼﺕ اﻟﺼﻐﻴﺮﻩ اﻟﻌﺎﻣﺔ اﻭ اﻟﺨﺎﺻﺔ  ﻭﺗﺠﻴﺰ ﻟﺤﺎﻣﻠﻬﺎ ﻗﻴﺎﺩﺓ اﻟﺤﺎﻓﻼﺕ اﻟﺘﻰ ﻻﻳﺰﻳﺪ ﻋﺪﺩ ﺭﻛﺎﺑﻬﺎ ﻋﻦ (15) ﺭاﻛﺒﺎ</li>
  <li>ﺭﺧﺼﺔ ﻗﻴﺎﺩﻩ اﻟﺤﺎﻓﻼﺕ اﻟﻜﺒﻴﺮﺓ ﻭﺗﺠﻴﺰ ﻟﺤﺎﻣﻠﻬﺎ ﻗﻴﺎﺩﺓ  اﻟﺤﺎﻓﻼﺕ اﻟﻌﺎﻣﻪ اﻭ اﻟﺨﺎﺻﺔ اﻟﺘﻰ ﻳﺰﻳﺪ ﻋﺪﺩ ﺭﻛﺎﺑﻬﺎ ﻋﻦ (15) ﺭاﻛﺒﺎ </li>
  <li>ﺭﺧﺼﺔ ﻗﻴﺎﺩﺓ ﻣﻌﺪاﺕ اﺷﻐﺎﻝ ﻋﺎﻣﺔ: ﻭﺗﻤﻨﺢ ﻟﻘﻴﺎﺩﺓ ﻣﺮﻛﺒﺎﺕ  اﻻﺷﻐﺎﻝ اﻟﻌﺎﻣﺔ ﻭﻳﺤﺪﺩ ﻧﻮﻉ اﻟﻤﻌﺪﺓ ﻓﻰ ﺭﺧﺼﺔ اﻟﻘﻴﺎﺩﺓ </li>
</ol>
<p dir="rtl">         ﺭﺧﺼﺔ اﻟﺪﺭاﺟﺔ اﻵﻟﻴﺔ: ﻭﺗﺠﻴﺰ  ﻟﺤﺎﻣﻠﻬﺎ ﻗﻴﺎﺩﺓ اﻟﺪﺭاﺟﺎﺕ اﻵﻟﻴﻪ ﺑﺎﻧﻮاﻋﻬﺎ</p> <br />
  <h2>  ﻣﻼﺣﻈﺔ ﻫﺎﻣﻪ</h2>
  ﻳﺠﻮﺯ اﻟﺠﻤﻊ ﺑﻴﻦ اﻛﺜﺮ ﻣﻦ ﻧﻮﻉ ﻣﻦ اﻧﻮاﻉ اﻟﺮﺧﺺ ﻭﻳﺤﺪﺩ  ﺫﻟﻚ ﻓﻰ ﺭﺧﺼﺔ اﻟﻘﻴﺎﺩﺓ<br />
 <strong class="orange"> ﻣﻮاﺻﻔﺎﺕ ﺭﺧﺺ اﻟﻘﻴﺎﺩﺓ</strong><br />
  ﻳﺠﺐ اﻥ ﺗﻜﻮﻥ ﺭﺧﺼﺔ اﻟﻘﻴﺎﺩﺓ ﺫاﺕ ﻣﻮاﺻﻔﺎﺕ ﺃﻣﻨﻴﺔ ﻋﺎﻟﻴﺔ  ﻭﺗﺸﺘﻤﻞ ﻋﻠﻰ اﻟﺒﻴﺎﻧﺎﺕ اﻟﺘﺎﻟﻴﻪ
<ol>
  <li>اﺳﻢ ﺻﺎﺣﺐ اﻟﺮﺧﺼﺔ</li>
  <li>ﺭﻗﻢ اﻟﺮﺧﺼﺔ</li>
  <li>ﺗﺎﺭﻳﺦ اﻟﻤﻴﻼﺩ</li>
  <li>اﻟﻘﻴﻮﺩ</li>
  <li>ﻧﻮﻉ اﻟﺮﺧﺼﺔ</li>
</ol>
</div>
</div>
<!-- end sr-box -->
  </div>
  <!-- end tab  -->
    <div class="tab-pane fade" id="three" role="tabpanel" aria-labelledby="pills-profile-tab">
<div class="sr-box">
    <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>ﺗﻌﻠﻴﻤﺎﺕ اﺩاﺭﺓ اﻟﻤﺮﻭﺭ ﻟﺤﻀﻮﺭ اﻟﻄﻼﺏ</h2>
    <div class="content">
<strong class="orange">ﺗﻠﺰﻡ اﺩاﺭﺓ اﻟﻤﺮﻭﺭ ﺑﺎﻟﺘﻘﻴﺪ اﻟﻻﺯﻡ ﻟﺤﻀﻮﺭ اﻟﻄﻼﺏ ﺑﺸﺮﻛﺔ  ﺩﻟﻪ ﻟﺘﻌﻠﻴﻢ ﻗﻴﺎﺩﻩ اﻟﺴﻴﺎﺭاﺕ ﺑﺎﻟﺘﺎﻟﻰ :</strong>
<ol>
  <li>اﺣﻀﺎﺭ اﻟﻬﻮﻳﻪ – اﻻﻗﺎﻣﺔ اﻻﺻﻞ ﻳﻮﻣﻴﺎ </li>
  <li>اﺣﻀﺎﺭ ﺳﻨﺪ اﻟﻘﺒﺾ ﻳﻮﻣﻴﺎ</li>
  <li>ﻋﺪﻡ اﺭﺗﺪاء اﻟﻤﻼﺑﺲ اﻟﻐﻴﺮ ﻻﺋﻘﺔ ( ﺑﻨﻄﻠﻮﻥ ﺑﺮﻣﻮﺩا-  ﻓﻨﻠﻪ ﻛﺖ – ﺛﻴﺎﺏ اﻟﻨﻮﻡ -–ﺷﻮﺭﺕ)</li>
  <li>اﻟﺘﻘﻴﺪ ﺑﻤﻮاﻋﻴﺪ اﻟﺪﻭﺭﺓ ﻭاﻻﺧﺘﺒﺎﺭ</li>
  <li>اﺣﻀﺎﺭ ﺳﺪاﺩ ﺭﺳﻮﻡ اﻟﺮﺧﺼﺔ اﻭ اﻟﺘﺼﺮﻳﺢ ﻓﻰ ﻣﻮﻋﺪ اﻻﺧﺘﺒﺎﺭ</li>
  <li>ﻋﺪﻡ اﻟﺘﺪﺧﻴﻦ اﺛﻨﺎء اﻟﺪﻭﺭﺓ ﻭاﻻﺧﺘﺒﺎﺭ</li>
</ol>


</div>
</div>
<!-- end sr-box -->
  </div>
  <!-- end tab  -->

    <div class="tab-pane fade" id="four" role="tabpanel" aria-labelledby="pills-profile-tab">
<div class="sr-box">
    <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>التدريب</h2>
    <div class="content">

<p dir="rtl">لقد تم اعداد وتطوير البرامج فى شركة دله لتعليم  قيادة السيارات للرفع من مستوى القيادة الامنه والحد من الحوادث المروريه التى  يشكل العنصر البشرى السبب الرئيسي فى ارتكابها مما ينتج عن هذه الحوادث خسائر  ماديه وبشريه</p>
<p dir="rtl">ونظرا لدور شركة دله لتعليم قيادة السيارات  فقد تم اعداد برنامج متطور عمليا ونظريا من اجل رفع مستوى القيادة الامنه لدى  قائدى المركبه ورفع مستوى الوعى المروري</p>
<br>
<h2 dir="rtl">التدريب النظرى يشمل:</h2>
<ol>
  <li>شرح نظام  الدراسة العملى والنظري – تسليم المناهج والهدف من هذه الدورة </li>
  <li>تعريف علامات  واشارات المرور الدوليه</li>
  <li>مبادئ  ميكانيكا السيارات واجزاء المركبة</li>
  <li>حدود  السرعة والمسارات وقواعد التجاوز واصول القياده الآمنه</li>
  <li>القيادة  الآمنه والوقائية ومحاضرات فى الامن والسلامه والاسعافات الأوليه</li>
  <li>درس فى  نظام المرور والمخالفات وماينتج عن هذه المخالفات</li>
  <li>عرض افلام  عن حوادث الطرق وكيفية القيادة الامنة</li>
</ol>
<br>
<h2 dir="rtl">التدريب العملى يشمل:</h2>
<ol>
  <li>التدريب  على جهاز المحاكه ( السميوليتور)</li>
  <li>التدريب  على القيادة داخل الميدان</li>
  <li>التدريب  على الرجوع للخلف </li>
  <li>التدريب  على التمارين المتنوعة ( T – X – 8)  والوقوف على منحدر والتدريب على الوقوف الطولى </li>
  <li>التدريب  على القيادة خارج الميدان وداخل الاحياء وسط المدينة والطرق السريعة</li>
  <li>عمل  اختبار تدريبى</li>
</ol>


</div>
</div>
<!-- end sr-box -->
  </div>
  <!-- end tab  -->



</div>
                  </div>
                 <!-- end col -->
               </div>
               <!-- end row -->
      <div class="row">
          <div class="col-12 col-md-12">
   
         </div>
        <!-- end col -->
      </div>
      <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->
@stop