<?php
namespace App\Http\Controllers;
?>
@extends('site.master')
@section('seo')
<title>شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
   <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-3.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">اﻭﻗﺎﺕ ﺩﻭاﻡ ﺩﻟﻪ</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container mt-5">
   <div class="row">
       <div class="col-12 col-md-6">
<div class="about-box">
                        <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/time-icon.png"></i> اﻭﻗﺎﺕ ﻋﻤﻞ ﺩﻟﻪ ﻟﺘﻌﻠﻴﻢ اﻟﻘﻴﺎﺩﺓ </h2>
                        <div class="content">
                            <strong>ﻭﻗﺖ ﺩﻭاﻡ ﺩﻟﺔ ﻟﺘﻌﻠﻴﻢ اﻟﻘﻴﺎﺩﺓ ﻓﻲ اﻷﻳﺎﻡ اﻟﻌﺎﺩﻳﺔ : </strong>
                            <p>ﻣﻦ اﻷﺣﺪ ﺇﻟﻰ اﻟﺨﻤﻴﺲ – ﻣﻦ 07:00 ﺻﺒﺎﺣﺎً ﺇﻟﻰ 09:00 ﻣﺴﺎءا
                                <br> اﻟﺠﻤﻌﺔ و اﻟﺴﺒﺖ ﻻﻳﻮﺟﺪ ﺩﻭاﻡ</p>

                        </div>
                        <a class="btn  left" href="{{url('/contact')}}">اﺗﺼﻞ ﺑﻨﺎ</a>

                    </div>
      </div>
     <!-- end col -->



   </div>
   <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->
@stop