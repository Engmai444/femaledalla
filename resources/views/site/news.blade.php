@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | الاخبار</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')

    
    <div class="news-block">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="title-orange">الاخبار</h2>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
				@foreach($news as $item)
                <div class="col-12 col-md-4 ">
                    <div class="news-box">
                        <div class="thumb"><img src="{{url('/public/photos/'.$item->photo)}}" alt=""></div>
                        <div class="content">
                            <h3 class="title">{{$item->title_ar}}</h3>
                            <p>التاريخ : {{date('Y/m/d',strtotime($item->created_at))}}</p>
                            <p> {{ str_limit(strip_tags($item->content_ar), $limit = 150, $end = '...') }}</p>
                            <a class="btn left" href="{{url('/news/'.$item->pageId)}}">اﻟﻤﺰﻳﺪ</a>
                        </div>
                    </div>
                </div>
                <!-- end col -->
				@endforeach
                
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
   
@stop