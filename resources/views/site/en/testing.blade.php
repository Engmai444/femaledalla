<?php
$branches = App\Http\Controllers\SiteController::branches();
?>
@extends('site.en.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | Questions</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
  <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-3.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">Questions</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">
			@if(count($orders)>0)
			<form action="{{url('testing_lang')}}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<label>اللغة</label>
				<select name="lang">
					<option value="1">عربى</option>
					<option value="2">انجليزى</option>
					<option value="3">اوردو</option>
					<option value="4">هندى</option>
					<option value="5">بنغالى</option>
					<option value="6">ماليومى</option>
					<option value="7">تاميلى</option>
					<option value="8">تركى</option>
					<option value="9">اندونيسى</option>
					<option value="10">تجالو</option>
					<option value="11">فرنسى</option>
					<option value="12">تلجو</option>
					<option value="13">باتشو</option>

				</select>

				<input type="submit" class="btn btn-green btn-icon btn-icon btn-lg" value="نفذ">

			</form>
            <div class="row">
                <div class=" col-md-8 text-center pb-3 pt-5">
				
                    <div class="table-responsive">
                     

					    <div id="rootwizard">
    	<div class="navbar">
    	  <div class="navbar-inner">
    	    <div class="container" >
    	<ul>
    	  	
			@foreach($items as $k => $item)
    	    <li><a href="#tab{{$k+1}}" data-toggle="tab">{{$k+1}}</a></li>
			@endforeach
    	</ul>
    	 </div>
    	  </div>
    	</div>
		<form id="testing">
    	<div class="tab-content">

			@foreach($items as $k => $item)
				<div class="tab-pane" id="tab{{$k+1}}" >
					<div class="text-right">
						<h3>{{$item->name}}</h3>
						@if($item->photo!='')
							@if(file_exists(public_path().'/questions/'.$item->photo.'.png'))
								<img src="{{url('/public/questions/'.$item->photo.'.png')}}" >
								<?php $i=0;?>
							@else
								<?php $i=1;?>
							@endif
						@else
							<?php $i='';?>
						@endif
						<input type="hidden" name="q[{{$k}}]" value="{{$item->id}}" />
						<div class="">
							<?php $co=1;?>
							@foreach($item->asw as $asw)
								<br />
								<label><input type="radio" name="sw[{{$k}}]" value="{{$asw->id}}">
									<?php if($i=='' || $i=='0'){?>
									{{$asw->name}}
									<?php }else{?>
									@if($co=='1')
										<img src="{{url('/public/questions/'.$item->photo.'A.png')}}" width="100px" height="100px" style="margin-bottom: -15px" >
									@elseif($co=='2')
										<img src="{{url('/public/questions/'.$item->photo.'B.png')}}" width="100px" height="100px" style="margin-bottom: -15px">
									@else
										<img src="{{url('/public/questions/'.$item->photo.'C.png')}}" width="100px" height="100px" style="margin-bottom: -15px">

									@endif
									<?php }?>
								</label>
								<?php $co++;?>
							@endforeach
						</div>
					</div>
				</div>
			@endforeach
    	   
    		<ul class="pager wizard">
    		  	<li class="next"><a href="javascript:;" class="btn btn-primary">Next</a></li>
    			<li class="finish"><a href="javascript:;" class="btn btn-success">Result</a></li>
    		</ul>
    	</div>
		</form>
		
    </div>

	<div class="result"></div>

                    </div>
                </div>
                <!-- end col -->
				<div class="col-md-2 text-center pb-3 pt-5">
					<h5>Result</h5>
					<div class="btn-success">
						<div id="success-re" >0</div>
						success
					</div>
					<div class="btn-danger">
					<div id="danger-re">0</div>
						Incorrect
					</div>
				</div>
				<div class="col-md-2 text-center pb-3 pt-5">
					<h5>Timer</h5>
					<div id="clock"></div>
				</div>
            </div>
            <!-- end row -->
			@else
				<div class="text-right border border-light  p-5">
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="icon fa fa-ban"></i>
						You don't register any course to make test exam
					</div>
				</div>
			@endif
        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->
@stop