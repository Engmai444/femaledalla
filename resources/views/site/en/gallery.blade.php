
@extends('site.en.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | Gallery</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
   <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-3.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">Gallery</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container mt-5">
		@foreach($items as $item)
		
               <div class="row">
                   <div class="col-12">
                <h2 class="title-bg">{{$item->catname_en}}</h2>
                  </div>
                 <!-- end col -->
               </div>
               <!-- end row -->
   <div class="row">
   @foreach($item->photos as $photo)
     <div class="col-12 col-md-4">
   <a class="venobox"  data-gall="myGallery"  href="{{url('/')}}/public/photos/{{$photo->photo}}"><img src="{{url('/')}}/public/photos/{{$photo->photo}}" alt=""/></a>
    </div>
   <!-- end col -->
   @endforeach

   </div>
   <!-- end row -->
	@endforeach




        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->
@stop