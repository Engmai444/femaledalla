@extends('site.en.master')
@section('seo')
<title></title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
   <div class="clearfix"></div>
    <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-1.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">About Us</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 text-center">
                    <div class="about-box">
                        <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i> About Us</h2>
                        <div class="content">

                            <ul class="circle">
                                <li>Dallah Driving Company is one of the leading Saudi companies specialized in training the drivers for exceptional driving skills and operates under the supervision of the General Directorate of Traffic. </li>
                                <li>The company enjoys the largest market share in this field and is well equipped to respond to present and future needs of Saudi Arabia. It continues to engage in coaching the Saudi cadres with the objective of creating a highly skilled and specialized pool of human resources which will cater to the labor market in Saudi Arabia with trained professionals in this field.  </li>
                                <li>Dallah Driving Company was founded in Jeddah in the year 1975 and is considered the most successful training centres for driving skills in the Kingdom of Saudi Arabia. The number of annual enrollments exceed 250,000 students. 
                                    </li>
                            </ul>
                            <br />
                          <p dir="rtl">After starting as one branch in 1975, it continued to expand its operation Kingdom-wide. The current number of its branches has reached ten, as listed below:</p>

<ul class="circle">
  <li>1 branch in Jeddah  </li>
  <li>2 branches in Riyadh (Al-Takhassosi and Al-Solay) </li>
  <li>1 branch in Al-Kharj </li>
  <li>1 branch in Taif</li>
  <li>1 branch in Jizan </li>
  <li>1 branch in Wadi Dawasser </li>
  <li>1 branch in Majmah</li>
  <li>1 branch in Shaqra</li>
  <li>1 branch in Dawadmi</li>
</ul>

<p>
Dallah Driving Co. offers training courses for enhancing driving skills and endeavours to raise the awareness of and respect for traffic regulations and safe driving. Theoretical and practical lessons are conducted using state-of-the-art high-resolution multi-display simulators running latest versions of real-time simulation software and a large fleet comprising more than 300 vehicles ranging from Private and Public vehicles (light and heavy), buses and motorcycles.
<br><br />
Bearing in mind the large expatriate community in our country, more than 200 trainers of different nationalities conduct the courses ensuring delivery of information to the students in their respective native languages. 
</p><br><br />



                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12 col-md-12 text-center">
                    <a class="pdf" href="#"><img src="{{url('/resources/views/site')}}/images/pdf.png" alt=""> </a>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12 col-md-12 orange-box text-center">
                    <h2>Our Task</h2>
                    <p>To provide practical and theoretical training preparing the trainees for handling various types of vehicles and to cope with all conditions they might come across in real-life driving situations thereby enabling them with skills that help them preserve lives and properties.</p>

                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12 col-md-12 grey-box text-center">
                    <h2>Our Objectives </h2>
<ul style="list-style: none;">
  <li>To provide unrivalled professional services in an appropriate educational environment</li>
  <li> To raise the quality of training to be in line with the aspirations of Saudi Arabia and the goals set forth in Vision 2030</li>
  <li>To ensure that all services are provided in accordance with highest standards associated with quality, efficiency and transparency.</li>
  <li>To ensure that our services not only helps our community at the local level but also prepares it for abroad.</li>
</ul>

                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12 col-md-12 text-center">
                    <div class="about-box">
                        <h2><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>Nature of activity</h2>
                        <div class="content">

                            <ul class="circle">
                                <li>We strive to train and prepare drivers of all types of vehicles (private – heavy public - public light buses – motorcycles) in such a manner that they meet all the challenges of real-life driving situations, have ample experience in safe driving skills and gain enough competence to pass the tests which eventually lead to their obtaining a driving license.
                                </li>

                            </ul>

                        </div>

                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{url('/resources/views/site')}}/images/cr1.jpg" alt="">
                </div>
                <!-- end col -->
                <div class="col-12 col-md-6">
                    <img src="{{url('/resources/views/site')}}/images/cr1.jpg" alt="">
                </div>
                <!-- end col -->

            </div>
            <!-- end row -->

        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->
    
@stop