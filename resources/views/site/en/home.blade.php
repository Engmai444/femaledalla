<?php
$options = App\Http\Controllers\SiteController::site_option();
?>
@extends('site.en.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}}</title>
<meta name="Description" content="Dallah Driving Company is one of the leading Saudi companies specialized in training the drivers for exceptional driving skills and operates" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
    <div class="container">
        <div class="slider">
            <div class="box-form" >
                <div class="title-box1">

                    <h2 class="inline title1">Register to</h2>
                    <h2 class="inline title2">Courses</h2>

                </div>

                <form  id="signup2">
				<div class="msg"></div>
							
                    <div class="row">
                        <!-- end col -->
						<div class="col-12 col-md-12">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa user-icon"></i></span>
                                </div>
                     
								<input type="text" class="form-control required" name="IDNumber" maxlength="10" placeholder="{{Lang::get('validation.IDNumber')}}">
                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-12 col-md-12">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa user-icon"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="{{Lang::get('validation.name')}}" autocomplete="off" onkeypress="return CheckArabicCharactersOnly(event);" aria-label="{{Lang::get('validation.name')}}" name="FullName" aria-describedby="basic-addon2">

                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-12 col-md-12">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa mail-icon"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="{{Lang::get('validation.email')}}" aria-label="{{Lang::get('validation.email')}}" name="email" aria-describedby="basic-addon2">

                            </div>
                        </div>
                        <!-- end col -->

                        <div class="col-12 col-md-12">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa phone-icon"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="{{Lang::get('validation.phone')}}" aria-label="{{Lang::get('validation.phone')}}" maxlength="10" name="mobile" aria-describedby="basic-addon2">

                            </div>
                        </div>
                        <!-- end col -->
						 
						
						<div class="col-12 col-md-12">
							<h3 style="font-size: 12px;margin: 0;padding: 0;">{{Lang::get('validation.dob')}}</h3>
							<label><input type="checkbox" value="1" name="d_type" class="swidate">Hijri ?</label>
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa date-icon"></i></span>
                                </div>
								
								
								<div class="datebox">
                                <select name="d_day" required>
									<option value="">Day</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
								</select>
								<select name="d_month" required>
									<option value="">Month</option>
									<option value='1'>Janaury</option>
									<option value='2'>February</option>
									<option value='3'>March</option>
									<option value='4'>April</option>
									<option value='5'>May</option>
									<option value='6'>June</option>
									<option value='7'>July</option>
									<option value='8'>August</option>
									<option value='9'>September</option>
									<option value='10'>October</option>
									<option value='11'>November</option>
									<option value='12'>December</option>
								</select>
								<select name="d_year" required>
									<option value="">year</option>
									<option value="2002">2006</option>
									<option value="2002">2002</option>
									<option value="2001">2001</option>
									<option value="2000">2000</option>
									<option value="1999">1999</option>
									<option value="1998">1998</option>
									<option value="1997">1997</option>
									<option value="1996">1996</option>
									<option value="1995">1995</option>
									<option value="1994">1994</option>
									<option value="1993">1993</option>
									<option value="1992">1992</option>
									<option value="1991">1991</option>
									<option value="1990">1990</option>
									<option value="1989">1989</option>
									<option value="1988">1988</option>
									<option value="1987">1987</option>
									<option value="1986">1986</option>
									<option value="1985">1985</option>
									<option value="1984">1984</option>
									<option value="1983">1983</option>
									<option value="1982">1982</option>
									<option value="1981">1981</option>
									<option value="1980">1980</option>
									<option value="1979">1979</option>
									<option value="1978">1978</option>
									<option value="1977">1977</option>
									<option value="1976">1976</option>
									<option value="1975">1975</option>
									<option value="1974">1974</option>
									<option value="1973">1973</option>
									<option value="1972">1972</option>
									<option value="1971">1971</option>
									<option value="1970">1970</option>
									<option value="1969">1969</option>
									<option value="1968">1968</option>
									<option value="1967">1967</option>
									<option value="1966">1966</option>
									<option value="1965">1965</option>
									<option value="1964">1964</option>
									<option value="1963">1963</option>
									<option value="1962">1962</option>
									<option value="1961">1961</option>
									<option value="1960">1960</option>
									<option value="1959">1959</option>
									<option value="1958">1958</option>
									<option value="1957">1957</option>
									<option value="1956">1956</option>
									<option value="1955">1955</option>
									<option value="1954">1954</option>
									<option value="1953">1953</option>
									<option value="1952">1952</option>
									<option value="1951">1951</option>
									<option value="1950">1950</option>
									<option value="1949">1949</option>
									<option value="1948">1948</option>
									<option value="1947">1947</option>
									<option value="1946">1946</option>
									<option value="1945">1945</option>
									<option value="1944">1944</option>
									<option value="1943">1943</option>
									<option value="1942">1942</option>
									<option value="1941">1941</option>
									<option value="1940">1940</option>
								</select>
								</div>
								
                            </div>
                        </div>
                        <!-- end col -->
						<div class="col-12 col-md-12">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa user-icon"></i></span>
                                </div>
                                <?php $nat_setting=DB::table('site_option')->where('varname','nationality_all')->first();?>

                                <select class="form-control required select2" name="NationalityId">
                                    <option value="">{{Lang::get('validation.Nationality')}}</option>
                                    @if($nat_setting->value=='1')

                                        @foreach(App\Http\Controllers\CustomerController::Nationality() as $it)
                                            <option value="{{$it->Id}}">{{$it->Name}}</option>
                                        @endforeach
                                    @else
                                        <option value="129">Saudi</option>

                                    @endif
                                </select>

                            </div>
                        </div>
                       
	    <div class="col-12 col-md-12">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa user-icon"></i></span>
                                </div>
                                <?php $msg_setting=DB::table('site_option')->where('varname','msg_female')->first();?>
                                <?php $gen=DB::table('site_option')->where('varname','gender')->first();?>

                                <select class="form-control required select2" name="gender" id="gender"  @if(@$msg_setting->value=='0') onchange="return get_gender()" @endif>
                                    <option value="">sex</option>

                                    @if($gen->value=='0')
                                        <option value="female">Female</option>
                                    @elseif($gen->value=='1')
                                        <option value="male">Male</option>
                                    @else
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>

                                    @endif

                                </select>

                            </div>
                        </div>
					
						<div class="col-12 col-md-12">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa user-icon"></i></span>
                                </div>
                                <input type="password" class="form-control" name="password" placeholder="{{Lang::get('validation.password')}}" aria-label="{{Lang::get('validation.password')}}" aria-describedby="basic-addon2">

                            </div>
                        </div>
                        <div class="col-12 col-md-12">
                            <label for="img">ID Number img <span style="color: red"> (jpg,jpeg,png)<br />(must not increase 100KB)</span></label><br/>
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa user-icon"></i></span>
                                </div>
                                <input type="file" class="form-control" id="img"  placeholder="صورة رقم الهوية / الاقامة"  aria-label="صورة رقم الهوية / الاقامة" name="img_national_id"  required>

                            </div>
                        </div>
                        <div class="col-12 col-md-12">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa user-icon"></i></span>
                                </div>
                                <select class="form-control required select2" name="school" >
                                    <option value="">المدرسة</option>

                                    @if($schools)
                                        @foreach($schools as $sc)
                                            <option value="{{$sc->Id}}">{{ $sc->Name }}</option>
                                        @endforeach
                                    @endif

                                </select>

                            </div>
                        </div>
                        <!-- end col -->
						<div class="form-group">
                                 <label class="" style="font-size:12px; font-family:tahoma;padding: 8px;"><input type="checkbox" value="1"  name="birthdayrequired" > {{Lang::get('validation.terms')}}
								  </label>
                            </div>

						

                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-12 text-center">
                            <button class="btn btn-orange btn-block" type="submit">{{Lang::get('validation.signup')}}</button>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
 <div class="bottom-signup-desc text-center" style="font-size:13px;font-weight:bold">  kindly enter your details in Arabic
</div>
                </form>
            </div>
            <!-- end box form -->
            <div id="rev_slider_471_1_wrapper" dir="ltr" class="rev_slider_wrapper fullwidthbanner-container " data-alias="media-carousel-autoplay30" data-source="gallery" style="margin:0px auto;background-color:#26292b;padding:0px;margin-top:0px;margin-bottom:0px;">

                <!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
                <div id="rev_slider_471_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
                    <ul>
                        @foreach($slider as $sm)
                        <!-- SLIDE  -->
                        <li data-index="rs-{{$sm->id}}" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500"  data-rotate="0" data-saveperformance="off" data-title="" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="{{url('/public/photos/'.$sm->photo)}}" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption Video-Title   tp-resizeme" id="slide-{{$sm->id}}-layer-2" data-x="-300" data-y="200" data-voffset="50" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:bottom;rZ:90deg;sX:2;sY:2;","speed":1500,"to":"o:1;tO:-20% 50%;","delay":500,"ease":"Power3.easeInOut"},{"delay":6000,"speed":1000,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[5,5,5,5]" data-paddingbottom="[5,5,5,5]" data-paddingleft="[5,5,5,5]" style="z-index: 7; white-space: nowrap;text-transform:left;">{{$sm->title}}</div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption orange-Title  rtl   tp-resizeme" id="slide-{{$sm->id}}-layer-2" data-x="-300" data-y="280" data-voffset="10" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:bottom;rZ:90deg;sX:2;sY:2;","speed":1500,"to":"o:1;tO:-20% 50%;","delay":750,"ease":"Power3.easeInOut"},{"delay":7000,"speed":1000,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['right','right','right','right']" data-paddingtop="[5,5,5,5]" data-paddingright="[5,5,5,5]" data-paddingbottom="[5,5,5,5]" data-paddingleft="[5,5,5,5]" style="z-index: 8; white-space: nowrap;text-transform:left;"> <span class="big-t"></span>{!! nl2br($sm->content) !!}</div>
                        </li>
						@endforeach
                    </ul>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->

        </div>

        <!-- end slider -->
    </div>
    <!-- end container -->
    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 text-center">
                    <div class="about-box">
                        <h2 style=" min-height: 60px;"><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i>About DALLAH DRIVING Company </h2>
                        <div class="content">
                            <p>DALLAH Driving Co. is one of the leading companies in the Kingdom of Saudi Arabia having expertise in training drivers on various types of vehicles. Our training not only enhances their driving skills to higher levels, it also promotes a culture of awareness of, and respect for, traffic rules thereby resulting in a safe environment for all.
                                <div style="height: 10px;"></div>
                            Founded in 1975 in the Kingdom of Saudi Arabia with its first centre in Jeddah, the company continues to engage in coaching the Saudi cadres with the objective of creating a highly skilled and specialized pool of human resources which will cater to the labor market in Saudi Arabia with trained professionals in this field. 
 </p>
                        </div>
                        <a class="btn btn-orange left" href="{{url('en/about')}}">More</a>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-12 col-md-5 text-center">
                    <div class="about-box">
                        <h2 style=" min-height: 60px;"><i class="fa"><img src="{{url('/resources/views/site')}}/images/time-icon.png"></i> Dallah Office hours and Training hours  </h2>
                        <div class="content">
                            <strong>DALLAH Driving time for driving education in the normal days : </strong>
                            <p>From Sunday to Thursday - 07:00 to 21:00
                                <br>Weekend : friday and saturday</p>
                          
                        </div>
                       

                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->
    <div class="block-bg m-20 counter-row">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-3">

                    <div id="shiva" class="count-box">
                        <h3>Number of trainees</h3>

                        <span class="count">{{$options['hm_Features_box1_desc']}}</span>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-12 col-sm-3">

                    <div id="shiva" class="count-box">
                        <h3>Training hours</h3>
                        <span class="count">{{$options['hm_Features_box1_title_ar']}}</span>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-12 col-sm-3">

                    <div id="shiva" class="count-box">
                        <h3>Number of licenses for 2018</h3>
                        <span class="count">{{$options['hm_Features_box1_title']}}</span>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-12 col-sm-3">

                    <div id="shiva" class="count-box">
                        <h3>Years</h3>
                        <span class="count">{{$options['hm_Features_box1_desc_ar']}}</span>
                    </div>
                </div>
                <!-- end col -->

            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>

    <div class="video-slider">

        <div id="rev_slider_74_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="youtube-gallery70" style="margin:0px auto;background-color:#000000;padding:0px;margin-top:0px;margin-bottom:0px;">
            <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
            <div id="rev_slider_74_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
                <ul>
                    
                    <!-- SLIDE  -->

                    

                    <li data-index="rs-2614" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-thumb="{{url('/resources/views/site')}}/images/video-thumb2.jpg" data-rotate="0" data-saveperformance="off" data-title="" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{url('/resources/views/site')}}/images/video-slide1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- BACKGROUND VIDEO LAYER -->
                        <div class="rs-background-video-layer" data-forcerewind="on" data-volume="100" data-ytid="BLdyjuuwT1I" data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;volume=100&amp;hd=1&amp;wmode=opaque&amp;showinfo=0&amp;rel=0;" data-videorate="1" data-videowidth="100%" data-videoheight="100%" data-videocontrols="none" data-videoloop="none" data-forceCover="1" data-aspectratio="16:9" data-autoplay="true" data-autoplayonlyfirsttime="false" data-nextslideatend="true"></div>
                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption Video-SubTitle   tp-resizeme" id="slide-1613-layer-1" data-x="center" data-y="300" data-voffset="50" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:bottom;rZ:90deg;sX:2;sY:2;","speed":1500,"to":"o:1;tO:-20% 50%;","delay":500,"ease":"Power3.easeInOut"},{"delay":3000,"speed":1000,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[5,5,5,5]" data-paddingbottom="[5,5,5,5]" data-paddingleft="[5,5,5,5]" style="z-index: 9; white-space: nowrap;text-transform:left;"></div>

                        <!-- LAYER NR. 6 -->
                        <div class="tp-caption Video-Title   tp-resizeme" id="slide-1613-layer-2" data-x="0" data-y="bottom" data-voffset="10" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:bottom;rZ:90deg;sX:2;sY:2;","speed":1500,"to":"o:1;tO:-20% 50%;","delay":750,"ease":"Power3.easeInOut"},{"delay":2500,"speed":1000,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[5,5,5,5]" data-paddingbottom="[5,5,5,5]" data-paddingleft="[5,5,5,5]" style="z-index: 10; white-space: nowrap;text-transform:left;"></div>
                    </li>
                    <!-- SLIDE  -->

                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
        </div>
        <!-- END REVOLUTION SLIDER -->

    </div>

    <div class="news-block">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="title-orange">News</h2>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
				@foreach($news as $item)
                <div class="col-12 col-md-4 ">
                    <div class="news-box">
                        <div class="thumb"><img src="{{url('/public/photos/'.$item->photo)}}" alt=""></div>
                        <div class="content">
                            <h3 class="title">{{$item->title}}</h3>
                            <p>Date : {{date('Y/m/d',strtotime($item->created_at))}}</p>
                            <p> {{ str_limit(strip_tags($item->content), $limit = 150, $end = '...') }}</p>
                            <a class="btn left" href="{{url('en/news/'.$item->pageId)}}">more</a>
                        </div>
                    </div>
                </div>
                <!-- end col -->
				@endforeach
                
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
   
@stop
   <script>
   function validate(){
		if($('#d_type').prop("checked") == true){
			   $('#full_date').val('');
			   
		}else{
			$('#d_year').attr('onchange','return validate_date()');
			$('#d_month').attr('onchange','return validate_date()');
			$('#d_day').attr('onchange','return validate_date()');
		}
   }
	function validate_date(){
	var year=$('#d_year').val();
	var month=$('#d_month').val();
	var day=$('#d_day').val();
	// alert(year);
		$('#full_date').val(year+'-'+month+'-'+day);	
	
	}
       function get_gender()
       {
           var gender=$('#gender').val();
           if(gender=='female'){
               alert('التسجيل غير متاح للإناث حاليا');
               $('#sign').attr('disabled',true);
           }else{
               $('#sign').attr('disabled',false);

           }
       }
   </script>