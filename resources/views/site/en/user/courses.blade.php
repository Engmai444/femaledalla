@extends('site.en.master')
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">
                <div class="row">
				<br />
                    <h2 class="page_title">My Courses</h2>
				<br />	
                </div>
            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="profile-sidebar">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic text-center">
                                    @if(Auth::guard('doctor')->user()->img_national_id != '')
                                        <img src="{{url('/public/photos/thumb/'.Auth::guard('doctor')->user()->IDNumber.'/')}}/{{Auth::guard('doctor')->user()->img_national_id}}">
                                    @else
                                        <img src="{{url('/public/photos')}}/avatar.jpg">
                                    @endif
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle text-center">

                                    <div class="profile-usertitle-name">
                                        {{Auth::guard('doctor')->user()->IDNumber}}
                                    </div>
                                </div>
                                <!-- END SIDEBAR USER TITLE -->
                                <!-- SIDEBAR BUTTONS -->

                                
                            </div>
                        </div>
                        <!-- end col -->
                        <!-- Default form register -->
						<div class="col-md-8">
						<div class="aside">
                            <div class="profile-usermenu">
                                <a href="{{url('/course_start')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i>Register for course 30 hours </a>
                            </div>
                            <div class="profile-usermenu">
                                <a href="{{url('/course_start_wait')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i>Register for course 30 hours waiting list</a>
                            </div>
                            <div class="profile-usermenu">
                                <a href="{{url('/evaluation')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i> Reserve limit evaluation test</a>
                            </div>
                             <!--   <div class="profile-usermenu">
                                   <a href="{{url('/profile')}}" class="btn btn-primary ">
                                                <i class="fa fa-user" aria-hidden="true"></i> Edite profile </a>
                                </div>
								
								<div class="profile-usermenu">
                                   <a href="{{url('/mycourses')}}" class="btn btn-primary">
                                     <i class="fa fa-user" aria-hidden="true"></i> My Courses </a>
                                </div>
							
                                <div class="profile-userbuttons">
                                    <a href="{{url('/logout')}}" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout </a>
                                </div>-->
                                <!-- END SIDEBAR BUTTONS -->
                            </div>    
                       <div class="text-right border border-light  p-5">
                       
					   
							<table class="table table-borderd">
								<tr>
									<td>Course Name</td>
									<td>Course Start In</td>
									<td>Details</td>
									<td>Course Ref.No</td>
								</tr>
								@foreach($courses as $course)
								<tr>
									<td>{{$course->CourseName}}</td>
									<td>{{date('d-m-Y',strtotime($course->CourseStartIn))}}</td>
									<td><a href="{{url('/mycourses/'.$course->OrderId)}}">Details</a></td>
									<td>{{$course->CourseRefNo}}</td>
								</tr>
								@endforeach
							</table>
							
					   </div>
                       </div>
                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->

            </div>
            <!--/.about-block  -->

        </div>
        <!-- /.content -->
@stop