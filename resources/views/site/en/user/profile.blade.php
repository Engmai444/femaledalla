@extends('site.en.master')
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">
                <div class="row">
				<br />
                    <h2 class="page_title">Profile</h2>
				<br />	
                </div>
            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="profile-sidebar">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic text-center">
									@if(Auth::guard('doctor')->user()->avatar != '')
									<img src="{{url('/public/photos')}}/{{Auth::guard('doctor')->user()->avatar}}">
									@else
                                    <img src="{{url('/public/photos')}}/avatar.jpg">
									@endif
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle text-center">

                                    <div class="profile-usertitle-name">
									{{Auth::guard('doctor')->user()->FullName}}
                                    </div>
                                </div>
                                <!-- END SIDEBAR USER TITLE -->
                                <!-- SIDEBAR BUTTONS -->

                                
                            </div>
                        </div>
                        <!-- end col -->
                        <!-- Default form register -->
						<div class="col-md-8">
					<!-- SIDEBAR MENU -->
                    <div class="aside">
                        <div class="profile-usermenu">
                            <a href="{{url('/course_start')}}" class="btn btn-primary ">
                                <i class="fa fa-user" aria-hidden="true"></i>Register for course 30 hours </a>
                        </div>
                        <div class="profile-usermenu">
                            <a href="{{url('/course_start_wait')}}" class="btn btn-primary ">
                                <i class="fa fa-user" aria-hidden="true"></i>Register for course 30 hours waiting list</a>
                        </div>
                        <div class="profile-usermenu">
                            <a href="{{url('/evaluation')}}" class="btn btn-primary ">
                                <i class="fa fa-user" aria-hidden="true"></i> Reserve limit evaluation test</a>
                        </div>
                                <!--<div class="profile-usermenu">
                                   <a href="{{url('/profile')}}" class="btn btn-primary ">
                                                <i class="fa fa-user" aria-hidden="true"></i> Edite profile </a>
                                </div>
								
								<div class="profile-usermenu">
                                   <a href="{{url('/mycourses')}}" class="btn btn-primary">
                                     <i class="fa fa-user" aria-hidden="true"></i> My courses </a>
                                </div>
							
                                <div class="profile-userbuttons">
                                    <a href="{{url('/logout')}}" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> logout </a>
                                </div>-->
                                <!-- END SIDEBAR BUTTONS -->
                            </div>    
                                <!-- END MENU -->	
                       
{{ Form::open(array('url' => '/profile','role' => 'form','id' => 'sendform','class' => 'text-right border border-light  p-5','files'=> true)) }}
                            @if(!empty($orders))
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                                   Sorry tou can't edit your profile to registered course before
                                </div>


                            @endif
                            
                        @if(Session::get('success') != '')
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  
                        {{Session::get('success')}}
                        {{Session::forget('success')}}
                    </div>
                    @endif
                   
					@if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                      @foreach ($errors->all() as $error)
                            - {{ $error }} <br />
                      @endforeach
                      </div>
                    @endif   
                            <div class="form-row mb-4">
        <div class="col">
            <!-- First name -->
            <input type="text" name="FullName" class="form-control" required placeholder="Full Name" autocomplete="off" onkeypress="return CheckArabicCharactersOnly(event);" value="{{Auth::guard('doctor')->user()->FullName}}">
        </div>
      
    </div>
	<div class="form-row mb-4">
        
        <div class="col">
            <!-- Last name -->
            <input type="text" name="IDNumber" class="form-control"  placeholder="ID Number" value="{{Auth::guard('doctor')->user()->IDNumber}}">
        </div>
    </div>
	
	<div class="form-row mb-4">
		<div class="col">
		{{Form::select('NationalityId',$Nationality,Auth::guard('doctor')->user()->NationalityId,['class'=>'form-control','placeholder' => 'Nationality'])}}
		</div>
	</div>
	 <!-- photos -->
	<label>profile avatar</label>
	<input type="file" id="photos" name="file" class="form-control mb-4" placeholder="" aria-describedby="photos">

    <!-- E-mail -->
    <input type="email" name="email" value="{{Auth::guard('doctor')->user()->email}}" class="form-control mb-4" required placeholder="email" readonly>

    <!-- Phone number -->
    <input type="text" name="mobile" value="{{Auth::guard('doctor')->user()->mobile}}" class="form-control mb-4" required placeholder="Mobile" aria-describedby="">
	
	
	<h3>Change password</h3>
	<input type="text" name="oldpassword" value="" class="form-control mb-4" required placeholder="Old password" aria-describedby="">
	<!-- Phone number -->
    <input type="text" name="newpassword" value="" class="form-control mb-4" required placeholder="New password" aria-describedby="">
	<input type="text" name="renewpassword" value="" class="form-control mb-4" required placeholder="re-New password" aria-describedby="">
                            
                           
                            </div>
                            <!-- Sign up button -->
                            <button class="btn btn-info my-4 btn-block" @if(!empty($orders)) disabled @endif type="submit">Save</button>

                       {{ Form::close() }} 
                        <!-- Default form register -->
                        </div>
                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->

            </div>
            <!--/.about-block  -->

        </div>
        <!-- /.content -->
				    <script type="text/javascript">
     function CheckArabicCharactersOnly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8) {
                if ( unicode >= 65 && unicode <= 90 || unicode >= 97 && unicode <= 122)
                    return false;
                else {
                    if ((unicode < 48 || unicode > 57) && (unicode < 0x0600 || unicode > 0x06FF)) //if not a number or arabicalert(unicode);


                        return true;
                }
            }


        }	
		</script>
@stop