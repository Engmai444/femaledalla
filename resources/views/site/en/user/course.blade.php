@extends('site.en.master')
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">
                <div class="row">
				<br />
                    <h2 class="page_title">{{$course->name}}</h2>
				<br />	
                </div>
            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="profile-sidebar">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic text-center">
									@if(Auth::guard('doctor')->user()->avatar != '')
									<img src="{{url('/public/photos')}}/{{Auth::guard('doctor')->user()->avatar}}">
									@else
                                    <img src="{{url('/public/photos')}}/avatar.jpg">
									@endif
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle text-center">

                                    <div class="profile-usertitle-name">
									{{Auth::guard('doctor')->user()->FullName}}
                                    </div>
                                </div>
                                <!-- END SIDEBAR USER TITLE -->
                                <!-- SIDEBAR BUTTONS -->

                                <!-- SIDEBAR MENU -->
                                <div class="profile-usermenu">
                                   <a href="{{url('/profile')}}" class="btn btn-primary btn-block">
                                                <i class="fa fa-user" aria-hidden="true"></i> Edite profile</a>
                                </div>
								<br />
                                <div class="profile-userbuttons">
                                    <a href="{{url('/logout')}}" class="btn btn-danger btn-block"><i class="fa fa-sign-out" aria-hidden="true"></i>logout </a>
                                </div>
                                <!-- END SIDEBAR BUTTONS -->
                                <!-- END MENU -->
                            </div>
                        </div>
                        <!-- end col -->
                        <!-- Default form register -->
						
						
                       <div class="text-right border border-light col-md-8 p-5">
					   <span class="print">
                                            <a href="javascript:printDiv('couseinfo')"  class="btn btn-link"><i class="fa fa-lg fa-print"></i> Print</a>
                                        </span>
							@if($course->IsPaid=='0')
							  <span class="pay">
								<a href="{{url('payment_course/'.$course->OrderId)}}"  class="btn btn-link" style="background:green"><i class="fa fa-lg fa-paypal"></i>  Pay Online By Mada </a>
							</span>
							@endif									
										<div id="couseinfo">
							<table class="table table-borderd" >
                                <tr>
									<td>ID Number</td>
									<td>{{$course->IDNumber}}</td>
								</tr>
                                <tr>
									<td>Full Name</td>
									<td>{{$course->FullName}}</td>
								</tr>
                                <tr>
									<td>Nationality</td>
									<td>{{$course->nationality}}</td>
								</tr>
                                <tr>
									<td>Mobile</td>
									<td>{{$course->mobile}}</td>
								</tr>
                                <tr>
									<td>CourseRefNo</td>
									<td>{{$course->CourseRefNo}}</td>
								</tr>
								@if($course->IsPaid == 1)
									<tr>
										<td>Payment status</td>
										<td>Paid</td>
									</tr>
									<tr>
										<td>رقم عملية السداد</td>
										<td>{{$course->trans_no}}</td>
									</tr>
								@else
									<tr>
										<td>Payment status</td>
										<td>Unpaid</td>
									</tr>
								@endif
								<tr>
									<td>Order Id</td>
									<td>{{$course->OrderId}}</td>
								</tr>
								<tr>
									<td>Course Name</td>
									<td>{{$course->CourseName}}</td>
								</tr>
								<tr>
									<td>Register date</td>
									<td>{{date('d-m-Y',strtotime($course->created_at))}}</td>
								</tr>
								<tr>
									<td>CourseStartIn</td>
									<td>{{date('d-m-Y',strtotime($course->CourseStartIn))}}</td>
								</tr>
								<tr>
									<td>Days</td>
									<td>{{$course->days}} days</td>
								</tr>
								<tr>
									<td>Period</td>
									<td>{{$course->fromH}} to {{$course->toH}}</td>
								</tr>	
							</table>
				<?php
							$option = DB::table('site_option')->where('varname','hm_mycoursrs_text')->first();
							?>
							<div class="notealert">{{$option->value}}</div>		
							</div>
					   </div>

                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->

            </div>
            <!--/.about-block  -->

        </div>
        <!-- /.content -->
		<iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>
@stop