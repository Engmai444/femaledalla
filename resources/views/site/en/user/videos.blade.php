@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | Videos </title>
<meta name="Description" content="" />
@stop
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">
                <div class="row">
				<br />
                    <h2 class="page_title">Videos</h2>
				<br />	
                </div>
            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="profile-sidebar">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic text-center">
                                    @if(Auth::guard('doctor')->user()->img_national_id != '')
                                        <img src="{{url('/public/photos/thumb/'.Auth::guard('doctor')->user()->IDNumber.'/')}}/{{Auth::guard('doctor')->user()->img_national_id}}">
                                    @else
                                        <img src="{{url('/public/photos')}}/avatar.jpg">
                                    @endif
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle text-center">

                                    <div class="profile-usertitle-name">
                                        {{Auth::guard('doctor')->user()->IDNumber}}
                                    </div>
                                </div>
                                <!-- END SIDEBAR USER TITLE -->
                                <!-- SIDEBAR BUTTONS -->

                                
                            </div>
                        </div>
                        <!-- end col -->
                        <!-- Default form register -->
						<div class="col-md-10">
						<div class="aside">
                            <div class="profile-usermenu">
                                <a href="{{url('/course_start')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i>اRegister for course 30 hours </a>
                            </div>
                            <div class="profile-usermenu">
                                <a href="{{url('/course_start_wait')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i> Register for course 30 hours waiting list </a>
                            </div>
                            <div class="profile-usermenu">
                                <a href="{{url('/evaluation')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i> Reserve limit evaluation test </a>
                            </div>
                            <div class="profile-usermenu">
                                <a href="{{url('/testing')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i> Test </a>
                            </div>
                            <div class="profile-usermenu">
                                <a href="{{url('/videos')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i> Video </a>
                            </div>

                               <!-- <div class="profile-usermenu">
                                   <a href="{{url('/profile')}}" class="btn btn-primary ">
                                      <i class="fa fa-user" aria-hidden="true"></i> تعديل الملف الشخصي </a>
                                </div>
								
								<div class="profile-usermenu">
                                   <a href="{{url('/mycourses')}}" class="btn btn-primary">
                                     <i class="fa fa-user" aria-hidden="true"></i> دورات مشترك بها </a>
                                </div>
							
                                <div class="profile-userbuttons">
                                    <a href="{{url('/logout')}}" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> ﺗﺴﺠﻴﻞ اﻟﺨﺮﻭﺝ </a>
                                </div>-->
                                <!-- END SIDEBAR BUTTONS -->
                            </div>    
                       <div class="text-right border border-light  p-5">

                           @if(count($orders)>0)
                                        @foreach($orders as $or)
                                   <iframe width="400" height="215" src="{{url('/public/videos/'.$or->link_video)}}" frameborder="0" allow=" encrypted-media" allowfullscreen></iframe>


                               @endforeach

                           @else
                               <div class="alert alert-danger alert-dismissable">
                                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                   <i class="icon fa fa-ban"></i>
                                  You don't regidter any course now
                               </div>

                           @endif

					   </div>
                       </div>
                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->

            </div>
            <!--/.about-block  -->

        </div>

        <!-- /.content -->
@stop