@extends('site.en.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | Courses</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
   <div class="clearfix"></div>
    <div id="featured-title" class="parallax parallax-bg-1" style="background: url(images/slide-3.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">Dallah Courses</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 text-center pb-3 pt-5">
				<form method="get">
							<div class="row">
								<div class="col-md-3">
									{{Form::select('x',$applicationdefs,$applicationdef,['class'=>'form-control','placeholder'=> 'Course Name'])}}
								</div>
								<div class="col-md-3">
									{{Form::select('SchoolId',$schools,$SchoolId,['class'=>'form-control','placeholder'=> 'Location'])}}
								</div>
								<div class="col-md-1">
									Start In
								</div>
								<div class="col-md-2">
									<input type="text" name="date_from" value="{{$date_from}}" class="form-control datepicker" placeholder="from" />
								</div>
								<div class="col-md-2">
									<input type="text"  name="date_to"  value="{{$date_to}}" class="form-control datepicker" placeholder="to" />
								</div>
								<input type="hidden" name="filter" value="1" />
								<div class="col-md-1">
									<input type="submit" value="search" class="btn btn-primary" />
								</div>
							</div>
						</form>
						<br/>
						
						<div class="row">
						<div class="col-md-12 text-center">
						    

						            <h2 style="color:#d95621;padding:10px;">Explain how to register in courses</h2>
						    </div>
        						<div style="width:1200px;min-height:500px;clear:both; margin-bottom:25px;">
        						    
        						    <iframe width="100%" height="600" src="https://www.youtube.com/embed/cKhhyLAwCFQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        
        					
						</div>
						</div>
						<br/>
						<br/>

                    <div class="table-responsive">
                        <div class="alert alert-danger">The number of hours of the course is subject to change based on the test score of the level</div>
                        <form method="get" action="ajax/subsCours">
                            <div class="table-responsive">
                                <div class="alert alert-danger">عدد ساعات الدورة قابل للتغيير بناءا علي نتيجة اختبار تحديد المستوي </div>
                                @if(count($courses)>0)
                                <table class="table table-st1">
                                    <tr>
                                        <th class="orange-bg">
                                            <p>Course Name</p>
                                        </th>

                                        <th class="orange-bg">
                                            <p>Start course</p>
                                        </th>
                                        <th class="orange-bg">
                                            <p>End course</p>
                                        </th>
                                        <th class="orange-bg">
                                            <p>Period</p>
                                        </th>
                                        <th class="orange-bg">
                                            <p>School</p>
                                        </th>
                                        <th class="grey-bg">
                                            <p>Price</p>
                                        </th>
                                        <th class="grey-bg">
                                            <p>Tax</p>
                                        </th>
                                        <th class="orange-bg">
                                            <p>Total</p>
                                        </th>
                                        <th class="orange-bg">
                                            <p>Register</p>
                                        </th>
                                    </tr>

                                        @foreach($courses as $item)
                                            <?php $c=DB::table('orders')->where('CourseId',$item->id)->where('orders.IsPaid','1')->count(); ?>

                                            @if($c < $item->seats)
                                                <tr>

                                                    <td>
                                                        <p>{{$item->CourseName}}
                                                        <h6>{{$item->CourseRefNo}}</h6>
                                                        </p>
                                                    </td>
                                                    <!-- <td> -->
                                                <!-- <p>{{$item->categoryName}}</p> -->
                                                    <!-- </td> -->
                                                    <td>
                                                        <p>{{$item->CourseStartIn}}</p>
                                                    </td>
                                                    <td>
                                                        <p>{{$item->CourseEndIn}}</p>
                                                    </td>
                                                    <td>
                                                        <p>{{$item->fromH}} الى {{$item->toH}}</p>
                                                    </td>
                                                    <td>
                                                        <p>{{$item->SchoolName}}</p>
                                                    </td>
                                                    <td>
                                                        <p>{{$item->price}} ريال</p>
                                                    </td>
                                                    <td>
                                                        <p>{{$item->tax}} ريال</p>
                                                    </td>
                                                    <td>
                                                        <p>{{ $item->price + $item->tax}} ريال</p>
                                                    </td>
                                                    <td>
                                                        <p><button class="btn btn-orange btn-large subscours" id="submitbutton{{$item->id}}" onclick="document.getElementById('submitbutton{{$item->id}}').disabled = true;document.getElementById('submitbutton{{$item->id}}').style.opacity='0.5';"  course-id="{{$item->id}}" style="font-weight: 500;">تسجيل</button></p>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach


                                </table>
                                @else

                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i>
                                       don't exist course now
                                    </div>
                                @endif
                            </div>
                        </form>
					
					


                    </div>
                </div>

                <!-- end col -->
            </div>
            <!-- end row -->
            <!--
               <div class="row">
                 <div class="col-12 text-center">
            
                </div>
                -->
               <!-- end col -->
               </div>
               <!-- end row -->

        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->

   
@stop