@extends('site.en.master')
@section('content')


<div class="content container-fluid read-bg">
  <div class="container">
  <div class="row">
    <div class="page_title">Login</div>
  </div>
</div>
</div>
<!-- End slider -->
<div class="content container-fluid">
  <div class="reada-block">
  <div class="container">
    <div class="row">
 <!-- Default form register -->

<form class="text-center border border-light col-md-8 p-5" id="login">
 <div class="msg"></div>
    <!-- E-mail -->
    <input type="email" name="email" class="form-control mb-4" required placeholder="ID Number">

	
	<input type="password" name="password" class="form-control mb-4" required placeholder="password">

    <!-- Sign up button -->
    <button class="btn btn-info my-4 btn-block" type="submit">Login</button> 

</form>
<a href="{{url('/resetpassword')}}">reset password?</a>
<!-- Default form register -->

    </div><!-- /.serv -->
</div><!-- /.container -->

</div><!--/.about-block  -->

</div><!-- /.content -->
@stop