@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | Choose Evaluation date </title>
<meta name="Description" content="" />
@stop
@section('content')

        <div class="content container-fluid read-bg">
            <div class="container">
                <div class="row">
				<br />
                    <h2 class="page_title">Choose Evaluation date</h2>
				<br />	
                </div>
            </div>
        </div>
        <!-- End slider -->
        <div class="content container-fluid">
            <div class="reada-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="profile-sidebar">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic text-center">
                                    @if(Auth::guard('doctor')->user()->img_national_id != '')
                                        <img src="{{url('/public/photos/thumb/'.Auth::guard('doctor')->user()->IDNumber.'/')}}/{{Auth::guard('doctor')->user()->img_national_id}}">
                                    @else
                                        <img src="{{url('/public/photos')}}/avatar.jpg">
                                    @endif
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle text-center">

                                    <div class="profile-usertitle-name">
                                        {{Auth::guard('doctor')->user()->IDNumber}}
                                    </div>
                                </div>
                                <!-- END SIDEBAR USER TITLE -->
                                <!-- SIDEBAR BUTTONS -->

                                
                            </div>
                        </div>
                        <!-- end col -->
                        <!-- Default form register -->
						<div class="col-md-8">
						<div class="aside">
                            <div class="profile-usermenu">
                                <a href="{{url('/course_start')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i>Register for course 30 hours </a>
                            </div>
                            <div class="profile-usermenu">
                                <a href="{{url('/course_start_wait')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i>Register for course 30 hours waiting list</a>
                            </div>
                            <div class="profile-usermenu">
                                <a href="{{url('/evaluation')}}" class="btn btn-primary ">
                                    <i class="fa fa-user" aria-hidden="true"></i> Reserve limit evaluation test</a>
                            </div>
                               <!-- <div class="profile-usermenu">
                                   <a href="{{url('/profile')}}" class="btn btn-primary ">
                                                <i class="fa fa-user" aria-hidden="true"></i> Edite profile </a>
                                </div>
								
								<div class="profile-usermenu">
                                   <a href="{{url('/mycourses')}}" class="btn btn-primary">
                                     <i class="fa fa-user" aria-hidden="true"></i> My Courses </a>
                                </div>
							
                                <div class="profile-userbuttons">
                                    <a href="{{url('/logout')}}" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout </a>
                                </div>-->
                                <!-- END SIDEBAR BUTTONS -->
                            </div>    
                       <div class="text-right border border-light  p-5">
                           <label>Evaluation shool</label>
                       <select name="school_id" class="form-control" onchange="return get_time(this.value)">
                           <option value="">choose</option>
                           @foreach($schools as $sc):
                               <option value="{{$sc->Id}}">{{$sc->Name}}</option>
                           @endforeach
                       </select>
<br />
                           <table class="table table-borderd" id="table4">
                               <thead>
                               <tr>
                                   <td>school Name</td>
                                   <td>Day</td>
                                   <td>From/to</td>
                                   <td>Reserve</td>
                               </tr>
                               </thead>
                               <tbody>

                               </tbody>

                           </table>
							
					   </div>
                       </div>
                    </div>
                    <!-- /.serv -->
                </div>
                <!-- /.container -->

            </div>
            <!--/.about-block  -->

        </div>
        <!-- /.content -->

    <script>
        function get_time(id) {
            $('#table4 tbody').empty();
            $.ajax({
                url: '{{url('get_time')}}',
                type: 'GET',
                dataType: 'json',
                success: 'success',
                data: {'id': id},
                success: function (result) {
                    if(result.result !='') {
                        $('#time').empty();
                        $.each(result.result, function (i, index) {
                            $('#time').append('<option value="' + index.id + '">' + index.from_time + ' إلى ' + index.to_time + '</option>');
                        });
                    }else{
                        $('#time').empty();

                    }
                }
            })
        }

        $(document).ready(function() {

            $('#day').datepicker({
                startDate: new Date(2021,8,19),
                format: 'yyyy-mm-dd',
                daysOfWeekDisabled: [5,6],
                autoclose: true


            });

        });
    </script>
@stop