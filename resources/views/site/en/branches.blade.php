<?php
$branches = App\Http\Controllers\SiteController::branches();
?>
@extends('site.en.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | Branches</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
  <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-3.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">Our branches</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 text-center pb-3 pt-5">
                    <div class="table-responsive">
                     
            <table class="table table-st1">
                          <tr>
                            <th  class="orange-bg"><p align="center" dir="RTL">name </p></th>
                            <th  class="orange-bg"><p align="center" dir="RTL">City</p></th>
                            <th  class="orange-bg"><p align="center" dir="RTL">Phone</p></th>
							<th  class="orange-bg"><p align="center" dir="RTL">Map</p></th>
                          </tr>
						  @foreach($branches as $branch)
                          <tr>
                            <td ><p dir="RTL">{{$branch->title}}</p></td>
                            <td ><p align="center" dir="RTL">{{$branch->address}}</p></td>
                            <td><p dir="RTL"><i class="fa fa-phone"></i> {{$branch->phone}}</p></td>
							<td><p dir="RTL"><a href="https://www.google.com/maps/?q={{$branch->lat}},{{$branch->lng}}" target="_blank"  class="btn btn-primary">Open Map</a></p></td>
                          </tr>
                          @endforeach
                      </table>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->
@stop