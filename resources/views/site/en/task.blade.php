
@extends('site.master')
@section('seo')
<title>دالة لتعليم القيادة</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
   <div class="clearfix"></div>
    <div id="featured-title" class="parallax parallax-bg-1" style="background: url(images/slide-3.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">ﺩﻭﺭاﺕ ﻣﺮﻛﺰ ﺩﻟﻪ</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 text-center pb-3 pt-5">
                    <div class="table-responsive">
                        <table class="table table-st1">
                            <tr>
                                <th class="orange-bg">
                                    <p>اﺳﻢ اﻟﺪﻭﺭﺓ</p>
                                </th>
                                <th class="grey-bg">
                                    <p>اﻟﻔﺌﺔ</p>
                                </th>
                                <th class="orange-bg">
                                    <p>اﻟﻤﺪﺓ</p>
                                </th>
                                <th class="grey-bg">
                                    <p>ﺳﻌﺮ اﻟﺪﻭﺭﺓ</p>
                                </th>
                                <th class="orange-bg">
                                    <p>اﻟﺴﻌﺮ اﻻﺟﻤﺎﻟﻲ</p>
                                </th>
								<th class="orange-bg">
                                    <p>أشترك</p>
                                </th>
                            </tr>
							@foreach($courses as $item)
                            <tr>
                                <td>
                                    <p>{{$item->CourseName}}</p>
                                </td>
                                <td>
                                    <p>{{$item->categoryName}}</p>
                                </td>
                                <td>
                                    <p>{{$item->days}} اﻳﺎﻡ</p>
                                </td>
                                <td>
                                    <p>{{$item->price}} ريال</p>
                                </td>
                                <td>
                                    <p>{{ $item->price + $item->tax}} ريال</p>
                                </td>
								<td>
                                    <p><a class="btn btn-orange btn-large subscours" href="#" course-id="{{$item->id}}" style="font-weight: 500;">إشترك</a></p>
                                </td>
                            </tr>
                           @endforeach
                            

                        </table>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
               <div class="row">
                 <div class="col-12 text-center">
            
                </div>
               <!-- end col -->
               </div>
               <!-- end row -->

        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->

    <div class="map-block">
        <div class="row">
            <div class="col-12 g-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d927764.3550487261!2d46.26206159131756!3d24.724150391445495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f03890d489399%3A0xba974d1c98e79fd5!2sRiyadh+Saudi+Arabia!5e0!3m2!1sen!2seg!4v1514188298001" width="600" height="435" frameborder="0" style="border:0" allowfullscreen=""></iframe>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
@stop