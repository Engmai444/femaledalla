<?php

$branches = App\Http\Controllers\SiteController::branches();
$Nationality = App\Http\Controllers\CustomerController::Nationality();

$options = App\Http\Controllers\SiteController::site_option();
?>
<!doctype html>
<html class="no-js" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
	@section('seo')
    <title>{{Lang::get('menu.sitename')}}</title>
	@show
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <!-- Theme styles and Menu styles -->
    <link href="{{url('/resources/views/site')}}/css/animate.min.css" rel="stylesheet">
    <link href="{{url('/resources/views/site')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url('/resources/views/site')}}/css/common.css" rel="stylesheet" type="text/css">
    <link href="{{url('/resources/views/site')}}/css/style-ar.css" rel="stylesheet" type="text/css">
	<link href="{{url('/resources/views/site')}}/css/style.css" rel="stylesheet" type="text/css">
    <!-- Call Fontawsome Icon Font from a CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="{{url('/resources/views/site')}}/js/revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="{{url('/resources/views/site')}}/js/revolution/css/layers.css">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="{{url('/resources/views/site')}}/js/revolution/css/navigation.css">
    <!--[if lt IE 9]>
      <script type="text/javascript" src="{{url('/resources/views/site')}}/js/html5.js"></script>
      <![endif]-->
      <link href="{{url('/resources/views/site')}}/css/venobox.css" rel="stylesheet" type="text/css" />
    <!--Modernizer Custom -->
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/modernizr.custom.48287.js"></script>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
	<script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- Fav icons -->
    <link rel="shortcut icon" href="{{url('/resources/views/site')}}/favicon.png">
	@section('header')
  
    @show
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	
</head>

<body class="">
    <div id="page-loader">
        <div class="loader-img">
            <img src="{{url('/resources/views/site')}}/images/logo.png" alt="">
        </div>
    </div>

    <!-- Header Start -->
    <div id="header" class="clearfix">
        <header class="top-head">
            <div class="bg">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 text-right ">

                        </div>
                        <!-- end col -->
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                    <div class="row ">
                        <div class="logo col-12 col-sm-12 col-md-4  hidden1 ">
                            <a class="nav-link" href="{{url('/')}}"><img src="{{url('/resources/views/site')}}/images/logo.png" alt=""></a>
                        </div>
                        <!-- end col -->
                        <div class="col-12 col-sm-12 col-md-8 ">
                            <div class="top-links">
                                <ul>
									@if (Auth::guard('doctor')->check())
<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa user-icon"></i> welcome ,{{Auth::guard('doctor')->user()->FullName}}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
           <!-- <a class="dropdown-item" href="{{url('course_start')}}">Register Course 30 hours</a>-->

            <a class="dropdown-item" href="{{url('evaluation')}}">Evaluation test</a>
          <a class="dropdown-item" href="{{url('profile')}}">Profile</a>
         <!-- <a class="dropdown-item" href="{{url('mycourses')}}">My courses</a>-->
          <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
        </div>
      </li>
@else
                                    <li><a href="#" data-toggle="modal" data-target="#register-model"><i class="fa key-icon"></i> {{Lang::get('validation.signup')}} </a> </li>
                                    <li><a href="#" data-toggle="modal" data-target="#login-model"><i class="fa lock-icon"></i> {{Lang::get('validation.login')}} </a> </li>
									@endif
                                    <li>
                                        <div id="social" class="social ">
                                            <p style="display: inline-block;">Follow us</p>
                                    @if(!empty($options['social_facebook'])) <a class="facebookBtn smGlobalBtn" target="_blank" href="{{$options['social_facebook']}}"></a>@endif
                                           @if(!empty($options['social_twitter'])) <a class="twitterBtn smGlobalBtn" target="_blank" href="{{$options['social_twitter']}}"></a>@endif
                                           @if(!empty($options['social_googleplus']))<a class="googleplusBtn smGlobalBtn" target="_blank" href="{{$options['social_googleplus']}}"></a>@endif
                                           @if(!empty($options['social_youtube'])) <a class="youtubeBtn smGlobalBtn" target="_blank" href="{{$options['social_youtube']}}"></a>@endif
                                            @if(!empty($options['social_instagram'])) <a  target="_blank" href="{{$options['social_instagram']}}"><img style="vertical-align: bottom;" width=45px height=45px src="{{url('/resources/views/site')}}/images/download.jpg"> </a>@endif

                                        </div>
                                    </li>
                                    <li>
                                        <ul class="lang">
                                            <li class="lang-title">Language</li>
                                            <li>
                                                <a href="{{url('/')}}"><img src="{{url('/resources/views/site')}}/images/ar.png"></a>
                                            </li>
                                            <li>
                                                <a href="{{url('/en')}}"><img src="{{url('/resources/views/site')}}/images/en.png"></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container -->
            </div>
            <div class=" top-menu">
                <!-- top navigation menu start -->
                <div class="container">
                    <nav class="navbar navbar-expand-lg  top-nav" id="mainNav">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">{{Lang::get('menu.menu')}}</button>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav">
                                <li> <a class="nav-link selected" href="{{url('/en')}}"> {{Lang::get('menu.home')}}</a></li>
                                <li> <a class="nav-link" href="{{url('en/about')}}">{{Lang::get('menu.about')}}</a> </li>
                                <li> <a class="nav-link" href="{{url('en/services')}}">{{Lang::get('menu.services')}}</a></li>
                                <li> <a class="nav-link" href="{{url('en/times')}}"> {{Lang::get('menu.times')}}</a></li>
                                <li> <a class="nav-link" href="{{url('en/news')}}"> {{Lang::get('menu.news')}}</a></li>
                              <!--  <li> <a class="nav-link" href="{{url('en/courses')}}"> {{Lang::get('menu.courses')}}</a></li>-->
								<!--<li> <a class="nav-link" href="{{url('en/testing')}}">{{Lang::get('menu.testing')}}</a> </li>-->
                                <li> <a class="nav-link" href="{{url('en/branches')}}">{{Lang::get('menu.branches')}}</a></li>
                                <li> <a class="nav-link" href="{{url('en/gallery')}}"> {{Lang::get('menu.gallery')}}</a></li>
                                <li> <a class="nav-link" href="{{url('en/faq')}}"> {{Lang::get('menu.faq')}}</a></li>
                                <li> <a class="nav-link" href="{{url('en/contact')}}">{{Lang::get('menu.contact')}}</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <!-- end container -->
            </div>
            <!-- end col -->
        </header>
    </div>
    <!-- end header -->
    <div class="clearfix"></div>
	
	@yield('content')
<div class="map-block">
        <div class="row">
            <div class="col-12 g-map">
                <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwdyy0YxcKKRAZ0_74_UxlnoS54nCKmcc&callback=initMap">
    </script>
  	<script>

      function initMap() {
        var myLatLng = {lat: 24.750566, lng: 46.6357844};
		var icon = "{{url('resources/views/site/images/about-icon.png')}}";
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 6,
          center: myLatLng
        });
		@foreach($branches as $mp)
        var marker{{$mp->id}} = new google.maps.Marker({
          position: {lat: {{$mp->lat}}, lng: {{$mp->lng}} },
          map: map,
		  icon : icon,
          title: '{{$mp->title}}'
        });
		@endforeach
      }
    </script>

                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1078.7933664277555!2d46.63512037790436!3d24.750665197982272!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2ee2f4a83d27b7%3A0x321991f7c1f3e68a!2sDallah%20Driving%20School!5e0!3m2!1sen!2seg!4v1627984505319!5m2!1sen!2seg" width="100%" height="420" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <footer class="footer">
        <div class="container f-border">
            <div class="row align-items-center justify-content-center" style="display:none;">
                <div class="col-12  col-md-6 col-lg-3  p-0 d-flex align-items-center" >
                    <h3 style="margin: 0;">{{Lang::get('menu.mailsubs')}} </h3>
                </div>
                <!-- end col -->
                <div class="col-12  col-md-6 col-lg-4  newsletter p-0">
                    <div class="input-group">
                        <input type="email" class="form-control" placeholder="{{Lang::get('validation.email')}}">
                        <span class="input-group-btn">
                        <button class="btn btn-theme" type="submit">{{Lang::get('validation.signup')}}</button>
                        </span>
                    </div>
                </div>
                <!-- end col -->

            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12  col-md-12 text-center">
                    <ul class="f-nav">
                        <li> <a class="nav-link selected" href="{{url('/')}}"> {{Lang::get('menu.home')}}</a></li>
						<li> <a class="nav-link" href="{{url('/about')}}">{{Lang::get('menu.about')}}</a> </li>
						<li> <a class="nav-link" href="{{url('/services')}}">{{Lang::get('menu.services')}}</a></li>
						<li> <a class="nav-link" href="{{url('/times')}}"> {{Lang::get('menu.times')}}</a></li>
						<li> <a class="nav-link" href="{{url('/news')}}"> {{Lang::get('menu.news')}}</a></li>
						<!--<li> <a class="nav-link" href="{{url('/courses')}}"> {{Lang::get('menu.courses')}}</a></li>-->
						<!--<li> <a class="nav-link" href="{{url('/testing')}}">{{Lang::get('menu.testing')}}</a> </li>-->
						<li> <a class="nav-link" href="{{url('/branches')}}">{{Lang::get('menu.branches')}}</a></li>
						<li> <a class="nav-link" href="{{url('/gallery')}}"> {{Lang::get('menu.gallery')}}</a></li>
						<li> <a class="nav-link" href="{{url('/faq')}}"> {{Lang::get('menu.faq')}}</a></li>
						<li> <a class="nav-link" href="{{url('/contact')}}">{{Lang::get('menu.contact')}}</a></li>
                    </ul>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

        </div>
        <!-- end container -->
        <div class="bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6 pay-icons" style="display:none;">
                        <img src="{{url('/resources/views/site')}}/images/pay-icons.png">
                    </div>
                    <div class="col-12 col-md-6 text-left" >
                        <p>Powered by <a href="https://is.net.sa" target="_blank">INTERNeT SOLUTIONS</a> </p>
                    </div>
                    <!-- end col -->
                    <div class="col-12 col-md-6  copy-right text-right">
                        <!--<p>{{Lang::get('validation.copyrights')}} </p>-->
                         <p>Dallah Driving © All rights reserved 2020|2021</p>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
    </footer>

    <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up" ></i></a>
    <!-- basic modal -->
    <div class="modal fade" id="login-model" tabindex="-1" role="dialog" aria-labelledby="login-model">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">{{Lang::get('validation.login')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-8 col-xs-8">
						<form  id="login">
							<div class="msg"></div>
                            <div class="form-group">
                                <label for="email">{{Lang::get('validation.IDNumber')}} :</label>
                                <input type="text" class="form-control required" name="email">
                            </div>
                            <div class="form-group">
                                <label for="pwd">{{Lang::get('validation.password')}} :</label>
                                <input type="password" class="form-control required" name="password" >
                            </div>
                            <div class="form-group">
                                <button type="submit" onclick="" class="btn btn-block">{{Lang::get('validation.login')}}</button>
                            </div>
							<a href="{{url('en/resetpassword')}}">Forgot password ?</a>
						</form>	
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <img src="{{url('/resources/views/site')}}/images/lock-big.png" style="width:100%; height:auto;">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- basic modal -->
    <div class="modal fade" id="register-model" tabindex="-1" role="dialog" aria-labelledby="register-model">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">{{Lang::get('validation.signup')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-9 col-xs-8">

							<div class="msg"></div>
							<form  id="signup">
							    <div class="form-group">
                                <label for="user1">{{Lang::get('validation.IDNumber')}}:</label>
                                <input type="text" class="form-control required" name="IDNumber" maxlength="10" >
                            </div>
                            <div class="form-group">
                                <label for="user1">{{Lang::get('validation.name')}}:</label>
                                <input type="text" class="form-control required" name="FullName" autocomplete="off" onkeypress="return CheckArabicCharactersOnly(event);">
                            </div>
							
							<label for="user1">{{Lang::get('validation.dob')}}:</label>
								
							<div class="form-group">
                               
							<label><input type="checkbox" value="1" name="d_type" class="swidate d_type">Hijri ?</label>

								
								<div class="datebox" style="float: right;">
                                <select name="d_day" required>
									<option value="">Day</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
								</select>
								<select name="d_month" required>
									<option value="">Month</option>
									<option value='1'>Janaury</option>
									<option value='2'>February</option>
									<option value='3'>March</option>
									<option value='4'>April</option>
									<option value='5'>May</option>
									<option value='6'>June</option>
									<option value='7'>July</option>
									<option value='8'>August</option>
									<option value='9'>September</option>
									<option value='10'>October</option>
									<option value='11'>November</option>
									<option value='12'>December</option>
								</select>
								<select name="d_year" required>
									<option value="">year</option>
									<option value="2002">2006</option>
									<option value="2002">2002</option>
									<option value="2001">2001</option>
									<option value="2000">2000</option>
									<option value="1999">1999</option>
									<option value="1998">1998</option>
									<option value="1997">1997</option>
									<option value="1996">1996</option>
									<option value="1995">1995</option>
									<option value="1994">1994</option>
									<option value="1993">1993</option>
									<option value="1992">1992</option>
									<option value="1991">1991</option>
									<option value="1990">1990</option>
									<option value="1989">1989</option>
									<option value="1988">1988</option>
									<option value="1987">1987</option>
									<option value="1986">1986</option>
									<option value="1985">1985</option>
									<option value="1984">1984</option>
									<option value="1983">1983</option>
									<option value="1982">1982</option>
									<option value="1981">1981</option>
									<option value="1980">1980</option>
									<option value="1979">1979</option>
									<option value="1978">1978</option>
									<option value="1977">1977</option>
									<option value="1976">1976</option>
									<option value="1975">1975</option>
									<option value="1974">1974</option>
									<option value="1973">1973</option>
									<option value="1972">1972</option>
									<option value="1971">1971</option>
									<option value="1970">1970</option>
									<option value="1969">1969</option>
									<option value="1968">1968</option>
									<option value="1967">1967</option>
									<option value="1966">1966</option>
									<option value="1965">1965</option>
									<option value="1964">1964</option>
									<option value="1963">1963</option>
									<option value="1962">1962</option>
									<option value="1961">1961</option>
									<option value="1960">1960</option>
									<option value="1959">1959</option>
									<option value="1958">1958</option>
									<option value="1957">1957</option>
									<option value="1956">1956</option>
									<option value="1955">1955</option>
									<option value="1954">1954</option>
									<option value="1953">1953</option>
									<option value="1952">1952</option>
									<option value="1951">1951</option>
									<option value="1950">1950</option>
									<option value="1949">1949</option>
									<option value="1948">1948</option>
									<option value="1947">1947</option>
									<option value="1946">1946</option>
									<option value="1945">1945</option>
									<option value="1944">1944</option>
									<option value="1943">1943</option>
									<option value="1942">1942</option>
									<option value="1941">1941</option>
									<option value="1940">1940</option>
								</select>
								</div>
                            </div>
							<div class="form-group" style="clear: both;">
                                <label for="user1">{{Lang::get('validation.Nationality')}}:</label>
                                <?php $nat_setting=DB::table('site_option')->where('varname','nationality_all')->first();?>

                                <select class="form-control required " name="NationalityId">
									<option value="">{{Lang::get('validation.Nationality')}}</option>
                                    @if($nat_setting->value=='1')
                                        @foreach($Nationality as $it)
                                            <option value="{{$it->Id}}">{{$it->Name}}</option>
                                        @endforeach
                                    @else
                                        <option value="129">Saudi</option>

                                    @endif
								</select>
                            </div>
                                <div class="form-group">
                                    <label for="user1">sex :</label>
                                        <?php $msg_setting=DB::table('site_option')->where('varname','msg_female')->first();?>
                                    <?php $gen=DB::table('site_option')->where('varname','gender')->first();?>

                                    <select class="form-control required " name="gender" id="gender"  @if(@$msg_setting->value=='0') onchange="return get_gender()" @endif>
                                            <option value="">{{Lang::get('validation.gender')}}</option>
                                        @if($gen->value=='0')
                                            <option value="female">{{ lang::get('validation.female') }}</option>
                                        @elseif($gen->value=='1')
                                            <option value="male">{{ lang::get('validation.male') }}</option>
                                        @else
                                            <option value="male">{{ lang::get('validation.male') }}</option>
                                            <option value="female">{{ lang::get('validation.female') }}</option>

                                        @endif
                                        </select>

                                    </div>							
							<div class="form-group">
                                <label for="user1">{{Lang::get('validation.phone')}} :</label>
                                <input type="text" class="form-control required" name="mobile" maxlength="10">
                            </div>
                            <div class="form-group">
                                <label for="email">{{Lang::get('validation.email')}}:</label>
                                <input type="text" class="form-control required" name="email" >
                            </div>
                            <div class="form-group">
                                <label for="pwd">{{Lang::get('validation.password')}} :</label>
                                <input type="password" class="form-control required" name="password">
                            </div>
                                <div class="form-group">
                                    <label for="pwd">ID Number img (jpg,jpeg,png) (must not increase 100KB):</label>

                                    <input type="file" class="form-control" id="img"  placeholder="صورة رقم الهوية / الاقامة"  aria-label="صورة رقم الهوية / الاقامة" name="img_national_id"  required>

                                </div>
                                <?php $schools = DB::table('schools')->where('active',0)->get();?>

                                <div class="form-group">
                                    <label>school</label>
                                    <select class="form-control required " name="school" required >
                                        <option value="">choose</option>

                                        @if($schools)
                                            @foreach($schools as $sc)
                                                <option value="{{$sc->Id}}">{{ $sc->Name }}</option>
                                            @endforeach
                                        @endif

                                    </select>


                                </div>
							<div class="form-group">
                                 <label class="" style="font-size:12px; font-family:tahoma;"><input type="checkbox" value="1"  name="birthdayrequired" > {{Lang::get('validation.terms')}}
								  </label>
                            </div>
                            <div class="form-group">
                                <button type="submit" onclick="" class="btn btn-block">{{Lang::get('validation.signup')}} </button>
                            </div>
							</form>	
                        </div>
                        <div class="col-md-3 col-xs-4">
                            <img src="{{url('/resources/views/site')}}/images/lock-big.png" style="width:100%; height:auto;">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end model -->

    <script src="{{url('/resources/views/site')}}/js/jquery.min.js"></script>
    <script src="{{url('/resources/views/site')}}/js/bootstrap.bundle.min.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/jquery.themepunch.revolution.min.js"></script>
  <script  src="{{url('/resources/views/site')}}/js/venobox.min.js"></script> 
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="{{url('/resources/views/site')}}/js/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="{{url('/resources/views/site')}}/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="{{url('/resources/views/site')}}/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
    <script type="text/javascript">
	     function CheckArabicCharactersOnly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8) {
                if (unicode >= 65 && unicode <= 90 || unicode >= 97 && unicode <= 122)
                    return false;
                else {
                    if ((unicode < 48 || unicode > 57) && (unicode < 0x0600 || unicode > 0x06FF)) //if not a number or arabicalert(unicode);


                        return true;
                }
            }


        }
	var baseurl = "{{url('/')}}";

	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
	</script>
    <script src="{{url('/resources/views/site')}}/js/script.js"></script>
	@section('footer')
   
	@show
</body>

</html>