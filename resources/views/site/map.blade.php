<?php

$branches = App\Http\Controllers\SiteController::branches();

$Nationality = App\Http\Controllers\CustomerController::Nationality();

$options = App\Http\Controllers\SiteController::site_option();
?>
<!doctype html>
<html class="no-js" lang="ar">

<head>
    <meta charset="utf-8">
    
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <!-- Theme styles and Menu styles -->
    <link href="{{url('/resources/views/site')}}/css/animate.min.css" rel="stylesheet">
    <link href="{{url('/resources/views/site')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url('/resources/views/site')}}/css/common.css" rel="stylesheet" type="text/css">
    <link href="{{url('/resources/views/site')}}/css/style-ar.css" rel="stylesheet" type="text/css">
    <!-- Call Fontawsome Icon Font from a CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="{{url('/resources/views/site')}}/js/revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="{{url('/resources/views/site')}}/js/revolution/css/layers.css">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="{{url('/resources/views/site')}}/js/revolution/css/navigation.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css" />
    <!--[if lt IE 9]>
      <script type="text/javascript" src="{{url('/resources/views/site')}}/js/html5.js"></script>
      <![endif]-->
      <link href="{{url('/resources/views/site')}}/css/venobox.css" rel="stylesheet" type="text/css" />
    <!--Modernizer Custom -->
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/modernizr.custom.48287.js"></script>
    <!-- Fav icons -->
    <link rel="shortcut icon" href="{{url('/resources/views/site')}}/favicon.png">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
	<script src='https://www.google.com/recaptcha/api.js?hl=ar'></script>
	@section('seo')
		
    @show
	@section('header')
  
    @show
	<meta name="csrf-token" content="{{ csrf_token() }}">
	    <style type="text/css">
        .map-full {
    position: fixed !important;
    width: 100%;
    height: 100%;
}
    </style>
	
</head>

<body class="rtl">
  

<div class="map-block"  style="width:100%; height:100%;     position: fixed;" >
        <div class="row">
            <div class="col-12 g-map">
                
                <a href="{{url('/map')}}" target=_blank></a>
                <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwdyy0YxcKKRAZ0_74_UxlnoS54nCKmcc&callback=initMap">
    </script>
    <script>

      function initMap() {
        var myLatLng = {lat: 24.750566, lng: 46.6357844};
        var icon = "{{url('resources/views/site/images/about-icon.png')}}";
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 6,
          center: myLatLng
        });
        @foreach($branches as $mp)
        var marker{{$mp->id}} = new google.maps.Marker({
          position: {lat: {{$mp->lat}}, lng: {{$mp->lng}} },
          map: map,
          icon : icon,
          title: '{{$mp->title_ar}}',
          url : 'https://www.google.com/maps/?q={{$mp->lat}},{{$mp->lng}}'
        });
        google.maps.event.addListener(marker{{$mp->id}}, 'click', function() {
            window.location.href = this.url;
        });
        @endforeach
        
        
        
        

      }
    </script>

                <div id="map_canvas" class="map-full" style="width:100%; height:100%;     position: fixed;" ></div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    
    
    <script src="{{url('/resources/views/site')}}/js/jquery.min.js"></script>
    <script src="{{url('/resources/views/site')}}/js/bootstrap.bundle.min.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/jquery.themepunch.revolution.min.js"></script>
  <script  src="{{url('/resources/views/site')}}/js/venobox.min.js"></script> 
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="{{url('/resources/views/site')}}/js/revolution/js/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="{{url('/resources/views/site')}}/js/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="{{url('/resources/views/site')}}/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="{{url('/resources/views/site')}}/js/localization/messages_ar.min.js"></script>
	<script type="text/javascript" src="{{url('/resources/views/site')}}/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
    <script type="text/javascript">
	var baseurl = "{{url('/')}}";

	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
	</script>
    <script src="{{url('/resources/views/site')}}/js/script.js"></script>
	@section('footer')
   
	@show
</body>

</html>