@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}} | {{$item->title_ar}}</title>
<meta name="Description" content="" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
   <div class="clearfix"></div>
    <div id="featured-title" class="parallax parallax-bg-1" style="background: url({{url('/resources/views/site')}}/images/slide-1.jpg) no-repeat center center; background-size: cover; ">
        <div class="overlay"></div>
        <div id="featured-title-inner" class="wprt-container">
            <div class="featured-title-inner-wrap">
                <div class="featured-title-heading-wrap">
                    <h1 class="featured-title-heading ">{{$item->title_ar}}</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">
		<br />
			{!! $item->content_ar !!}
			@if($item->pageId == 28)
            <!-- end row -->
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{url('/resources/views/site')}}/images/cr1.jpg" alt="">
                </div>
                <!-- end col -->
                <div class="col-12 col-md-6">
                    <img src="{{url('/resources/views/site')}}/images/cr1.jpg" alt="">
                </div>
                <!-- end col -->

            </div>
            <!-- end row -->
			@endif
        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->

@stop