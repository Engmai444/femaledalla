<?php
$options = App\Http\Controllers\SiteController::site_option();
?>
@extends('site.master')
@section('seo')
<title>{{Lang::get('menu.sitename')}}</title>
<meta name="Description" content="ﺷﺮﻛﺔ ﺩﻟﻪ ﻟﺘﻌﻠﻴﻢ اﻟﻘﻴﺎﺩﺓ ﻣﻦ اﻟﺸﺮﻛﺎﺕ اﻟﺮاﺋﺪﺓ ﻓﻲ ﻣﺠﺎﻝ ﺗﻌﻠﻴﻢ ﻣﻬﺎﺭاﺕ اﻟﻘﻴﺎﺩﺓ ﺑﺎﻟﻤﻤﻠﻜﺔ اﻟﻌﺮﺑﻴﺔ اﻟﺴﻌﻮﺩﻳﺔ" />
@stop
@section('bodyid')
id="home"
@stop
@section('loader')
<div class="fakeLoader"></div>
@stop
@section('footer')
@stop
@section('content')
    <div class="container">
        <div class="slider">
            <div class="box-form">
                <div class="title-box1">

                    <h2 class="inline title1">التسجيل في</h2>
                    <h2 class="inline title2">الدورات</h2>

                </div>

                <form  id="signup2" method="Post" action="/signup" enctype="multipart/form-data">
				<div class="msg"></div>

                    <div class="row">
						<!-- end col -->
						<div class="col-12 col-md-6">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-id-card" aria-hidden="true"></i></span>
                                </div>

								<input type="text" class="form-control required" maxlength="10"  name="IDNumber" placeholder="{{Lang::get('validation.IDNumber')}}" onkeypress="return numbersonly(event);">
                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-12 col-md-6">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-university" aria-hidden="true"></i></span>
                                </div>
                                <select class="form-control required select2" name="school" >
                                    <option value="">المدرسة</option>

                                    @if($schools)
                                        @foreach($schools as $sc)
                                    <option value="{{$sc->Id}}">{{ $sc->Name }}</option>
                                        @endforeach
                                    @endif

                                </select>

                            </div>
                        </div>

                        <!-- end col -->
                        <div class="col-12 col-md-6">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-user" aria-hidden="true"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="{{Lang::get('validation.name')}}" autocomplete="off" onkeypress="return CheckArabicCharactersOnly(event);" onkeydown="return chk(event);" aria-label="{{Lang::get('validation.name')}}" id="fullname" name="FullName" aria-describedby="basic-addon2">

                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-12 col-md-6">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="{{Lang::get('validation.email')}}" aria-label="{{Lang::get('validation.email')}}" name="email" aria-describedby="basic-addon2" required>

                            </div>
                        </div>
                        <!-- end col -->


                        
						<div class="col-12 col-md-6">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-lock" aria-hidden="true"></i></span>
                                </div>
                                <input type="password" class="form-control" name="password" placeholder="{{Lang::get('validation.passwordcreate')}}" aria-label="{{Lang::get('validation.password')}}" aria-describedby="basic-addon2">

                            </div>
                        </div>
    <!-- end col -->
    <div class="col-12 col-md-6">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                </div>
                                <input type="text" class="form-control" onkeypress="return numbersonly(event);" placeholder="{{Lang::get('validation.phone')}}" maxlength="10" aria-label="{{Lang::get('validation.phone')}}" name="mobile" aria-describedby="basic-addon2">

                            </div>
                        </div>
                        <!-- end col -->

						<div class="col-12 col-md-12">
                            <div class="inline-box">
							<h3 style="font-size: 12px;margin: 0;padding: 0;">{{Lang::get('validation.dob')}}</h3>
							<label><input type="checkbox" value="1" name="d_type" id="d_type" class="swidate" onchange="return validate();">هجري؟</label>
                            </div>
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>


								<div class="datebox">
                                <select name="d_day"  id="d_day" required onchange="return validate_date();">
									<option value="">اليوم</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
								</select>
								<select name="d_month" id="d_month" required onchange="return validate_date();">
									<option value="">الشهر</option>
									<option value='1'>يناير</option>
									<option value='2'>فبراير</option>
									<option value='3'>مارس</option>
									<option value='4'>ابريل</option>
									<option value='5'>مايو</option>
									<option value='6'>يونيو</option>
									<option value='7'>يوليو</option>
									<option value='8'>اغسطس</option>
									<option value='9'>سبتمبر</option>
									<option value='10'>اكتوبر</option>
									<option value='11'>نوفمبر</option>
									<option value='12'>ديسمبر</option>
								</select>
								<select name="d_year" id="d_year" required onchange="return validate_date();">
									<option value="">السنة</option>
									<option value="2006">2006</option>
                                    <option value="2005">2005</option>
                                    <option value="2004">2004</option>
                                    <option value="2003">2003</option>
									<option value="2002">2002</option>
									<option value="2001">2001</option>
									<option value="2000">2000</option>
									<option value="1999">1999</option>
									<option value="1998">1998</option>
									<option value="1997">1997</option>
									<option value="1996">1996</option>
									<option value="1995">1995</option>
									<option value="1994">1994</option>
									<option value="1993">1993</option>
									<option value="1992">1992</option>
									<option value="1991">1991</option>
									<option value="1990">1990</option>
									<option value="1989">1989</option>
									<option value="1988">1988</option>
									<option value="1987">1987</option>
									<option value="1986">1986</option>
									<option value="1985">1985</option>
									<option value="1984">1984</option>
									<option value="1983">1983</option>
									<option value="1982">1982</option>
									<option value="1981">1981</option>
									<option value="1980">1980</option>
									<option value="1979">1979</option>
									<option value="1978">1978</option>
									<option value="1977">1977</option>
									<option value="1976">1976</option>
									<option value="1975">1975</option>
									<option value="1974">1974</option>
									<option value="1973">1973</option>
									<option value="1972">1972</option>
									<option value="1971">1971</option>
									<option value="1970">1970</option>
									<option value="1969">1969</option>
									<option value="1968">1968</option>
									<option value="1967">1967</option>
									<option value="1966">1966</option>
									<option value="1965">1965</option>
									<option value="1964">1964</option>
									<option value="1963">1963</option>
									<option value="1962">1962</option>
									<option value="1961">1961</option>
									<option value="1960">1960</option>
									<option value="1959">1959</option>
									<option value="1958">1958</option>
									<option value="1957">1957</option>
									<option value="1956">1956</option>
									<option value="1955">1955</option>
									<option value="1954">1954</option>
									<option value="1953">1953</option>
									<option value="1952">1952</option>
									<option value="1951">1951</option>
									<option value="1950">1950</option>
									<option value="1949">1949</option>
									<option value="1948">1948</option>
									<option value="1947">1947</option>
									<option value="1946">1946</option>
									<option value="1945">1945</option>
									<option value="1944">1944</option>
									<option value="1943">1943</option>
									<option value="1942">1942</option>
									<option value="1941">1941</option>
									<option value="1940">1940</option>
								</select>
								</div>

                            </div>
                        </div>
						<input type="hidden" value="" name="full_date" id="full_date">
                        <!-- end col -->
						<div class="col-12 col-md-6">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-globe" aria-hidden="true"></i></span>
                                </div>
                                <?php $nat_setting=DB::table('site_option')->where('varname','nationality_all')->first();?>

                                <select class="form-control required select2" name="NationalityId">
									<option value="">{{Lang::get('validation.Nationality')}}</option>
                                    @if($nat_setting->value=='1')

                                    @foreach(App\Http\Controllers\CustomerController::Nationality() as $it)
										<option value="{{$it->Id}}">{{$it->Name}}</option>
									@endforeach
                                    @else
                                        <option value="129">سعودية</option>

                                    @endif
								</select>

                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="input-group mb-1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-user" aria-hidden="true"></i></span>
                                </div>
                                <?php $msg_setting=DB::table('site_option')->where('varname','msg_female')->first();?>
                                <?php $gen=DB::table('site_option')->where('varname','gender')->first();?>

                                <select class="form-control required select2" name="gender" id="gender"  >
                                    <option value="">{{Lang::get('validation.gender')}}</option>
                                    @if($gen->value=='0')
                                        <option value="female">{{ lang::get('validation.female') }}</option>
                                    @elseif($gen->value=='1')
                                        <option value="male">{{ lang::get('validation.male') }}</option>
                                    @else
                                        <option value="male">{{ lang::get('validation.male') }}</option>
                                        <option value="female">{{ lang::get('validation.female') }}</option>

                                    @endif

                                </select>

                            </div>
                        </div>


                        <div class="col-12 col-md-12">
                            <div class="note">
                            <label for="img">صورة رقم الهوية / الاقامة</label><span style="color: red"> (JPG,JPEG,PNG) <span>(يجب ان لايزيد حجم الصورة عن 100KB)</span> </span> <br >
                            </div>
                            <div class="input-group mb-1">

                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-file" aria-hidden="true"></i></span>
                                </div>
                                <input type="file" class="form-control" id="img"  placeholder="صورة رقم الهوية / الاقامة"  aria-label="صورة رقم الهوية / الاقامة" name="img_national_id"  required>

                            </div>
                        </div>


						<div class="form-group m-0">
                                 <label class="" style="font-size:12px; padding: 8px;"><input type="checkbox" value="1"  name="birthdayrequired" > {{Lang::get('validation.terms')}}
								  </label>
                            </div>



                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-12 text-center">
                            <button class="btn btn-orange btn-block" type="submit" id="sign">{{Lang::get('validation.signup')}}</button>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                    <div class="bottom-signup-desc text-center" style="font-size:14px;font-weight:bold">  من فضلك ادخل بياناتك باللغة العربية
</div>
                    </form>
          

		  </div>
            <!-- end box form -->
            <div id="rev_slider_471_1_wrapper" dir="ltr" class="rev_slider_wrapper fullwidthbanner-container " data-alias="media-carousel-autoplay30" data-source="gallery" style="margin:0px auto;background-color:#26292b;padding:0px;margin-top:0px;margin-bottom:0px;">

                <!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
                <div id="rev_slider_471_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
                    <ul>
						@foreach($slider as $sm)
                        <!-- SLIDE  -->
                        <li data-index="rs-{{$sm->id}}" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500"  data-rotate="0" data-saveperformance="off" data-title="" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="{{url('/public/photos/'.$sm->photo)}}" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption Video-Title   tp-resizeme" id="slide-{{$sm->id}}-layer-2" data-x="-300" data-y="200" data-voffset="50" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:bottom;rZ:90deg;sX:2;sY:2;","speed":1500,"to":"o:1;tO:-20% 50%;","delay":500,"ease":"Power3.easeInOut"},{"delay":6000,"speed":1000,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[5,5,5,5]" data-paddingbottom="[5,5,5,5]" data-paddingleft="[5,5,5,5]" style="z-index: 7; white-space: nowrap;text-transform:left;">{{$sm->title_ar}}</div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption orange-Title  rtl   tp-resizeme" id="slide-{{$sm->id}}-layer-2" data-x="-300" data-y="280" data-voffset="10" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:bottom;rZ:90deg;sX:2;sY:2;","speed":1500,"to":"o:1;tO:-20% 50%;","delay":750,"ease":"Power3.easeInOut"},{"delay":7000,"speed":1000,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['right','right','right','right']" data-paddingtop="[5,5,5,5]" data-paddingright="[5,5,5,5]" data-paddingbottom="[5,5,5,5]" data-paddingleft="[5,5,5,5]" style="z-index: 8; white-space: nowrap;text-transform:left;"> <span class="big-t"></span>{!! nl2br($sm->content_ar) !!}</div>
                        </li>
						@endforeach
                        <!-- SLIDE  -->
                       
                        
                    </ul>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->

        </div>

        <!-- end slider -->
    </div>
    <!-- end container -->
    <div class="clearfix"></div>
    <div class="block1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 text-center">
                    <div class="about-box">
                        <h2 style=" min-height: 60px;"><i class="fa"><img src="{{url('/resources/views/site')}}/images/about-icon.png"></i> معلومات حول شركة دله لتعليم قيادة السيارات</h2>
                        <div class="content">
                            <p>ﺗﻌﺘﺒﺮ ﺷﺮﻛﺔ ﺩﻟﻪ ﻟﺘﻌﻠﻴﻢ ﻗﻴﺎﺩﺓ اﻟﺴﻴﺎﺭاﺕ ﻣﻦ اﻟﺸﺮﻛﺎﺕ اﻟﺮاﺋﺪﺓ ﻓﻲ اﻟﻤﻤﻠﻜﺔ اﻟﻌﺮﺑﻴﺔ اﻟﺴﻌﻮﺩﻳﺔ ﻓﻲ اﻟﺘﻌﻠﻴﻢ ﻭاﻟﺘﺪﺭﻳﺐ ﻓﻲ ﻗﻴﺎﺩﺓ اﻟﻤﺮﻛﺒﺎﺕ ﺑﺄﻧﻮاﻋﻬﺎ ﻭﺭﻓﻊ ﻣﻬﺎﺭاﺕ اﻟﺴﺎﺋﻘﻴﻦ ﻭﺯﻳﺎﺩﺓ اﻟﺜﻘﺎﻓﺔ اﻟﻤﺮﻭﺭﻳﺔ ﻭاﻟﺴﻼﻣﺔ اﻟﻤﺮﻭﺭﻳﺔ.
                                <div style="height: 10px;"></div>
                             ﺗﺄﺳﺴﺖ ﺷﺮﻛﺔ ﺩﻟﻪ ﻟﺘﻌﻠﻴﻢ اﻟﻘﻴﺎﺩﺓ ﻋﺎﻡ 1975 ﻡ ﻓﻲ اﻟﻤﻤﻠﻜﺔ اﻟﻌﺮﺑﻴﺔ اﻟﺴﻌﻮﺩﻳﺔ , ﻭﻛﺎﻧﺖ ﺃﻭﻝ ﻣﺪﺭﺳﺔ ﻓﻲ ﻣﺪﻳﻨﺔ ﺟﺪﺓ ﺗﻘﻮﻡ اﻟﺸﺮﻛﺔ ﺣﺎﻟﻴﺎ ﺑﺈﻋﺪاﺩ اﻟﻜﻮاﺩﺭ اﻟﺴﻌﻮﺩﻳﺔ ﻋﺎﻟﻴﺔ اﻟﻤﻬﺎﺭاﺕ ﻭاﻟﺘﺨﺼﺺ ﻭاﻟﺘﻲ ﺗﻤﺜﻞ اﻟﻨﻮاﺓ اﻟﺤﻘﻴﻘﻴﺔ ﻟﺘﺰﻭﻳﺪ ﺳﻮﻕ اﻟﻌﻤﻞ ﺑﺎﻟﻤﻤﻠﻜﺔ ﺑﺎﻟﻔﻨﻴﻴﻦ ﻭاﻟﻤﺪﺭﺑﻴﻦ ﻭﻗﺎﺋﺪﻱ اﻟﻤﺮﻛﺒﺎﺕ ﻓﻲ ﻫﺬا اﻟﻤﺠﺎﻝ
 </p>
                        </div>
                        <a class="btn btn-orange left" href="{{url('/about')}}">اﻟﻤﺰﻳﺪ ﻋﻦ ﺩﻟﻪ</a>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-12 col-md-5 text-center">
                    <div class="about-box">
                        <h2 style=" min-height: 60px;"><i class="fa"><img src="{{url('/resources/views/site')}}/images/time-icon.png"></i>   أوقات العمل الرسمية لشركة دله لتعليم قيادة السيارات </h2>
                        <div class="content">
                            <strong>أوقات ﺩﻭاﻡ شركة دله ﻟﺘﻌﻠﻴﻢ قيادة السيارات فى اﻷﻳﺎﻡ اﻟﻌﺎﺩﻳﺔ : </strong>
                            <p>ﻣﻦ اﻷﺣﺪ ﺇﻟﻰ اﻟﺨﻤﻴﺲ – ﻣﻦ 07:00 ﺻﺒﺎﺣﺎً ﺇﻟﻰ 09:00 ﻣﺴﺎءا 
                                <br>أيام العطل :  اﻟﺠﻤﻌﺔ و اﻟﺴﺒﺖ</p>
                          
                        </div>
                      

                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end block1 -->
    <div class="block-bg m-20 counter-row">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-3">

                    <div id="shiva" class="count-box">
                        <h3> عدد الرخص لعام 2018</h3>
                        <span class="count">{{$options['hm_Features_box1_title']}}</span>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-12 col-sm-3">

                    <div id="shiva" class="count-box">
                        <h3> المتدربين لعام 2018 </h3>
                        <span class="count">{{$options['hm_Features_box1_desc']}}</span>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-12 col-sm-3">

                    <div id="shiva" class="count-box">
                        <h3> عدد ساعات العمل لعام 2018</h3>
                        <span class="count">{{$options['hm_Features_box1_title_ar']}}</span>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-12 col-sm-3">

                    <div id="shiva" class="count-box">
                        <h3>ﺳﻨﻴﻦ اﻟﺘﻮاﺟﺪ ﻓﻲ اﻟﺴﻮﻕ</h3>
                        <span class="count">{{$options['hm_Features_box1_desc_ar']}}</span>
                    </div>
                </div>
                <!-- end col -->

            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>

    <div class="video-slider">

        <div id="rev_slider_74_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="youtube-gallery70" style="margin:0px auto;background-color:#000000;padding:0px;margin-top:0px;margin-bottom:0px;">
            <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
            <div id="rev_slider_74_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
                <ul>
                    

                    

                    <li data-index="rs-2614" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" data-thumb="{{url('/resources/views/site')}}/images/video-thumb2.jpg" data-rotate="0" data-saveperformance="off" data-title="" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{url('/resources/views/site')}}/images/video-slide1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- BACKGROUND VIDEO LAYER -->
                        <div class="rs-background-video-layer" data-forcerewind="on" data-volume="100" data-ytid="BLdyjuuwT1I" data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;volume=100&amp;hd=1&amp;wmode=opaque&amp;showinfo=0&amp;rel=0;" data-videorate="1" data-videowidth="100%" data-videoheight="100%" data-videocontrols="none" data-videoloop="none" data-forceCover="1" data-aspectratio="16:9" data-autoplay="true" data-autoplayonlyfirsttime="false" data-nextslideatend="true"></div>
                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption Video-SubTitle   tp-resizeme" id="slide-1613-layer-1" data-x="center" data-y="300" data-voffset="50" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:bottom;rZ:90deg;sX:2;sY:2;","speed":1500,"to":"o:1;tO:-20% 50%;","delay":500,"ease":"Power3.easeInOut"},{"delay":3000,"speed":1000,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[5,5,5,5]" data-paddingbottom="[5,5,5,5]" data-paddingleft="[5,5,5,5]" style="z-index: 9; white-space: nowrap;text-transform:left;">ﻳﻌﺮﺽ اﻻﻥ !</div>

                        <!-- LAYER NR. 6 -->
                        <div class="tp-caption Video-Title   tp-resizeme" id="slide-1613-layer-2" data-x="0" data-y="bottom" data-voffset="10" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:bottom;rZ:90deg;sX:2;sY:2;","speed":1500,"to":"o:1;tO:-20% 50%;","delay":750,"ease":"Power3.easeInOut"},{"delay":2500,"speed":1000,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[5,5,5,5]" data-paddingbottom="[5,5,5,5]" data-paddingleft="[5,5,5,5]" style="z-index: 10; white-space: nowrap;text-transform:left;">ﻋﻠﻢ ﺗﻠﺒﻴﻖ اﻟﺴﻴﺎﺭﻩ اﺧﺘﺒﺎﺭ اﻟﻘﻴﺎﺩﻩ ﺩﻟﻪ</div>
                    </li>
                    <!-- SLIDE  -->

                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
        </div>
        <!-- END REVOLUTION SLIDER -->

    </div>

    <div class="news-block">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="title-orange">أحدث الاخبار</h2>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
				@foreach($news as $item)
                <div class="col-12 col-md-4 ">
                    <div class="news-box">
                        <div class="thumb"><img src="{{url('/public/photos/'.$item->photo)}}" alt=""></div>
                        <div class="content">
                            <h3 class="title">{{$item->title_ar}}</h3>
                            <p>التاريخ : {{date('Y/m/d',strtotime($item->created_at))}}</p>
                            <p> {{ str_limit(strip_tags($item->content_ar), $limit = 150, $end = '...') }}</p>
                            <a class="btn left" href="{{url('/news/'.$item->pageId)}}">اﻟﻤﺰﻳﺪ</a>
                        </div>
                    </div>
                </div>
                <!-- end col -->
				@endforeach
                
            </div>	          

            <!-- end row -->
        </div>
        <!-- end container -->
    </div>

   <script>

   function validate(){
		if($('#d_type').prop("checked") == true){
			   $('#full_date').val('');
			   
		}else{
			$('#d_year').attr('onchange','return validate_date()');
			$('#d_month').attr('onchange','return validate_date()');
			$('#d_day').attr('onchange','return validate_date()');
		}
   }
	function validate_date(){
	var year=$('#d_year').val();
	var month=$('#d_month').val();
	var day=$('#d_day').val();
	// alert(year);
		$('#full_date').val(year+'-'+month+'-'+day);	
	
	}
       function get_gender()
       {
           var gender=$('#gender').val();
           if(gender=='female'){
               alert('التسجيل غير متاح للإناث حاليا');
               $('#sign').attr('disabled',true);
           }else{
               $('#sign').attr('disabled',false);

           }
       }
   </script>
@stop