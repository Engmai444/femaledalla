printDivCSS = new String ('<style>@media print{*{-webkit-print-color-adjust:exact;print-color-adjust:exact}.table{direction:rtl;width:100%;max-width:100%;margin-bottom:1rem;background-color:transparent}table{width:100%;border:1px solid #e2e2e2;border-spacing:0;border-collapse:collapse}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #e9ecef}caption,td{border-right:1px solid #e2e2e2;border-bottom:1px solid #e2e2e2}.notealert{direction:rtl;border:2px solid #000;padding:5px;font-size:18px;color:red}}</style>')
function printDiv(divId) {
    window.frames["print_frame"].document.body.innerHTML=printDivCSS + document.getElementById(divId).innerHTML;
    window.frames["print_frame"].window.focus();
    window.frames["print_frame"].window.print();
}
//////////////////////////////////////////////////////Ahmed
jQuery(document).ready(function($)
{
	


/*
	(c) 2018 @mrit01 - Ahmed abdel moneem
*/  



	var success_re = 0;
	var danger_re = 0;
	

  	$('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
		var $total = navigation.find('li').length;
		var $current = index+1;
		var $percent = ($current/$total) * 100;
		$('#rootwizard').find('.bar').css({width:$percent+'%'});
	},
	'onNext': function(tab, navigation, index) {
		$.ajax({
			url: baseurl + '/ajax/wrongAnswers',
			method: 'POST',
			dataType: 'json',
			data: $('#tab'+index).find('select, textarea, input').serialize(),
			error: function()
			{
				//alert("An error occoured!");
			}, beforeSend: function(){
				//$('#testing').hide()
			},
			success: function(response)
			{
				
				if(response.correct == 1){
					success_re += 1;
					$('#success-re').html(success_re);
				}else{
					danger_re += 1;
					$('#danger-re').html(danger_re);
				}
				
				
				//var login_status = response.status;
			   // $('#rootwizard').hide();
				//$('.result').html(response.items);
			}
		});
		
	}
	});
	
	var fiveSeconds = new Date().getTime() + 2400000;
	$('#clock').countdown(fiveSeconds, {elapse: false})
	.on('update.countdown', function(event) {
	  var $this = $(this);
	  $this.html(event.strftime('<span>%H:%M:%S</span>'));
	}).on('finish.countdown', function() {
	  $('#rootwizard').html('انتهي الوقت <br /><a class="btn btn-primary" href="javascript:location.reload(true)">اعادة تحميل</a>');
	});
	
	$('#rootwizard .finish').click(function() {
		//alert('تم انهاء الاختبار');
		$.ajax({
        url: baseurl + '/ajax/testing',
        method: 'POST',
        dataType: 'json',
        data: $('#testing').serialize(),
        error: function()
        {
            alert("An error occoured!");
        }, beforeSend: function(){
			$('#rootwizard').hide()
		},
        success: function(response)
        {
            //var login_status = response.status;
           // $('#rootwizard').hide();
			$('.result').html(response.items);
			$('#success-re').html(response.success);
			$('#danger-re').html(response.danger);
			$('div#clock').countdown('stop');
			$('div#clock').html('00:00:00');
        }
    });
		
		
		//$('#rootwizard').find("a[href*='tab1']").trigger('click');
	});

	$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    //startDate: '-1d'
});

	 $('.select2').select2();
	 
$('.swidate').change(function(){
	

if ($(this).is(':checked')) {
	$('.datebox').html('<select name="d_day" required><option value="">اليوم</option><option value="01">1</option><option value="02">2</option><option value="03">3</option><option value="04">4</option><option value="05">5</option><option value="06">6</option><option value="07">7</option><option value="08">8</option><option value="09">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option></select><select name="d_month" required><option value="">الشهر</option><option value="01">محرم </option><option value="02">صفر </option><option value="03">ربيع الأول</option><option value="04">ربيع الثاني</option><option value="05">جمادي الأول</option><option value="06">جمادي الثاني</option><option value="07">رجب </option><option value="08">شعبان </option><option value="09">رمضان </option><option value="10">شوال </option><option value="11"> ذو القعدة</option><option value="12">ذو الحجة</option></select><select required name="d_year"><option value="">السنة</option><option value="1435">1435</option><option value="1434">1434</option><option value="1433">1433</option><option value="1432">1432</option><option value="1431">1431</option><option value="1430">1430</option><option value="1429">1429</option><option value="1428">1428</option><option value="1427">1427</option><option value="1426">1426</option><option value="1425">1425</option><option value="1424">1424</option><option value="1423">1423</option><option value="1422">1422</option><option value="1421">1421</option><option value="1420">1420</option><option value="1419">1419</option><option value="1418">1418</option><option value="1417">1417</option><option value="1416">1416</option><option value="1415">1415</option><option value="1414">1414</option><option value="1413">1413</option><option value="1412">1412</option><option value="1411">1411</option><option value="1410">1410</option><option value="1409">1409</option><option value="1408">1408</option><option value="1407">1407</option><option value="1406">1406</option><option value="1405">1405</option><option value="1404">1404</option><option value="1403">1403</option><option value="1402">1402</option><option value="1401">1401</option><option value="1400">1400</option><option value="1399">1399</option><option value="1398">1398</option><option value="1397">1397</option><option value="1396">1396</option><option value="1395">1395</option><option value="1394">1394</option><option value="1393">1393</option><option value="1392">1392</option><option value="1391">1391</option><option value="1390">1390</option><option value="1389">1389</option><option value="1388">1388</option><option value="1387">1387</option><option value="1386">1386</option><option value="1385">1385</option><option value="1384">1384</option><option value="1383">1383</option><option value="1382">1382</option><option value="1381">1381</option><option value="1380">1380</option><option value="1379">1379</option><option value="1378">1378</option><option value="1377">1377</option><option value="1376">1376</option><option value="1375">1375</option><option value="1374">1374</option></select>');
	
}else{
	$('.datebox').html('<select name="d_day" required><option value="">اليوم</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select><select name="d_month" required><option value="">الشهر</option><option value="1">يناير</option><option value="2">فبراير</option><option value="3">مارس</option><option value="4">ابريل</option><option value="5">مايو</option><option value="6">يونيو</option><option value="7">يوليو</option><option value="8">اغسطس</option><option value="9">سبتمبر</option><option value="10">اكتوبر</option><option value="11">نوفمبر</option><option value="12">ديسمبر</option></select><select required name="d_year"><option value="">السنة</option><option value="2002">2006</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option><option value="1964">1964</option><option value="1963">1963</option><option value="1962">1962</option><option value="1961">1961</option><option value="1960">1960</option><option value="1959">1959</option><option value="1958">1958</option><option value="1957">1957</option><option value="1956">1956</option><option value="1955">1955</option><option value="1954">1954</option><option value="1953">1953</option><option value="1952">1952</option><option value="1951">1951</option><option value="1950">1950</option><option value="1949">1949</option><option value="1948">1948</option><option value="1947">1947</option><option value="1946">1946</option><option value="1945">1945</option><option value="1944">1944</option><option value="1943">1943</option><option value="1942">1942</option><option value="1941">1941</option><option value="1940">1940</option></select>');
}
	
});

$("#signup").validate({
	 ignore: ":hidden",
	 rules: {
         FullName: {
             required: true,
             minlength: 6
         },
         IDNumber: {
             required: true,
             minlength: 10
         },
         NationalityId: {
             required: true
         },
         mobile: {
             required: true,
             minlength: 10
         },
         email: {
             required: true,

         },
         password: {
             required: true
         }
	 },
	 submitHandler: function (form) {
         var form = $('#signup')[0];
         var formData = new FormData(form);
         $.ajax({
             url: baseurl + '/ajax/register',
             method: 'POST',
             dataType: 'json',
             data: formData,
             cache:false,
             contentType: false,
             processData: false,
			 error: function(response)
        {
           // alert("An error occoured!");
           alert(response.message);
        }, beforeSend: function(){
         // Handle the beforeSend event
         $("button").prop('disabled', true);
            $(".loader").show();
            $('#signup').hide();
       },
        success: function(response)
        {
            var login_status = response.status;
            setTimeout(function()
            {
                if(login_status == 'error')
                {
                    $(".loader").hide();

                    $(".msg").html('<div class="alert alert-danger alert-dismissable">' + response.message+' </div >');
                    $("button").prop('disabled', false);
                    $('#signup').show();
                }
                else if(login_status == 'done')
                {

                    setTimeout(function()
                    {
                        var redirect_url = baseurl;

                        if(response.redirect_url && response.redirect_url.length)
                        {
                            redirect_url = response.redirect_url;
                        }
                        window.location.href = redirect_url+'/profile';
                    }, 400);
                }
            }, 1000);
        }
		 });
		 return false; // required to block normal submit since you used ajax
	 }
 });
	 /*
$('#signup').submit(function(e){
    

    $.ajax({
        url: baseurl + '/ajax/register',
        method: 'POST',
        dataType: 'json',
        data: $(this ).serialize(),
        error: function()
        {
            alert("An error occoured!");
        }, beforeSend: function(){
         // Handle the beforeSend event
         $("button").prop('disabled', true);
            $(".loader").show();
            $('#signup').hide();
       },
        success: function(response)
        {
            var login_status = response.status;
            setTimeout(function()
            {
                if(login_status == 'error')
                {
                    $(".loader").hide();
                    //alert(response.msg);
                    $(".msg").html('<div class="alert alert-danger alert-dismissable">' + response.message+' </div >');
                    $("button").prop('disabled', false);
                    $('#signup').show();
                }
                else if(login_status == 'done')
                {
                    setTimeout(function()
                    {
                        var redirect_url = baseurl;
                        if(response.redirect_url && response.redirect_url.length)
                        {
                            redirect_url = response.redirect_url;
                        }
                        window.location.href = redirect_url+'/profile';
                    }, 400);
                }
            }, 1000);
        }
    });
    
    e.preventDefault();
});
*/ 

$("#signup2").validate({
	 //ignore: ":hidden",
	 rules: {
		 FullName: {
			 required: true,
			 minlength: 6
		 },
		 IDNumber: {
			 required: true,
			 minlength: 10
		 },
		 NationalityId: {
			 required: true
		 },
		 mobile: {
			 required: true,
			 minlength: 10
		 },
         email: {
             required: true,

         },
		 password: {
			 required: true
		 }
	 },
	 submitHandler: function (form) {
         var form = $('#signup2')[0];
         var formData = new FormData(form);
         alert('ok');
		 $.ajax({
			url: baseurl + '/ajax/register',
			method: 'POST',
			dataType: 'json',
             //data: $(form).serialize(),
			 data: formData,
             cache:false,
             contentType: false,
             processData: false,
			 error: function(response)
        {
            alert(response.message);
        }, beforeSend: function(){
         // Handle the beforeSend event
         $("button").prop('disabled', true);
            $(".loader").show();
            $('#signup2').hide();
       },
        success: function(response)
        {
            var login_status = response.status;
            setTimeout(function()
            {
                if(login_status == 'error')
                {
                    $(".loader").hide();
                    //alert(response.msg);
                    $(".msg").html('<div class="alert alert-danger alert-dismissable">' + response.message+' </div >');
                    $("button").prop('disabled', false);
                    $('#signup2').show();
                }
                else if(login_status == 'done')
                {

                    setTimeout(function()
                    {
                        var redirect_url = baseurl;

                        if(response.redirect_url && response.redirect_url.length)
                        {
                            redirect_url = response.redirect_url;
                        }
                        window.location.href = redirect_url+'/profile';
                    }, 400);
                }
            }, 1000);
        }
		 });
		 return false; // required to block normal submit since you used ajax
	 }
 });

$('#login').submit(function(e){
    

    $.ajax({
        url: baseurl + '/ajax/login',
        method: 'POST',
        dataType: 'json',
        data: $(this ).serialize(),
        error: function()
        {
            alert("An error occoured!");
        }, beforeSend: function(){
         // Handle the beforeSend event
         $("button").prop('disabled', true);
            $(".loader").show();
            $('#signup').hide();
       },
        success: function(response)
        {
            var login_status = response.status;
            setTimeout(function()
            {
                if(login_status == 'error')
                {
                    $(".loader").hide();
                    //alert(response.msg);
                    $(".msg").html('<div class="alert alert-danger alert-dismissable">' + response.message+' </div >');
                    $("button").prop('disabled', false);
                    $('#signup').show();
                }
                else if(login_status == 'done')
                {
                    setTimeout(function()
                    {
                        var redirect_url = baseurl;
                        if(response.redirect_url && response.redirect_url.length)
                        {
                            redirect_url = response.redirect_url;
                        }
                        window.location.href = redirect_url+'/profile';
                    }, 400);
                }
            }, 1000);
        }
    });
    
    e.preventDefault();
});

$('.subscours').click(function(e){

    $.ajax({
        url: baseurl + '/ajax/subsCours',
        method: 'POST',
        dataType: 'json',
        data: {id : $(this).attr('course-id')},
        error: function()
        {
            alert("An error occoured!");
        }, beforeSend: function(){
         
       },
        success: function(response)
        {
			
           // var login_status = response.status;

			if(response.status == 'login'){
                alert(response.message);
				window.location.href = baseurl+"/login";
			}
		//	alert(response.id);
			if(response.id > 0){
			    if(response.payonline=='1') {
                    //window.location.href = baseurl+"/mycourses/"+response.id;
                    //alert(baseurl+"/payment_course/"+response.id);
                    window.location.href = baseurl + "/payment_course/" + response.id;
                }else{
                    alert(response.message);
                }
			}else{
                alert(response.message);
            }
        }
    });
    
  // e.preventDefault();
});



$('#contact').submit(function(e){
    

    $.ajax({
        url: baseurl + '/ajax/contact',
        method: 'POST',
        dataType: 'json',
        data: $(this ).serialize(),
        error: function()
        {
            alert("An error occoured!");
        }, beforeSend: function(){
         
       },
        success: function(response)
        {
			
           // var login_status = response.status;
            alert(response.message);
			
        }
    });
    
    e.preventDefault();
});

$('#resetpassword').submit(function(e){

    var form = $('#resetpassword')[0];
    var formData = new FormData(form);
    $.ajax({
        url: baseurl + '/ajax/resetpassword',
        method: 'POST',
        dataType: 'json',
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        error: function()
        {
            alert("حدث خطأ براجاء التواصل مع الدعم الفني");
        }, beforeSend: function(){
         $('#resetpassword').hide();
		 $(".resetpasswordbutton").prop('disabled', true);
       },
        success: function(response)
        {

			if(response.status == 'error')
                {
                    //$(".loader").hide();
                    //alert(response.msg);
                    $(".msg").html('<div class="alert alert-danger alert-dismissable">' + response.message+' </div >');
                    $(".resetpasswordbutton").prop('disabled', false);
                    $('#resetpassword').show();
                }
                else if(response.status == 'done')
                {
                    $(".msg").html('<div class="alert alert-success alert-dismissable">' + response.message+' </div >');
                }
           // var login_status = response.status;
            alert(response.message);
			
        }
    });
    
    e.preventDefault();
});


});
(function($) { 
  
  "use strict";

    jQuery(window).load(function() {
        
        $(".loader-imsg").delay(500).fadeOut();
        $("#page-loader").delay(1000).fadeOut("slow");


setTimeout("$('.logo').addClass('animated bounceInRight ').removeClass('hidden1')",1100);


        });

    $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
    // ===== Scroll to Top ==== 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});


       $('.venobox').venobox(); 


})(jQuery);






            var tpj=jQuery;
      
      var revapi471;
      tpj(document).ready(function() {
        if(tpj("#rev_slider_471_1").revolution == undefined){
          revslider_showDoubleJqueryError("#rev_slider_471_1");
        }else{
          revapi471 = tpj("#rev_slider_471_1").show().revolution({
            sliderType:"carousel",
jsFileLocation:"revolution/js/",
            sliderLayout:"fullwidth",
            dottedOverlay:"none",
            delay:6000,
            navigation: {
              keyboardNavigation:"off",
              keyboard_direction: "horizontal",
              mouseScrollNavigation:"off",
              mouseScrollReverse:"default",
              onHoverStop:"off",
              touch:{
                touchenabled:"on",
                swipe_threshold: 75,
                swipe_min_touches: 1,
                swipe_direction: "horizontal",
                drag_block_vertical: false
              }
              ,
              arrows: {
                style:"gyges",
                enable:true,
                hide_onmobile:false,
                hide_onleave:false,
                tmp:'',
                left: {
                  h_align:"left",
                  v_align:"center",
                  h_offset:20,
                  v_offset:0
                },
                right: {
                  h_align:"right",
                  v_align:"center",
                  h_offset:20,
                  v_offset:0
                }
              }
              ,
              tabs: {
                style:"gyges",
                enable:false,
                width:180,
                height:80,
                min_width:250,
                wrapper_padding:30,
                wrapper_color:"#26292b",
                wrapper_opacity:"1",
                tmp:'<div class="tp-tab-content">  <span class="tp-tab-date">{{param1}}</span>  <span class="tp-tab-title">{{title}}</span></div><div class="tp-tab-image"></div>',
                visibleAmount: 3,
                hide_onmobile: false,
                hide_onleave:false,
                hide_delay:200,
                direction:"horizontal",
                span:true,
                position:"outer-bottom",
                space:0,
                h_align:"left",
                v_align:"bottom",
                h_offset:0,
                v_offset:0
              }
            },
            carousel: {
              horizontal_align: "center",
              vertical_align: "center",
              fadeout: "on",
              vary_fade: "on",
              maxVisibleItems:1,
              infinity: "on",
              space: 0,
              stretch: "on"
            },
            visibilityLevels:[1240,1024,778,480],
            gridwidth:720,
            gridheight:550,
            lazyType:"none",
            shadow:0,
            spinner:"off",
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",
            autoHeight:"off",
            disableProgressBar:"on",
            hideThumbsOnMobile:"off",
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            debugMode:false,
            fallbacks: {
              simplifyAll:"off",
              nextSlideOnWindowFocus:"off",
              disableFocusListener:false,
            }
          });
        }


                if(tpj("#rev_slider_471_2").revolution == undefined){
          revslider_showDoubleJqueryError("#rev_slider_471_2");
        }else{
          revapi471 = tpj("#rev_slider_471_2").show().revolution({
            sliderType:"carousel",
jsFileLocation:"revolution/js/",
            sliderLayout:"fullwidth",
            dottedOverlay:"none",
            delay:9000,
            navigation: {
              keyboardNavigation:"off",
              keyboard_direction: "horizontal",
              mouseScrollNavigation:"off",
              mouseScrollReverse:"default",
              onHoverStop:"off",
              touch:{
                touchenabled:"on",
                swipe_threshold: 75,
                swipe_min_touches: 1,
                swipe_direction: "horizontal",
                drag_block_vertical: false
              }
              ,
              arrows: {
                style:"gyges",
                enable:true,
                hide_onmobile:false,
                hide_onleave:false,
                tmp:'',
                left: {
                  h_align:"left",
                  v_align:"center",
                  h_offset:20,
                  v_offset:0
                },
                right: {
                  h_align:"right",
                  v_align:"center",
                  h_offset:20,
                  v_offset:0
                }
              }
              ,
              tabs: {
                style:"gyges",
                enable:true,
                width:178,
                height:113,
                min_width:250,
                wrapper_padding:30,
                wrapper_color:"#26292b",
                wrapper_opacity:"1",
     
                visibleAmount: 5,
                hide_onmobile: true,
                hide_onleave:false,
                hide_delay:200,
                direction:"horizontal",
                span:true,
                position:"outer-bottom",
                space:0,
                h_align:"center",
                v_align:"bottom",
                h_offset:0,
                v_offset:0
              }
            },
            carousel: {
              horizontal_align: "center",
              vertical_align: "center",
              fadeout: "on",
              vary_fade: "on",
              maxVisibleItems: 0,
              infinity: "on",
              space: 0,
              stretch: "on"
            },
            visibilityLevels:[1240,1024,778,480],
            gridwidth:720,
            gridheight:405,
            lazyType:"none",
            shadow:0,
            spinner:"off",
            stopLoop:"on",
            stopAfterLoops:0,
            stopAtSlide:0,
            shuffle:"off",
            autoHeight:"off",
            disableProgressBar:"on",
            hideThumbsOnMobile:"off",
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            debugMode:false,
            fallbacks: {
              simplifyAll:"off",
              nextSlideOnWindowFocus:"off",
              disableFocusListener:false,
            }
          });
        }


      }); /*ready*/



        var tpj=jQuery;         
          var revapi74;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_74_1").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_74_1");
            }else{
              revapi74 = tpj("#rev_slider_74_1").show().revolution({
                sliderType:"standard",
                jsFileLocation:"../../revolution/js/",
                sliderLayout:"auto",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                  keyboardNavigation:"off",
                  keyboard_direction: "horizontal",
                  mouseScrollNavigation:"off",
                  onHoverStop:"off",
                  arrows: {
                    style:"uranus",
                    enable:true,
                    hide_onmobile:true,
                    hide_under:778,
                    hide_onleave:true,
                    hide_delay:200,
                    hide_delay_mobile:1200,
                    tmp:'',
                    left: {
                      h_align:"left",
                      v_align:"center",
                      h_offset:20,
                      v_offset:0
                    },
                    right: {
                      h_align:"right",
                      v_align:"center",
                      h_offset:20,
                      v_offset:0
                    }
                  }
                  ,
                  thumbnails: {
                    style:"erinyen",
                    enable:true,
                    width:200,
                    height:113,
                    min_width:170,
                    wrapper_padding:30,
                    wrapper_color:"#333333",
                    wrapper_opacity:"1",
                    tmp:'<span class="tp-thumb-over"></span><span class="tp-thumb-image"></span><span class="tp-thumb-title">{{title}}</span><span class="tp-thumb-more"></span>',
                    visibleAmount:10,
                    hide_onmobile:false,
                    hide_onleave:false,
                    direction:"horizontal",
                    span:true,
                    position:"outer-bottom",
                    space:20,
                    h_align:"center",
                    v_align:"bottom",
                    h_offset:0,
                    v_offset:0
                  }
                },
                gridwidth:1230,
                gridheight:392,
                lazyType:"none",
                shadow:0,
                spinner:"spinner2",
                stopLoop:"on",
                stopAfterLoops:0,
                stopAtSlide:1,
                shuffle:"off",
                autoHeight:"off",
                disableProgressBar:"on",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                  simplifyAll:"off",
                  nextSlideOnWindowFocus:"off",
                  disableFocusListener:false,
                }
              });
            }
          }); /*ready*/





