<?php
namespace App\Http\Controllers;
$options = SiteController::get_option();
use Auth;
use Session;
use Form;
?>
@extends('site.ar.master')
@section('content')
<div class="bg2">
<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
			@include('auth.sidebar')
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
			   <h3>حسابي</h3>
			   @if(Session::get('success') != '')
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="fa fa-info-circle"></i>  
					{{Session::get('success')}}
					{{Session::forget('success')}}
				</div>
				@endif
			   
				@if (isset($errors) && count($errors) > 0)
				<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Error!</h4>
				  @foreach ($errors->all() as $error)
						- {{ $error }} <br />
				  @endforeach
				  </div>
				@endif
				
				{{ Form::open(array('url' => '/profile','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered')) }}
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">الاسم الاول</label>
					<div class="col-sm-5">
						{{ Form::text('firstname',Auth::guard()->user()->firstname, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">اسم العائلة</label>
					<div class="col-sm-5">
						{{ Form::text('lastname',Auth::guard()->user()->lastname, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group  row">
					<label for="field-1" class="col-sm-3 control-label">رقم الجوال</label>
					<div class="col-sm-5">
						{{ Form::text('mobile',Auth::guard()->user()->mobile, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">الايميل</label>
					<div class="col-sm-5">
						{{ Form::text('email',Auth::guard()->user()->email, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<h4>تغيير كلمة المرور</h4>
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">كلمة المرور القديمة</label>
					<div class="col-sm-5">
						{{ Form::text('old_password',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">كلمة المرور الجديدة</label>
					<div class="col-sm-5">
						{{ Form::text('new_password',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">تأكيد كلمة المرور الجديدة</label>
					<div class="col-sm-5">
						{{ Form::text('renew_password',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group row">
					
					<div class="col-sm-5">
						<input type="submit" value="حفظ" class="btn btn-primary" />
					</div>
				</div>
				{{ Form::close() }} 
						
            </div>
		</div>
	</div>
</div>
</div>
@endsection
