<?php
namespace App\Http\Controllers;
$options = SiteController::get_option();
use Auth;
use Session;
use Form;
?>
@extends('site.en.master')
@section('content')
<div class="bg2">
<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
			@include('auth.en.sidebar')
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
			   <h3>Account</h3>
			   @if(Session::get('success') != '')
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="fa fa-info-circle"></i>  
					{{Session::get('success')}}
					{{Session::forget('success')}}
				</div>
				@endif
			   
				@if (isset($errors) && count($errors) > 0)
				<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Error!</h4>
				  @foreach ($errors->all() as $error)
						- {{ $error }} <br />
				  @endforeach
				  </div>
				@endif
				
				{{ Form::open(array('url' => '/profile','role' => 'form','id' => 'sendform','class' => 'form-horizontal form-groups-bordered')) }}
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">Firat name</label>
					<div class="col-sm-5">
						{{ Form::text('firstname',Auth::guard()->user()->firstname, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">Family name</label>
					<div class="col-sm-5">
						{{ Form::text('lastname',Auth::guard()->user()->lastname, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group  row">
					<label for="field-1" class="col-sm-3 control-label">Phone</label>
					<div class="col-sm-5">
						{{ Form::text('mobile',Auth::guard()->user()->mobile, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">Email</label>
					<div class="col-sm-5">
						{{ Form::text('email',Auth::guard()->user()->email, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<h4>Change password</h4>
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">Old password</label>
					<div class="col-sm-5">
						{{ Form::text('old_password',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">New password</label>
					<div class="col-sm-5">
						{{ Form::text('new_password',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group row">
					<label for="field-1" class="col-sm-3 control-label">Re-New password</label>
					<div class="col-sm-5">
						{{ Form::text('renew_password',null, array('class' => 'form-control','placeholder'=>'')) }} 
					</div>
				</div>
				<div class="form-group row">
					
					<div class="col-sm-5">
						<input type="submit" value="Save" class="btn btn-primary" />
					</div>
				</div>
				{{ Form::close() }} 
						
            </div>
		</div>
	</div>
</div>
</div>
@endsection
