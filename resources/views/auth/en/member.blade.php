<?php
namespace App\Http\Controllers;
$options = SiteController::get_option();
use Auth;
?>
@extends('site.en.master')
@section('content')
<div class="bg2">
<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
			@include('auth.en.sidebar')
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
			   <h3>My orders</h3>
			   <table class="table table-borderd">
			   		<tr>
					   <th>order #</th>
					   <th>Date</th>
					   <th>Status</th>
					   <th>Price</th>
					   <th>tools</th>
					</tr>
					@foreach($orders as $item)
					<tr>
					   <td>{{$item->order_id}}</td>
					   <td>{{$item->created_at->format('Y-m-d')}}</td>
					   
					   @if($item->paid == 0)
					   <td><div class="btn btn-danger">Unpaid</div></td>
					   <td>{{$item->total}} SAR</td>
					   <td><a href="{{url('en/invoice/'.$item->order_hash)}}" class="btn btn-primary">Pay</a> 
					   <a href="{{url('/order/'.$item->order_hash)}}/cancel" class="btn btn-danger">cancel</a></td>
					   @else
						<td><div class="btn btn-success">Paid</div></td>
						<td>{{$item->total}} SAR</td>
					   <td><a href="{{url('en/print/'.$item->order_hash)}}" class="btn btn-primary">View</a> 
					   <a href="{{url('/order/'.$item->order_hash)}}/cancel" class="btn btn-danger">cancel</a></td>
					   @endif
					</rt>
					@endforeach
			   </table>
            </div>
		</div>
	</div>
</div>
</div>
@endsection
