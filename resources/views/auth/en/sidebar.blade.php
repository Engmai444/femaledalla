<!-- SIDEBAR USERPIC -->
<div class="profile-userpic">
	<img src="{{url('/resources/views/site')}}/assets/img/avatar.png" class="img-responsive" alt="">
</div>
<!-- END SIDEBAR USERPIC -->
<!-- SIDEBAR USER TITLE -->
<div class="profile-usertitle">
	<div class="profile-usertitle-name">
		{{Auth::guard()->user()->firstname}}
	</div>
	<div class="profile-usertitle-job">
	{{Auth::guard()->user()->created_at->format('d/m/Y')}}
	</div>
</div>
<!-- END SIDEBAR USER TITLE -->
<!-- SIDEBAR BUTTONS -->
<!-- END SIDEBAR BUTTONS -->
<!-- SIDEBAR MENU -->
<div class="profile-usermenu">
	<ul class="nav">
		<li class="active">
			<a href="{{url('en/member')}}">
			<i class="fa fa-home"></i>
			My orders </a>
		</li>
		<li>
			<a href="{{url('en/profile')}}">
			<i class="fa fa-user"></i>
			Profile </a>
		</li>
		<li>
			<a href="{{url('en/logout')}}">
			<i class="fa fa-user"></i>
			Logout </a>
		</li>
	</ul>
</div>
<!-- END MENU -->