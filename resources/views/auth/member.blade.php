<?php
namespace App\Http\Controllers;
$options = SiteController::get_option();
use Auth;
?>
@extends('site.ar.master')
@section('content')
<div class="bg2">
<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
			@include('auth.sidebar')
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
			   <h3>طلباتي</h3>
			   <table class="table table-borderd">
			   		<tr>
					   <th>رقم الطلب #</th>
					   <th>التاريخ</th>
					   <th>الحالة</th>
					   <th>السعر</th>
					   <th>الادوات</th>
					</tr>
					@foreach($orders as $item)
					<tr>
					   <td>{{$item->order_id}}</td>
					   <td>{{$item->created_at->format('Y-m-d')}}</td>
					   
					   @if($item->paid == 0)
					   <td><div class="btn btn-danger">غير مدفوع</div></td>
					   <td>{{$item->total}} ر.س</td>
					   <td><a href="{{url('/invoice/'.$item->order_hash)}}" class="btn btn-primary">دفع</a> <a href="{{url('/order/'.$item->order_hash)}}/cancel" class="btn btn-danger">الغاء الطلب</a></td>
					   @else
						<td><div class="btn btn-success">مدفوع</div></td>
						<td>{{$item->total}} ر.س</td>
					   <td><a href="{{url('/print/'.$item->order_hash)}}" class="btn btn-primary">عرض</a> <a href="{{url('/order/'.$item->order_hash)}}/cancel" class="btn btn-danger">الغاء الطلب</a></td>
					   @endif
					</rt>
					@endforeach
			   </table>
            </div>
		</div>
	</div>
</div>
</div>
@endsection
