<!-- SIDEBAR USERPIC -->
<div class="profile-userpic">
	<img src="{{url('/resources/views/site')}}/assets/img/avatar.png" class="img-responsive" alt="">
</div>
<!-- END SIDEBAR USERPIC -->
<!-- SIDEBAR USER TITLE -->
<div class="profile-usertitle">
	<div class="profile-usertitle-name">
		{{Auth::guard()->user()->firstname}}
	</div>
	<div class="profile-usertitle-job">
	{{Auth::guard()->user()->created_at->format('d/m/Y')}}
	</div>
</div>
<!-- END SIDEBAR USER TITLE -->
<!-- SIDEBAR BUTTONS -->
<!-- END SIDEBAR BUTTONS -->
<!-- SIDEBAR MENU -->
<div class="profile-usermenu">
	<ul class="nav">
		<li class="active">
			<a href="{{url('/member')}}">
			<i class="fa fa-home"></i>
			طلباتي </a>
		</li>
		<li>
			<a href="{{url('/profile')}}">
			<i class="fa fa-user"></i>
			حسابي </a>
		</li>
		<li>
			<a href="{{url('/logout')}}">
			<i class="fa fa-user"></i>
			خروج </a>
		</li>
	</ul>
</div>
<!-- END MENU -->