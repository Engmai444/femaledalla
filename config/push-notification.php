<?php

return array(

    'kazzeemIOS'     => array(
        'environment' =>'development',
        'certificate' =>'/path/to/certificate.pem',
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ),
    'kazzeemAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyB_GtajJ3UlQVh-CykEUhCvR3YkeEhs-EU',
        'service'     =>'gcm'
    )

);