<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
	protected $table = 'users';
	
	protected $primaryKey = 'StudentId';
	// const CREATED_AT = 'creation_date';
    // const UPDATED_AT = 'last_update';
	
	protected $fillable = [
         'email', 'password','FullName','mobile','type_reg','img_national_id','school'
    ];
	
	protected $hidden = [
        'password', 'remember_token',
    ];
}
