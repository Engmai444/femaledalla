<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
 use Storage;
 use DB;
 use Excel;
use Lang;
use File;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ExportData::class,
		Commands\PaymentCheck::class,
        Commands\TestSms::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
		//$schedule->command('export:data')->everyThirtyMinutes();	
		
		$schedule->call('App\Http\Controllers\CoursesController@exportOrders')->everyThirtyMinutes();
        $schedule->call('App\Http\Controllers\CoursesController@exportOrdersunpaid')->everyThirtyMinutes();
        $schedule->call('App\Http\Controllers\CoursesController@exportOrders_new')->everyThirtyMinutes();

        // $schedule->command('payment:check')->everyMinute();
		$schedule->call('App\Http\Controllers\CoursesController@checkPayment')->everyThirtyMinutes();
        //$schedule->call('App\Http\Controllers\CustomerController@remind_sms')->daily();
        $schedule->call('App\Http\Controllers\Evaluation_timeController@export_drive')->everyThirtyMinutes();
        $schedule->call('App\Http\Controllers\CustomerController@remind_pay')->daily();
        $schedule->call('App\Http\Controllers\SchoolController@exportreport_drive')->everyThirtyMinutes();
        $schedule->call('App\Http\Controllers\CustomerController@customers_drive')->everyThirtyMinutes();
        $schedule->call('App\Http\Controllers\CustomerController@cancel_unpaid')->daily();

        // $schedule->call(function () {
			// $query = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->join('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period');
			// $query->where('orders.deleted_at','0000-00-00 00:00:00');
			// $query->orwhereNULL('orders.deleted_at');
			// $items = $query->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.IDNumber','users.gender','users.FullName','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))->where('orders.deleted_at','0000-00-00 00:00:00')->get();


			
			// $file = Excel::create('getOnline', function($excel) use($items) {
				// $excel->sheet('getOnline', function($sheet) use($items) {
				  
				  // $sheet->loadView('excel.orders', ['items' => $items]);
				// });
			// })->store('xlsx', storage_path('app/public/excel'), true);
			
			// Storage::disk('googledrive')->put('getOnline.xlsx', file_get_contents(storage_path('app/public/excel/getOnline.xlsx')));
        // })->daily();		
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
		//$this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
