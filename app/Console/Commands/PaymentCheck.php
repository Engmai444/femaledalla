<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use DB;
class PaymentCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check payment status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            // DB::table('orders')
                        // ->where('OrderId',6441)
                        // ->update(
                            // [
                                // 'trans_paid'    =>date('Y-m-d H:i:s'),
                                // 'created_at'    =>date('Y-m-d H:i:s'),
							// ]
                        // );       
		// $orders = DB::table('orders')->where('created_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 1 HOUR)'))->where('IsPaid',0)->get();
			$now = date('Y-m-d H:i:s');
		$prev_hour = date('Y-m-d H:i:s', strtotime('-1 hour'));		
		$orders = DB::table('orders')->wheredate('created_at','>=',$prev_hour)->where('IsPaid',0)->get();

		if(!empty($orders) && $orders->count() > 0){
			foreach($orders as $order){
				$params = array(
					'ivp_method'  => 'check',
					'ivp_store'   => '23827',
					'ivp_authkey' => '4m8CB@TWP4X#8QLS',
					'order_ref' => $order->order_ref
				);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
				curl_setopt($ch, CURLOPT_POST, count($params));
				curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
				$results2 = curl_exec($ch);
				$results2 = json_decode($results2,true);
	
				if(isset($results2['order']) && !empty($results2['order'])){
					$code=$results2['order']['status']['code'];
					if($code == 3){
						$order_ref=$results2['order']['transaction']['ref'];
						DB::table('orders')
							->where('OrderId',$order->OrderId)
							->update(
								[
									'IsPaid'        =>1,
									'trans_no'      =>$order_ref,
									'trans_paid'    =>date('Y-m-d H:i:s'),
									'deleted_at'   =>''
								]
							);			
					}					
				}

			}				
		}
	$this->info('Payments Checked');

    }
}
