<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
 use Storage;
 use DB;
 use Excel;
use Lang;
use File;
class ExportData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export table data to excel ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $schools=DB::table('schools')->get();
        foreach ($schools as $sc):
        $query = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->join('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period');
        $query->where('orders.SchoolId',$sc->Id);
        $query->where('orders.deleted_at','0000-00-00 00:00:00');
        $query->orwhereNULL('orders.deleted_at');
		$items = $query->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.IDNumber','users.gender','users.FullName','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))->where('orders.deleted_at','0000-00-00 00:00:00')->get();

		$file = Excel::create('getOnline', function($excel) use($items) {
			$excel->sheet('getOnline', function($sheet) use($items) {
			  
			  $sheet->loadView('excel.orders', ['items' => $items]);
			});
		})->store('xlsx', storage_path('app/public/excel'), true);
		 
		Storage::disk($sc->Id)->put('getOnline.xlsx', file_get_contents(storage_path('app/public/excel/getOnline.xlsx')));
		endforeach;
		$this->info('All Data Exported');
    }
}
