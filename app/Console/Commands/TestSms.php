<?php

namespace App\Console\Commands;

use App\Models\Evaluation_reserve;
use App\User;
use Illuminate\Console\Command;
 use Storage;
 use DB;
 use Excel;
use Lang;
use File;
class TestSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send sms reminder ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date=date('Y-m-d',strtotime("+1 day"));
        $query=Evaluation_reserve::leftjoin('evaluation_times','evaluation_reserves.time_id','=','evaluation_times.id')
                                 ->where('day',$date)->get();
        if(!empty($query))
        {
            foreach ($query as $qu):
                $student=$qu->student_id;
            $stu=User::where('StudentId',$student)->first();

                $phonenew=substr($stu->mobile,0,1);
                if($phonenew!='0')
                {
                    $phone=$stu->mobile;
                }else{
                    $phone=substr($stu->mobile,1);
                }
                $msg="تذكير لديكم اختبار غدا";

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, "https://www.4jawaly.net/api/sendsms.php");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);

                curl_setopt($ch, CURLOPT_POST, TRUE);

                curl_setopt($ch, CURLOPT_POSTFIELDS, 'username=dalla&password=565656&message='.$msg.'&numbers=966'.$phone.'&sender=DDC-DRIVING&unicode=utf8&Rmduplicated=1&return=json');


                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    "Content-Type: application/x-www-form-urlencoded"
                ));

                $response = curl_exec($ch);

                curl_close($ch);

            endforeach;
        }

       $this->info('All sms send');
    }
}
