<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Agents;
use App\User;
use App\Service;
use App\Address;
use App\Pages;
use App\settings;
use Validator;
use Auth;
use Response;
use Redirect;
use Image;
use PushNotification;
use Session;
use Lang;
use App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\DB;
use Config;
class SiteController extends Controller
{
	private function permations($name){
		$res = AdminController::permations($name);
		return $res;
	}
	static public function site_option (){
		$optios = DB::table('site_option')->get();
		foreach($optios as $item){
			$arr[$item->varname] = $item->value;
		}
		return $arr;
		
	}
	
    //
	public function get_home(){
		
		if(!$this->permations('siteoption')){
			return view('admin.permations');
		}
		$items = $this->get_home_option();
		// dd($items);
		return view('admin.option_home')->with('items',$items);
	}
	public function home(Request $request){
	
	
		DB::table('site_option')->where('varname', 'hm_bl4_address')->update(['value' => $request->input('hm_bl4_address')]);
		DB::table('site_option')->where('varname', 'hm_bl4_address_ar')->update(['value' => $request->input('hm_bl4_address_ar')]);
		DB::table('site_option')->where('varname', 'hm_bl4_email')->update(['value' => $request->input('hm_bl4_email')]);
		DB::table('site_option')->where('varname', 'hm_bl4_phone')->update(['value' => $request->input('hm_bl4_phone')]);

		DB::table('site_option')->where('varname', 'hm_bl4_whatsapp')->update(['value' => $request->input('hm_bl4_whatsapp')]);
		DB::table('site_option')->where('varname', 'hm_bl4_contactform')->update(['value' => $request->input('hm_bl4_contactform')]);
		DB::table('site_option')->where('varname', 'hm_bl1_title')->update(['value' => $request->input('hm_bl1_title')]);
		DB::table('site_option')->where('varname', 'hm_bl1_desc')->update(['value' => $request->input('hm_bl1_desc')]);

		DB::table('site_option')->where('varname', 'hm_bl1_title_ar')->update(['value' => $request->input('hm_bl1_title_ar')]);
		DB::table('site_option')->where('varname', 'hm_bl1_desc_ar')->update(['value' => $request->input('hm_bl1_desc_ar')]);
		DB::table('site_option')->where('varname', 'site_title')->update(['value' => $request->input('site_title')]);
		DB::table('site_option')->where('varname', 'site_desc')->update(['value' => $request->input('site_desc')]);
		DB::table('site_option')->where('varname', 'site_title_ar')->update(['value' => $request->input('site_title_ar')]);
		DB::table('site_option')->where('varname', 'site_desc_ar')->update(['value' => $request->input('site_desc_ar')]);
        DB::table('site_option')->where('varname', 'tax')->update(['value' => $request->input('tax')]);


		DB::table('site_option')->where('varname', 'social_facebook')->update(['value' => $request->input('social_facebook')]);
		DB::table('site_option')->where('varname', 'social_twitter')->update(['value' => $request->input('social_twitter')]);
		DB::table('site_option')->where('varname', 'social_youtube')->update(['value' => $request->input('social_youtube')]);
		DB::table('site_option')->where('varname', 'social_instagram')->update(['value' => $request->input('social_instagram')]);
		DB::table('site_option')->where('varname', 'social_linkedin')->update(['value' => $request->input('social_linkedin')]);
		DB::table('site_option')->where('varname', 'social_googleplus')->update(['value' => $request->input('social_googleplus')]);
		
		DB::table('site_option')->where('varname', 'hm_Features_box1_title')->update(['value' => $request->input('hm_Features_box1_title')]);
		DB::table('site_option')->where('varname', 'hm_Features_box1_desc')->update(['value' => $request->input('hm_Features_box1_desc')]);
		DB::table('site_option')->where('varname', 'hm_Features_box1_title_ar')->update(['value' => $request->input('hm_Features_box1_title_ar')]);
		DB::table('site_option')->where('varname', 'hm_Features_box1_desc_ar')->update(['value' => $request->input('hm_Features_box1_desc_ar')]);
		DB::table('site_option')->where('varname', 'hm_Features_box2_title')->update(['value' => $request->input('hm_Features_box2_title')]);
		DB::table('site_option')->where('varname', 'hm_Features_box2_desc')->update(['value' => $request->input('hm_Features_box2_desc')]);
		DB::table('site_option')->where('varname', 'hm_Features_box2_title_ar')->update(['value' => $request->input('hm_Features_box2_title_ar')]);
		DB::table('site_option')->where('varname', 'hm_Features_box2_desc_ar')->update(['value' => $request->input('hm_Features_box2_desc_ar')]);
		DB::table('site_option')->where('varname', 'hm_Features_box3_title')->update(['value' => $request->input('hm_Features_box3_title')]);
		DB::table('site_option')->where('varname', 'hm_Features_box3_desc')->update(['value' => $request->input('hm_Features_box3_desc')]);
		DB::table('site_option')->where('varname', 'hm_Features_box3_title_ar')->update(['value' => $request->input('hm_Features_box3_title_ar')]);
		DB::table('site_option')->where('varname', 'hm_Features_box3_desc_ar')->update(['value' => $request->input('hm_Features_box3_desc_ar')]);
		DB::table('site_option')->where('varname', 'hm_video_link')->update(['value' => $request->input('hm_video_link')]);
		DB::table('site_option')->where('varname', 'hm_video_link2')->update(['value' => $request->input('hm_video_link2')]);
		DB::table('site_option')->where('varname', 'hm_footer_about')->update(['value' => $request->input('hm_footer_about')]);
		DB::table('site_option')->where('varname', 'hm_footer_about_ar')->update(['value' => $request->input('hm_footer_about_ar')]);
		DB::table('site_option')->where('varname', 'hm_map_lat')->update(['value' => $request->input('hm_map_lat')]);
		DB::table('site_option')->where('varname', 'hm_map_lng')->update(['value' => $request->input('hm_map_lng')]);
		
		DB::table('site_option')->where('varname', 'hm_bl4_required')->update(['value' => $request->input('hm_bl4_required')]);
        DB::table('site_option')->where('varname', 'msg_female')->update(['value' => $request->input('msg_female')]);
        DB::table('site_option')->where('varname', 'hm_mycoursrs_text')->update(['value' => $request->input('hm_mycoursrs_text')]);
        DB::table('site_option')->where('varname', 'gender')->update(['value' => $request->input('gender')]);

        DB::table('site_option')->where('varname', 'evaluation_time')->update(['value' => $request->input('evaluation_time')]);
        DB::table('site_option')->where('varname', 'nationality_all')->update(['value' => $request->input('nationality_all')]);
        DB::table('site_option')->where('varname', 'id_setting')->update(['value' => $request->input('id_setting')]);

		// hm_bl4_required
		
		$photo = $request->file('hm_Features_box1_photo');
		if(!empty($photo)){
			$photo_name = str_random(5).time();
			$photo_ext = $photo->getClientOriginalExtension();
			$photo_fullname = $photo_name.'.'.$photo_ext;
			$path = public_path().'/photos/';
			//$upload = $photo->move($path,$photo_fullname);
			$thumb = Image::make($photo)->resize(1190, 500)->save($path.$photo_fullname);
			DB::table('site_option')->where('varname', 'hm_Features_box1_photo')->update(['value' => $photo_fullname]);
		}

		$photo2 = $request->file('hm_Features_box2_photo');
		if(!empty($photo2)){
			$photo_name = str_random(5).time();
			$photo_ext = $photo2->getClientOriginalExtension();
			$photo_fullname = $photo_name.'.'.$photo_ext;
			$path = public_path().'/photos/';
			//$upload = $photo->move($path,$photo_fullname);
			$thumb = Image::make($photo2)->resize(1190, 500)->save($path.$photo_fullname);
			DB::table('site_option')->where('varname', 'hm_Features_box2_photo')->update(['value' => $photo_fullname]);
		}

		$photo3 = $request->file('hm_Features_box3_photo');
		if(!empty($photo3)){
			$photo_name = str_random(5).time();
			$photo_ext = $photo3->getClientOriginalExtension();
			$photo_fullname = $photo_name.'.'.$photo_ext;
			$path = public_path().'/photos/';
			//$upload = $photo->move($path,$photo_fullname);
			$thumb = Image::make($photo3)->resize(1190, 500)->save($path.$photo_fullname);
			DB::table('site_option')->where('varname', 'hm_Features_box3_photo')->update(['value' => $photo_fullname]);
		}

		
		 Session::put('success',Lang::get('validation.Saved'));
		 return redirect()->back();
	}
	public function get_about(){
		/*if(!$this->permations('siteoption')){
			return view('admin.permations');
		}*/
		$items = $this->get_home_option();
		return view('admin.option_about')->with('items',$items);
	}
	
	public function get_home_option(){
		$optios = DB::table('site_option')->get();
		foreach($optios as $item){
			$arr[$item->varname] = $item->value;
		}
		return $arr;
	}

	static function get_option(){
		$optios = DB::table('site_option')->get();
		foreach($optios as $item){
			$arr[$item->varname] = $item->value;
		}
		return $arr;
	}
	
	

	

	private function youtubeId($link){
		if(!empty($link)){
			parse_str( parse_url( $link, PHP_URL_QUERY ), $my_array_of_vars );
			return $my_array_of_vars['v'];
		}
	}
	
	
	public function contactform(Request $request)
	{

		$validation = Validator::make($request->all(),[
			'name' 		=> 'required',
			
			'msg' 		=> 'required',
			'phone' 	=> 'required',
		]);

		if($validation->fails()){
			$errors = $validation->errors();
			$res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> $errors->first()
				);
		}else{

			$options = $this->get_home_option();
	    	$data  = array(
	    		'name' 		=> $request->name,
				'email' 	=> $request->email,
				'mobile' 	=> $request->phone,
	    		'msg' 		=> $request->msg,
	    	);

			if (App::environment('production', 'staging'))
			{
	    	/*$sent = Mail::send('emails.contact', $data, function($message) use ($data,$options)
			{
				$message->from('info@dallahdrivingschool.sa' , $data['name']);
				$message->to($options['hm_bl4_contactform'], 'Admin')->subject('Dalla Contact Form :'.$data['name']);
			});	*/

                $con='<html>
<head>
  <style>
      table,td{
          border:1px solid #ccc;
      }
      td{
          padding:6px;
      }
  </style> 
</head>
<body>
<h1><img src="'.url('/resources/views/site/assets/img/footer-logo.png').'" /></h1>
<h3>Contact form</h3>
<table align="center" width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td> Name : </td>
  <td> '.$request->name.'</td>
</tr>
<tr>
  <td> Mobile : </td>
  <td> '.$request->phone.' </td>
</tr>
<tr>
  <td> Email : </td>
  <td> '.$request->email.' </td>
</tr>
<tr>
  <td> Massage : </td>
  <td> '.$request->msg .' </td>
</tr>
</table>
</body>
</html>';
                $this->sendemail($options['hm_bl4_contactform'],$fromemail='',$con);

            }

	    	//if(count(Mail::failures()) ==  0){
	    		$res = array(
					'status' => 'done',
					'message' 	=> 'sent successfuly'
				);
	    	/*}else{
	    		$res = array(
					'status' => 'error',
					'code' 		=> 231,
					'message' 	=> 'error'
				);
	    	}*/
			
	    }
		return response()->json($res);
			
	}
	
	public function contactform_ar(Request $request)
	{

		$messages = [
			'name.required' => 'الاسم مطلوب',
			'msg.required' => 'اكتب استفسارك',
			'phone.required' => 'رقم التليفون مطولب',
			'email.required' => 'البريد الالكتروني مطلوب',
		];
		$validation = Validator::make($request->all(),[
			'name' 		=> 'required',
			'msg' 		=> 'required',
			'phone' 	=> 'required',
		],$messages);

		if($validation->fails()){
			$errors = $validation->errors();
			$res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> $errors->first()
				);
		}else{
		    $secret = "6LeamI4UAAAAAIfi90bdoAYJdSeBl3w4CmY4Cqyg";
		    $response = $request->input('g-recaptcha-response');
		    $verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");

        	$captcha_success=json_decode($verify);
        
        	if ($captcha_success->success != true) {
        	    if($request->input('lang') == 'en'){
        	        $msg = "Error in google recaptcha";
        	    }else{
        	        $msg = "خطأ في رمز تحقق جوجل";
        	    }
                $res = array(
					'status' => 'error',
					'code' 		=> 231,
					'message' 	=> $msg
				);
				return response()->json($res);
        	}

		   
			$options = $this->get_home_option();
	    	$data  = array(
	    		'name' 		=> $request->name,
				'email' 	=> $request->email,
				'mobile' 	=> $request->phone,
	    		'msg' 		=> $request->msg,
	    	);

			if (App::environment('production', 'staging'))
			{
				/*$sent = Mail::send('emails.contact', $data, function($message) use ($data,$options)
				{
					$message->from('info@dallahdrivingschool.sa' , $data['name']);
					$message->to($options['hm_bl4_contactform'], 'Admin')->subject('dalla Contact Form :'.$data['name']);
					$message->to('R.kh.alhassani@hotmail.com', 'Admin')->subject('dalla Contact Form :'.$data['name']);
				});	*/

                $con='<html>
<head>
  <style>
      table,td{
          border:1px solid #ccc;
      }
      td{
          padding:6px;
      }
  </style> 
</head>
<body>
<h1><img src="'.url('/resources/views/site/assets/img/footer-logo.png').'" /></h1>
<h3>Contact form</h3>
<table align="center" width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td> Name : </td>
  <td> '.$request->name.'</td>
</tr>
<tr>
  <td> Mobile : </td>
  <td> '.$request->phone.' </td>
</tr>
<tr>
  <td> Email : </td>
  <td> '.$request->email.' </td>
</tr>
<tr>
  <td> Massage : </td>
  <td> '.$request->msg .' </td>
</tr>
</table>
</body>
</html>';
                $this->sendemail($options['hm_bl4_contactform'],$fromemail='',$con);
			}

	    	/*if(count(Mail::failures()) ==  0){
				if($request->input('lang') == 'en'){
					$res = array(
						'status' => 'done',
						'message' 	=> 'sent successfuly'
					);
				}else{*/
					$res = array(
						'status' => 'done',
						'message' 	=> 'تم استقبال استفسارك وسيتم التواصل معك ..شكراً لك'
					);
				//}
	    		
	    	/*}else{
	    		$res = array(
					'status' => 'error',
					'code' 		=> 231,
					'message' 	=> 'error'
				);
	    	}*/
			
	    }
		return response()->json($res);
			
	}

	##### arabic ############
	public function page($id){
		$page = Pages::find($id);
		$options = $this->get_home_option();
		
		if(App::getLocale() == 'en'){
			return view('site.en.page')->with('item',$page)->with('options',$options);
		}else{
			return view('site.page')->with('item',$page)->with('options',$options);
		}

	}

	public function site_home(Request $request){
		$news = DB::table('pages')->where('type_','news')->orderBy('pageId','desc')->take(3)->get();
		$slider = DB::table('slider')->orderBy('id','asc')->take(30)->get();
        $schools = DB::table('schools')->where('active',0)->get();

        if(App::getLocale() == 'en'){
			return view('site.en.home',compact('news','slider','schools'));
		}else{
			return view('site.home',compact('news','slider','schools'));
		}
    }

	public function aboutpage(){
		$page = Pages::find(28);
		$options = $this->get_home_option();
		//return view('site.page')->with('item',$page)->with('options',$options);
		
		if(App::getLocale() == 'en'){
			return view('site.en.page')->with('item',$page)->with('options',$options);
		}else{
			return view('site.page')->with('item',$page)->with('options',$options);
		}
	}
	public function servicespage(){
		$page = Pages::find(3);
		$options = $this->get_home_option();
		//return view('site.page')->with('item',$page)->with('options',$options);
		
		if(App::getLocale() == 'en'){
			return view('site.en.page')->with('item',$page)->with('options',$options);
		}else{
			return view('site.page')->with('item',$page)->with('options',$options);
		}
	}
	public function timespage(){
		$page = Pages::find(29);
		$options = $this->get_home_option();
		//return view('site.page')->with('item',$page)->with('options',$options);
		
		if(App::getLocale() == 'en'){
			return view('site.en.page')->with('item',$page)->with('options',$options);
		}else{
			return view('site.page')->with('item',$page)->with('options',$options);
		}
	}
	static function branches(){
		$items = DB::table('branches')->orderBy('id','asc')->get();
		return $items;
	}
	public function branchespage(){
		$options = $this->get_home_option();
		
		if(App::getLocale() == 'en'){
			return view('site.en.branches')->with('options',$options);
		}else{
			return view('site.branches')->with('options',$options);
		}
	}
	
	public function gallerypage(){
		$options = $this->get_home_option();
		
		$items = DB::table('photos_cat')->orderBy('id','asc')->get();
		$items->map(function($item){
			$item->photos = DB::table('photos')->where('type','photo')->where('parent_id',$item->id)->get();
		});

		if(App::getLocale() == 'en'){
			return view('site.en.gallery',compact('items'))->with('options',$options);
		}else{
			return view('site.gallery',compact('items'))->with('options',$options);
		}
	}
	public function faqpage(){
		$options = $this->get_home_option();

		$items = DB::table('faqs')->get();
		
		if(App::getLocale() == 'en'){
			return view('site.en.faq',compact('items'))->with('options',$options);
		}else{
			return view('site.faq',compact('items'))->with('options',$options);
		}
	}
	public function contactpage(){
		$options = $this->get_home_option();
		
		if(App::getLocale() == 'en'){
			return view('site.en.contact')->with('options',$options);
		}else{
			return view('site.contact')->with('options',$options);
		}
	}
	public function testingpage(){
		$options = $this->get_home_option();
		$items = DB::table('questions')->get()->random(32);
		$items->map(function($item){
			$item->asw = DB::table('answers')->where('q_id',$item->id)->get();
		});
        $studentid=Auth::guard('doctor')->user()->IDNumber;

        $today=date('Y-m-d');
        $orders=DB::table('orders')->leftjoin('course','orders.CourseId','=','course.id')
            ->leftjoin('parent_courses','parent_courses.id','=','course.type')
            ->leftjoin('videos','videos.course_id','=','parent_courses.id')
            ->where('orders.idnumber',$studentid)
            ->where('orders.deleted_at','0000-00-00 00:00:00')
            ->where('orders.course_start','>=',$today)
            ->orwhere('orders.course_end','>=',$today)
            ->get();
		if(App::getLocale() == 'en'){
			return view('site.en.testing',compact('items','orders'))->with('options',$options);
		}else{
			return view('site.testing',compact('items','orders'))->with('options',$options);
		}
	}

    public function testingpage_lang(Request $request){
        $options = $this->get_home_option();
        $items = DB::table('questions')->get()->random(32);
        $items->map(function($item){
            $item->asw = DB::table('answers')->where('q_id',$item->id)->get();
        });

            $lang = $request->lang;


        if(App::getLocale() == 'en'){
            return view('site.en.testing_lang',compact('items','lang'))->with('options',$options);
        }else{
            return view('site.testing_lang',compact('items','lang'))->with('options',$options);
        }
    }

	public function news(Request $request){
		$news = DB::table('pages')->where('type_','news')->orderBy('pageId','desc')->get();
		if(App::getLocale() == 'en'){
			return view('site.en.news',compact('news'));
		}else{
			return view('site.news',compact('news'));
		}
	}
	
	public function news_item($id){
		$item = DB::table('pages')->where('type_','news')->where('pageId',$id)->first();
		if(App::getLocale() == 'en'){
			return view('site.en.news_item',compact('item'));
		}else{
			return view('site.news_item',compact('item'));
		}
    }

	public function checkAnswers(Request $request){
		foreach($request->input('q') as $k => $v){
			if(isset($request->input('sw')[$k])){
				$arr = [
					'q' => $v,
					'sw' => $request->input('sw')[$k],
					'correct' => ($this->wrongAnswers($v) == $request->input('sw')[$k]) ? 1 : 0
				];
			}else{
				$arr = [
					'correct' =>  0
				];
			}
		}
		return response()->json($arr);
	}

	public function testing(Request $request){


		//
		$total = 0;
		foreach($request->input('q') as $k => $v){
			if(isset($request->input('sw')[$k])){
				if($this->wrongAnswers($v) == $request->input('sw')[$k]){
					$total += 1;
				}
				$arr[] = [
					'q' => $v,
					'sw' => $request->input('sw')[$k],
					'correct' => ($this->wrongAnswers($v) == $request->input('sw')[$k]) ? 1 : 0
				];
			}
		}

		if($request->input('lang') == 'en'){
			$table = "<h3>Result : {$total} / 32</h3>";
		if($total < 24){
			$table .= "<h3 style='color:red;'>Failure</h3>";
		}else{
			$table .= "<h3 style='color:green;'>successful</h3>";
		}
		/*
		$table .= "<h2>correct answers</h2>";
		$table .= "<table class='table table-borderd resulq'>";
		$table .= "<tr>
				<td></td>
				<td>question</td>
				<td>correct answer</td>
			</tr>" ;
		$items = DB::table('questions')->whereIn('id',$request->input('q'))->select('id','name_en','photo')->get();
		foreach($items as $item){
			$asw = DB::table('answers')->where('q_id',$item->id)->where('correct',1)->first();
			$item->asw = $asw->name_en;
			
			$table .= "<tr>
				<td><img src='".url('/public/questions/'.$item->id.'.jpg')."' /></td>
				<td>{$item->name_en}</td>
				<td>{$asw->name_en}</td>
			</tr>" ;
		}
		$table .= "</table>" ;
		*/
		}else{
			
		

		$table = "<h3>النتيجة : {$total} / 32</h3>";
		if($total < 24){
			$table .= "<h3 style='color:red;'>راسب</h3>";
		}else{
			$table .= "<h3 style='color:green;'>ناجح</h3>";
		}
		/*
		$table .= "<h2>الاجابات الصحيحة</h2>";
		$table .= "<table class='table table-borderd resulq'>";
		$table .= "<tr>
				<td></td>
				<td>السؤال</td>
				<td>الاجابة الصحيحة</td>
			</tr>" ;
		$items = DB::table('questions')->whereIn('id',$request->input('q'))->select('id','name','photo')->get();
		foreach($items as $item){
			$asw = DB::table('answers')->where('q_id',$item->id)->where('correct',1)->first();
			$item->asw = $asw->name;
			
			$table .= "<tr>
				<td><img src='".url('/public/questions/'.$item->id.'.jpg')."' /></td>
				<td>{$item->name}</td>
				<td>{$asw->name}</td>
			</tr>" ;
		}
		$table .= "</table>" ;
		*/
	}

		return response()->json(['items'=>$table,'total' => $total.'/100','success'=>$total,'danger'=>(32 - $total)]);

	}

	private function wrongAnswers($id){
		$item = DB::table('answers')->where('q_id',$id)->where('correct',1)->first();
		return $item->id;
	}


    public function sendemail($toemail,$fromemail,$con)
    {
        $fromemail='info@dallahdrivingschool.sa';
        $config = '{ 
                     "sender":{  
                              "name":"Dallah driving ",
                              "email":"'.$fromemail.'"
                           },
                           "to":[  
                              {  
                                 "email":"'.$toemail.'",
                                 "name":"Reciver Name"
                              }
                           ],
                           "subject":"Dallah driving",
                           "htmlContent":'.json_encode($con).'
                     }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.sendinblue.com/v3/smtp/email',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$config,
            CURLOPT_HTTPHEADER => array(
                'api-key:'.Config::get('app.sendblue_apikey'),
                'Accept: application/json',
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);

        curl_close($curl);


    }
	

	
}
