<?php

namespace App\Http\Controllers;
use App\Models\Evaluation_reserve;
use Excel;
use PDF;
use Validator;
use App;
use App\Customer;
use App\User;
use Auth;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Classes\Hijri;
use Carbon\Carbon;
use Password;
use Storage;

use Config;
class CustomerController extends Controller
{
	private function permations($name){
		$res = AdminController::permations($name);
		return $res;
    }
    
    public function condate()
    {
        /*$DateConv=new Hijri;
				$format="YYYYMMDD";
				$datev="14100412";
				$date =$DateConv->HijriToGregorian($datev,$format);
				$d=date('Y-m-d',strtotime($date));
				dd($date.'/'.$d);
        $startDate = Carbon::parse('1438/01/01');
        $endDate   = Carbon::parse('1439/01/01');
        $dateDiff = $startDate->diff($endDate)->format("%y");

             $today = Carbon::today();
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da=date('Ymd',strtotime($today));

            $datev="{$da}";
            $d =$DateConv->GregorianToHijri($datev,$format);
            $dh_today=date('Y/m/d',strtotime($d));

        $date="2004/06/10";
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da_st=date('Ymd',strtotime($date));

        $date_sta="{$da_st}";
        $d_st =$DateConv->GregorianToHijri($date_sta,$format);
        $dh_start=date('Y/m/d',strtotime($d_st));


        $today = Carbon::today();
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da=date('Ymd',strtotime($today));

        $datev="{$da}";
        $d =$DateConv->GregorianToHijri($datev,$format);
        $dh_today=date('Y/m/d',strtotime($d));

        $startDate = Carbon::parse($dh_start);
        $endDate   = Carbon::parse($dh_today);
        $y = $startDate->diff($endDate)->format("%y");*/
        $datest="10/9/1419";
        //$d =$DateConv->HijriToGregorian($datev,$format);
        //		$date=date('Y-m-d',strtotime($d));

        /*$today = Carbon::today();
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da=date('Ymd',strtotime($today));

        $datev="{$da}";
        $d =$DateConv->GregorianToHijri($datev,$format);
        $dh_today=date('Y/m/d',strtotime($d));

        $startDate = Carbon::parse($datest);
        $endDate   = Carbon::parse($dh_today);
        $y = $startDate->diff($endDate)->format("%y");


        dd($y);
        $date='1423/12/29';
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da_st=date('Ymd',strtotime($date));

        $date_sta="{$da_st}";
        $d_st =$DateConv->GregorianToHijri($date_sta,$format);
        $dh_start=$date;


        $today = Carbon::today();
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da=date('Ymd',strtotime($today));

        $datev="{$da}";
        $d =$DateConv->GregorianToHijri($datev,$format);
        $dh_today=date('Y/m/d',strtotime($d));

        $startDate = Carbon::parse($dh_start);
        $endDate   = Carbon::parse($dh_today);
        $y = $startDate->diff($endDate)->format("%y");
        $date="2002/07/15";

        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da_st=date('Ymd',strtotime($date));

        $date_sta="{$da_st}";
        $d_st =$DateConv->GregorianToHijri($date_sta,$format);
        $dh_start=date('Y/m/d',strtotime($d_st));


        $today = Carbon::today();
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da=date('Ymd',strtotime($today));

        $datev="{$da}";
        $d =$DateConv->GregorianToHijri($datev,$format);
        $dh_today=date('Y/m/d',strtotime($d));

        $startDate = Carbon::parse($dh_start);
        $endDate   = Carbon::parse($dh_today);
        $y = $startDate->diff($endDate)->format("%y");*/
        $dateinput="2002/07/15";

        $date1=date('Y/m/d',strtotime($dateinput));
        // $dateinput="{$request->d_year}/{$request->d_month}/{$request->d_day}";
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da_st=date('Ymd',strtotime($date1));

        $date_sta="{$da_st}";
        $d_st =$DateConv->GregorianToHijri($date_sta,$format);
        $dh_start=date('Y/m/d',strtotime($d_st));


        $today = Carbon::today();
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da=date('Ymd',strtotime($today));

        $datev="{$da}";
        $d =$DateConv->GregorianToHijri($datev,$format);
        $dh_today=date('Y/m/d',strtotime($d));

        $startDate = Carbon::parse($dh_start);
        $endDate   = Carbon::parse($dh_today);
        $y = $startDate->diff($endDate)->format("%y");
        dd($y);


    }

    public function allcustomers(Request $request)
    {
        $columns = array(
            0=>'#',
            1 =>'id',
            2 =>'name',
            3=> 'id_no',
            4=> 'email',
            5=> 'mobile',
            6=>'nationality',
            7=> 'dob',
            8=>'school',
            9=>'created_at',
            10=>'gender',
            11=>'options'
        );

        $totalData = Customer::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = Customer::leftjoin('nationality','nationality.Id','users.NationalityId')
                ->leftJoin('schools','schools.Id','=','users.school');

            $posts=$posts->select('users.*','schools.Name as sname',DB::RAW('nationality.Name as Nationality'))
                ->offset($start)
                ->limit($limit)
                ->orderBy('users.StudentId')
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  Customer::leftjoin('nationality','nationality.Id','users.NationalityId')
                ->leftJoin('schools','schools.Id','=','users.school')
                ->where('users.StudentId','LIKE',"%{$search}%")
                ->orWhere('FullName', 'LIKE',"%{$search}%")
                ->orWhere('IDNumber', 'LIKE',"%{$search}%")
                ->orWhere('email', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->select('users.*','schools.Name as sname',DB::RAW('nationality.Name as Nationality'))
                ->offset($start)
                ->limit($limit)
                //->orderBy($order,$dir)
                ->get();

            $totalFiltered = Customer::leftjoin('nationality','nationality.Id','users.NationalityId')
                ->leftJoin('schools','schools.Id','=','users.school')
                ->where('users.StudentId','LIKE',"%{$search}%")
                ->orWhere('FullName', 'LIKE',"%{$search}%")
                ->orWhere('IDNumber', 'LIKE',"%{$search}%")
                ->orWhere('email', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  url('admin/customers/'.$post->StudentId.'/delete');
                $edit =  url('admin/customers/'.$post->StudentId.'/update');
                $nestedData['#']='<input type="checkbox" value="'.$post->StudentId.'" name="StudentId[]">';
                $nestedData['id'] = $post->StudentId;
                $nestedData['name'] = $post->FullName;
                $nestedData['id_no'] = $post->IDNumber;
                $nestedData['email'] = $post->email;
                $nestedData['mobile'] = $post->mobile;
                $nestedData['nationality'] = $post->Nationality;
                if($post->date_type == 1){
                     \GeniusTS\HijriDate\Date::setToStringFormat('Y-m-d');
                     $date = date('Y-m-d', strtotime($post->DoB. ' - 1 days'));
                    $date=\GeniusTS\HijriDate\Hijri::convertToHijri($date);
                    $time=explode("-",$date);
                    $day=$time['2'];
                    $month=$time['1'];
                    $year=$time['0'];
                    $ss=$year.'-'.$month.'-'.$day;
                }else{
                    $ss= $post->DoB ;
                }
                $nestedData['dob'] = $ss;
                $nestedData['school'] = $post->sname;
                $nestedData['created_at'] = date('d-m-Y h:i',strtotime($post->created_at));
                $nestedData['gender'] = $post->gender;
                $nestedData['options'] = "&emsp;<a href='{$edit}' title='edit' class='btn btn-default btn-sm btn-icon icon-left'><i class='entypo-pencil'></i>تعديل</a>
                                          &emsp;<a data-href='{$show}' title='delete' href='#'  data-toggle='modal' data-target='#confirm-delete' class='btn btn-danger'><i class='entypo-cross'></i>حذف</a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
    public function customers(Request $request){
        $query = Customer::leftjoin('nationality','nationality.Id','users.NationalityId')
                         ->leftJoin('schools','schools.Id','=','users.school');
        if(!empty($request->FullName)){
            $query->where('FullName','LIKE','%'.$request->FullName.'%');
        }
        if(!empty($request->email)){
            $query->where('email','LIKE','%'.$request->email.'%');
        }
        if(!empty($request->mobile)){
            $query->where('mobile','LIKE','%'.$request->mobile.'%');
        }
        if(!empty($request->IDNumber)){
            $query->where('IDNumber','LIKE',$request->IDNumber);
        }

        if(!empty($request->school_id)){
            $query->where('users.school',$request->school_id);
        }
        if(!empty($request->nationality)){
            $query->where('users.NationalityId',$request->nationality);
        }
        $customers = $query->select('users.*','schools.Name as sname',DB::RAW('nationality.Name as Nationality'))
            ->orderByDesc('created_at')->get();
        $nationalitys=DB::table('nationality')->get();
        $schools = DB::table('schools')->get();
        return view('admin.customers.customers',compact('customers','nationalitys','schools'));
    }
    public function search_customer(Request $request){
        $query = Customer::leftjoin('nationality','nationality.Id','users.NationalityId')
            ->leftJoin('schools','schools.Id','=','users.school');
        if(!empty($request->FullName)){
            $query->where('FullName','LIKE','%'.$request->FullName.'%');
        }
        if(!empty($request->email)){
            $query->where('email','LIKE','%'.$request->email.'%');
        }
        if(!empty($request->mobile)){
            $query->where('mobile','LIKE','%'.$request->mobile.'%');
        }
        if(!empty($request->IDNumber)){
            $query->where('IDNumber','LIKE',$request->IDNumber);
        }

        if(!empty($request->school_id)){
            $query->where('users.school',$request->school_id);
        }
        if(!empty($request->nationality)){
            $query->where('users.NationalityId',$request->nationality);
        }
        $customers = $query->select('users.*','schools.Name as sname',DB::RAW('nationality.Name as Nationality'))
            ->orderByDesc('created_at')->get();
        $nationalitys=DB::table('nationality')->get();
        $schools = DB::table('schools')->get();
        return view('admin.customers.customers_result',compact('customers','nationalitys','schools'));
    }
    public function uncourse(){
        $query = Customer::leftjoin('nationality','nationality.Id','users.NationalityId')
            ->leftJoin('schools','schools.Id','=','users.school');

        $customers = $query->select('users.*','schools.Name as sname',DB::RAW('nationality.Name as Nationality'))
            ->orderByDesc('created_at')->get();
        return view('admin.customers.customers_uncourse',compact('customers'));
    }

    public function Delete($id){
        $item = Customer::find($id);
        if($item->delete()){
            Session::put('success',Lang::get('validation.Saved'));
            return redirect('/admin/customers');
        }else{
            return redirect()->back()->withErrors('تم حذف المشترك بنجاح');
        }
    }

    public function multiDelete(Request $request){
        if(!empty($request->StudentId) && Customer::whereIn('StudentId',$request->StudentId)->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect()->back();
        }else{
            return redirect()->back()->withErrors('لم تقم بتحديد اي عناصر لحذفها');
        }
    }

    public function getCustomerUpdate($userid){
        $customer = Customer::find($userid);
        $nationality = DB::table('nationality')->pluck('Name','Id');
        $schools = DB::table('schools')->get();

        return view('admin.customers.update',compact('customer','nationality','schools'));
    }

    public function customerUpdate($userid,Request $request){
		$lang = App::getLocale();
		if(!isset($request->d_type) || $request->d_type!= '1'){	
			$message = [
				'FullName.required' => 'الاسم مطلوب',
				'email.email' => 'بريد الالكتروني غير صحيح',
			 //   'email.unique' => 'البريد الالكتروني مستخدم بالفعل',
				'mobile.required' => 'رقم الجوال مطلوب',
				'NationalityId.required' => 'الجنسية مطلوبة',

					'd_day.required' => 'ادخل تاريخ الميلاد',
					'd_month.required' => 'ادخل تاريخ الميلاد',
					'd_year.required' => 'ادخل تاريخ الميلاد',			
			];
			$rules = [
				'FullName' => 'required|min:2|max:150',
				'mobile' 	=> 'required',
				'd_day' 	=> 'required',
				'd_month' 	=> 'required',
				'd_year' 	=> 'required',
			];
			
		}else{
			
			$message = [
				'FullName.required' => 'الاسم مطلوب',
				'email.email' => 'بريد الالكتروني غير صحيح',
			 //   'email.unique' => 'البريد الالكتروني مستخدم بالفعل',
				'mobile.required' => 'رقم الجوال مطلوب',
				'NationalityId.required' => 'الجنسية مطلوبة',

					'd_day2.required' => 'ادخل تاريخ الميلاد',
					'd_month2.required' => 'ادخل تاريخ الميلاد',
					'd_year2.required' => 'ادخل تاريخ الميلاد',			
			];
			$rules = [
				'FullName' => 'required|min:2|max:150',
				'mobile' 	=> 'required',
				'd_day2' 	=> 'required',
				'd_month2' 	=> 'required',
				'd_year2' 	=> 'required',
			];
			
		}
        $validation = Validator::make($request->all(),$rules,$message);
        if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
        }else{
			if(!$this->is_arabic($request->input('FullName'))){
				if(isset($lang) && $lang == 'en'){
					$res = array(
						'status' 	=> 'error',
						'code' 		=> 231,
						'message' 	=> 'Enter Full Name With Arabic'
					);						
				}else{
					$res = array(
						'status' 	=> 'error',
						'code' 		=> 231,
						'message' 	=> 'أدخل الاسم باللغة العربية'
					);					
				}
				//return response()->json($res);
                return redirect()->back()->withErrors($res['message']);

            }
			
			$d_type = $request->d_type;

			if(!isset($d_type) || $d_type!= '1'){				
				if(!checkdate($request->d_month,$request->d_day,$request->d_year)){
					return Redirect::back()->withErrors(['msg', 'أدخل تاريخ الميلاد صحيح'])->withInput();
				}				
				
			}



			if(isset($d_type) && $d_type== '1'){

				$Date = \GeniusTS\HijriDate\Hijri::convertToGregorian($request->d_day2, $request->d_month2, $request->d_year2);
				$date = date('Y-m-d', strtotime($Date. ' + 1 days'));


			}else{
				$date="{$request->d_year}-{$request->d_month}-{$request->d_day}";
                $d_type=0;
 

			}			
				
                $add = Customer::find($userid);
                $add->FullName	= $request->input('FullName');
				$add->mobile 	= $request->input('mobile');
                $add->IDNumber 	= $request->input('IDNumber');
                $add->email 	= $request->input('email');
                $add->school 	= $request->input('school');
                $add->NationalityId 		= $request->input('NationalityId');
                $add->DoB 		= $date;
				$add->date_type = $d_type;
				
			

				$photo = $request->file('file');
				if(!empty($photo)){
					$photo_name = str_random(5).time();
					$photo_ext = $photo->getClientOriginalExtension();
					$photo_fullname = $photo_name.'.'.$photo_ext;
					$path = public_path().'/photos/';
					//$upload = $photo->move($path,$photo_fullname);
					$thumb = Image::make($photo)->resize(400, 400)->save($path.$photo_fullname);
					$add->avatar = $photo_fullname;
				}
				
				if(!empty($request->input('newpassword'))){
					$add->password = Hash::make($request->input('newpassword'));
				}
				
				
                if($add->save()){
                    Session::put('success',Lang::get('validation.Saved'));
                    return redirect()->back();
                }else{
                    return redirect()->back()->withErrors(Lang::get('validation.error'));
                }
        }
    }

    public function customers_report(Request $request){
        ini_set('memory_limit', '-1');
        $query= Customer::leftjoin('nationality','nationality.Id','users.NationalityId')
            ->leftJoin('schools','schools.Id','=','users.school');

		if($request->from){
			$query->whereDate('users.created_at','>=',$request->from);
		}
		if($request->to){
			$query->whereDate('users.created_at','<=',$request->to);
		}
		
		 $customers = $query->select('users.*','schools.Name as sname',DB::RAW('nationality.Name as Nationality'))->get();
        Excel::create('getCustomers', function($excel) use($customers) {
			$excel->sheet('customers', function($sheet) use($customers) {
			  $sheet->loadView('excel.customers', ['customers' => $customers]);
			});
		})->export('xls');
    }
    public function customers_drive(Request $request){
        ini_set('memory_limit', '-1');
        $query= Customer::leftjoin('nationality','nationality.Id','users.NationalityId')
            ->leftJoin('schools','schools.Id','=','users.school');

        $customers = $query->select('users.*','schools.Name as sname',DB::RAW('nationality.Name as Nationality'))->get();
        Excel::create('Customersall', function($excel) use($customers) {
            $excel->sheet('customers', function($sheet) use($customers) {
                $sheet->loadView('excel.customers', ['customers' => $customers]);
            });
        })->store('xlsx', storage_path('app/public/excel'), true);

        Storage::disk('googledrive')->put('Customersall.xlsx', file_get_contents(storage_path('app/public/excel/Customersall.xlsx')));

    }


    static function Nationality(){
        $Nationality = DB::table('nationality')->orderBy('_order','desc')->get();
        return $Nationality;
    }
	public function get_signup(){
        $Nationality = DB::table('nationality')->pluck('Name','Id');
        return view('site.user.signup',compact('Nationality'));
        //return "login page";
    }
	
	public function dateDifference($date_1 , $date_2 , $differenceFormat = '%y' )
	{
		$datetime1 = date_create($date_1);
		$datetime2 = date_create($date_2);
	   
		$interval = date_diff($datetime1, $datetime2);
	   
		return $interval->format($differenceFormat);
	   
	}
function HijriToJD($m, $d, $y){
   $DateConv=new Hijri;
				$format="YYYY-MM-DD";
				$datev="{$y}-{$m}-{$d}";
				$date =$DateConv->HijriToGregorian($datev,$format);
				
				
				$today = Carbon::today();
			$y = $this->dateDifference($date,$today);
				return $date.'<br />'.$today.'<br />'.intval($y);
}
	/**
     * signup Customer.
     *
     * @param
     * @return
     */
	public function runquery(){
		$rows = Customer::where('date_type',1)->select('StudentId','DoB')->get();
		if($rows->count() > 0){
			foreach($rows as $rw){
				$id = $rw->StudentId;
				$customer = Customer::find($id);
				$olddate = $rw->DoB;
				$time=explode("-",$olddate);

				$day=$time['2'];
				$month=$time['1'];
				$year=$time['0'];
				$Date = \GeniusTS\HijriDate\Hijri::convertToGregorian($day, $month, $year);
				$date = date('Y-m-d', strtotime($Date. ' + 1 days'));		
				$customer->DoB = $date;
				$customer->save();
			}
		}

	}		
	 public function new_dir()
     {
        // Storage::disk('googledrive')->makeDirectory('national');

         $name='mnfVj1614775019.jpg';
         Storage::disk('godrive')->put($name, file_get_contents(storage_path('../public/photos/thumb/2154785412/mnfVj1614775019.jpg' )));

         /*$dir = '/';
         $recursive = true;
         $files = Storage::drive('googledrive')->listContents($dir, $recursive);
         dd($files);*/
     }
    /*public function signup(Request $request)
    {		

		// print_r($request->all());
		// return;
		$lang = App::getLocale();	
        if(App::getLocale() == 'en'){
            $message = [];
        }else{
            $message = [
                'FullName.required' => 'الاسم مطلوب',
                'FullName.min' => 'برجاء ادخال الاسم ثلاثي',
                'email.email' => 'بريد الالكتروني غير صحيح',
                'email.unique' => 'البريد الالكتروني مستخدم بالفعل',
                'mobile.required' => 'رقم الجوال مطلوب',
                'password.required' => 'ادخل كلمة المرور',
    			'd_day.required' => 'ادخل تاريخ الميلاد',
    			'd_month.required' => 'ادخل تاريخ الميلاد',
    			'd_year.required' => 'ادخل تاريخ الميلاد',
				//'full_date.date' => 'ادخل تاريخ الميلاد صحيح',
    			'IDNumber.required' => 'رقم الهوية مطلوب',
    			'IDNumber.digits_between' => 'رقم الهوية لا تزيد عن 10 ارقام',
    			//'IDNumber.required' => 'تاريخ الميلاد مطلوب',
                //'IDNumber.date' => 'ادخل تاريخ ميلاد صحيح',
                'gender.required'=>'الجنس مطلوب',
                'img_national_id.required'=>'صورة رقم الهوية مطلوب',
                'IDNumber.unique' => 'رقم الهوية مسجل بالفعل قم باستعادة كلمة المرور عن طريق البريد او التواصل مع الادارة',
            ];
        }
		
		$rules = [
					'FullName' 	=> 'required|min:10|max:150',
			'email' 	=> 'unique:users|email|min:5|max:30',
			'mobile' 	=> 'required|unique:users',
			'password' 	=> 'required|min:6|max:50',
			'd_day' 	=> 'required',
			'gender'    =>'required',
			'd_month' 	=> 'required',
			'd_year' 	=> 'required',
			'IDNumber' 	=> 'required|unique:users|digits_between:10,12',
            'img_national_id'=>'required'
			];
			// if($request->d_type != 1){
				// $rules['full_date'] = 'nullable|date';
			// }
			
		$validation = Validator::make($request->all(),$rules,$message);

		if($validation->fails()){
			$errors = $validation->errors();
			$res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> $errors->first()
				);
		}else{
			
			if(!$this->is_arabic($request->input('FullName'))){
				if(isset($lang) && $lang == 'en'){
					$res = array(
						'status' 	=> 'error',
						'code' 		=> 231,
						'message' 	=> 'Enter Full Name With Arabic'
					);						
				}else{
					$res = array(
						'status' 	=> 'error',
						'code' 		=> 231,
						'message' 	=> 'أدخل الاسم باللغة العربية'
					);					
				}
				return response()->json($res);
			}			
			$d_type = $request->d_type;

			if(!isset($d_type) || $d_type!= '1'){				
				if(!checkdate($request->d_month,$request->d_day,$request->d_year)){
					if(App::getLocale() == 'en'){
						$res = array(
							'status' 	=> 'error',
							'code' 		=> 231,
							'message' 	=> 'Please Enter BirthDate Correctly'
						);						
						
					}else{
						$res = array(
							'status' 	=> 'error',
							'code' 		=> 231,
							'message' 	=> 'من فضلك أدخل تاريخ الميلاد صحيح'
						);						
						
						
					}

					return response()->json($res);
				}				
				
			}			
			
			$token  = str_random(60);
            $mobile = $request->input('mobile');
            
            if(intval($request->input('birthdayrequired')) != 1){
                $res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> 'لابد من الموافقة على التعهد لاكمال التسجيل'
                );
                return response()->json($res);
            }
			$d_type=$request->d_type;
			if(isset($d_type) && $d_type== '1'){
				//$DateConv=new Hijri;
				//$format="YYYYMMDD";
				$dateinput="{$request->d_year}/{$request->d_month}/{$request->d_day}";

                $dh_start=date('Y/m/d',strtotime($dateinput));


                $today = Carbon::today();
                $DateConv=new Hijri;
                $format="YYYYMMDD";
                $da=date('Ymd',strtotime($today));

                $datev="{$da}";
                $d =$DateConv->GregorianToHijri($datev,$format);
                $dh_today=date('Y/m/d',strtotime($d));

                $startDate = Carbon::parse($dh_start);
                $endDate   = Carbon::parse($dh_today);
                $y = $startDate->diff($endDate)->format("%y");


				$Date = \GeniusTS\HijriDate\Hijri::convertToGregorian($request->d_day, $request->d_month, $request->d_year);
				$date = date('Y-m-d', strtotime($Date. ' + 1 days'));


			}else{
				$date="{$request->d_year}-{$request->d_month}-{$request->d_day}";
                $d_type=0;
                $dateinput="{$request->d_year}/{$request->d_month}/{$request->d_day}";
                $DateConv=new Hijri;
                $format="YYYYMMDD";
                $da_st=date('Ymd',strtotime($dateinput));

                $date_sta="{$da_st}";
                $d_st =$DateConv->GregorianToHijri($date_sta,$format);
                $dh_start=date('Y/m/d',strtotime($d_st));


                $today = Carbon::today();
                $DateConv=new Hijri;
                $format="YYYYMMDD";
                $da=date('Ymd',strtotime($today));

                $datev="{$da}";
                $d =$DateConv->GregorianToHijri($datev,$format);
                $dh_today=date('Y/m/d',strtotime($d));

                $startDate = Carbon::parse($dh_start);
                $endDate   = Carbon::parse($dh_today);
                $y = $startDate->diff($endDate)->format("%y");


			}
			//$today = Carbon::today();
			//$y = $this->dateDifference($date,$today);
			// dd($y);
			if(intval($y) < 17){
                $res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> 'لا يمكن التسجيل فى الدورات لمن عمره أقل من 17 عام'
                );
                return response()->json($res);
            }

            if ($request->hasFile('img_national_id')) {

                $photo = $request->file('img_national_id');
                //$name = str_slug($image) . '.' . $image->getClientOriginalExtension();

                $photo_name = $request->input('IDNumber');
                $photo_ext = $photo->getClientOriginalExtension();


                if($photo->getClientOriginalExtension()!='jpg' and $photo->getClientOriginalExtension()!='jpeg' and $photo->getClientOriginalExtension()!='png') {
                    $res = array(
                        'status' => 'error',
                        'code' => 231,
                        'message' => 'من فضلك ارفع صورة الهوية بصيغة  (jpg,jpeg,png)'
                    );
                    return response()->json($res);
                }else if($photo->getSize() >'100000') {
                    $res = array(
                        'status' => 'error',
                        'code' => 231,
                        'message' => 'من فضلك ارفع صورة بحجم لا يزيد عن 100KB'
                    );
                    return response()->json($res);
                }else{
                    $photo_fullname = $photo_name . '.' . $photo_ext;
                    $path = public_path().'/photos/thumb/' . $request->input('IDNumber').'/';
                    File::makeDirectory($path);
                    $thumb = Image::make($photo)->save($path . $photo_fullname);
                    Storage::disk($request->input('school'))->put($photo_fullname, file_get_contents(storage_path('../public/photos/thumb/'.$request->input('IDNumber').'/'.$photo_fullname )));

                }
            }else{
                $photo_fullname='';
            }

			$add = new Customer;
            $add->FullName	= $request->input('FullName');
            $add->IDNumber	= $request->input('IDNumber');
            $add->gender	= $request->input('gender');
            $add->school	= $request->input('school');
            $add->DoB	= $date;
            $add->NationalityId	= $request->input('NationalityId');
			$add->email 	= $request->input('email');
            $add->date_type 	= $d_type;
            $add->type_reg=0;
			//$add->country 	= $request->input('country');
			//$add->city 		= $request->input('city');
			$add->mobile 	= $mobile;
			$add->mobile_st = 1;
			$add->img_national_id=$photo_fullname;
			$add->password 	= Hash::make($request->input('password'));
	    	if($add->save() && Auth::guard('doctor')->attempt(['email' => $request->email, 'password' => $request->password])){
				
					$res = array(
						'status' 	=> 'done',
						'code' 		=> 200,
						'message' 	=> 'تم التسجيل بنجاح'
					);
	    	}else{
				$res = array(
					'status' 	=> 'error',
					'code' 		=> 400,
					'message' 	=> 'خطأ فى التسجيل'
				);
			}
		}
		return response()->json($res);
    }*/
    public function signup(Request $request)
    {
        $lang = App::getLocale();
        // if(isset()){
        // $lang=$request->header('Accept-Language');
        // }


        if(isset($lang) && $lang == 'en'){
            $message = [
                'FullName.required' => 'Name is Required',
                //'FullName.min' => 'برجاء ادخال الاسم ثلاثي',
                'email.email' => 'Email is not Correct' ,
                'email.required' => 'Email is Required ',
                'email.unique' => 'This Email is already Exist',

                'mobile.required' => 'Mobile is Required',
                'mobile.between'=>'Max Mobile Length 10 Number',
                'password.required' => 'Password is Required',
                'd_day.required' => 'ادخل تاريخ الميلاد',
                'd_month.required' => 'ادخل تاريخ الميلاد',
                'd_year.required' => 'ادخل تاريخ الميلاد',                //'IDNumber.required' => 'رقم الهوية مطلوب',
                //'IDNumber.between' => 'Max ID / Residence number Length 10 Number',
                'IDNumber.required' => 'ID / Residence number is Required',
                'NationalityId.required' => 'Nationality is Required',
                'IDNumber.unique' => 'The ID / Residence number is already registered. Recover the password by mail or contact the administration',
                'school.required' => 'School is Required',
            ];
        }else{
            $message = [
                'FullName.required' => 'الاسم مطلوب',
                //'FullName.min' => 'برجاء ادخال الاسم ثلاثي',
                'email.email' => 'بريد الالكتروني غير صحيح',
                'email.required' => 'ادخل البريد الالكترونى ',
                'email.unique' => 'البريد الالكترونى موجود من قبل',
                'gender.required' => 'ادخل الجنس ',
                'mobile.required' => 'رقم الجوال مطلوب',
                'mobile.between'=>'رقم الجوال لا يزيد عن 10 ارقام',
                'password.required' => 'ادخل كلمة المرور',
                'd_day.required' => 'ادخل تاريخ الميلاد',
                'd_month.required' => 'ادخل تاريخ الميلاد',
                'd_year.required' => 'ادخل تاريخ الميلاد',                // 'Birthday.date' => 'ادخل تاريخ ميلاد صحيح',
                //'IDNumber.required' => 'رقم الهوية مطلوب',
               // 'IDNumber.between' => 'رقم الهوية لا تزيد عن 10 ارقام',
                'IDNumber.required' => 'رقم الإقامة مطلوب',
                'NationalityId.required' => 'الجنسية مطلوبة',
                'IDNumber.unique' => 'رقم الهوية مسجل بالفعل قم باستعادة كلمة المرور عن طريق البريد او التواصل مع الادارة',
                'school.required' => 'ادخل المدرسة',

            ];
        }
        $validation = Validator::make($request->all(),[
            'FullName' 	=> 'required|min:10|max:150',
            'email' 	=> 'unique:users|email|min:5|max:30',
            'mobile' 	=> 'required|unique:users|between:1,10',
            'password' 	=> 'required|min:6|max:50',
            'd_day' 	=> 'required',
            'd_month' 	=> 'required',
            'd_year' 	=> 'required',            'IDNumber' 	=> 'required|unique:users',
            'gender' 	=> 'required',
            'school' 	=> 'required',

        ],$message);

        if($validation->fails()){
            $errors = $validation->errors();
            $res = array(
                'status' 	=> 'error',
                'code' 		=> 231,
                'message' 	=> $errors->first()
            );
        }else{
            $token  = str_random(150);
            $mobile = $request->input('mobile');

            if(intval($request->input('birthdayrequired')) != 1){
                if(isset($lang) && $lang == 'en'){
                    $res = array(
                        'status' 	=> 'error',
                        'code' 		=> 231,
                        'message' 	=> 'لابد من الموافقة على التعهد لاكمال التسجيل'
                    );
                }else{
                    $res = array(
                        'status' 	=> 'error',
                        'code' 		=> 231,
                        'message' 	=> 'Conditions must be agreed to complete the registration'
                    );
                }
                return response()->json($res);
            }

            if(!$this->is_arabic($request->input('FullName'))){
                if(isset($lang) && $lang == 'en'){
                    $res = array(
                        'status' 	=> 'error',
                        'code' 		=> 231,
                        'message' 	=> 'Enter Full Name With Arabic'
                    );
                }else{
                    $res = array(
                        'status' 	=> 'error',
                        'code' 		=> 231,
                        'message' 	=> 'أدخل الاسم باللغة العربية'
                    );
                }
                return response()->json($res);
            }
            if($request->input('gender')=='male'){
                if(isset($lang) && $lang == 'en'){
                    $res = array(
                        'status' 	=> 'error',
                        'code' 		=> 231,
                        'message' 	=> 'Registeration for male only'
                    );
                }else{
                    $res = array(
                        'status' 	=> 'error',
                        'code' 		=> 231,
                        'message' 	=> 'التسجيل للاناث فقط '
                    );
                }
                return response()->json($res);
            }


            $d_type = $request->d_type;
            if(!isset($d_type) || $d_type!= '1'){
                if(!checkdate($request->d_month,$request->d_day,$request->d_year)){
               /* $day=$time['2'];
                $month=$time['1'];
                $year=$time['0'];
                if(!checkdate($month,$day,$year)){*/
                    if(isset($lang) && $lang == 'en'){
                        $res = array(
                            'status' 	=> 'error',
                            'code' 		=> 231,
                            'message' 	=> 'Please Enter BirthDate Correctly'
                        );

                    }else{
                        $res = array(
                            'status' 	=> 'error',
                            'code' 		=> 231,
                            'message' 	=> 'من فضلك أدخل تاريخ الميلاد صحيح'
                        );


                    }

                    return response()->json($res);
                }

            }
            /*  if($request->d_type == 'hj'){
                  $DateConv=new Hijri;
                  $format="YYYY-MM-DD";
                  $datev="{$request->d_year}-{$request->d_month}-{$request->d_day}";
                  $date =$DateConv->HijriToGregorian($datev,$format);
              }else{
                  $date="{$request->d_year}-{$request->d_month}-{$request->d_day}";
              }*/

           // $date = $request->input('Birthday');
            $d_type=$request->d_type;
            if(isset($d_type) && $d_type== '1'){
                //$DateConv=new Hijri;
                //$format="YYYYMMDD";
                $dateinput="{$request->d_year}/{$request->d_month}/{$request->d_day}";

                $dh_start=date('Y/m/d',strtotime($dateinput));


                $today = Carbon::today();
                $DateConv=new Hijri;
                $format="YYYYMMDD";
                $da=date('Ymd',strtotime($today));

                $datev="{$da}";
                $d =$DateConv->GregorianToHijri($datev,$format);
                $dh_today=date('Y/m/d',strtotime($d));

                $startDate = Carbon::parse($dh_start);
                $endDate   = Carbon::parse($dh_today);
                $y = $startDate->diff($endDate)->format("%y");


                $Date = \GeniusTS\HijriDate\Hijri::convertToGregorian($request->d_day, $request->d_month, $request->d_year);
                $date = date('Y-m-d', strtotime($Date. ' + 1 days'));


            }else{
                $date="{$request->d_year}-{$request->d_month}-{$request->d_day}";
                $d_type=0;
                $dateinput="{$request->d_year}/{$request->d_month}/{$request->d_day}";
                $DateConv=new Hijri;
                $format="YYYYMMDD";
                $da_st=date('Ymd',strtotime($dateinput));

                $date_sta="{$da_st}";
                $d_st =$DateConv->GregorianToHijri($date_sta,$format);
                $dh_start=date('Y/m/d',strtotime($d_st));


                $today = Carbon::today();
                $DateConv=new Hijri;
                $format="YYYYMMDD";
                $da=date('Ymd',strtotime($today));

                $datev="{$da}";
                $d =$DateConv->GregorianToHijri($datev,$format);
                $dh_today=date('Y/m/d',strtotime($d));

                $startDate = Carbon::parse($dh_start);
                $endDate   = Carbon::parse($dh_today);
                $y = $startDate->diff($endDate)->format("%y");


            }
            if(intval($y) < 17){
                if(isset($lang) && $lang == 'en'){
                    $res = array(
                        'status' 	=> 'error',
                        'code' 		=> 231,
                        'message' 	=> 'It is not possible to register for courses for those under the age of 17'
                    );

                }else{
                    $res = array(
                        'status' 	=> 'error',
                        'code' 		=> 231,
                        'message' 	=> 'لا يمكن التسجيل فى الدورات لمن عمره أقل من 17 عام'
                    );

                }

                return response()->json($res);
            }
            if ($request->hasFile('img_national_id')) {

                $photo = $request->file('img_national_id');
                //$name = str_slug($image) . '.' . $image->getClientOriginalExtension();

                $photo_name = $request->input('IDNumber');
                $photo_ext = $photo->getClientOriginalExtension();
                $size=$request->file('img_national_id')->getSize();


                if($photo->getClientOriginalExtension()!='jpg' and $photo->getClientOriginalExtension()!='jpeg' and $photo->getClientOriginalExtension()!='png') {
                    $res = array(
                        'status' => 'error',
                        'code' => 231,
                        'message' => 'من فضلك ارفع صورة الهوية بصيغة  (jpg,jpeg,png)'
                    );
                    return response()->json($res);
                }else if($size > 100000) {
                    $res = array(
                        'status' => 'error',
                        'code' => 231,
                        'message' => 'من فضلك حجم الصورة لا يزيد عن 100KB'
                    );
                    return response()->json($res);

                }else{
                    $laststudent=Customer::Orderby('StudentId','desc')->first();
                    $fo=md5($laststudent->StudentId);
                    $photo_fullname = $photo_name . '.' . $photo_ext;

                    $path = public_path().'/photos/thumb/' .$fo .'/';
                    if(!File::exists($path)) {
                        File::makeDirectory($path);
                    }
                    $thumb = Image::make($photo)->save($path . $photo_fullname);
                    Storage::disk($request->input('school'))->put($photo_fullname, file_get_contents(storage_path('../public/photos/thumb/'.$fo.'/'.$photo_fullname )));

                }
            }else{
                $photo_fullname='';
            }



            $add = new Customer;
            $add->FullName	= $request->input('FullName');
            $add->IDNumber	= $request->input('IDNumber');
            $add->date_type 	=$d_type;
            if(!empty($date)){
                $add->DoB	    = $date;
            }
            $add->NationalityId	= $request->input('NationalityId');
            if(!empty($request->input('email'))){
                $add->email = $request->input('email');
            }
            //$add->api_token = $token;
            $add->mobile 	= $mobile;
            $add->mobile_st = 1;
            $add->type_reg = 0;
            $add->password 	= Hash::make($request->input('password'));
            $add->gender	= $request->input('gender');
            $add->img_national_id=$photo_fullname;
            $add->school	= $request->input('school');
            if($add->save() && Auth::guard('doctor')->attempt(['email' => $request->email, 'password' => $request->password])){

                $res = array(
                    'status' 	=> 'done',
                    'code' 		=> 200,
                    'message' 	=> 'تم التسجيل بنجاح'
                );
            }else{
                $res = array(
                    'status' 	=> 'error',
                    'code' 		=> 400,
                    'message' 	=> 'خطأ فى التسجيل'
                );
            }
        }
        return response()->json($res);
    }

	public function get_login(){
			
        return view('site.user.login');
        //return "login page";
    }
    public function login(Request $request){
		$lang = App::getLocale();
		if($lang == 'ar'){
			$message = [
				'email.required' => 'رقم الاقامة مطلوب',
				'password.required' => 'ادخل كلمة المرور',
			];			
		}else{
			$message = [
				'email.required' => 'ID Number is Required',
				'password.required' => 'Password is Required',
			];			
			
		}


        $validation = Validator::make($request->all(),[
            'email' 	=> 'required',
            'password' 	=> 'required',
        ],$message);

        if($validation->fails()){
            $errors = $validation->errors();
            $res = array(
                    'status' 	=> 'error',
                    'code' 		=> 231,
                    'message' 	=> $errors->first()
                );
        }else{
            if (Auth::guard('doctor')->attempt(['IDNumber' => $request->email, 'password' => $request->password])) {
                $res = array(
                    'status' 	=> 'done',
                    'code' 		=> 200,
                    'message' 	=> 'تم تسجيل دخولك بنجاح'
                );
            }else{
                $res = array(
                    'status' 	=> 'error',
                    'code' 		=> 231,
                    'message' 	=> 'بيانات الدخول غير صحيحة'
                );
            }
        }
        return response()->json($res);
	}
	
	public function profile(){
        $Nationality = DB::table('nationality')->pluck('Name','Id');
        $user=Auth::guard('doctor')->user()->StudentId;
        $orders=DB::table('orders')->where('StudentId',$user)
                                   ->first();
        $schools = DB::table('schools')->get();
        if(App::getLocale() == 'en'){
            return view('site.en.user.profile',compact('Nationality','orders','schools'));
        }else{
            return view('site.user.profile',compact('Nationality','orders','schools'));
        }
		
	}
	
	public function changepassword(Request $request){
        $validation = Validator::make($request->all(),[
        
        ]);
        if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }else{
                if(!empty($request->input('password')) && $request->input('password') != $request->input('repassword')){
                    return redirect()->back()->withErrors('New password does not match');
                }
                $add = Customer::find(Auth::guard('doctor')->user()->userid);
                $add->password	= Hash::make($request->input('password'));
              //  $add->lastname 	= $request->input('repassword');
                if($add->save()){
                    Session::put('success',Lang::get('validation.Saved'));
                    return redirect()->back();
                }else{
                    return redirect()->back()->withErrors(Lang::get('validation.error'));
                }
        }
	}
	
	public function profileUpdate(Request $request){

        $message = [
            'FullName.required' => 'الاسم مطلوب',
         //   'email.email' => 'بريد الالكتروني غير صحيح',
         //   'email.unique' => 'البريد الالكتروني مستخدم بالفعل',
        //    'mobile.required' => 'رقم الجوال مطلوب',
		//	'NationalityId.required' => 'الجنسية مطلوبة',
        ];

        $validation = Validator::make($request->all(),[
			'FullName' => 'required|min:2|max:150',
			'mobile' 	=> 'required',
        ],$message);
        if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }else{
                $add = Customer::find(Auth::guard('doctor')->user()->StudentId);
                $add->FullName	= $request->input('FullName');
				$add->mobile 	= $request->input('mobile');
				//$add->school 	= $request->input('school');
                //$add->NationalityId 		= $request->input('NationalityId');
               // $add->DoB 		= $request->input('DoB');
				
			

				$photo = $request->file('file');
				if(!empty($photo)){
					$photo_name = str_random(5).time();
					$photo_ext = $photo->getClientOriginalExtension();
					$photo_fullname = $photo_name.'.'.$photo_ext;
					$path = public_path().'/photos/';
					//$upload = $photo->move($path,$photo_fullname);
					$thumb = Image::make($photo)->resize(400, 400)->save($path.$photo_fullname);
					$add->avatar = $photo_fullname;
				}
				
				if(Hash::check($request->input('oldpassword'),$add->password) && !empty($request->input('newpassword')) && $request->input('newpassword') == $request->input('renewpassword')){
					$add->password = Hash::make($request->input('newpassword'));
				}
				
				
                if($add->save()){
                    Session::put('success',Lang::get('validation.Saved'));
                    return redirect()->back();
                }else{
                    return redirect()->back()->withErrors(Lang::get('validation.error'));
                }
        }
    }

    public function logout(){
        Auth::guard('doctor')->logout();
        return Redirect::to('/');
	}
	

	public function get_ressetpassword(){
        return view('site.user.resetpassword');
    }
	
	public function get_ressetpassword_en(){
        return view('site.en.user.resetpassword');
    }

    public function ressetpassword_confirm(Request $request){
        
		$messag = [
			'email.required' => 'ادخل بريد الكتروني صحيح',
			'email.email' => 'ادخل بريد الكتروني صحيح',
		];
		$validation = Validator::make($request->all(),[
			'email' 	=> 'required|email',
        ],$messag);
        if ($validation->fails()) {
               $errors = $validation->errors();
            $res = array(
                    'status' 	=> 'error',
                    'code' 		=> 231,
                    'message' 	=> $errors->first()
                );
        }else{
			$user = User::where('email', $request->input('email'))->first();

			if(!empty($user)){

			$token = Password::broker()->createToken($user);
                DB::table('password_resets')->where('email',$request->input('email'))->update(['token'=>$token]);

			$msg 		= "
				<h1><img src='http://dallahdrivingschool.sa/resources/views/site/images/logo.png' /></h1>
				<h4>استعادة كلمة المرور</h4>
				<a href='".url('/')."/resetlink/{$token}'>Reset password</a>
				
			";
			
			#send msg to email(1)
			/*Mail::send('emails.send', ['msg' => $msg], function ($message) use ($user){
				$message->from('noreply@dallahdrivingschool.sa', 'شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ');
				$message->to($user->email)->subject("استعادة كلمة المرور");
			});*/
$con=html_entity_decode($msg);
                $this->sendemail($user->email,$fromemail='',$con);

			/*if(Mail::failures()){
				$res = array(
                    'status' 	=> 'error',
                    'code' 		=> 231,
                    'message' 	=> 'لم يتم اعادة تعيين كلمة المرور'
                );
				
			}else{*/
				$res = array(
                    'status' 	=> 'done',
                    'code' 		=> 200,
                    'message' 	=> 'تم ارسال رابط تعيين كلمة المرور على البريد الخاص بك'
                );
				
			}
			
			/*}else{
				$res = array(
                    'status' 	=> 'error',
                    'code' 		=> 231,
                    'message' 	=> 'بريد غير مسجل لدينا'
                );
			}*/

		}
		return response()->json($res);
    }

    public function setNewPassword($token){
        $password_resets = DB::table('password_resets')->where('token',$token)->first();
        if(empty($password_resets)){
            return "رابط غير صالح";
        }
		
        $user = Customer::where('email', $password_resets->email)->first();
		
		
		
        $password = str_random(6);
        $user->password = Hash::make($password);
		
		
		
        if($user->save()){
        $msg 		= "
            <h1><img src='http://dallahdrivingschool.sa/resources/views/site/images/logo.png' /></h1>
            <h4> New password : {$password}</h4>
           
        ";
        
        #send msg to email(1)
       /* Mail::send('emails.send', ['msg' => $msg], function ($message) use ($user){
            $message->from('noreply@dallahdrivingschool.sa', 'شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ');
				$message->to($user->email)->subject("كلمة المرور الجديدة");
        });*/
            $con=html_entity_decode($msg);
            $this->sendemail($user->email,$fromemail='',$con);
         DB::table('password_resets')->where('token',$token)->delete();

            if(Mail::failures()){
                return "Error";
            }else{
                return "تم ارسال كلمة المرور الجديدة على البريد الخاص بك";
            }
        }else{
            return "Error";
        }
    }

   
function uniord($u) {
    // i just copied this function fron the php.net comments, but it should work fine!
    $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
    $k1 = ord(substr($k, 0, 1));
    $k2 = ord(substr($k, 1, 1));
    return $k2 * 256 + $k1;
}
function is_arabic($str) {
    if(mb_detect_encoding($str) !== 'UTF-8') {
        $str = mb_convert_encoding($str,mb_detect_encoding($str),'UTF-8');
    }

    /*
    $str = str_split($str); <- this function is not mb safe, it splits by bytes, not characters. we cannot use it
    $str = preg_split('//u',$str); <- this function woulrd probably work fine but there was a bug reported in some php version so it pslits by bytes and not chars as well
    */
    preg_match_all('/.|\n/u', $str, $matches);
    $chars = $matches[0];
    $arabic_count = 0;
    $latin_count = 0;
    $total_count = 0;
    foreach($chars as $char) {
        //$pos = ord($char); we cant use that, its not binary safe 
        $pos = $this->uniord($char);
       // echo $char ." --> ".$pos.PHP_EOL;

        if($pos >= 1536 && $pos <= 1791) {
            $arabic_count++;
        } else if($pos > 123 && $pos < 123) {
            $latin_count++;
        }
        $total_count++;
    }
    if(($arabic_count/$total_count) > 0.6) {
        // 60% arabic chars, its probably arabic
        return true;
    }
    return false;
}
	
	public function remind_sms()
    {
        $date=date('Y-m-d',strtotime("+1 day"));
        $query=Evaluation_reserve::leftjoin('evaluation_times','evaluation_reserves.time_id','=','evaluation_times.id')
            ->where('day',$date)->get();

        if(!empty($query))
        {
            foreach ($query as $qu):
                $student=$qu->student_id;
                $stu=User::where('StudentId',$student)->first();

                $phonenew=substr($stu->mobile,0,1);
                if($phonenew!='0')
                {
                    $phone=$stu->mobile;
                }else{
                    $phone=substr($stu->mobile,1);
                }
                $msg="تذكير لديكم اختبار غدا";

               $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, "https://www.4jawaly.net/api/sendsms.php");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);

                curl_setopt($ch, CURLOPT_POST, TRUE);

                curl_setopt($ch, CURLOPT_POSTFIELDS, 'username=dalla&password=565656&message='.$msg.'&numbers=966'.$phone.'&sender=DDC-DRIVING&unicode=utf8&Rmduplicated=1&return=json');


                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    "Content-Type: application/x-www-form-urlencoded"
                ));

                $response = curl_exec($ch);

                curl_close($ch);

            endforeach;
        }

    }


    function test_send()
    {
        $user = User::where('email', 'moamohamed202@gmail.com')->first();

        $token = Password::broker()->createToken($user);

        $msg 		= "
				<h1><img src='http://dallahdrivingschool.sa/resources/views/site/images/logo.png' /></h1>
				<h4>استعادة كلمة المرور</h4>
				<a href='".url('/')."/resetlink/{$token}'>Reset password</a>
				
			";

        #send msg to email(1)
        Mail::send('emails.send', ['msg' => $msg], function ($message){
            $message->from('noreply@dallahdrivingschool.sa', 'شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ');
            $message->to('moamohamed202@gmail.com')->subject("استعادة كلمة المرور");
        });
    }

    function remind_pay()
    {
        $orders=DB::table('orders')->join('course','course.id','=','orders.CourseId')
                                  ->join('users','users.StudentId','=','orders.StudentId')
                                  ->where('IsPaid',0)
                                  ->where('waiting_list','1')->get();
        $todafter=date('Y-m-d',strtotime('+5 days'));
        if(!empty($orders)){
            foreach ($orders as $ord):

                if($todafter==$ord->course_start){

                    $phonenew=substr($ord->mobile,0,1);
                    if($phonenew!='0')
                    {
                        $phone=$ord->mobile;
                    }else{
                        $phone=substr($ord->mobile,1);
                    }

                    $msg='يمكنكم الدفع من خلال الرابط '.'https://female.dallahdrivingschool.sa/payment_course/'.$ord->OrderId;


                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, "https://www.4jawaly.net/api/sendsms.php");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($ch, CURLOPT_HEADER, FALSE);

                    curl_setopt($ch, CURLOPT_POST, TRUE);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, 'username=dalla&password=565656&message='.$msg.'&numbers=966'.$phone.'&sender=DDC-DRIVING&unicode=utf8&Rmduplicated=1&return=json');


                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        "Content-Type: application/x-www-form-urlencoded"
                    ));

                    $response = curl_exec($ch);

                    curl_close($ch);
                }
                endforeach;
        }

    }

    function cancel_unpaid(){
        $orders=DB::table('orders')->join('course','course.id','=','orders.CourseId')
            ->join('users','users.StudentId','=','orders.StudentId')
            ->where('IsPaid',0)
            ->where('waiting_list','1')->get();
        $todafter=date('Y-m-d',strtotime('+2 days'));
        if(!empty($orders)){
            foreach ($orders as $ord):
                if($todafter==$ord->course_start){

                    $phonenew=substr($ord->mobile,0,1);
                    if($phonenew!='0')
                    {
                        $phone=$ord->mobile;
                    }else{
                        $phone=substr($ord->mobile,1);
                    }

                    $msg="تم الغاء اشتراكم بالدورة وذلك لعدم السداد";


                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, "https://www.4jawaly.net/api/sendsms.php");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($ch, CURLOPT_HEADER, FALSE);

                    curl_setopt($ch, CURLOPT_POST, TRUE);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, 'username=dalla&password=565656&message='.$msg.'&numbers=966'.$phone.'&sender=DDC-DRIVING&unicode=utf8&Rmduplicated=1&return=json');


                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        "Content-Type: application/x-www-form-urlencoded"
                    ));

                    $response = curl_exec($ch);

                    curl_close($ch);

                    DB::table('orders')->where('OrderId',$ord->OrderID)->delete();
                }
            endforeach;
        }
    }


    function special_send()
    {
        $orders=DB::table('orders')->join('course','course.id','=','orders.CourseId')
            ->join('users','users.StudentId','=','orders.StudentId')
            ->where('IsPaid',0)
            ->where('OrderId','7999936')
            ->get();
        $todafter=date('Y-m-d',strtotime('+5 days'));
        if(!empty($orders)){
            foreach ($orders as $ord):

              //  if($todafter==$ord->course_start){

                    $phonenew=substr($ord->mobile,0,1);
                    if($phonenew!='0')
                    {
                        $phone=$ord->mobile;
                    }else{
                        $phone=substr($ord->mobile,1);
                    }

                    $msg='رجاء السداد بهذا الرابط فقط'.'https://female.dallahdrivingschool.sa/payment_course/'.$ord->OrderId;


                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, "https://www.4jawaly.net/api/sendsms.php");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($ch, CURLOPT_HEADER, FALSE);

                    curl_setopt($ch, CURLOPT_POST, TRUE);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, 'username=dalla&password=565656&message='.$msg.'&numbers=966'.$phone.'&sender=DDC-DRIVING&unicode=utf8&Rmduplicated=1&return=json');


                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        "Content-Type: application/x-www-form-urlencoded"
                    ));

                    $response = curl_exec($ch);

                    curl_close($ch);
                //}
            endforeach;
        }

    }

    public function idtest()
    {
       // dd(Auth::guard('doctor')->user()->StudentId);
        //$dir = url('/public/photos/thumb/');
        $path = public_path('photos/thumb');
        $customers=Customer::get();
        $files = File::allfiles($path);

$i=0;
             foreach($files as $file)
             {

                     $oldpath = explode('/', $file->getpath());
                     $old = $oldpath[0] . '/' . $oldpath[1] . '/' . $oldpath[2] . '/' . $oldpath[3] . '/' . $oldpath[4] . '/' . $oldpath[5] . '/' . $oldpath[6] . '/' . $oldpath[7] . '/' . @$oldpath[8];
                     $old2 = $oldpath[0] . '/' . $oldpath[1] . '/' . $oldpath[2] . '/' . $oldpath[3] . '/' . $oldpath[4] . '/' . $oldpath[5] . '/' . $oldpath[6] . '/' . $oldpath[7] . '/';
                     $wo = str_replace(' ', '', @$oldpath[8]);
                     //print_r($oldpath).'<br>';
                 if(strlen($wo)<= 11) {

                     $imgname = explode('/', $file->getpathname());
                     $img = @$imgname[9];


                     $wo2 = str_replace(' ', '', $img);
                     //echo $wo2.'<br />';
                     $mm = explode('.', $wo2);
                     $imgid = $mm[0];
                      //if($imgid== '1087400709') {
                       //  $imgid= '1087400709';

                          $st = Customer::where('IDNumber', $imgid)->first();
                         // dd($st);
                          //dd($old2 . $wo);
                          //echo $old2 . @$oldpath[8] . '<br />';exit;
                          //echo $imgname[8].'/'.@$imgname[9].'<br />'.md5($st->IDNumber);
                          if (!empty($st)) {
                            //echo $oldpath[8];
                              @rename($old2 . @$oldpath[8], $old2 . md5($st->StudentId));
                          }
                          //echo $old2 . @$oldpath[8] . '<br />';exit;
                     // }
                     //dd('ok');
                     // echo $imgname[8].'/'.@$imgname[9].'<br />'.md5($st->IDNumber);exit;

                     //}
                     /* $wo2=str_replace(' ','',@$oldpath[9]);
                                $mm=explode('.',$wo2);
                                $imgid=$mm[0];
                                if($imgid=='1234567891') {
                                    $st = Customer::where('IDNumber', '1234567891')->first();
                                    dd($st->StudentId.'\\'.md5($st->StudentId));
                                    //rename($old2 . $wo, $old2 . md5($st->StudentId));
                                }*/

                     /* foreach($customers as $cu):
                          $nat=$cu['IDNumber	'];
                          $natt=md5($nat);
                          if(file_exists($old2.$wo)  ) {
                              if(strlen($wo)<= 11) {
                                  rename($old2 . $wo, $old2 . md5($cu['StudentId']));
                              }else{
                                  if(md5($cu['IDNumber'])==$wo) {
                                      rename($old2 . $wo, $old2 . md5($cu['StudentId']));
                                  }
                              }
                          }
                          endforeach;*/

                     // dd($old2);
                 }
                 $i++;
             }


        // Foreach directory, recursively get and add sub directories
        // Return list of sub directories
       // return $subDir;
       // return $directories;



    }


    public function sendemail($toemail,$fromemail,$con)
    {
        $fromemail='info@dallahdrivingschool.sa';
        $config = '{ 
                     "sender":{  
                              "name":"Dallah driving ",
                              "email":"'.$fromemail.'"
                           },
                           "to":[  
                              {  
                                 "email":"'.$toemail.'",
                                 "name":"Reciver Name"
                              }
                           ],
                           "subject":"Dallah driving",
                           "htmlContent":'.json_encode($con).'
                     }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.sendinblue.com/v3/smtp/email',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$config,
            CURLOPT_HTTPHEADER => array(
                'api-key:'.Config::get('app.sendblue_apikey'),
                'Accept: application/json',
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);

        curl_close($curl);


    }
}
