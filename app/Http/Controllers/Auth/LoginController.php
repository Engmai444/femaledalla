<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Order;
use App\User;
use App;
use Hash;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/member';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout','member','profile','profileUpdate']]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    public function member(){
        $orders = Order::where('customer_id',Auth::guard()->user()->customer_id)->where('cancelled',0)->orderBy('created_at','desc')->paginate(50);
        if(App::getLocale() == 'en'){
            return view('auth.en.member',compact('orders'));
        }else{
            return view('auth.member',compact('orders'));
        } 
    }

    public function profile(){
        if(App::getLocale() == 'en'){
            return view('auth.en.profile');
        }else{
            return view('auth.profile');
        } 
    }

    public function profileUpdate(Request $request){
        $validation = Validator::make($request->all(),[
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'mobile' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
            $user = User::find(Auth::guard()->user()->customer_id);
            $user->firstname    = $request->firstname;
            $user->lastname     = $request->lastname;
            $user->email        = $request->email;
            $user->mobile       = $request->mobile;
            if (Hash::check($request->old_password, $user->password) && !empty($request->new_password) && $request->new_password == $request->renew_password) {
                $user->password       = Hash::make($request->new_password);
            }
            if($user->save()){
                Session::put('success',Lang::get('validation.Saved'));
                return redirect()->back();
            }else{
                return redirect()->back()->withErrors(Lang::get('validation.passworderror'));
            }

        }
    }

}
