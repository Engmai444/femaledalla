<?php

namespace App\Http\Controllers;

use Validator;
use App\Pages;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    //
	private function permations($name){
		$res = AdminController::permations($name);
		return $res;
	}
	
	
	public function view_new(){
		return view('admin.pages_new');
	}
	public function store(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'title' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = new Pages;
		    $add->title			= $request->input('title');
			$add->content 		= $request->input('content');
			$add->seo_title		= $request->input('seo_title');
			$add->seo_desc 		= $request->input('seo_desc');
			$add->title_ar		= $request->input('title_ar');
			$add->content_ar 	= $request->input('content_ar');
			$add->seo_title_ar	= $request->input('seo_title_ar');
			$add->seo_desc_ar 	= $request->input('seo_desc_ar');
			
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	public function view_update($id){
		$item = Pages::find($id);
		return view('admin.pages_update')->with('page',$item);
	}
	public function update($id,Request $request)
    {
		$validation = Validator::make($request->all(),[
			'title' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = Pages::find($id);
		    $add->title			= $request->input('title');
			$add->content 		= $request->input('content');
			$add->seo_title		= $request->input('seo_title');
			$add->seo_desc 		= $request->input('seo_desc');
			$add->title_ar		= $request->input('title_ar');
			$add->content_ar 	= $request->input('content_ar');
			$add->seo_title_ar	= $request->input('seo_title_ar');
			$add->seo_desc_ar 	= $request->input('seo_desc_ar');
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	public function pages(){
		$pages = Pages::where('type_','page')->paginate(100);
		return view('admin.pages')->with('pages',$pages);
	}
	
	
	public function page($id){
		$page = Pages::find($id);
		return view('site.en.page')->with('page',$page);
	}
	

}
