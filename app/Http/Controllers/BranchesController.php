<?php

namespace App\Http\Controllers;

use Validator;
use App\Branches;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class BranchesController extends Controller
{
    public function view_new(){
		return view('admin.branches_add');
	}


    //
    public function store(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'address' 	=> 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{

		
			$add = new Branches;
			$add->address		= $request->input('address');
		    $add->address_ar 	= $request->input('address_ar');
			$add->phone 		= $request->input('phone');
			$add->title 		= $request->input('title');
			$add->title_ar		= $request->input('title_ar');
            $add->lat           = $request->input('lat');
            $add->lng           = $request->input('lng');
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }

    public function view_update($id){
        $item = Branches::find($id);
		return view('admin.branches_update')->with('item',$item);
	}
    public function update($id,Request $request)
    {
		$validation = Validator::make($request->all(),[
			'address' 	=> 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$update = Branches::find($id);
			$update->address		= $request->input('address');
		    $update->address_ar 	= $request->input('address_ar');
			$update->phone 			= $request->input('phone');
			$update->title 			= $request->input('title');
			$update->title_ar			= $request->input('title_ar');
            $update->lat           = $request->input('lat');
            $update->lng           = $request->input('lng');
	    	if($update->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }

    public function destroy($id){
		$item = Branches::find($id);
		if($item->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}

	public function get_all(){
		$items = Branches::paginate(100);
		return view('admin.branches')->with('items',$items);
	}
}
