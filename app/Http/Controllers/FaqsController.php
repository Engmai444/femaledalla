<?php

namespace App\Http\Controllers;
use Validator;
use App\Faqs;
use App\User;
use App\Address;
use Auth;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class FaqsController extends Controller
{

	private function permations($name){
		$res = AdminController::permations($name);
		return $res;
	}

	public function get_newfaq(){
		if(!$this->permations('faqs')){
			return view('admin.permations');
		}
		return view('admin.faqs_new');
	}
	public function newfaq(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'question_ar' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = new Faqs;
		   
			$add->question_ar	= $request->input('question_ar');
		    $add->answer_ar 	= $request->input('answer_ar');
			$add->question		= $request->input('question');
		    $add->answer 		= $request->input('answer');
			$add->ordered 		= $request->input('ordered');
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	
	public function get_updatefaq($id){
		if(!$this->permations('faqs')){
			return view('admin.permations');
		}
		$faqs = Faqs::find($id);
		return view('admin.faqs_update')->with('faqs',$faqs);
	}
	public function updatefaq($id,Request $request)
    {
		$validation = Validator::make($request->all(),[
			'question_ar' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = Faqs::find($id);
		    $add->question		= $request->input('question');
		    $add->answer 		= $request->input('answer');
		
			$add->question_ar	= $request->input('question_ar');
		    $add->answer_ar 	= $request->input('answer_ar');
			$add->ordered 		= $request->input('ordered');
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }

	public function get_faqs(){
		if(!$this->permations('faqs')){
			return view('admin.permations');
		}
		$faqs = DB::table('faqs')->paginate(100);
		return view('admin.faqs')->with('faqs',$faqs);
	}
	
	//*-------------------delete -------------------*//
	public function destroy_faqs($id){
		if(!$this->permations('faqs')){
			return view('admin.permations');
		}
		$cat = Faqs::find($id);
		if($cat->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}
	
	
	public function get_agent_faqs(Request $request){
		$lang = ($request->input('lang') == 'ar') ? 'ar' : 'en' ;
		$faqs = DB::table('faqs')->where('utype','=','agent')->get();
		return view('faqs')->with('faqs',$faqs)->with('lang',$lang);
	}
	
	public function get_customer_faqs(Request $request){
		$lang = ($request->input('lang') == 'ar') ? 'ar' : 'en' ;
		$faqs = DB::table('faqs')->where('utype','=','customer')->get();
		return view('faqs')->with('faqs',$faqs)->with('lang',$lang);
	}


  






}
