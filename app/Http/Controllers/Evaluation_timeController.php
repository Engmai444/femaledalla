<?php

namespace App\Http\Controllers;

use App\Application;
use App\Category;
use App\Customer;
use App\Models\Evaluation_time;
use Carbon\Carbon;
use Validator;
use App\Pages;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use App;
use Auth;
use Excel;
use Storage;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;
use DateTime;

class Evaluation_timeController extends Controller
{
    ######### Evaluations time ############

    public function get_newPeriod(){
        $schools=DB::table('schools')->get();
        $periods=App\Models\Evaluation_period::get();
        return view('admin.evaluation_time.new',compact('schools','periods'));
    }
    public function newPeriod(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'period_id' => 'required',
            'slots_male' => 'required',
            'slots_female' => 'required',
        ]);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            $add = new Evaluation_time();

            $add->period_id	= $request->input('period_id');
            $add->slots_male	= $request->input('slots_male');
            $add->slots_avaliable_male	= $request->input('slots_male');
            $add->slots_female	= $request->input('slots_female');
            $add->slots_avaliable_female	= $request->input('slots_female');
            $add->school_id	= $request->input('school_id');

            if($add->save()){
                Session::put('success',Lang::get('validation.Saved'));
                return redirect()->to('admin/evaluation_time');
            }else{
                return redirect()->back()->withErrors(Lang::get('validation.error'));
            }
        }


        /* $messages = [
             'file.required' => 'من فضلك اختر الملف'
         ];
         $validation = Validator::make($request->all(),[
             'file'=> 'required',
         ],$messages);

         if($validation->fails()){
             return redirect()->back()->withErrors($validation)->withInput();
         }else{
             if($request->hasFile('file')){
                 ini_set('memory_limit', '-1');
                 // set_time_limit(-1);
                 ini_set('max_execution_time', 0);
                 $extension = File::extension($request->file->getClientOriginalName());
                 if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                     $path = $request->file->getRealPath();
                     $data = Excel::load($path, function($reader) {
                     })->get();

                     if(!empty($data) && $data->count()){
                         $insert  = [];
                         foreach ($data as $key => $value) {
                          $school_name=(int)$value->school;
                         // dd($school_name);
                         // $school=DB::table('schools')->where('Name','like','%'.$school_name.'%')->first();
                          $day=date('Y-m-d',strtotime($value->day));
                          $times=Evaluation_time::where('school_id',$school_name)
                                                 ->where('day',$day)
                                                 ->where('from_time',date('H:i:s A',strtotime($value->from)))
                                                 ->where('to_time',date('H:i:s A',strtotime($value->to)))
                                                 ->first();
                              if(empty($times)) {

                                  Evaluation_time::create(['school_id' => $school_name,
                                      'day' => $day,
                                      'from_time' => date('H:i:s A', strtotime($value->from)),
                                      'to_time' => date('H:i:s A', strtotime($value->to)),
                                      'slots_male' => $value->slots_male,
                                      'slots_female' => $value->slots_female,
                                      'slots_avaliable_male' => $value->slots_male,
                                      'slots_avaliable_female' => $value->slots_female,]);
                              }else{
                                  Evaluation_time::where('school_id',$school_name)
                                      ->where('day',$day)
                                      ->where('from_time',date('H:i:s A',strtotime($value->from)))
                                      ->where('to_time',date('H:i:s A',strtotime($value->to)))
                                       ->update([
                                      'slots_male' => $value->slots_male,
                                      'slots_female' => $value->slots_female,
                                      'slots_avaliable_male' => $value->slots_male,
                                      'slots_avaliable_female' => $value->slots_female,]);
                              }


                         }

                                Session::put('success',Lang::get('validation.Saved'));
                                return redirect()->back();
                     }
                 }else {
                     return redirect()->back()->withErrors('صيغة الملف غير مدعومة');
                 }
             }
         }*/
    }


    public function get_updatePeriod($id){
        $item = Evaluation_time::find($id);
        $schools=DB::table('schools')->get();
        $periods=App\Models\Evaluation_period::get();

        return view('admin.evaluation_time.update')->with(['item'=>$item,'schools'=>$schools,'periods'=>$periods]);
    }
    public function updatePeriod($id,Request $request)
    {
       $validation = Validator::make($request->all(),[
            'period_id' => 'required',
            'slots_male' => 'required',
           'slots_female' => 'required',
        ]);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            $add = Evaluation_time::find($id);

            $add->period_id	= $request->input('period_id');
            $add->slots_male	= $request->input('slots_male');
            $add->slots_avaliable_male	= $request->input('slots_male');
            $add->slots_female	= $request->input('slots_female');
            $add->slots_avaliable_female	= $request->input('slots_female');
            $add->school_id	= $request->input('school_id');

            if($add->save()){
                Session::put('success',Lang::get('validation.Saved'));
                return redirect()->to('admin/evaluation_time');
            }else{
                return redirect()->back()->withErrors(Lang::get('validation.error'));
            }
        }


    }

    public function get_Period(){
        $items = Evaluation_time::leftjoin('schools','schools.Id','=','evaluation_times.school_id')
                                 ->leftJoin('evaluation_periods','evaluation_periods.id','=','evaluation_times.period_id')
                                 ->select('evaluation_times.*','schools.Name','evaluation_periods.fromH','evaluation_periods.toH')->paginate(100);
        return view('admin.evaluation_time.items')->with('items',$items);
    }

    //*-------------------delete -------------------*//
    public function destroy_Period($id){
        $cat = Evaluation_time::find($id);
        if($cat->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect()->back();
        }
    }

    public function active($id){
        $add = Evaluation_time::find($id);
        $add->id	= $id;
        $add->status	= 1;
        if($add->save()){
            Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();
        }
    }
    public function inactive($id){
        $add = Evaluation_time::find($id);
        $add->id	= $id;
        $add->status	= 0;
        if($add->save()){
            Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();
        }
    }

    public function orders_evaluation(Request $request)
    {
        $columns = array(
            0 =>'id',
            1 =>'name',
            2=> 'id_no',
            3=> 'email',
            4=> 'mobile',
            5=>'nationality',
            6=> 'dob',
            7=>'day',
            8=>'period',
            9=>'school',
            10=>'gender',
            11=>'created_at',
            12=>'options'
        );

        $totalData = App\Models\Evaluation_reserve::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = App\Models\Evaluation_reserve::leftJoin('evaluation_periods','evaluation_periods.id','=','evaluation_reserves.time_id')
                ->leftjoin('users','users.StudentId','=','evaluation_reserves.student_id')
                ->leftJoin('schools','schools.id','=','users.school')
                ->select('evaluation_reserves.*','schools.Name','users.FullName','users.IDNumber','users.email','users.mobile','users.DoB','users.gender','users.NationalityId','evaluation_reserves.day','evaluation_periods.fromH','evaluation_periods.toH')
                ->offset($start)
                ->limit($limit)
                ->orderBy('evaluation_reserves.id','DESC')
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  App\Models\Evaluation_reserve::leftJoin('evaluation_periods','evaluation_periods.id','=','evaluation_reserves.time_id')
                ->leftjoin('users','users.StudentId','=','evaluation_reserves.student_id')
                ->leftJoin('schools','schools.id','=','users.school')
                ->where('users.StudentId','LIKE',"%{$search}%")
                ->orWhere('FullName', 'LIKE',"%{$search}%")
                ->orWhere('IDNumber', 'LIKE',"%{$search}%")
                ->orWhere('email', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->select('evaluation_reserves.*','schools.Name','users.FullName','users.IDNumber','users.email','users.mobile','users.DoB','users.gender','users.NationalityId','evaluation_reserves.day','evaluation_periods.fromH','evaluation_periods.toH')
                ->offset($start)
                ->limit($limit)
                //->orderBy($order,$dir)
                ->get();

            $totalFiltered = App\Models\Evaluation_reserve::leftJoin('evaluation_periods','evaluation_periods.id','=','evaluation_reserves.time_id')
                ->leftjoin('users','users.StudentId','=','evaluation_reserves.student_id')
                ->leftJoin('schools','schools.id','=','users.school')
                ->where('users.StudentId','LIKE',"%{$search}%")
                ->orWhere('FullName', 'LIKE',"%{$search}%")
                ->orWhere('IDNumber', 'LIKE',"%{$search}%")
                ->orWhere('email', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  url('admin/evaluation_time/'.$post->id.'/delete_reserve');
                if($post->attend==0) {
                    $edit = url('admin/evaluation_time/'.$post->id.'/attend');
                    $ti='حضور';
                }else{
                    $edit = url('admin/evaluation_time/'.$post->id.'/inattend');
                    $ti='إلغاء حضور';

                }

                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->FullName;
                $nestedData['id_no'] = $post->IDNumber;
                $nestedData['email'] = $post->email;
                $nestedData['mobile'] = $post->mobile;
                $nat=DB::table('nationality')->where('Id',$post->NationalityId)->first();
                $nestedData['nationality'] = $post->Name;
                if($post->date_type == 1){
                    \GeniusTS\HijriDate\Date::setToStringFormat('Y-m-d');
                    $date = date('Y-m-d', strtotime($post->DoB. ' - 1 days'));
                    $date=\GeniusTS\HijriDate\Hijri::convertToHijri($date);
                    $time=explode("-",$date);
                    $day=$time['2'];
                    $month=$time['1'];
                    $year=$time['0'];
                    $ss=$year.'-'.$month.'-'.$day;
                }else{
                    $ss= $post->DoB ;
                }
                $nestedData['dob'] = $ss;
                $nestedData['day'] = $post->day;
                $nestedData['period'] = $post->fromH.' To '.$post->toH;
                $nestedData['school'] = $post->Name;
                $nestedData['gender'] = $post->gender;
                $nestedData['created_at'] = date('Y-m-d H:i',strtotime($post->created_at));
                $nestedData['options'] = "&emsp;<a href='{$edit}' title='edit' class='btn btn-default btn-sm btn-icon icon-left'><i class='entypo-pencil'></i>".$ti."</a>
                                          &emsp;<a data-href='{$show}' title='delete' href='#'  data-toggle='modal' data-target='#confirm-delete' class='btn btn-danger'><i class='entypo-cross'></i>حذف</a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
    public function orders(Request $request)
    {
        $query=App\Models\Evaluation_reserve::leftJoin('evaluation_periods','evaluation_periods.id','=','evaluation_reserves.time_id')
            ->leftjoin('users','users.StudentId','=','evaluation_reserves.student_id')
            ->leftJoin('schools','schools.id','=','users.school');

        if($request->from){
            $query->whereDate('evaluation_reserves.day','>=',$request->from);
        }
        if($request->to){
            $query->whereDate('evaluation_reserves.day','<=',$request->to);
        }
        if($request->SchoolId ){
            $query->where('users.school',$request->SchoolId);
        }
        if($request->gender){
            $query->where('users.gender',$request->gender);
        }
        if($request->mobile ){
            $query->where('users.mobile',$request->mobile);
        }
        if($request->id_no ){
            $query->where('users.IDNumber',$request->id_no);
        }
        if($request->email ){
            $query->where('users.email',$request->email);
        }
        if($request->name){
            $query->where('users.FullName','%like%',$request->name);
        }
        if($request->period_id){
            $query->where('evaluation_reserves.time_id',$request->period_id);
        }

            $items=$query->select('evaluation_reserves.*','schools.Name','users.FullName','users.IDNumber','users.email','users.mobile','users.DoB','users.gender','users.NationalityId','evaluation_reserves.day','evaluation_periods.fromH','evaluation_periods.toH')->get();

        $schools=DB::table('schools')->get();
        $periods=App\Models\Evaluation_period::get();
        return view('admin.evaluation_time.reserves')->with(['items'=>$items,'schools'=>$schools,'periods'=>$periods]);

    }
public function search_orders_ev(Request $request)
{
    $query=App\Models\Evaluation_reserve::leftJoin('evaluation_periods','evaluation_periods.id','=','evaluation_reserves.time_id')
        ->leftjoin('users','users.StudentId','=','evaluation_reserves.student_id')
        ->leftJoin('schools','schools.id','=','users.school');

    if($request->from){
        $query->whereDate('evaluation_reserves.day','>=',$request->from);
    }
    if($request->to){
        $query->whereDate('evaluation_reserves.day','<=',$request->to);
    }
    if($request->SchoolId ){
        $query->where('users.school',$request->SchoolId);
    }
    if($request->gender){
        $query->where('users.gender',$request->gender);
    }
    if($request->mobile ){
        $query->where('users.mobile',$request->mobile);
    }
    if($request->id_no ){
        $query->where('users.IDNumber',$request->id_no);
    }
    if($request->email ){
        $query->where('users.email',$request->email);
    }
    if($request->name){
        $query->where('users.FullName','%like%',$request->name);
    }
    if($request->period_id){
        $query->where('evaluation_reserves.time_id',$request->period_id);
    }

    $items=$query->select('evaluation_reserves.*','schools.Name','users.FullName','users.IDNumber','users.email','users.mobile','users.DoB','users.gender','users.NationalityId','evaluation_reserves.day','evaluation_periods.fromH','evaluation_periods.toH')->get();

    $schools=DB::table('schools')->get();
    $periods=App\Models\Evaluation_period::get();
    return view('admin.evaluation_time.reserves_search')->with(['items'=>$items,'schools'=>$schools,'periods'=>$periods]);

}

    public function attend($id){
        $add = App\Models\Evaluation_reserve::find($id);
        $add->id	= $id;
        $add->attend	= 1;
        if($add->save()){
            Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();
        }
    }
    public function inattend($id){
        $add = App\Models\Evaluation_reserve::find($id);
        $add->id	= $id;
        $add->attend	= 0;
        if($add->save()){
            Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();
        }
    }
    public function delete_reserve($id){
        $cat = App\Models\Evaluation_reserve::find($id);
        if($cat->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect('/admin/evaluation_time/orders');
        }
    }

    public function export_excel()
    {
        $items=App\Models\Evaluation_reserve::leftJoin('evaluation_periods','evaluation_periods.id','=','evaluation_reserves.time_id')
            ->leftjoin('users','users.StudentId','=','evaluation_reserves.student_id')
            ->leftJoin('schools','schools.id','=','users.school')
            ->select('evaluation_reserves.*','schools.Name','schools.Id as sid','users.FullName','users.IDNumber','evaluation_periods.fromH','evaluation_periods.toH','users.email','users.mobile','users.DoB','users.gender','users.NationalityId')->get();

        $file = Excel::create('getcustomers', function($excel) use($items) {
            $excel->sheet('getcustomers', function($sheet) use($items) {

                $sheet->loadView('excel.evaluation', ['items' => $items]);
            });
        })->export('xlsx');

    }
    public function export_drive()
    {
        //$schools=DB::table('schools')->get();
       // foreach ($schools as $sc):
        $items=App\Models\Evaluation_reserve::leftJoin('evaluation_periods','evaluation_periods.id','=','evaluation_reserves.time_id')
            ->leftjoin('users','users.StudentId','=','evaluation_reserves.student_id')
            ->leftJoin('schools','schools.id','=','users.school')
            ->select('evaluation_reserves.*','schools.Name','schools.Id as sid','users.FullName','users.IDNumber','evaluation_periods.fromH','evaluation_periods.toH','users.email','users.mobile','users.DoB','users.gender','users.NationalityId')->get();

        $file = Excel::create('getcustomers', function($excel) use($items) {
            $excel->sheet('getcustomers', function($sheet) use($items) {

                $sheet->loadView('excel.evaluation', ['items' => $items]);
            });
        })->store('xlsx', storage_path('app/public/excel'), true);

            Storage::disk('googledrive')->put('getcustomers.xlsx', file_get_contents(storage_path('app/public/excel/getcustomers.xlsx')));
       // endforeach;
    }



    public function result($id){
        return view('admin.evaluation_time.result')->with(['type'=>$id]);
    }
    public function import_result(Request $request)
    {
        $messages = [
            'file.required' => 'من فضلك اختر الملف'
        ];
        $validation = Validator::make($request->all(),[
            'file'=> 'required',
        ],$messages);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            if($request->hasFile('file')){
                $extension = File::extension($request->file->getClientOriginalName());
                if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                    $path = $request->file->getRealPath();
                    $data = Excel::load($path, function($reader) {
                    })->get();
                    if(!empty($data) && $data->count()){

                        $insert  = [];
                        foreach ($data as $key => $value) {

                            $user=App\User::where('IDNumber',(int)$value->idnumber)->first();

                            $course=DB::table('course')->where('SchoolId',$value->schoolid)
                                                       ->where('CourseRefNo',$value->courserefno)->first();
                            if($value->ispaid=='1'){
                                $del_date="0000-00-00 00:00:00";
                            }else{
                                $del_date=date('Y-m-d H:i:s');
                            }
                            if(!empty($user)){
                                $us=$user->StudentId;
                            }else{
                                $us='';
                            }
                            if(!empty($course)){
                                $courseid=$course->id;
                            }else{
                                $courseid=0;
                            }

                            DB::table('orders')->insert(['SchoolId'=>$value->schoolid,
                                                         'StudentId'=>$us,
                                                          'ApplicationDefId'=>$value->applicationdefid,
                                                          'CourseRefNo'=>$value->courserefno,
                                                           'IsPaid'=>$value->ispaid,
                                                           'CourseId'=>$courseid,
                                                            'assign_grade'=>$value->assigngrade,
                                                            'course_start'=>$value->coursestartdate,
                                                            'course_end'=>$value->courseenddate,
                                                            'fullname'=>$value->fullname,
                                                            'idnumber'=>$value->idnumber,
                                                            'dob'=>$value->dob,
                                                            'mobile'=>$value->mobile,
                                                            'nationality_id'=>$value->nationalityid,
                                                            'gender'=>$value->gender,
                                                            'deleted_at'=>$del_date,
                                                             'type'=>$request->type]);
                            $code=DB::table('parent_courses')->where('co_id',$value->assigngrade)->first();
                            if($user  !=NULL) {
                                $msg = 'تم تسجيلك فى دورة ' . $code->code;
                                $this->send_sms($user->mobile, $msg);
                            }else{
                                $msg = 'تم تسجيلك فى دورة ' . $code->code;
                                $this->send_sms($value->mobile, $msg);
                            }
                        }



                        Session::put('success',Lang::get('validation.Saved'));
                        return redirect()->back();
                    }
                }else {
                    return redirect()->back()->withErrors('صيغة الملف غير مدعومة');
                }
            }
        }
    }

    function viewresult($id)
    {
        $orders=DB::table('orders')
            ->leftJoin('schools','schools.id','=','orders.SchoolId')
            ->where('type',$id)->get();
        return view('admin.evaluation_time.viewresult')->with(['orders'=>$orders]);
    }
    ######### Evaluation times ############

////////////////front end ////////////

    public function evaluation(){
        $schools=DB::table('schools')->get();
        $userid = Auth::guard('doctor')->user()->StudentId;
         $user=App\User::where('StudentId',$userid)->first();
        $times=Evaluation_time::where('status',1)
                       ->where('slots_'.$user->gender,'>','0')->get();
        $setting=DB::table('site_option')->where('varname','evaluation_time')->first();
        $setting_id=DB::table('site_option')->where('varname','id_setting')->first();
        $idno=$user->IDNumber;


        $userid = Auth::guard('doctor')->user()->StudentId;
        $user=App\User::where('StudentId',$userid)->first();
        $counte = DB::table('evaluation_reserves')->join('evaluation_periods', 'evaluation_periods.id', '=', 'evaluation_reserves.time_id')
            ->Join('users','users.StudentID','=','evaluation_reserves.student_id')
            ->where('school', Auth::guard('doctor')->user()->school)
            ->where('day','!=','0000-00-00')
            ->where('day','!=','1970-01-01')
            ->whereNULL('evaluation_reserves.deleted_at')
            ->groupBy('evaluation_reserves.day')
            ->select('school','day',DB::RAW('COUNT(evaluation_reserves.student_id) as booked'))->get();


        $count_time=Evaluation_time::where('school_id',Auth::guard('doctor')->user()->school)
            ->select(DB::RAW('SUM(slots_female) as realf'),DB::RAW('SUM(slots_female) as realtm'))->first();

        if(!empty($count_time)) {
            if ($user->gender == 'female') {
                $real_time = $count_time->realf;
            } else {
                $real_time = $count_time->realtm;
            }
        }
          $days=array();
        if(!empty($counte))
        {
          foreach ($counte as $cou):
              if ((int)$cou->booked >= (int)$real_time) {
                  $days[] = date('Y-n-j',strtotime($cou->day));
              }

          endforeach;
        }

        $days='["'.implode('","',$days).'"]';

        if(App::getLocale() == 'en'){
            return view('site.en.user.evaluation',compact('times','schools','setting','days','setting_id','idno'));
        }else{
            return view('site.user.evaluation',compact('times','schools','setting','days','setting_id','idno'));
        }
    }
    public function get_time(Request  $request)
    {
        $day=date('Y-m-d',strtotime($request->id));
        $userid = Auth::guard('doctor')->user()->StudentId;
        $school=Auth::guard('doctor')->user()->school;
        $user=App\User::where('StudentId',$userid)->first();

        $times_school = App\Models\Evaluation_time::where('school_id', Auth::guard('doctor')->user()->school)
                                                  ->where('status','1')
            ->get();
        $re=array();

        if(!empty($times_school)) {

            foreach ($times_school as $co):

                $counter = App\Models\Evaluation_reserve::join('evaluation_periods', 'evaluation_periods.id', '=', 'evaluation_reserves.time_id')
                    ->Join('users', 'users.StudentID', '=', 'evaluation_reserves.student_id')
                    ->where('day', $day)
                    ->where('time_id', $co->period_id)
                   // ->groupBy('evaluation_reserves.time_id')
                    ->where('school', Auth::guard('doctor')->user()->school)
                    ->get();
                $hour = App\Models\Evaluation_time::where('period_id', $co->period_id)
                    ->where('school_id', Auth::guard('doctor')->user()->school)
                    ->first();

                if (!empty($hour)) {
                    if ($user->gender == 'female') {
                        $ge = $hour->slots_female;
                    } else {
                        $ge = $hour->slots_male;
                    }

                    if (count($counter) < $ge) {
                        $today=date('Y-m-d');
                        if($day==$today) {
                            $ho = date('h A');
                            $hh = str_split($ho, strlen($ho) - 2);
                            $da = DB::table('evaluation_periods')->WhereNull('deleted_at')->where('id', $co->period_id)->first();

                            if (!empty($da)) {


                                $ho1 = $da->fromH;
                                $hours = str_split($ho1, strlen($ho1) - 2);
                                $format = $hours[1];

                                if ($format == 'AM' and $hh[1] == 'AM') {

                                    if ($hours[0] > $hh[0]) {
                                        $res = 'true';
                                        $re[] = $da;
                                    } else {
                                        $res = 'false';
                                    }
                                } else if ($format == 'AM' and $hh[1] == 'PM') {
                                    $res = 'false';

                                } else if ($format == 'PM' and $hh[1] == 'AM') {
                                    $res = 'true';
                                    $re[] = $da;
                                } else if ($format == 'PM' and $hh[1] == 'PM') {
                                    if ($hours[0] < $hh[0]) {
                                        $res = 'true';
                                        $re[] = $da;
                                    } else {
                                        $res = 'false';
                                    }
                                }

                            }

                        }else {

                            $data = DB::table('evaluation_periods')->WhereNull('deleted_at')->where('id', $co->period_id)->first();

                            $re[] = $data;
                        }
                    }
                }
            endforeach;

        }

        $times_schoolss = App\Models\Evaluation_time::where('school_id', Auth::guard('doctor')->user()->school)
            ->first();
        $sen['success'] = true;
        // $sen['result'] = $re->toArray();
        sort($re);
        $sen['result'] = $re;
        $sen['reserve'] = count($counter);
        $sen['count'] = $times_schoolss->slots_female;

        return Response::json( $sen );
      
    }

    public function reserve(Request $request)
    {

        $userid = Auth::guard('doctor')->user()->StudentId;
        $user=App\User::where('StudentId',$userid)->first();
        $counte = App\Models\Evaluation_reserve::join('evaluation_periods', 'evaluation_periods.id', '=', 'evaluation_reserves.time_id')
            ->Join('users','users.StudentID','=','evaluation_reserves.student_id')
            ->where('day', $request->day)
            ->where('time_id', $request->time)
            ->where('school', Auth::guard('doctor')->user()->school)
            ->count();

        $hour_sc = App\Models\Evaluation_time::where('period_id', $request->time)
            ->where('school_id', Auth::guard('doctor')->user()->school)
            ->first();
        if($user->gender=='female')
        {
            $ge=$hour_sc->slots_female;
        }else{
            $ge=$hour_sc->slots_male;
        }

        if ($counte < $ge) {

            $reserve = App\Models\Evaluation_reserve::leftjoin('evaluation_times', 'evaluation_times.id', '=', 'evaluation_reserves.time_id')
                ->where('student_id', $userid)
                ->where('attend', 0)->select('evaluation_reserves.*')->first();
            //dd($request->time);
            //$time=Evaluation_time::where('id',$request->time)->first();


            $hour = App\Models\Evaluation_period::where('id', $request->time)->first();

            $ho = $hour->fromH;
            $hours = str_split($ho, strlen($ho) - 2);
            $format = $hours[1];
            if ($format == 'AM') {
                $fo = "صباحا" . $hours[0];
            } else {
                $fo = "مساءا" . $hours[0];
            }
            $time = Evaluation_time::where('period_id', $hour->id)
                ->where('school_id', Auth::guard('doctor')->user()->school)->first();
            // dd($time);
            $day = date('Y-m-d', strtotime($request->day));
            if (!empty($reserve)) {
                $datetime1 = new DateTime($request->day);
                $datetime2 = new DateTime($reserve->day);
                $interval = $datetime1->diff($datetime2);

                $dayno = $interval->format('%a');
            }
            if (empty($reserve)) {
                App\Models\Evaluation_reserve::create(['time_id' => $request->time,
                    'day' => $day,
                    'student_id' => $userid]);
                /* if($user->gender=='male') {
                     $ti = $time->slots_avaliable_male;
                     Evaluation_time::where('id', $time->id)->update(['slots_avaliable_male' => $ti - 1]);

                 }else{
                     $ti = $time->slots_avaliable_female;
                     Evaluation_time::where('id', $time->id)->update(['slots_avaliable_female' => $ti - 1]);

                 }*/

                $msg = "عزيزتى الطالبة " . $user->IDNumber . " تم حجز موعد اختبار تحديد المستوى بنجاح وتاريخ الاختبار " . $day . " الساعة " . $fo;
                $this->send_sms($user->mobile, $msg);
                Session::put('success', "تم الحجز بنجاح");
                return redirect()->back()->with('msg', 'تم الحجز بنجاح');
            } else if ($dayno > 30) {
                App\Models\Evaluation_reserve::where('id', $reserve->id)->delete();
                App\Models\Evaluation_reserve::create(['time_id' => $request->time,
                    'day' => $request->day,
                    'student_id' => $userid]);
                if ($user->gender == 'male') {
                    Evaluation_time::where('id', $time->id)->update(['slots_avaliable_male' => $time->slots_avliable_male - 1]);
                } else {
                    Evaluation_time::where('id', $time->id)->update(['slots_avaliable_female' => $time->slots_avliable_female - 1]);

                }
                $msg = "عزيزتى الطالبة " . $user->IDNumber . " تم إلغاء الموعد السابق و حجز موعد اختبار تحديد المستوى جديد بنجاح وتاريخ الاختبار " . $day . " الساعة " . $fo;
                $this->send_sms($user->mobile, $msg);
                Session::put('success', "تم الحجز بنجاح");
                return redirect()->back()->with('msg', 'تم الحجز بنجاح');
            } else {
                Session::put('error', "انت مسجل فى موعد اختبار اخر");
                return redirect()->back()->with('msg', 'انت مسجل فى اختبار اخر');
            }
        }else{
            Session::put('error', "لا توجد اماكن متاحة فى هذا الموعد");
            return redirect()->back()->with('msg', 'لا توجد اماكن متاحة فى هذا الموعد');
        }
    }

    public function send_sms($phone,$msg)
    {
        $phonenew=substr($phone,0,1);
        if($phonenew!='0')
        {
            $phone=$phone;
        }else{
            $phone=substr($phone,1);
        }
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://www.4jawaly.net/api/sendsms.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, 'username=dalla&password=565656&message='.$msg.'&numbers=966'.$phone.'&sender=DDC-DRIVING&unicode=utf8&Rmduplicated=1&return=json');


        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/x-www-form-urlencoded"
        ));

        $response = curl_exec($ch);

        curl_close($ch);

    }


    public function ques(Request $request)
    {
        $messages = [
            'file.required' => 'من فضلك اختر الملف'
        ];
        $validation = Validator::make($request->all(),[
            'file'=> 'required',
        ],$messages);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            if($request->hasFile('file')){
                $extension = File::extension($request->file->getClientOriginalName());
                if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                    $path = $request->file->getRealPath();
                    $data = Excel::load($path, function($reader) {
                    })->get();
                    if(!empty($data) && $data->count()){
                        $insert  = [];
                        foreach ($data as $key => $value) {
                            if($value->lang=='1') {

                                $ans[1] = $value->answer1;
                                $ans[2] = $value->answer2;
                                $ans[3] = $value->answer3;
                                $anss = DB::table('answers')->where('q_id', $value->no)->get();
                                $i = 1;
                                foreach ($anss as $an):
                                 $cor=$value->anser;
                                if($i==$cor){
                                    DB::table('answers')->where('id', $an->id)
                                        ->update(['correct' => 1]);
                                }

                                    $i++;
                                endforeach;

                            }
                        }

                        Session::put('success',Lang::get('validation.Saved'));
                        return redirect()->back();
                    }
                }else {
                    return redirect()->back()->withErrors('صيغة الملف غير مدعومة');
                }
            }
        }
    }

    public function multiDelete(Request $request){

        if(!empty($request->orderID) && DB::table('evaluation_reserves')->whereIn('id',$request->orderID)->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect('/admin/evaluation_time/orders');
        }else{
            return redirect()->back()->withErrors('لم تقم بتحديد اي عناصر لحذفها');
        }
    }
}
