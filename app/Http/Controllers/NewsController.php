<?php

namespace App\Http\Controllers;

use Validator;
use App\Pages;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    public function view_new(){
		return view('admin.news_new');
	}
	public function store(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'title' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
            $photo = $request->file('photo');
            if(!empty($photo)){
                $photo_name = str_random(5).time();
                $photo_ext = $photo->getClientOriginalExtension();
                $photo_fullname = $photo_name.'.'.$photo_ext;
                $path = public_path().'/photos/';
                //$upload = $photo->move($path,$photo_fullname);
                $thumb = Image::make($photo)->resize(710,360)->save($path.$photo_fullname);
            }

            $photo2 = $request->file('photo2');
            if(!empty($photo2)){
                $photo2_name = str_random(5).time();
                $photo2_ext = $photo2->getClientOriginalExtension();
                $photo2_fullname = $photo2_name.'.'.$photo2_ext;
                $path = public_path().'/photos/';
                //$upload = $photo->move($path,$photo_fullname);
                $thumb = Image::make($photo2)->resize(710, 360)->save($path.$photo2_fullname);
            }


			$add = new Pages;
		    $add->title			= $request->input('title');
			$add->content 		= $request->input('content');
			$add->seo_title		= $request->input('seo_title');
			$add->seo_desc 		= $request->input('seo_desc');
			$add->title_ar		= $request->input('title_ar');
			$add->content_ar 	= $request->input('content_ar');
			$add->seo_title_ar	= $request->input('seo_title_ar');
			$add->seo_desc_ar 	= $request->input('seo_desc_ar');
            $add->type_ 	    = 'news';
            if(!empty($photo)){
				$add->photo 	= $photo_fullname;
            }
            if(!empty($photo2)){
				$add->photo2 	= $photo2_fullname;
            }
            
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	public function view_update($id){
		$item = Pages::find($id);
		return view('admin.news_update')->with('page',$item);
	}
	public function update($id,Request $request)
    {
		$validation = Validator::make($request->all(),[
			'title' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
            $photo = $request->file('photo');
            if(!empty($photo)){
                $photo_name = str_random(5).time();
                $photo_ext = $photo->getClientOriginalExtension();
                $photo_fullname = $photo_name.'.'.$photo_ext;
                $path = public_path().'/photos/';
                //$upload = $photo->move($path,$photo_fullname);
                $thumb = Image::make($photo)->resize(710,360)->save($path.$photo_fullname);
            }
            $photo2 = $request->file('photo2');
            if(!empty($photo2)){
                $photo2_name = str_random(5).time();
                $photo2_ext = $photo2->getClientOriginalExtension();
                $photo2_fullname = $photo2_name.'.'.$photo2_ext;
                $path = public_path().'/photos/';
                //$upload = $photo->move($path,$photo_fullname);
                $thumb = Image::make($photo2)->resize(710,360)->save($path.$photo2_fullname);
            }


			$add = Pages::find($id);
		    $add->title			= $request->input('title');
			$add->content 		= $request->input('content');
			$add->seo_title		= $request->input('seo_title');
			$add->seo_desc 		= $request->input('seo_desc');
			$add->title_ar		= $request->input('title_ar');
			$add->content_ar 	= $request->input('content_ar');
			$add->seo_title_ar	= $request->input('seo_title_ar');
            $add->seo_desc_ar 	= $request->input('seo_desc_ar');
            if(!empty($photo)){
				$add->photo 	= $photo_fullname;
            }
            if(!empty($photo2)){
				$add->photo2 	= $photo2_fullname;
            }
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	public function news(){
		$pages = Pages::where('type_','news')->orderBy('pageId','desc')->paginate(100);
		return view('admin.news')->with('pages',$pages);
    }
    
    public function destroy($id){
		$item = Pages::find($id);
		if($item->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
    }

    /**
     * site
     */
    public function GetAllNewsPage(){
		$news = Pages::where('type_','news')->orderBy('pageId','desc')->paginate(40);
		return view('site.en.news')->with('news',$news);
    }
    public function newsPage($id){
		$item = Pages::where('type_','news')->where('pageId',$id)->first();
		return view('site.en.news_item')->with('item',$item);
    }
    public function GetAllNewsPage_ar(){
		$news = Pages::where('type_','news')->orderBy('pageId','desc')->paginate(40);
		return view('site.ar.news')->with('news',$news);
    }
    public function newsPage_ar($id){
		$item = Pages::where('type_','news')->where('pageId',$id)->first();
		return view('site.ar.news_item')->with('item',$item);
    }

}
