<?php

namespace App\Http\Controllers;

use Validator;
use Auth;
use Response;
use Redirect;
use Image;
use Session;
use App\Admin;
use Lang;
use PDO;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    public function index()
    {

       $type=Auth::guard('admins')->user()->permations;
if($type=='staff')
{
    return view('admin.staff');
}else {

    return view('admin.home');
}
    }
	
	
	public function get_changepassword(){
		 return view('admin.changepassword'); 
	}
	
	public function changepassword(Request $request)
    {

		$validation = Validator::make($request->all(),[
			'oldpassword' 			=> 'required',
			'password' 				=> 'required|confirmed',
			
		]);


		 if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
			$userid =Auth::guard('admins')->user()->id;
			$user = Admin::find($userid);
			if(Hash::check($request->input('oldpassword'), $user->password)){
				$user->password			= Hash::make(trim($request->input('password')));
				if($user->save()){
					//Session::put('success',Lang::get('validation.Saved'));
					return redirect()->to('/admin/logout');
				}else{
					return redirect()->back()->withErrors(Lang::get('validation.error'));
				}
			}else{
				return redirect()->back()->withErrors($user->password);
			}
	    	
		}
    }
	

	
	public function get_add_staff(){
		if(!$this->permations('staff_new')){
			return view('admin.permations');
		}
		 return view('admin.staff_new'); 
	}
	
	
	
	public function add_staff(Request $request)
    {

		$validation = Validator::make($request->all(),[
			'name' 					=> 'required',
			'email' 				=> 'required|unique:admins|email|min:5|max:25',
			'password' 				=> 'required',
		],['fullname_ar.regex'=>'Full Name (Arabic) Only Arabic characters']);


		 if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
			$add = new Admin;
			$add->name				= $request->input('name');
			$add->email				= $request->input('email');
			$add->password			= Hash::make(trim($request->input('password')));
			$add->agents			= (empty($request->input('agents'))) ? 0 : $request->input('agents');
			$add->agent_new			= (empty($request->input('agent_new'))) ? 0 :$request->input('agent_new');
			$add->agent_update		= (empty($request->input('agent_update'))) ? 0 :$request->input('agent_update');
			$add->agent_sendpassword= (empty($request->input('agent_sendpassword'))) ? 0 :$request->input('agent_sendpassword');
			$add->agent_profile		= (empty($request->input('agent_profile'))) ? 0 :$request->input('agent_profile');
			$add->agent_sendpush	= (empty($request->input('agent_sendpush'))) ? 0 :$request->input('agent_sendpush');
			$add->agent_requests	= (empty($request->input('agent_requests'))) ? 0 :$request->input('agent_requests');
			$add->customers			= (empty($request->input('customers'))) ? 0 :$request->input('customers');
			$add->customer_update	= (empty($request->input('customer_update'))) ? 0 :$request->input('customer_update');
			$add->customer_profile	= (empty($request->input('customer_profile'))) ? 0 :$request->input('customer_profile');
			$add->customer_tickets	= (empty($request->input('customer_tickets'))) ? 0 :$request->input('customer_tickets');
			$add->customer_sendpush	= (empty($request->input('customer_sendpush'))) ? 0 :$request->input('customer_sendpush');
			$add->customer_requests	= (empty($request->input('customer_requests'))) ? 0 :$request->input('customer_requests');
			$add->updaterequest		= (empty($request->input('updaterequest'))) ? 0 :$request->input('updaterequest');
			$add->all_requests		= (empty($request->input('all_requests'))) ? 0 :$request->input('all_requests');
			$add->staff				= (empty($request->input('staff'))) ? 0 :$request->input('staff');
			$add->staff_new			= (empty($request->input('staff_new'))) ? 0 :$request->input('staff_new');
			$add->staff_update		= (empty($request->input('staff_update'))) ? 0 :$request->input('staff_update');
			$add->staff_delete		= (empty($request->input('staff_delete'))) ? 0 :$request->input('staff_delete');
			$add->settings			= (empty($request->input('settings'))) ? 0 :$request->input('settings');
			$add->faqs				= (empty($request->input('faqs'))) ? 0 :$request->input('faqs');
			$add->pages				= (empty($request->input('pages'))) ? 0 :$request->input('pages');
			$add->governorate		= (empty($request->input('governorate'))) ? 0 :$request->input('governorate');
			$add->areas				= (empty($request->input('areas'))) ? 0 :$request->input('areas');
			$add->siteoption		= (empty($request->input('siteoption'))) ? 0 :$request->input('siteoption');
			$add->slider			= (empty($request->input('slider'))) ? 0 :$request->input('slider');
	    	if($add->save()){
	    		Session::put('success',Lang::get('validation.Saved'));
	    		return redirect()->back();
	    	}else{
				return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	public function get_update_staff($id){
		if(!$this->permations('staff_new')){
			return view('admin.permations');
		}
		$staff = Admin::find($id);
		 return view('admin.staff_update')->with('staff',$staff); 
	}
	
	
	public function update_staff($id,Request $request)
    {

		$validation = Validator::make($request->all(),[
			'name' 					=> 'required',
			'email' 				=> 'required|email|min:5|max:25',
			//'password' 				=> 'required',
		],['fullname_ar.regex'=>'Full Name (Arabic) Only Arabic characters']);


		 if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
			$add = Admin::find($id);
			$add->name				= $request->input('name');
			$add->email				= $request->input('email');
			if(!empty($request->input('password'))){
				$add->password			= Hash::make(trim($request->input('password')));
			}
			$add->agents			= (empty($request->input('agents'))) ? 0 : $request->input('agents');
			$add->agent_new			= (empty($request->input('agent_new'))) ? 0 :$request->input('agent_new');
			$add->agent_update		= (empty($request->input('agent_update'))) ? 0 :$request->input('agent_update');
			$add->agent_sendpassword= (empty($request->input('agent_sendpassword'))) ? 0 :$request->input('agent_sendpassword');
			$add->agent_profile		= (empty($request->input('agent_profile'))) ? 0 :$request->input('agent_profile');
			$add->agent_sendpush	= (empty($request->input('agent_sendpush'))) ? 0 :$request->input('agent_sendpush');
			$add->agent_requests	= (empty($request->input('agent_requests'))) ? 0 :$request->input('agent_requests');
			$add->customers			= (empty($request->input('customers'))) ? 0 :$request->input('customers');
			$add->customer_update	= (empty($request->input('customer_update'))) ? 0 :$request->input('customer_update');
			$add->customer_profile	= (empty($request->input('customer_profile'))) ? 0 :$request->input('customer_profile');
			$add->customer_tickets	= (empty($request->input('customer_tickets'))) ? 0 :$request->input('customer_tickets');
			$add->customer_sendpush	= (empty($request->input('customer_sendpush'))) ? 0 :$request->input('customer_sendpush');
			$add->customer_requests	= (empty($request->input('customer_requests'))) ? 0 :$request->input('customer_requests');
			$add->updaterequest		= (empty($request->input('updaterequest'))) ? 0 :$request->input('updaterequest');
			$add->all_requests		= (empty($request->input('all_requests'))) ? 0 :$request->input('all_requests');
			$add->staff				= (empty($request->input('staff'))) ? 0 :$request->input('staff');
			$add->staff_new			= (empty($request->input('staff_new'))) ? 0 :$request->input('staff_new');
			$add->staff_update		= (empty($request->input('staff_update'))) ? 0 :$request->input('staff_update');
			$add->staff_delete		= (empty($request->input('staff_delete'))) ? 0 :$request->input('staff_delete');
			$add->settings			= (empty($request->input('settings'))) ? 0 :$request->input('settings');
			$add->faqs				= (empty($request->input('faqs'))) ? 0 :$request->input('faqs');
			$add->pages				= (empty($request->input('pages'))) ? 0 :$request->input('pages');
			$add->governorate		= (empty($request->input('governorate'))) ? 0 :$request->input('governorate');
			$add->areas				= (empty($request->input('areas'))) ? 0 :$request->input('areas');
			$add->siteoption		= (empty($request->input('siteoption'))) ? 0 :$request->input('siteoption');
			$add->slider			= (empty($request->input('slider'))) ? 0 :$request->input('slider');
	    	if($add->save()){
	    		Session::put('success',Lang::get('validation.Saved'));
	    		return redirect()->back();
	    	}else{
				return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	
	public function destroy_staff($id){
		$cat = Admin::find($id);
		if($cat->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}
	
	public function get_staff_list(){
		if(!$this->permations('staff')){
			return view('admin.permations');
		}
		$staff = Admin::where('permations','=','staff')->get();
		 return view('admin.staff')->with('staff',$staff); 
	}
	
	static function permations($name=''){
	//	DB::setFetchMode(PDO::FETCH_ASSOC);
		$arr = Auth::guard('admins')->user();
		$user = array(
			'agents' => $arr->agents,
			'agent_new' => $arr->agent_new,
			'agent_update' => $arr->agent_update,
			'agent_sendpassword' => $arr->agent_sendpassword,
			'agent_profile' => $arr->agent_profile,
			'agent_sendpush' => $arr->agent_sendpush,
			'agent_requests' => $arr->agent_requests,
			'customers' => $arr->customers,
			'customer_profile' => $arr->customer_profile,
			'customer_update' => $arr->customer_update,
			'customer_sendpush' => $arr->customer_sendpush,
			'customer_tickets' => $arr->customer_tickets,
			'customer_requests' => $arr->customer_requests,
			'settings' => $arr->settings,
			'faqs' => $arr->faqs,
			'governorate' => $arr->governorate,
			'areas' => $arr->areas,
			'siteoption' => $arr->siteoption,
			'slider' => $arr->slider,
			'updaterequest' => $arr->updaterequest,
			'pages' => $arr->pages,
			'staff' => $arr->staff,
			'staff_new' => $arr->staff_new,
			'staff_update' => $arr->staff_update,
			'staff_delete' => $arr->staff_delete,
			'all_requests' => $arr->all_requests,
			'permations' => $arr->permations,
		);
		if($user[$name] == 1){
			return true;
		}else{
			return false;
		}
	}
	
}
