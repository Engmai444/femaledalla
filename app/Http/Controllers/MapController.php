<?php

namespace App\Http\Controllers;

use Validator;
use App\Pages;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class MapController extends Controller
{
    //
	private function permations($name){
		$res = AdminController::permations($name);
		return $res;
	}
	
	
	public function view_map(){
		return view('site/map');
	}
}