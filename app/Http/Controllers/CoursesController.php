<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Models\Evaluation_time;
use http\Client\Curl\User;
use Validator;
use App\Course;
use App\Order;
use App\Payment_log;
use App\Application;
use App\Period;
use App\Category;
use Response;
use Redirect;
use Image;
use App;
use Session;
use Excel;
use Lang;
use File;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Classes\Hijri;
use Carbon\Carbon;
use Storage;
use Config;

class CoursesController extends Controller
{
    public $allValues2 = array();

    public function orders(Request $request){

        DB::enableQueryLog();
		$query = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period');

		if($request->OrderId){
			$query->where('orders.OrderId',$request->OrderId);
		}
		if($request->from){
			$query->whereDate('orders.created_at','>=',$request->from);
		}
		if($request->to){
			$query->whereDate('orders.created_at','<=',$request->to);
		}
		if($request->IDNumber){
			$query->where('users.IDNumber',$request->IDNumber);
		}
        if($request->SchoolId ){
            $query->where('orders.SchoolId',$request->SchoolId);
        }
		if($request->FullName){
			$query->where('users.FullName','LIKE','%'.$request->FullName.'%');
		}
		if($request->mobile){
			$query->where('users.mobile',$request->mobile);
		}
        if($request->email){
            $query->where('users.email',$request->email);
        }
        if($request->courseref){
            $query->where('orders.CourseRefNo',$request->courseref);
        }
        if($request->course_name){
            $query->where('course.CourseName','like','%'.$request->course_name.'%');
        }
        if($request->order_ref){
            $query->where('orders.trans_no',$request->order_ref);
        }
       // $query->where('orders.type',1);
		$schools = DB::table('schools')->get();
        //$query->where('orders.deleted_at','!=','0000-00-00 00:00:00');
      //  $query->whereNULL('orders.deleted_at');
          $query->whereIn('orders.deleted_at', ['',null, '0000-00-00 00:00:00']);
          $query->where('IsPaid','1');
        //$query->where('orders.deleted_at','0000-00-00 00:00:00');
		$items = $query->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.IDNumber','users.FullName','users.mobile','users.email','users.gender','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))
            ->orderByDesc('OrderId')->get();
        //$query = DB::getQueryLog();
       // dd($query);
		return view('admin.orders',compact('items','schools'));
	}

    public function orders_unpaid(Request $request){
        DB::enableQueryLog();
        $query = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period');

        if($request->OrderId){
            $query->where('orders.OrderId',$request->OrderId);
        }
        if($request->from){
            $query->whereDate('orders.created_at','>=',$request->from);
        }
        if($request->to){
            $query->whereDate('orders.created_at','<=',$request->to);
        }
        if($request->IDNumber){
            $query->where('users.IDNumber',$request->IDNumber);
        }
        if($request->SchoolId ){
            $query->where('orders.SchoolId',$request->SchoolId);
        }
        if($request->FullName){
            $query->where('users.FullName','LIKE','%'.$request->FullName.'%');
        }
        if($request->mobile){
            $query->where('users.mobile',$request->mobile);
        }
        if($request->email){
            $query->where('users.email',$request->email);
        }
        if($request->courseref){
            $query->where('orders.CourseRefNo',$request->courseref);
        }
        if($request->order_ref){
            $query->where('orders.trans_no',$request->order_ref);
        }
        if($request->course_name){
            $query->where('course.CourseName','like','%'.$request->course_name.'%');
        }
        // $query->where('orders.type',1);
        $schools = DB::table('schools')->get();
        //$query->where('orders.deleted_at','!=','0000-00-00 00:00:00');
        //  $query->whereNULL('orders.deleted_at');
        $query->where('IsPaid',0);
        //$query->where('orders.deleted_at','0000-00-00 00:00:00');
        $items = $query->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))
            ->orderByDesc('OrderId')->groupby('StudentId','CourseId')->get();
        //$query = DB::getQueryLog();
        return view('admin.orders_unpaid',compact('items','schools'));
    }

    public function DeleteOrder($id){
         $item = DB::table('orders')->where('OrderId',$id)->first();
        // dd($item);
        /*if($item !=null or $item=='0000-00-00 00:00:00'){
            DB::table('orders')->where('OrderId',$id)->update(['deleted_at'=>date('Y-m-d H:s:i')]);
            Session::put('success','تم حذف الاشتراك بنجاح');
            return redirect()->back();
        }else{*/
            DB::table('orders')->where('OrderId',$id)->delete();
            return redirect('/admin/orders_unpaid')->withErrors('تم حذف الاشتراك بنجاح');
        //}
        
    }
    public function multiDelete(Request $request){
        if(!empty($request->orderID) && DB::table('new_orders')->whereIn('id',$request->orderID)->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect('/admin/orders_new/');
        }else{
            return redirect('/admin/orders_new/')->withErrors('لم تقم بتحديد اي عناصر لحذفها');
        }
    }
    public function multiDelete_unpaid(Request $request){
        if(!empty($request->StudentId) && DB::table('orders')->whereIn('OrderId',$request->StudentId)->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect('/admin/orders_unpaid/');
        }else{
            return redirect('/admin/orders_unpaid/')->withErrors('لم تقم بتحديد اي عناصر لحذفها');
        }
    }
    public function multiDelete_paid(Request $request){
        if(!empty($request->StudentId) && DB::table('orders')->whereIn('OrderId',$request->StudentId)->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect('/admin/orders/');
        }else{
            return redirect('/admin/orders/')->withErrors('لم تقم بتحديد اي عناصر لحذفها');
        }
    }
    public function multiDelete_course(Request $request){
        if(!empty($request->orderID) && DB::table('course')->whereIn('id',$request->orderID)->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect()->back();
        }else{
            return redirect()->back()->withErrors('لم تقم بتحديد اي عناصر لحذفها');
        }
    }

	public function GetorderUpdate($orderid){
        $item = DB::table('orders')->where('OrderId',$orderid)->first();
		return view('admin.order_update',compact('item'));
	}
	public function OrderUpdate($id,Request $request)
    {
		$messages = [
			
		];
		$validation = Validator::make($request->all(),[
            
		],$messages);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
        

			$add = DB::table('orders')->where('OrderId',$id)->update(['IsPaid'=>$request->input('IsPaid')]);
		    //$add->IsPaid	= $request->input('IsPaid');
			
            
	    	//if($add){
                 Session::put('success','تم تأكيد الحضور بنجاح');
                 return redirect()->back();
         //   }else{
             //    return redirect()->back()->withErrors(Lang::get('validation.error'));
			//}
		}
    }

    public function order_report(Request $request){
        $query = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period');

        // $query->where('orders.type',1);
        $schools = DB::table('schools')->get();
        //$query->where('orders.deleted_at','!=','0000-00-00 00:00:00');
        //  $query->whereNULL('orders.deleted_at');
        //$query->whereIn('orders.deleted_at', ['',null, '0000-00-00 00:00:00']);
        $query->where('IsPaid','1');
        //$query->where('orders.deleted_at','0000-00-00 00:00:00');


		if($request->from){
			$query->whereDate('orders.created_at','>=',$request->from);
		}
		if($request->to){
			$query->whereDate('orders.created_at','<=',$request->to);
		}


        $items = $query->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.IDNumber','users.FullName','users.mobile','users.email','users.gender','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))
            ->orderByDesc('OrderId')->get();


		Excel::create('getOnline', function($excel) use($items) {
			$excel->sheet('getOnline', function($sheet) use($items) {
			  
			  $sheet->loadView('excel.orders', ['items' => $items]);
			});
		})->export('xlsx');
		//return response()->json($customersr);
    }
    public function order_report_unpaid(Request $request){

        $query = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->join('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period');
        //$query->where('orders.deleted_at','0000-00-00 00:00:00');
        //$query->orwhereNULL('orders.deleted_at');
        $query->where('orders.IsPaid','0');
        if($request->from){
            $query->whereDate('orders.created_at','>=',$request->from);
        }
        if($request->to){
            $query->whereDate('orders.created_at','<=',$request->to);
        }


        $items = $query->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.mobile','users.email','users.DoB','users.IDNumber','users.FullName','users.gender','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))->groupby('StudentId','CourseId')->get();



        Excel::create('getOnlineUnpaid', function($excel) use($items) {
            $excel->sheet('getOnlineUnpaid', function($sheet) use($items) {

                $sheet->loadView('excel.orders', ['items' => $items]);
            });
        })->export('xlsx');
        //return response()->json($customersr);
    }
	public function exportOrders(Request $request){


        $query = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->join('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period');
        $query->whereIn('orders.deleted_at', ['',null, '0000-00-00 00:00:00']);
        $query->where('IsPaid','1');

        //$query->where('orders.deleted_at','0000-00-00 00:00:00');
        //$query->orwhereNULL('orders.deleted_at');
        //$query->whereDate('orders.created_at','>=',Carbon::now());
		//$query->whereDate('orders.created_at','<',Carbon::now()->addDay());
       // $query->where('orders.deleted_at','0000-00-00 exportOrders00:00:00');
        //$query->orwhereNULL('orders.deleted_at');
		$items = $query->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.IDNumber','users.gender','users.email','users.mobile','users.FullName','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))->get();



		$file = Excel::create('getOnlinef_paid', function($excel) use($items) {
			$excel->sheet('getOnlinef_paid', function($sheet) use($items) {

			  $sheet->loadView('excel.orders', ['items' => $items]);
			});
		})->store('xlsx', storage_path('app/public/excel'), true);

		Storage::disk('googledrive')->put('getOnlinef_paid.xlsx', file_get_contents(storage_path('app/public/excel/getOnlinef_paid.xlsx')));

		/*
		$photo = $request->file('photo');

			$photo_name      = str_random(5).time();
			$photo_ext       = $photo->getClientOriginalExtension();
			$photo_fullname  = $photo_name.'.'.$photo_ext;



		Storage::disk('googledrive')->put($photo_fullname, file_get_contents($photo));
		*/
		return "ok";

		//return response()->json($customersr);
    }
    public function exportOrdersunpaid(Request $request){

        $query = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->join('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period');
        $query->where('IsPaid','0');

        $items = $query->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.email','users.mobile','users.IDNumber','users.gender','users.FullName','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))->groupby('StudentId','CourseId')->get();



        $file = Excel::create('getOnlinef_unpaid', function($excel) use($items) {
            $excel->sheet('getOnlinef_unpaid', function($sheet) use($items) {

                $sheet->loadView('excel.orders', ['items' => $items]);
            });
        })->store('xlsx', storage_path('app/public/excel'), true);

        Storage::disk('googledrive')->put('getOnlinef_unpaid.xlsx', file_get_contents(storage_path('app/public/excel/getOnlinef_unpaid.xlsx')));

        /*
        $photo = $request->file('photo');

            $photo_name      = str_random(5).time();
            $photo_ext       = $photo->getClientOriginalExtension();
            $photo_fullname  = $photo_name.'.'.$photo_ext;



        Storage::disk('googledrive')->put($photo_fullname, file_get_contents($photo));
        */
        return "ok";

        //return response()->json($customersr);
    }
    public function items(Request $request){

        $applicationdefs = DB::table('applicationdef')->pluck('AppDefName','Id');
        $schools = DB::table('schools')->get();
       // $periods = DB::table('periods')->get();
	   
	   
        $query = DB::table('course')->leftjoin('periods','periods.PeriodId','course.Period')->leftjoin('applicationdef','applicationdef.Id','course.applicationdef')->leftjoin('category','category.Id','course.category')->leftjoin('schools','schools.Id','course.SchoolId')->leftjoin('orders','orders.CourseId','course.id')->select('course.*','applicationdef.AppDefName','periods.*',DB::RAW('schools.Name schoolName'),DB::RAW('category.Name categoryName'),DB::RAW('count(orders.CourseId) as booked'));
                //$query->whereIn('orders.deleted_at', ['',null, '0000-00-00 00:00:00']);

       // $query->whereNULL('orders.deleted_at');
        //$query->orwhere('orders.deleted_at','0000-00-00 00:00:00');
        //$query->where('orders.IsPaid',1);
        if($request->applicationdef){
            $query->where('course.applicationdef',$request->applicationdef);
        }
        if($request->SchoolId){
            $query->where('course.SchoolId',$request->SchoolId);
        }

        if($request->date_from){
            $query->whereDate('course.CourseStartIn','>=',$request->date_from.' 00:00:00');
        }
        if($request->SchoolId){
            $query->where('course.SchoolId',$request->SchoolId);
        }
        if($request->CourseRefNo){
            $query->where('course.CourseRefNo',$request->CourseRefNo);
        }

        if($request->date_to){
            $query->whereDate('course.CourseStartIn','<=',$request->date_to.' 00:00:00');
        }
        $courses = $query->groupBy('course.id')->orderBy('course.id','desc')->get();
    
        $applicationdef = $request->applicationdef;
        $SchoolId = $request->SchoolId;
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        
      
		return view('admin.courses.courses',compact('courses','applicationdefs','schools','applicationdef','SchoolId','date_from','date_to'));
		
        
    }

    public function courses_report(){
        
        $query = DB::table('course')->leftjoin('applicationdef','applicationdef.Id','course.applicationdef')->leftjoin('category','category.Id','course.category')->select('course.*',DB::RAW('category.Name categoryName'));
        $courses = $query->get();

        Excel::create('getCourses', function($excel) use($courses) {
			$excel->sheet('courses', function($sheet) use($courses) {
			  $sheet->loadView('excel.courses', ['courses' => $courses]);
			});
		})->export('xlsx');
    }
	
	public function destroy_Course($id){
		$cat = Course::where('id',$id)->first();
		if(!empty($cat)) {
            $items = Order::where('CourseId', $cat->id)->delete();
        }
		if(Course::where('id',$id)->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}

    //
    public function view_new(){
        $applicationdef = DB::table('applicationdef')->select('Id','AppDefName')->get();
        $schools = DB::table('schools')->select('Id','Name')->get();
        $periods = DB::table('periods')->get();
		return view('admin.courses.new',compact('applicationdef','schools','periods'));
	}
	public function store(Request $request)
    {
		$messages = [
			'applicationdef.required' => 'من فضلك اختر النموذج',
			'category.required' => 'من فضلك اختر الفئة',
			'days.required' => 'من فضلك ادخل مدة الدورة',
			'Period.required' => 'من فضلك ادخل الفترة',
			'price.required' => 'من فضلك ادخل سعر الدورة',
			'CourseRefNo.required' => 'رقم تعريف الدورة مطلوب',
            //'CourseRefNo.unique' => 'رقم تعريف الدورة موجود من قبل',
			'CourseStartIn.required' => 'من افضلك ادخل تاريخ بداية الدورة',
			'CourseEndIn.required' => 'من افضلك ادخل تاريخ نهاية الدورة',
			'SchoolId.required' => 'من فضلك اختر المدرسة',
			'seats.required' => 'من فضلك ادخل عدد المقاعد المتاحة',
		];
		$validation = Validator::make($request->all(),[
            'applicationdef'=> 'required',
            'category'      => 'required',
            'days'          => 'required',
            'Period'        => 'required',
            'price'         => 'required',
			'CourseRefNo'         => 'required',
			'CourseStartIn'         => 'required',
			'CourseEndIn'         => 'required',
			'SchoolId'         => 'required',
			'seats'         => 'required',
		],$messages);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
            $corse=DB::table('course')->where('CourseRefNo',$request->input('CourseRefNo'))
                ->where('SchoolId',$request->input('SchoolId'))->first();
            if(!empty($corse)){
                return redirect()->back()->withErrors('الدورة موجوده من قبل')->withInput();
            }
			if(strtotime($request->input('CourseEndIn')) < strtotime($request->input('CourseStartIn'))){
				return redirect()->back()->withErrors('تاريخ نهاية الدورة لابد ان يكون اكبر من تاريخ بدايتها')->withInput();
			}
$setting=DB::table('site_option')->where('varname', 'tax')->first();
			$code_cat=DB::table('category')->where('Id',$request->input('category'))->first();
			$add = new Course;
		    $add->CourseRefNo		= $request->input('CourseRefNo');
            $add->gender		= $request->input('gender');
			$add->CourseName 		= $request->input('CourseName');
			$add->CourseStartIn		= $request->input('CourseStartIn');
            $add->CourseEndIn 		= $request->input('CourseEndIn');
            $add->applicationdef	= $request->input('applicationdef');
			$add->category		    = $request->input('category');
			$add->gearShiftLever 	= $request->input('gearShiftLever');
			$add->Period	        = $request->input('Period');
			$add->days 	            = $request->input('days');
            $add->price 	        = $request->input('price');
            $add->tax 	            = round(($request->input('price') * $setting->value) / 100,2);
            $add->SchoolId 	        = $request->input('SchoolId');
			$add->enable 	        = $request->input('enable');
			$add->seats 	        = $request->input('seats');
            $add->type 	        = $code_cat->code;
            
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }

    public function GetImportCoursers(){
        return view('admin.courses.import');
    }
    public function importCoursers(Request $request){
        $messages = [
            'file.required' => 'من فضلك اختر الملف'
        ];
        $validation = Validator::make($request->all(),[
            'file'=> 'required',
        ],$messages);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            if($request->hasFile('file')){
                $extension = File::extension($request->file->getClientOriginalName());
                if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                    $path = $request->file->getRealPath();
                    $data = Excel::load($path, function($reader) {
                    })->get();
                    if(!empty($data) && $data->count()){
                        $insert  = [];
                        foreach ($data as $key => $value) {
                            $corse=DB::table('course')->where('CourseRefNo',$value->courserefno)
                                                     ->where('SchoolId',(int)$value->schoolid)->first();
                            if(empty($corse)) {
                                if (!empty($value->applicationdef)) {
                                    $CourseName = "";

                                    $application = Application::find((int)$value->applicationdef);
                                    if (!empty($application)) {
                                        $CourseName .= $application->AppDefName . ' - ';
                                    }
                                    $category = Category::find((int)$value->category);
                                    if (!empty($category)) {
                                        $CourseName .= $category->Name;
                                    }
                                    $setting = DB::table('site_option')->where('varname', 'tax')->first();
                                    $gender = $value->gender;
                                    if ($gender == '0') {
                                        $gen = 'male';
                                    } else {
                                        $gen = 'female';
                                    }
                                    $code_cat=DB::table('category')->where('Id',(int)$value->category)->first();

                                    $insert[] = [
                                        'CourseRefNo' => $value->courserefno,
                                        'CourseName' => $CourseName,
                                        'CourseStartIn' => $value->coursestartin,
                                        'CourseEndIn' => $value->courseendin,
                                        'applicationdef' => (int)$value->applicationdef,
                                        'category' => (int)$value->category,
                                        'gearShiftLever' => ((int)$value->gearshiftlever == 1) ? 'Automatic' : 'Manuel',
                                        'Period' => (int)$value->period,
                                        'days' => (int)$value->days,
                                        'price' => $value->price,
                                        'tax' => round(($value->price * $setting->value) / 100, 2),
                                        'SchoolId' => (int)$value->schoolid,
                                        'enable' => '1',
                                        'seats' => (int)$value->seats,
                                        'gender' => $gen,
                                        'type' => $code_cat->code,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now()
                                    ];
                                }
                            }else{
                                return redirect()->back()->withErrors('يوجد دورة متكررة من قبل'.$corse->CourseRefNo);
                            }

                        }

                        if(!empty($insert)){
                            DB::table('course')->insert($insert);
                            Session::put('success',Lang::get('validation.Saved'));
                            return redirect()->back();
                        }else{
                            return redirect()->back()->withErrors('الملف فارغ');
                        }
                    }
                }else {
                    return redirect()->back()->withErrors('صيغة الملف غير مدعومة');
                }
            }
        }
    }


	
	public function view_update($id){
		$course = Course::find($id);
        $applicationdef = DB::table('applicationdef')->select('Id','AppDefName')->get();
        $schools = DB::table('schools')->select('Id','Name')->get();
		$category = DB::table('category')->where('applicationdef_id',$course->applicationdef)->select('Id','Name')->get();
        $periods = DB::table('periods')->get();
		return view('admin.courses.update',compact('applicationdef','category','schools','periods','course'));
	}
	public function update($id,Request $request)
    {
		$messages = [
			'applicationdef.required' => 'من فضلك اختر النموذج',
			'category.required' => 'من فضلك اختر الفئة',
			'days.required' => 'من فضلك ادخل مدة الدورة',
			'Period.required' => 'من فضلك ادخل الفترة',
			'price.required' => 'من فضلك ادخل سعر الدورة',
			'CourseRefNo.required' => 'رقم تعريف الدورة مطلوب',
			'CourseStartIn.required' => 'من افضلك ادخل تاريخ بداية الدورة',
			'CourseEndIn.required' => 'من افضلك ادخل تاريخ نهاية الدورة',
			'SchoolId.required' => 'من فضلك اختر المدرسة',
			'seats.required' => 'من فضلك ادخل عدد المقاعد المتاحة',
		];
		$validation = Validator::make($request->all(),[
            'applicationdef'=> 'required',
            'category'      => 'required',
            'days'          => 'required',
            'Period'        => 'required',
            'price'         => 'required',
			'CourseRefNo'         => 'required',
			'CourseStartIn'         => 'required',
			'CourseEndIn'         => 'required',
			'SchoolId'         => 'required',
			'seats'         => 'required',
		],$messages);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
            $setting=DB::table('site_option')->where('varname', 'tax')->first();
            $code_cat=DB::table('category')->where('Id',$request->input('category'))->first();


            $add = Course::find($id);
		    $add->CourseRefNo		= $request->input('CourseRefNo');
			$add->CourseName 		= $request->input('CourseName');
            $add->gender 		= $request->input('gender');
			$add->CourseStartIn		= $request->input('CourseStartIn');
            $add->CourseEndIn 		= $request->input('CourseEndIn');
            $add->applicationdef	= $request->input('applicationdef');
			$add->category		    = $request->input('category');
			$add->Period	        = $request->input('Period');
			$add->days 	            = $request->input('days');
            $add->price 	        = $request->input('price');
            $add->tax 	            = round(($request->input('price') * $setting->value) / 100,2);
            $add->SchoolId 	        = $request->input('SchoolId');
            $add->type 	        = $code_cat->code;
			//dd($add);
            if($request->input('enable') ==  $add->enable && DB::table('orders')->where('CourseId',$id)->where('orders.IsPaid','1')->count() >= $request->input('seats')){
                $msg="لا يمكن تغيير عدد الاماكن لعدد اقل من الاماكن المحجوزة";
                Session::put('success',$msg);
                return redirect()->back();
            }else {
                if ($request->input('seats') > $add->seats) {
                    $add->seats = $request->input('seats');
                    $add->enable = '1';
                } else {
                    $add->seats = $request->input('seats');
                    $add->enable = $request->input('enable');
                }

            }
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }

    public function ajax_get_category(Request $request){
        $applicationdef = DB::table('applicationdef')->where('Id',$request->id)->first();
        $category = DB::table('category')->where('applicationdef_id',$request->id)->select('Id','Name')->get();
        
        return response()->json(['items' => $category,'applicationdef'=>$applicationdef]);
    }

    public function getCourses(Request $request){
//dd(Carbon::today()->addDays(-2));
        $applicationdefs = DB::table('applicationdef')->pluck('AppDefName','Id');
        $schools = DB::table('schools')->pluck('Name','Id');
       // $periods = DB::table('periods')->get();
        $query = DB::table('course')->leftjoin('applicationdef','applicationdef.Id','course.applicationdef')->leftjoin('category','category.Id','course.category')->leftjoin('schools','schools.Id','course.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')->select('course.*',DB::RAW('category.Name categoryName'),'periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'));
        if($request->applicationdef){
            $query->where('course.applicationdef',$request->applicationdef);
        }
        if($request->SchoolId){
            $query->where('course.SchoolId',$request->SchoolId);
        }
		$query->whereDate('course.CourseStartIn','>=',Carbon::today()->addDays(+1));

        $filter = (int)$request->filter;

        if($filter != 1){
            $query->where('course.id',0);
        }
		$query->where('course.enable',1);

        if($request->date_from and $request->date_to==NULL){
            $query->whereDate('course.CourseStartIn',$request->date_from.' 00:00:00');

        }

        if($request->date_to and $request->date_from==NULL){
            $query->whereDate('course.CourseEndIn',$request->date_to.' 00:00:00');
        }
        if($request->date_from and $request->date_to and $request->date_from!=NULL and $request->date_to!=NULL){

           $query->whereDate('course.CourseStartIn','>=',$request->date_from.' 00:00:00');
            $query->whereDate('course.CourseStartIn','<=',$request->date_to.' 00:00:00');

        }
        if(!empty(Auth::guard('doctor')->user())) {
            //$today = Carbon::today();
            //$y = $this->dateDifference(Auth::guard('doctor')->user()->DoB, $today);
            $date=Auth::guard('doctor')->user()->DoB;
            $date_type=Auth::guard('doctor')->user()->date_type;

if($date_type==0) {
    $date=Auth::guard('doctor')->user()->DoB;
}else if($date_type==1) {
    $time=explode("-",$date);
    $day=$time['2'];

    $month=$time['1'];
    $year=$time['0'];

   // $Date = \GeniusTS\HijriDate\Hijri::convertToGregorian($day, $month, $year);
   // $date = date('Y-m-d', strtotime($Date. ' + 1 days'));
    $date=Auth::guard('doctor')->user()->DoB;

}

            $date1=date('Y/m/d',strtotime($date));
           // $dateinput="{$request->d_year}/{$request->d_month}/{$request->d_day}";
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da_st=date('Ymd',strtotime($date1));

            $date_sta="{$da_st}";
            $d_st =$DateConv->GregorianToHijri($date_sta,$format);
            $dh_start=date('Y/m/d',strtotime($d_st));


            $today = Carbon::today();
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da=date('Ymd',strtotime($today));

            $datev="{$da}";
            $d =$DateConv->GregorianToHijri($datev,$format);
            $dh_today=date('Y/m/d',strtotime($d));

            $startDate = Carbon::parse($dh_start);
            $endDate   = Carbon::parse($dh_today);
            $y = $startDate->diff($endDate)->format("%y");


            //$today = Carbon::today();
            //$y = $this->dateDifference($date,$today);
            if (intval($y) < 18) {
                $query->where('applicationdef', '5');
            }else{
                $query->where('applicationdef', '!=', '5');
            }
        }

        $courses = $query->get();

        $applicationdef = $request->applicationdef;
        $SchoolId = $request->SchoolId;
        $date_from = $request->date_from;
        $date_to = $request->date_to;

        if(App::getLocale() == 'en'){
			return view('site.en.courses',compact('courses','filter','applicationdefs','schools','applicationdef','SchoolId','date_from','date_to'));
		}else{
			return view('site.courses',compact('courses','filter','applicationdefs','schools','applicationdef','SchoolId','date_from','date_to'));
        }

    }
	public function dateDifference($date_1 , $date_2 , $differenceFormat = '%y' )
	{
		$datetime1 = date_create($date_1);
		$datetime2 = date_create($date_2);
	   
		$interval = date_diff($datetime1, $datetime2);
	   
		return $interval->format($differenceFormat);
	   
	}

    public function mycourses(){
        $courses = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('schools','schools.Id','orders.SchoolId')->where('orders.StudentId',Auth::guard('doctor')->user()->StudentId)->select('orders.*','course.CourseName','course.id as cid','course.CourseStartIn',DB::RAW('schools.Name as schoolName'))->get();
        if(App::getLocale() == 'en'){
			return view('site.en.user.courses',compact('courses'));
		}else{
			return view('site.user.courses',compact('courses'));
        }
    }

    public function mycourse($id){
        @$course = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('nationality','nationality.Id','users.NationalityId')->leftjoin('periods','periods.PeriodId','course.Period')->where('orders.StudentId',Auth::guard('doctor')->user()->StudentId)->where('orders.OrderId',$id)->select('orders.*','course.CourseName','course.CourseStartIn','course.days','periods.fromH','periods.toH','users.FullName','users.gender','users.IDNumber','users.mobile',DB::RAW('nationality.Name as nationality'),DB::RAW('schools.Name as schoolName'))->first();
        if(App::getLocale() == 'en'){
			return view('site.en.user.course',compact('course'));
		}else{
			return view('site.user.course',compact('course'));
        }
	}
	
	public function checkUserSubs($userid,$date){
		$orders = DB::table('orders')->join('course','course.id','orders.CourseId')->where('orders.StudentId',$userid)->where('course.CourseEndIn','>',Carbon::today())->count();
		return $orders;
	}

    public function subsCours(Request $request){
       if(!Auth::guard('doctor')->check()){
        return response()->json(['status'=> 'login','message'=>'لابد من التسجيل او الدخول لكي تتمكن من الاشتراك فى الدورات']);
       }
       $userid = Auth::guard('doctor')->user()->StudentId;
	   
	   $course = Course::find($request->id);

        $date=Auth::guard('doctor')->user()->DoB;
        $date_type=Auth::guard('doctor')->user()->date_type;
        if($date_type==0) {
            $date=Auth::guard('doctor')->user()->DoB;

        }else if($date_type==1) {
            $time=explode("-",$date);
            $day=$time['2'];
            $month=$time['1'];
            $year=$time['0'];
           // $Date = \GeniusTS\HijriDate\Hijri::convertToGregorian($day, $month, $year);
           // $date = date('Y-m-d', strtotime($Date. ' + 1 days'));
            $date=Auth::guard('doctor')->user()->DoB;
        }
	  // $today = Carbon::today();
		//$y = $this->dateDifference($date,$today);
        $date1=date('Y/m/d',strtotime($date));
        // $dateinput="{$request->d_year}/{$request->d_month}/{$request->d_day}";
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da_st=date('Ymd',strtotime($date1));

        $date_sta="{$da_st}";
        $d_st =$DateConv->GregorianToHijri($date_sta,$format);
        $dh_start=date('Y/m/d',strtotime($d_st));


        $today = Carbon::today();
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da=date('Ymd',strtotime($today));

        $datev="{$da}";
        $d =$DateConv->GregorianToHijri($datev,$format);
        $dh_today=date('Y/m/d',strtotime($d));

        $startDate = Carbon::parse($dh_start);
        $endDate   = Carbon::parse($dh_today);
        $y = $startDate->diff($endDate)->format("%y");

		if(intval($y) < 18 && $course->applicationdef != 5){
			$res = array(
				'status' 	=> 'error',
				'code' 		=> 231,
				'message' 	=> 'لا يمكن التسجيل في هذه الدورة لمن عمره اقل من 18 عام'
			);
			return response()->json($res);
		}
		
		if(intval($y) >= 18 && $course->applicationdef == 5){
			$res = array(
				'status' 	=> 'error',
				'code' 		=> 231,
				'message' 	=> 'لا يمكن التسجيل في هذه الدورة لمن عمره أكبر من 18 عام'
			);
			return response()->json($res);
		}
       $st=DB::table('users')->where('StudentId',$userid)->first();
		if($st->gender!=$course->gender){
            return response()->json(['message'=>'لا يمكن التسجيل في هذه الدورة لانها لنوع أخر ']);

        }

       $order = DB::table('orders')->where('StudentId',$userid)->where('CourseId',$course->id)->where('orders.IsPaid','1')->first();
if(!empty($order)){
	       return response()->json(['message'=>'لقد قمت بالاشتراك فى هذه الدورة من قبل ']);
	   }
       if($course->waiting_list=='0') {
           if (DB::table('orders')->where('CourseId', $course->id)->where('orders.IsPaid', '1')->count() >= $course->seats) {
               return response()->json(['message' => 'نعتذر لك ..نفذت المقاعد المتاحه للحجز, يمكنك معاودة المحاولة لاحقاً في الدورات القادمة']);
           }
       }else{
           if (DB::table('orders')->where('CourseId', $course->id)->count() >= $course->seats) {
               return response()->json(['message' => 'نعتذر لك ..نفذت المقاعد المتاحه للحجز, يمكنك معاودة المحاولة لاحقاً في الدورات القادمة']);
           }
       }
	   if($this->checkUserSubs($userid,$course->CourseStartIn) > 0){
				$res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> 'لا يمكنك التسجيل فى دورة اخري او مدرسة اخري الا بعد الانتهاء من الدورة المشترك بيها حالياً'
				);
				return response()->json($res);
		}

       if($course != NULL){
          /* $insert=DB::table('orders')->insert(['StudentId'=>$userid,
                                        'SchoolId'=>$course->SchoolId,
                                        'CourseId'=>$course->id,
                                        'CourseRefNo'=>$course->CourseRefNo,
                                        'ApplicationDefId'=>$course->applicationdef,
                                        'deleted_at'=>date('Y-m-d H:s:i')]);*/
          $insert = new Order;
            $insert->StudentId = $userid;
            $insert->SchoolId = $course->SchoolId;
            $insert->CourseId = $course->id;
            $insert->CourseRefNo = $course->CourseRefNo;
            $insert->ApplicationDefId = $course->applicationdef;
           $insert->deleted_at = date('Y-m-d H:s:i');
           $insert->course_start = $course->CourseStartIn;
           $insert->course_end = $course->CourseEndIn;
           $insert->IsPaid = 0;

            if($insert->save()){
				
				$countr = Order::where('CourseId',$course->id)->count();
				$no_seats=$course->seats;
				if($countr == $course->seats){
					$course->enable = 0;
					$course->save();
				}
               // $insert_course = new Course;
				//$course->seats=$no_seats-1;
               // $course->save();

				$todafter=date('Y-m-d',strtotime('+5 days'));
                $now = time(); // or your date as well
                $your_date = strtotime($course->CourseStartIn);
                $datediff =  $your_date-$now;

                $dif= round($datediff / (60 * 60 * 24));
				if($course->waiting_list=='1')
				{
				    if($dif <= 5){
				        $payonline=1;
                    }else{
				        $payonline=0;
                    }

                }else{
                    $payonline=1;
                }

                return response()->json(['message'=>'تم اشتراكك بنجاح يجب السداد فور استلام الرابط خلال 72 ساعه والا سوف يتم الغاء الاشتراك تلقائيا','id'=>$insert->OrderId,'payonline'=>$payonline]);
            }else{
                return response()->json(['message'=>'خطأ فى الاشتراك']);
            }
       }
    }
	
	######### application ############
	
	public function get_newApp(){
		return view('admin.applications.new');
	}
	public function newApp(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'AppDefName' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = new Application;
		   
			$add->AppDefName	= $request->input('AppDefName');
		    $add->AppDefName_en 	= $request->input('AppDefName_en');
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	
	public function get_updateApp($id){
		$item = Application::find($id);
		return view('admin.applications.update')->with('item',$item);
	}
	public function updateApp($id,Request $request)
    {
		$validation = Validator::make($request->all(),[
			'AppDefName' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = Application::find($id);
			$add->Id			= $request->input('Id');
			$add->AppDefName	= $request->input('AppDefName');
		    $add->AppDefName_en = $request->input('AppDefName_en');
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->to('admin/applications');
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }

	public function get_Apps(){
		$items = Application::paginate(100);
		return view('admin.applications.items')->with('items',$items);
	}
	
	//*-------------------delete -------------------*//
	public function destroy_App($id){
		$cat = Application::find($id);
		if($cat->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}
	
	######### application ############
	
	######### category ############
	
	public function get_newCategory(Request $request){
		return view('admin.category.new')->with('appid',(int)$request->appid);
	}
	public function newCategory(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'Name' => 'required',
			'applicationdef_id' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = new Category;
		   
			$add->Name	= $request->input('Name');
            $add->code	= $request->input('code');
			$add->Name_en	= $request->input('Name_en');
		    $add->applicationdef_id 	= $request->input('applicationdef_id');
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	
	public function get_updateCategory($id){
		$item = Category::find($id);
		return view('admin.category.update')->with('item',$item);
	}
	public function updateCategory($id,Request $request)
    {
		$validation = Validator::make($request->all(),[
			'Name' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = Category::find($id);
			$add->Id			= $request->input('Id');
            $add->code	= $request->input('code');
			$add->Name	= $request->input('Name');
			$add->Name_en	= $request->input('Name_en');
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->to('admin/category/'.$add->applicationdef_id);
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }

	public function get_Category($id){
		$cat = Application::find($id);
		$items = Category::where('applicationdef_id',$id)->paginate(100);
		return view('admin.category.items')->with('items',$items)->with('cat',$cat);
	}
	
	//*-------------------delete -------------------*//
	public function destroy_Category($id){
		$cat = Category::find($id);
		if($cat->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}
	
	######### category ############
	
	
	######### periods ############
	
	public function get_newPeriod(){
		return view('admin.period.new');
	}
	public function newPeriod(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'fromH' => 'required',
			'toH' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = new Period;
		   
			$add->fromH	= $request->input('fromH');
		    $add->toH 	= $request->input('toH');
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	
	public function get_updatePeriod($id){
		$item = Period::find($id);
		return view('admin.period.update')->with('item',$item);
	}
	public function updatePeriod($id,Request $request)
    {
		$validation = Validator::make($request->all(),[
			'fromH' => 'required',
			'toH' => 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = Period::find($id);
			$add->PeriodId	= $request->input('Id');
			$add->fromH	= $request->input('fromH');
		    $add->toH 	= $request->input('toH');
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->to('admin/period');
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }

	public function get_Period(){
		$items = Period::paginate(100);
		return view('admin.period.items')->with('items',$items);
	}
	
	//*-------------------delete -------------------*//
	public function destroy_Period($id){
		$cat = Period::find($id);
		if($cat->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}
	
	######### periods ############
	function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function payment_course($id){
       // $userid = Auth::guard('doctor')->user()->StudentId;
        $order=DB::table('orders')->where('OrderId',$id)->first();
        if(!empty($order)) {
            $course = DB::table('orders')->leftjoin('course', 'course.id', 'orders.CourseId')->where('OrderId', $id)->select('course.*', 'orders.*')->first();

            return view('site.pay_fee_external')->with(['id' => $id, 'course' => $course]);
        }else{
            $order=DB::table('orders')->where('old_no',$id)->first();
            return redirect('/payment_course/'.$order->OrderId);

        }
    }
	public function payment($id,$r){
	            // $userid = Auth::guard('doctor')->user()->StudentId;

         $course = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->where('OrderId',$id)->select('course.*','orders.*')->first();
		 date_default_timezone_set('Asia/Riyadh');
		$now = date('Y-m-d H:i:s');
		$prev_min = date('Y-m-d H:i:s', strtotime('-20 minute'));	
	
		$query = DB::table('course')->where('id',$course->CourseId)->first();
		$reserved_places =DB::table('orders')->where('orders.CourseId',$course->CourseId)->where('orders.IsPaid',1)->count();
		$msg = '';
		if( $query->seats - $reserved_places > 0){
			
			$msg = 'done';
			$cou=DB::table('orders')->join('payment_logs', function($join)
				{
				   $join->on('orders.OrderId','=','payment_logs.orderid');
					$join->on('orders.order_ref','=','payment_logs.order_ref');

				})->where('orders.CourseId',$course->CourseId)->where('orders.order_ref','!=',NULL)->groupby('StudentId')->get();
			// dd($cou);
			if($query->seats - $cou->count() == 0 || $query->seats - $cou->count() < 0){
				$last_registered=DB::table('orders')->join('payment_logs', function($join)
				{
				   $join->on('orders.OrderId','=','payment_logs.orderid');
					$join->on('orders.order_ref','=','payment_logs.order_ref');

				})->where('orders.CourseId',$course->CourseId)->where('orders.order_ref','!=',NULL)->where('orders.IsPaid',0)
					->groupby('StudentId')->whereBetween('payment_logs.created_at', [$prev_min, $now])->get();
				// dd($last_registered);
				if($last_registered->count() > 0){
					$msg = 'عدد الاماكن غير متاحه حاليا .. حاول لاحقا بعد 20 دقيقة';
				}else{
					$cou=DB::table('orders')->where('orders.CourseId',$course->CourseId)->where('orders.IsPaid',1)->count();
					if($query->seats - $cou == 0){
						$msg = 'عدد الاماكن غير متاحة';
					
					}
				}				
			}
		}else{
			$msg = 'عدد الاماكن غير متاحة';	
		}
		
		
		

		// dd($msg);
		if($msg != 'done'){
			return view('site.pay_fee_external')->with(['id'=>$id,'course'=>$course , 'msg'=>$msg]);
		}else{
		    // $course=Order::leftjoin('course','course.id','orders.CourseId')->where('OrderId',$id)->select('course.*','orders.*')->first();
		 //  dd($course);
			 $cart=$this->generateRandomString();
			 session(['cart'=>$course->OrderId]);
			$params = array(
				'ivp_method'  => 'create',
                // 'ivp_store'   => '23827',
                // 'ivp_authkey' => '4m8CB@TWP4X#8QLS',
					'ivp_store'   => '22077',
					'ivp_authkey' => 'GBXg#bs95h^LPLpS',			
				'ivp_cart'    => $course->OrderId,
				'ivp_test'    => '0',
				'ivp_amount'  => @$course->price+@$course->tax,
				'ivp_currency'=> 'SAR',
				'ivp_desc'    => $course->OrderId.'-'.$course->CourseName,
				'return_auth' => url('/courses/getpay/'.$course->OrderId.'/'.$r),
				'return_can'  => url('/mycourses/'.$course->OrderId),
				'return_decl' => url('/courses/getpay/'.$course->OrderId.'/'.$r),
				'ivp_update_url'=>url('/courses/getpay/'.$course->OrderId.'/'.$r)
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
			curl_setopt($ch, CURLOPT_POST, count($params));
			curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
			$results = curl_exec($ch);
			curl_close($ch);
			$results = json_decode($results,true);
			// print('<pre>');
			// print_r($results);
			// return;
			if(isset($results['error'])) {
                if($results['error']['message']=="E56:Duplicate transaction"){
                    $msg="تم محاولة الدفع لهذا الطلب ولم تنجح هل تريد إعادة المحاولة؟";
                    Session::put('error', $msg);

                    return view('site.pay_fail')->with(['msg'=>$msg,'id'=>$id]);

                }else{
                    print_r('ERROR: '.$results['error']['note']);
                }


			}else {
				$ref = trim($results['order']['ref']);
				session(['ref' => $ref]);
				$url = trim($results['order']['url']);

			if (empty($ref) || empty($url)) {
				print_r($results['error']['note']);

			}else {
				session(['ref' => $ref]);
						 date_default_timezone_set('Asia/Riyadh');

				DB::table('orders')->where('OrderId',$course->OrderId)->update(['order_ref'=>$ref]);
				$insert = new Payment_log;
				$insert->orderid = $id;
				$insert->url = $url;
				$insert->cart_id =$course->OrderId;
				$insert->order_ref = $ref;
			   $insert->created_at	 = date('Y-m-d H:i:s');
			   $insert->save();

				//print_r($results2);
				return redirect($url);

				// return $url;
			}
			}		
		}
		
		

	}
	
	function getpay($id,$r){
		$ord = DB::table('orders')->where('OrderId', $id)->first();
		$payment_logs = DB::table('payment_logs')->where('orderid',$id)->get();
		if(!empty($payment_logs) && $payment_logs->count() > 0){
				foreach($payment_logs as $log){

					 $params2 = array(
						'ivp_method'  => 'check',
                // 'ivp_store'   => '23827',
                // 'ivp_authkey' => '4m8CB@TWP4X#8QLS',
					'ivp_store'   => '22077',
					'ivp_authkey' => 'GBXg#bs95h^LPLpS',				
						'order_ref'   => $log->order_ref,
					);
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
					curl_setopt($ch, CURLOPT_POST, count($params2));
					curl_setopt($ch, CURLOPT_POSTFIELDS,$params2);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
					$results2 = curl_exec($ch);
					$results2 = json_decode($results2,true);
					// print('<pre>');
					// print_r($results2);exit;
					$code=$results2['order']['status']['code'];

					curl_close($ch);

				  //  $studentid=Auth::guard('doctor')->user()->StudentId;
					if($code=='3' or $code=='2'){
							if($results2['order']['transaction']['status']=='A' or $results2['order']['transaction']['status']=='H'){
						
							$order_ref=$results2['order']['transaction']['ref'];
							$transactionType=$results2['order']['transaction']['type'];
							$transactionCode=$results2['order']['transaction']['code'];
						   $return_order_ref ='';
						   $order = DB::table('orders')->where('OrderId',$id)->where('order_ref',$results2['order']['ref'])->first();
							if(isset($order->order_ref) && !empty($order->order_ref)){
								$return_order_ref  = $order->order_ref;
							}

						   $payment_logs = DB::table('payment_logs')->where('orderid',$id)->where('order_ref',$results2['order']['ref'])->first();
							if(isset($payment_logs->order_ref) && !empty($payment_logs->order_ref)){
								$return_order_ref  = $payment_logs->order_ref;
							}					


							if($return_order_ref != ''){
								DB::table('orders')
								->where('OrderId',$id)
								->update(
									[
										'order_ref'  => $return_order_ref,
										'IsPaid'        =>1,
										'trans_no'      =>$order_ref,
										'trans_paid'    =>date('Y-m-d H:s'),
										'deleted_at'   =>'',
										'type_insert'=>$r
									]
								);					
							}
							$course = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('nationality','nationality.Id','users.NationalityId')->leftjoin('periods','periods.PeriodId','course.Period')->where('orders.OrderId',$id)->select('orders.*','course.CourseName','course.CourseStartIn','course.CourseRefNo','course.days','course.price','course.tax','periods.fromH','periods.toH','users.FullName','users.IDNumber','users.mobile','users.email',DB::RAW('nationality.Name as nationality'),DB::RAW('schools.Name as schoolName'))->first();

							$price=$course->price+$course->tax;
							$optios = DB::table('site_option')->where('varname','hm_bl4_contactform')->first();
							$data = [
								'FullName'          => $course->FullName ,
								'CourseName'        => $course->CourseName,
								'order_ref'         => $order_ref,
								'price'             => $price,
								'transactionType'   => $transactionType,
								'transactionCode'   => $transactionCode,
								'PaymentDate'       => Carbon::now()->format('h:i A on D M Y')
							];
                                $userid = Auth::guard('doctor')->user()->StudentId;
                                $user=App\User::where('StudentId',$userid)->first();

                                $msg=" تم الدفع بنجاح لدورة  ".$course->CourseName." وتاريخ الدورة  ".$course->CourseStartIn	;
                                $this->send_sms($user->mobile,$msg);
						  try{
							  /*Mail::send('emails.order', $data, function ($message) use ($course,$optios){
								  $message->from('noreply@dallahdrivingschool.sa', 'شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ');
								  $message->to($course->email)->subject("عملية دفع كورس");
								  $message->to($optios->value)->subject("عملية دفع كورس");
							  });*/
$con='<html>
                        <head>
                         <style>
                        .body{
                        direction: rtl;
						background:#F4F4F4;
                        }
                        .head{
                       
                        padding:30px 40px 0;
                        overflow: hidden;
                      
                        }
						.title2{
						
                        padding: 5px;
						color:#666;
							
						}
                        .logo{
                   
                        color:#F5913A;
                        font-weight:bold;
                        font-size:24px;
                        padding:0 40px;
                        }
                        .head img{
                        height: 50px;
                        float: right;
                        }
                        .footer{
                      
                        padding: 12px;
                        overflow: hidden;
                        color:#fff;	
                        }
                        .footer .copyright{
                        float:right;
						color:#666;
                        }
                        .footer .shareicon{
                        float:left;
                        }
                        .desc{
                        color:#333;
                        padding:0 40px;
                        font-weight:bold;
                        }
                        .desc p{
                        color:#ccc;
                        }
                        .title{
                        color:#b2b2b2;
                        width:25%;
                        }
                        .table{
                            font-weight:bold;
                        padding:30px;
						color:#333;
						background:#fff;
						border: 1px solid #ccc;
						border-radius:5px;
						margin:50px;
                        }
                        table {
							
                        width:80%;
                          border-collapse: collapse;
						  margin: 0 auto;
                        }
                        .shareicon{
                        list-style:none;
                        
                        }
                        .shareicon li{
                            float:left;
                        }
                        .shareicon li a{
                            color:#fff;
                        }
                        table, th, td {
                          
                        }
                        table td {
                          padding:5px;
                        }
                        </style>
                        </head>
                        <body>
                       
                        <div class="body">
                        <div class="head"><img src="https://dallahdrivingschool.sa/resources/views/site/images/logo.png"/></div>
                        <div class="logo">تأكيد عملية الدفع</div>
                        <div class="desc">
							مرحباً ,'.$course->FullName
                        .'</div>
                        <div class="table">
                            <table>
								<tr>
                                    <td class="title">رقم العملية:</td>
                                    <td>'.$order_ref.'</td>
                                </tr>
                                <tr>
                                    <td class="title">نوع العملية:</td>
                                    <td>'.$transactionType.'</td>
                                </tr>
                                <tr>
                                    <td class="title">Authorisation Code:</td>
                                    <td>'.$transactionCode.'</td>
                                </tr>
                                <tr>
                                    <td class="title">المبلغ:</td>
                                    <td>'.$price.' ر.س</td>
                                </tr>
                                <tr>
                                    <td class="title">تاريخ العملية</td>
                                    <td>'.Carbon::now()->format('h:i A on D M Y').'</td>
                                </tr>
								<tr>
                                    <td class="title">الكورس</td>
                                    <td>'.$course->CourseName.'</td>
                                </tr>
							
								
                            </table>
                        </div>
                        <div class="footer">
                            
                            <div class="copyright">
                                Copyright &copy; Dalla Driving School 2019 . All Right Reserved
                            </div>
                        </div>
                        </div>
                        </body>
                        </html>';

                              $this->sendemail($course->email,$fromemail='',$con);
						  }catch(\Exception $e) {

							  $msg = "تمت العملية بنجاح برقم " . $order_ref;
							  Session::put('success', $msg);
						  }
							$msg = "تمت العملية بنجاح برقم " . $order_ref;
							Session::put('success', $msg);
							break;
						}else{
							$msg="لم تتم عملية الدفع";
							Session::put('error',$msg);
						}
					}else{
						$msg="لم تتم عملية الدفع لوجود خطأ فى بيانات الدفع";
					}
				
					
					
					
				}
							
				if($r==1) {
					//return view('site.pay_fee_external_test')->with(['msg' => $msg]);
                    return redirect('/mycourses/'.$course->CourseId)->with('msg',$msg);

                }else{
					//return view('site.pay_fee_external_site')->with(['msg' => $msg]);
                    return redirect('/mycourses/'.$course->CourseId)->with('msg',$msg);

				}	
		}
	
	
	
	

	    
	}
    public function send_sms($phone,$msg)
    {

        $phonenew=substr($phone,0,1);
        if($phonenew!='0')
        {
            $phone=$phone;
        }else{
            $phone=substr($phone,1);
        }
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://www.4jawaly.net/api/sendsms.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, 'username=dalla&password=565656&message='.$msg.'&numbers=966'.$phone.'&sender=DDC-DRIVING&unicode=utf8&Rmduplicated=1&return=json');


        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/x-www-form-urlencoded"
        ));

        $response = curl_exec($ch);

        curl_close($ch);

    }
    public function sendsmstest()
    {
        $phone='0591880902';
        $msg='الرجاء الدفع على الرابط الجديد المرسل';
        $msg.='https://female.dallahdrivingschool.sa/payment_course/6296900';
        $phonenew=substr($phone,0,1);
        if($phonenew!='0')
        {
            $phone=$phone;
        }else{
            $phone=substr($phone,1);
        }
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://www.4jawaly.net/api/sendsms.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, 'username=dalla&password=565656&message='.$msg.'&numbers=966'.$phone.'&sender=DDC-DRIVING&unicode=utf8&Rmduplicated=1&return=json');


        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/x-www-form-urlencoded"
        ));

        $response = curl_exec($ch);

        curl_close($ch);

    }

    public function checkPayment2(){
	
						
		// $orders = DB::table('orders')->where('created_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 1 HOUR)'))->where('IsPaid',0)->get();
		date_default_timezone_set('Asia/Riyadh');
		$now = date('Y-m-d H:i:s');
		$prev_hour = date('Y-m-d H:i:s', strtotime('-14 hour'));		
		// $orders = DB::table('orders')->where('order_ref','!=',NULL)->where('update_type',NULL)->orderby('OrderId','desc')->limit('40')->get();
		$date = '2020-08-01 00:00:00';
		$orders = DB::table('orders')->where('order_ref','!=',NULL)
		// ->where('OrderId',25739)
		->where('update_type','!=','2')
		->where('IsPaid',0)
		->where('created_at', '>=',$date)->orderby('OrderId','desc')->limit('20')->get();

// print('<pre>');
// print_r($orders);
// return;
		// print($orders->count().'*****************');
		if(!empty($orders) && $orders->count() > 0){
			foreach($orders as $order){
				$payment_logs = DB::table('payment_logs')->where('orderid',$order->OrderId)->get();
					// dd($order->OrderId);
				if(!empty($payment_logs) && $payment_logs->count() > 0){
						foreach($payment_logs as $log){
							$params = array(
								'ivp_method'  => 'check',

								'ivp_store'   => '22077',
								'ivp_authkey' => 'GBXg#bs95h^LPLpS',					
								'order_ref' => $log->order_ref
							);
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
							curl_setopt($ch, CURLOPT_POST, count($params));
							curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
							$results2 = curl_exec($ch);
							$results2 = json_decode($results2,true);
							if(isset($results2['order']) && !empty($results2['order'])){
								$code=$results2['order']['status']['code'];
								// print($order->OrderId.'///');
								
								$update_type = '0';
								if($order->update_type != '1' && $order->update_type != '2'){
									$update_type = '1';
								}else if($order->update_type == '1'){
									$update_type = '2';	
								}
								DB::table('orders')
									->where('OrderId',$order->OrderId)
									->update(
										[
											'update_type'   =>$update_type,
											'code'=> $code, 
											'run_at'=>date('Y-m-d H:i:s') ,
										]
									);		

									
								if($code == 3){
									$order_ref=$results2['order']['transaction']['ref'];
									DB::table('orders')
										->where('OrderId',$order->OrderId)
										->update(
											[
												'order_ref'   =>$log->order_ref,
												'IsPaid'        =>1,
												'trans_no'      =>$order_ref,
												'trans_paid'    =>date('Y-m-d H:i:s'),
												'deleted_at'   =>''
											]
										);		
									// print($order->order_ref .'-'.$order->OrderId.'-'.$code.'//////////');
				
								}					
							}
						}
				}
	


			}				
		}
		
		

	}


	public function checkPayment(){
	
						
		// $orders = DB::table('orders')->where('created_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 1 HOUR)'))->where('IsPaid',0)->get();
		date_default_timezone_set('Asia/Riyadh');
		$now = date('Y-m-d H:i:s');
		$prev_hour = date('Y-m-d H:i:s', strtotime('-14 hour'));		
		// $orders = DB::table('orders')->where('order_ref','!=',NULL)->where('update_type',NULL)->orderby('OrderId','desc')->limit('40')->get();
		$date = '2020-08-01 00:00:00';
		
		$orders = DB::table('orders')->where('order_ref','!=',NULL)
		// ->where('OrderId',25739)
		->where('update_type','!=','2') ->orWhere(function($query) {
                $query->where('update_type', NULL);
            })
		->where('IsPaid',0)
		->whereDate('created_at', '>',$date)
		->orderby('OrderId','desc')->limit('20')->get();
		
		// print('<pre>');
		// print_r($orders);
		// return;
		// print($orders->count().'*****************');
		if(!empty($orders) && $orders->count() > 0){
			foreach($orders as $order){
				$payment_logs = DB::table('payment_logs')->where('orderid',$order->OrderId)->get();
					// dd($order->OrderId);
				if(!empty($payment_logs) && $payment_logs->count() > 0){
						foreach($payment_logs as $log){
							$params = array(
								'ivp_method'  => 'check',

								'ivp_store'   => '22077',
								'ivp_authkey' => 'GBXg#bs95h^LPLpS',					
								'order_ref' => $log->order_ref
							);
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
							curl_setopt($ch, CURLOPT_POST, count($params));
							curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
							$results2 = curl_exec($ch);
							$results2 = json_decode($results2,true);
							if(isset($results2['order']) && !empty($results2['order'])){
								$code=$results2['order']['status']['code'];
								// print($order->OrderId.'///');
								$update_type = '0';
								if($order->update_type != '1' && $order->update_type != '2'){
									$update_type = '1';
								}else if($order->update_type == '1'){
									$update_type = '2';	
								}
								DB::table('orders')
									->where('OrderId',$order->OrderId)
									->update(
										[
											'update_type'   =>$update_type,
											'code'=> $code, 
											'run_at'=>date('Y-m-d H:i:s') ,
										]
									);		

									
								if($code == 3){
									$order_ref=$results2['order']['transaction']['ref'];
									DB::table('orders')
										->where('OrderId',$order->OrderId)
										->update(
											[
												'order_ref'   =>$log->order_ref,
												'IsPaid'        =>1,
												'trans_no'      =>$order_ref,
												'trans_paid'    =>date('Y-m-d H:i:s'),
												'deleted_at'   =>''
											]
										);		
									// print($order->order_ref .'-'.$order->OrderId.'-'.$code.'//////////');
				
								}					
							}
						}
				}
	


			}				
		}
		
		

	}
	




	function testpay($id){
	    
            $studentid=Auth::guard('doctor')->user()->StudentId;
$order_ref=123456789;
          
                    $course = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('nationality','nationality.Id','users.NationalityId')->leftjoin('periods','periods.PeriodId','course.Period')->where('orders.StudentId',$studentid)->where('orders.OrderId',$id)->select('orders.*','course.CourseName','course.CourseStartIn','course.CourseRefNo','course.days','course.price','course.tax','periods.fromH','periods.toH','users.FullName','users.IDNumber','users.mobile','users.email',DB::RAW('nationality.Name as nationality'),DB::RAW('schools.Name as schoolName'))->first();
                    DB::table('orders')->where('StudentId',$studentid)
                        ->where('OrderId',$id)
                        ->update(
                            [
                                'IsPaid'        =>1,
                                'trans_no'      =>$order_ref,
                                'trans_paid'    =>date('Y-m-d H:s'),
                                'deleted_at'   =>''
                            ]
                        );
                    $price=$course->price+$course->tax;
                    $optios = DB::table('site_option')->where('varname','hm_bl4_contactform')->first();
                  
               
                    $msg = "تمت العملية بنجاح برقم " . $order_ref;
                    Session::put('success', $msg);
                
            
        
	    return redirect('/mycourses/'.$id)->with('msg',$msg);
	    
	}

function updatepay($id){
     $studentid=Auth::guard('doctor')->user()->StudentId;
if(session('code')=='3'){
    
     $order= Order::where('StudentId',$studentid)
            ->where('CourseId',$id)
            ->update(['IsPaid'=>1,
                        'trans_no'=>session('order_ref'),
                        'trans_paid'=>date('Y-m-d H:s')]);
                    
                    
                    
       
    $msg=session('order_ref')."تمت عمليه الدفع بنجاح ورقم العملية" ;

    Session::put('success',$msg);

}else{
    $msg="لم تتم عملية الدفع";
    Session::put('error',$msg);
}

            return redirect('/mycourses/'.$id)->with('msg',$msg);	
}
function result($post_data,$secretKey,$fieldList) {
  $signatureParams = explode(',', $fieldList);
   print_r($post_data);exit;

  $signatureString = $secretKey;
  foreach ($signatureParams as $param) {
    if (array_key_exists($param, $post_data)) {
      $signatureString .= ':' . trim($post_data[$param]);
    } else {
      $signatureString .= ':';
    }
  }
  
  return sha1($signatureString);
}

function s_mail(){
    $price=500;
    $optios = DB::table('site_option')->where('varname','hm_bl4_contactform')->first();
    $data = [
        'CourseName'    =>'اسم الكورس هنا',
        'FullName'      => 'احمد',
        'order_ref'     => 56164112,
        'price'         => $price,
        'PaymentDate'   => Carbon::now()->format('h:i A on D M Y')
    ];

   /* Mail::send('emails.order', $data, function ($message) use ($optios){
        $message->from('noreply@dallahdrivingschool.sa', 'شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ');
       // $message->to($course->email)->subject("عملية دفع كورس");
       // $message->to($optios->value)->subject("عملية دفع كورس");
        $message->to('R.kh.alhassani@hotmail.com')->subject(" عملية دفع كورس");
    });*/
    $con='<html>
                        <head>
                         <style>
                        .body{
                        direction: rtl;
						background:#F4F4F4;
                        }
                        .head{
                       
                        padding:30px 40px 0;
                        overflow: hidden;
                      
                        }
						.title2{
						
                        padding: 5px;
						color:#666;
							
						}
                        .logo{
                   
                        color:#F5913A;
                        font-weight:bold;
                        font-size:24px;
                        padding:0 40px;
                        }
                        .head img{
                        height: 50px;
                        float: right;
                        }
                        .footer{
                      
                        padding: 12px;
                        overflow: hidden;
                        color:#fff;	
                        }
                        .footer .copyright{
                        float:right;
						color:#666;
                        }
                        .footer .shareicon{
                        float:left;
                        }
                        .desc{
                        color:#333;
                        padding:0 40px;
                        font-weight:bold;
                        }
                        .desc p{
                        color:#ccc;
                        }
                        .title{
                        color:#b2b2b2;
                        width:25%;
                        }
                        .table{
                            font-weight:bold;
                        padding:30px;
						color:#333;
						background:#fff;
						border: 1px solid #ccc;
						border-radius:5px;
						margin:50px;
                        }
                        table {
							
                        width:80%;
                          border-collapse: collapse;
						  margin: 0 auto;
                        }
                        .shareicon{
                        list-style:none;
                        
                        }
                        .shareicon li{
                            float:left;
                        }
                        .shareicon li a{
                            color:#fff;
                        }
                        table, th, td {
                          
                        }
                        table td {
                          padding:5px;
                        }
                        </style>
                        </head>
                        <body>
                       
                        <div class="body">
                        <div class="head"><img src="https://dallahdrivingschool.sa/resources/views/site/images/logo.png"/></div>
                        <div class="logo">تأكيد عملية الدفع</div>
                        <div class="desc">
							مرحباً ,'.$course->FullName
        .'</div>
                        <div class="table">
                            <table>
								<tr>
                                    <td class="title">رقم العملية:</td>
                                    <td>'.$order_ref.'</td>
                                </tr>
                                <tr>
                                    <td class="title">نوع العملية:</td>
                                    <td>'.$transactionType.'</td>
                                </tr>
                                <tr>
                                    <td class="title">Authorisation Code:</td>
                                    <td>'.$transactionCode.'</td>
                                </tr>
                                <tr>
                                    <td class="title">المبلغ:</td>
                                    <td>'.$price.' ر.س</td>
                                </tr>
                                <tr>
                                    <td class="title">تاريخ العملية</td>
                                    <td>'.Carbon::now()->format('h:i A on D M Y').'</td>
                                </tr>
								<tr>
                                    <td class="title">الكورس</td>
                                    <td>'.$course->CourseName.'</td>
                                </tr>
							
								
                            </table>
                        </div>
                        <div class="footer">
                            
                            <div class="copyright">
                                Copyright &copy; Dalla Driving School 2019 . All Right Reserved
                            </div>
                        </div>
                        </div>
                        </body>
                        </html>';

    $this->sendemail($course->email,$fromemail='',$con);

			
}

public function test_mail()
{
    $optios = DB::table('site_option')->where('varname','hm_bl4_contactform')->first();
    $data = [
        'CourseName'    =>'اسم الكورس هنا',
        'FullName'      => 'احمد',
        'order_ref'     => 56164112,
        'price'         => 50,
        'transactionType'   => 1,
        'transactionCode'   => 1215,
        'PaymentDate'   => Carbon::now()->format('h:i A on D M Y')
    ];

    Mail::send('emails.order', $data, function ($message) use ($optios){
        $message->from('noreply@dallahdrivingschool.sa', 'شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ');
        // $message->to($course->email)->subject("عملية دفع كورس");
        // $message->to($optios->value)->subject("عملية دفع كورس");
        $message->to('maiosh_mimo@hotmail.com')->subject(" عملية دفع كورس");
    });

    return "Your email has been sent successfully";
}


///Mai start ///
    public function course_start(){
        $query = DB::table('course')->leftjoin('applicationdef','applicationdef.Id','course.applicationdef')->leftjoin('category','category.Id','course.category')->leftjoin('schools','schools.Id','course.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')->select('course.*',DB::RAW('category.Name categoryName'),'periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'));

        $query->whereDate('course.CourseStartIn','>=',Carbon::today()->addDays(+1));
        $query->where('course.type',2);
        $query->where('course.enable',1);
        $query->where('course.waiting_list',0);
        if(!empty(Auth::guard('doctor')->user())) {

            $date=Auth::guard('doctor')->user()->DoB;
            $date_type=Auth::guard('doctor')->user()->date_type;


            if($date_type==0) {
                $date=Auth::guard('doctor')->user()->DoB;
            }else if($date_type==1) {
                $time=explode("-",$date);
                $day=$time['2'];

                $month=$time['1'];
                $year=$time['0'];

                $date=Auth::guard('doctor')->user()->DoB;

            }

            $date1=date('Y/m/d',strtotime($date));
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da_st=date('Ymd',strtotime($date1));

            $date_sta="{$da_st}";
            $d_st =$DateConv->GregorianToHijri($date_sta,$format);
            $dh_start=date('Y/m/d',strtotime($d_st));


            $today = Carbon::today();
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da=date('Ymd',strtotime($today));

            $datev="{$da}";
            $d =$DateConv->GregorianToHijri($datev,$format);
            $dh_today=date('Y/m/d',strtotime($d));

            $startDate = Carbon::parse($dh_start);
            $endDate   = Carbon::parse($dh_today);
            $y = $startDate->diff($endDate)->format("%y");

            if (intval($y) < 18) {
                $query->where('applicationdef', '5');
            }else{
                $query->where('applicationdef', '!=', '5');
            }

            $school=Auth::guard('doctor')->user()->school;
            $query->where('course.SchoolId',$school);

        }

        $courses = $query->get();
        if(App::getLocale() == 'en'){
            return view('site.en.user.course_start',compact('courses'));
        }else{
            return view('site.user.course_start',compact('courses'));
        }

    }

    public function course_start_wait(){
        $query = DB::table('course')->leftjoin('applicationdef','applicationdef.Id','course.applicationdef')->leftjoin('category','category.Id','course.category')->leftjoin('schools','schools.Id','course.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')->select('course.*',DB::RAW('category.Name categoryName'),'periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'));

        $query->whereDate('course.CourseStartIn','>=',Carbon::today()->addDays(+1));
        $query->where('course.type',2);
        $query->where('course.enable',1);
        $query->where('course.waiting_list',1);
        if(!empty(Auth::guard('doctor')->user())) {

            $date=Auth::guard('doctor')->user()->DoB;
            $date_type=Auth::guard('doctor')->user()->date_type;


            if($date_type==0) {
                $date=Auth::guard('doctor')->user()->DoB;
            }else if($date_type==1) {
                $time=explode("-",$date);
                $day=$time['2'];

                $month=$time['1'];
                $year=$time['0'];

                $date=Auth::guard('doctor')->user()->DoB;

            }

            $date1=date('Y/m/d',strtotime($date));
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da_st=date('Ymd',strtotime($date1));

            $date_sta="{$da_st}";
            $d_st =$DateConv->GregorianToHijri($date_sta,$format);
            $dh_start=date('Y/m/d',strtotime($d_st));


            $today = Carbon::today();
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da=date('Ymd',strtotime($today));

            $datev="{$da}";
            $d =$DateConv->GregorianToHijri($datev,$format);
            $dh_today=date('Y/m/d',strtotime($d));

            $startDate = Carbon::parse($dh_start);
            $endDate   = Carbon::parse($dh_today);
            $y = $startDate->diff($endDate)->format("%y");

            if (intval($y) < 18) {
                $query->where('applicationdef', '5');
            }else{
                $query->where('applicationdef', '!=', '5');
            }

            $school=Auth::guard('doctor')->user()->school;
            $query->where('course.SchoolId',$school);

        }

        $courses_q = $query->get();
        $courses=array();
        if(!empty($courses_q)) {
            foreach ($courses_q as $co):
            $countr = DB::table('orders')->where('CourseId', $co->id)->count();
            $no_seats = $co->seats;
            if ($countr >= $co->seats) {

                DB::table('course')->where('id',$co->id)->update(['enable'=>0]);
            }
        $courses=$query->get();
            endforeach;

        }

        if(App::getLocale() == 'en'){
            return view('site.en.user.course_start_wait',compact('courses'));
        }else{
            return view('site.user.course_start_wait',compact('courses'));
        }

    }

    public function videos()
    {
        $studentid=Auth::guard('doctor')->user()->IDNumber;

        $today=date('Y-m-d');
        $orders= \Illuminate\Support\Facades\DB::table('orders')
            ->join('course','orders.CourseId','=','course.id')
            //->join('parent_courses','parent_courses.co_id','=','course.type')
            ->join('videos','videos.course_id','=','course.type')
            ->where('orders.StudentId',$studentid)
            ->where('orders.IsPaid','1')
            ->whereNULL('videos.deleted_at')
            ->where('orders.course_start','<=',$today)
            ->where('orders.course_end','>=',$today)
            ->get();

        if(App::getLocale() == 'en'){
            return view('site.en.user.videos',compact('orders'));
        }else{
            return view('site.user.videos',compact('orders'));
        }
    }


    public function wait($id)
    {
        $add = Course::find($id);
        $add->waiting_list		= 1;
        if($add->save()) {
            Session::put('success', Lang::get('validation.Saved'));
            return redirect()->back();
        }
    }

    public function unwait($id)
    {
        $add = Course::find($id);
        $add->waiting_list		= 0;
        if($add->save()) {
            Session::put('success', Lang::get('validation.Saved'));
            return redirect()->back();
        }
    }

    public function add_no()
    {
        $orders=App\Models\New_order::where('order_no','6484349')->get();
        foreach ($orders as $order) {
            $order_no=random_int(100000, 99999999);

            App\Models\New_order::where('id',$order->id)->update(['order_no'=>$order_no]);
            $user=App\User::where('StudentId',$order->student_id)->first();
            $school_name=DB::table('schools')->where('Id',$user->school)->first();
            $msg="تم الاشتراك فى قائمه الانتظار بشركه دله لتعليم قياده السيارات – ".$school_name->Name." ورقم الاشتراك الجديد ".$order_no	;
            $this->send_sms($user->mobile,$msg);
        }
    }

    public  function course_start_new()
    {
        $student_id= Auth::guard('doctor')->user()->StudentId;
        $add =App\Models\New_order::where('student_id',$student_id)->first();
        if(!empty($add)){
            Session::put('error', 'أنت مشترك من قبل');
            return redirect()->back();
        }else{
            $user=App\User::where('StudentId',$student_id)->first();
            $school_name=DB::table('schools')->where('Id',$user->school)->first();
            $order_no=random_int(100000, 99999999);
            $order_same=App\Models\New_order::where('order_no',$order_no)->first();
            if(empty($order_same)) {
                $msg = "تم الإشتراك فى قائمة الإنتظار بشركة دله لتعليم قيادة السيارات – " . $school_name->Name . " ورقم الاشتراك  " . $order_no." لعضوية رقم ".$user->IDNumber." وسيتم تحديد الموعد وارسال رساله بالموعد عند توفره";
                App\Models\New_order::create(['student_id' => $student_id, 'order_no' => $order_no]);
                $this->send_sms($user->mobile, $msg);
            }else{
                $order_no=random_int(100000, 99999999);
                $msg = "تم الإشتراك فى قائمة الإنتظار بشركة دله لتعليم قيادة السيارات – " . $school_name->Name . " ورقم الاشتراك  " . $order_no." لعضوية رقم ".$user->IDNumber." وسيتم تحديد الموعد وارسال رساله بالموعد عند توفره";
                App\Models\New_order::create(['student_id' => $student_id, 'order_no' => $order_no]);
                $this->send_sms($user->mobile, $msg);
            }

            Session::put('success', 'تم الاشتراك بنجاح');
            return redirect()->back();
        }
    }


    public function orders_new(Request $request){
        DB::enableQueryLog();
        $query = App\Models\New_order::leftjoin('users','users.StudentId','new_orders.student_id')->leftjoin('schools','schools.Id','users.school');

        if($request->OrderId){
            $query->where('new_orders.order_no',$request->OrderId);
        }
        if($request->from){
            $query->whereDate('orders.created_at','>=',$request->from);
        }
        if($request->to){
            $query->whereDate('orders.created_at','<=',$request->to);
        }
        if($request->IDNumber){
            $query->where('users.IDNumber',$request->IDNumber);
        }
        if($request->SchoolId ){
            $query->where('users.school',$request->SchoolId);
        }
        if($request->FullName){
            $query->where('users.FullName','LIKE','%'.$request->FullName.'%');
        }
        if($request->mobile){
            $query->where('users.mobile',$request->mobile);
        }
        if($request->email){
            $query->where('users.email',$request->email);
        }

        $schools = DB::table('schools')->get();
      $items = $query->select('new_orders.*','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender',DB::RAW('schools.Name as SchoolName'))
            ->orderByDesc('new_orders.id')->groupby('new_orders.student_id')->get();

        return view('admin.orders_new',compact('items','schools'));
    }

    public function delete_order($id){
        DB::table('new_orders')->where('id',$id)->delete();
        Session::put('success',Lang::get('validation.deleted'));

        return redirect('/admin/orders_new');

    }

    public function order_report_new(Request $request){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $query = App\Models\New_order::leftjoin('users','users.StudentId','new_orders.student_id')->leftjoin('schools','schools.Id','users.school');

        if($request->from){
            $query->whereDate('new_orders.created_at','>=',$request->from);
        }
        if($request->to){
            $query->whereDate('new_orders.created_at','<=',$request->to);
        }


        $items = $query->select('new_orders.*','users.school','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender',DB::RAW('schools.Name as SchoolName'))
            ->orderByDesc('new_orders.id')->groupby('new_orders.student_id')->get();



        Excel::create('getOnlinenew', function($excel) use($items) {
            $excel->sheet('getOnlinenew', function($sheet) use($items) {

                $sheet->loadView('excel.orders_new', ['items' => $items]);
            });
        })->export('xlsx');
    }


    public function orders_new_course()
    {
        return view('admin.courses.import_orders');
    }

    public function import_orders(Request $request){
        $messages = [
            'file.required' => 'من فضلك اختر الملف'
        ];
        $validation = Validator::make($request->all(),[
            'file'=> 'required',
        ],$messages);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else {
            if ($request->hasFile('file')) {
                $extension = File::extension($request->file->getClientOriginalName());
                if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                    $path = $request->file->getRealPath();
                    $data = Excel::load($path, function ($reader) {
                    })->get();

                    if (!empty($data) && $data->count()) {
                        $insert = [];
                        $stu=[];
                        $seats=[];
                        foreach ($data as $key => $value) {

                            $student = App\Customer::where('IDNumber',$value->idnumber)->first();

                            if (!empty($student)) {
                                $course = DB::table('course')->where('CourseRefNo', $value->courserefno)
                                    ->where('SchoolId', $student->school)->first();

                                $orders=DB::table('orders')->where('StudentId',$student->StudentId)
                                                            ->where('CourseRefNo',$course->CourseRefNo)->first();
                                $orders_course=DB::table('orders')->where('CourseRefNo',$course->CourseRefNo)
                                    ->where('SchoolId', $student->school)->get();
                                if(count($orders_course) < $course->seats) {
                                    if (!empty($course) and empty($orders)) {

                                        $insert[] = [
                                            'OrderId' => $value->orderno,
                                            'StudentId' => $student->StudentId,
                                            'SchoolId' => $student->school,
                                            'CourseRefNo' => $course->CourseRefNo,
                                            'CourseId' => $course->id,
                                            'ApplicationDefId' => $course->applicationdef,
                                            'course_start' => $course->CourseStartIn,
                                            'course_end' => $course->CourseEndIn,
                                            'IsPaid' => 0
                                        ];

                                    } else {
                                        $stu[] = ['Studentid' => $student->StudentId];

                                    }
                                }else{
                                    $seats[]=['course' => $course->CourseRefNo];
                                }

                            }
                        }
                        if(!empty($insert)){
                            DB::table('orders')->insert($insert);
                            /*$countr = DB::table('orders')::where('CourseId', $course->id)->count();
                            $no_seats = $course->seats;
                            if ($countr == $course->seats) {
                                $course->enable = 0;
                                $course->save();
                            }*/
                            Session::put('success',Lang::get('validation.Saved'));
                            return redirect()->back();
                        }elseif(!empty($orders)) {

                            return redirect()->back()->withErrors('فى تكرار فى البيانات ');
                        } elseif(!empty($seats)){

                            return redirect()->back()->withErrors('عدد الاماكن غير متاحة فى الدورة ');
                        }else{
                            return redirect()->back()->withErrors('الملف فارغ');
                        }


                    } else {
                        return redirect()->back()->withErrors('صيغة الملف غير مدعومة');
                    }
                }
            }
        }
    }

    public function exportOrders_new(Request $request){

        $query = App\Models\New_order::leftjoin('users','users.StudentId','new_orders.student_id')->leftjoin('schools','schools.Id','users.school')->leftjoin('nationality','nationality.Id','users.NationalityId');
        $items = $query->select('new_orders.*','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender','users.school',DB::RAW('nationality.Name as nationality'))
            ->orderByDesc('new_orders.id')->groupby('new_orders.student_id')->get();


        $file = Excel::create('getonlinenew', function($excel) use($items) {
            $excel->sheet('getonlinenew', function($sheet) use($items) {

                $sheet->loadView('excel.orders_new_drive', ['items' => $items]);
            });
        })->store('xlsx', storage_path('app/public/excel'), true);

        Storage::disk('googledrive')->put('getonlinenew.xlsx', file_get_contents(storage_path('app/public/excel/getonlinenew.xlsx')));

        /*
        $photo = $request->file('photo');

            $photo_name      = str_random(5).time();
            $photo_ext       = $photo->getClientOriginalExtension();
            $photo_fullname  = $photo_name.'.'.$photo_ext;



        Storage::disk('googledrive')->put($photo_fullname, file_get_contents($photo));
        */
        return "ok";

        //return response()->json($customersr);
    }

    public function allorders_new(Request $request)
    {
        $columns = array(
            0=>'#',
            1 =>'id',
            2 =>'name',
            3=> 'id_no',
            4=> 'mobile',
            5=> 'email',
            6=>'created_at',
            7=>'school',
            8=>'gender',
            9=>'options'
        );

        $totalData = App\Models\New_order::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {

            $posts = App\Models\New_order::leftjoin('users','users.StudentId','new_orders.student_id')->leftjoin('schools','schools.Id','users.school')->select('evaluation_reserves.*','schools.Name','users.FullName','users.IDNumber','users.email','users.mobile','users.DoB','users.gender','users.NationalityId','evaluation_reserves.day','evaluation_periods.fromH','evaluation_periods.toH')
               ->select('new_orders.*','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender',DB::RAW('schools.Name as SchoolName'))
                ->offset($start)
                ->limit($limit)
                ->orderBy('new_orders.id','DESC')
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =   App\Models\New_order::leftjoin('users','users.StudentId','new_orders.student_id')->leftjoin('schools','schools.Id','users.school')->select('evaluation_reserves.*','schools.Name','users.FullName','users.IDNumber','users.email','users.mobile','users.DoB','users.gender','users.NationalityId','evaluation_reserves.day','evaluation_periods.fromH','evaluation_periods.toH')
                ->select('new_orders.*','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender',DB::RAW('schools.Name as SchoolName'))
                ->where('users.StudentId','LIKE',"%{$search}%")
                ->orWhere('FullName', 'LIKE',"%{$search}%")
                ->orWhere('IDNumber', 'LIKE',"%{$search}%")
                ->orWhere('email', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->select('new_orders.*','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender',DB::RAW('schools.Name as SchoolName'))
                ->offset($start)
                ->limit($limit)
                //->orderBy($order,$dir)
                ->get();

            $totalFiltered =  App\Models\New_order::leftjoin('users','users.StudentId','new_orders.student_id')->leftjoin('schools','schools.Id','users.school')->select('evaluation_reserves.*','schools.Name','users.FullName','users.IDNumber','users.email','users.mobile','users.DoB','users.gender','users.NationalityId','evaluation_reserves.day','evaluation_periods.fromH','evaluation_periods.toH')
                ->select('new_orders.*','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender',DB::RAW('schools.Name as SchoolName'))
                ->where('users.StudentId','LIKE',"%{$search}%")
                ->orWhere('FullName', 'LIKE',"%{$search}%")
                ->orWhere('IDNumber', 'LIKE',"%{$search}%")
                ->orWhere('email', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  url('admin/orders/'.$post->id.'/delete_order');

                $nestedData['#']='<input type="checkbox" value="'.$post->id.'" name="orderID[]">';

                $nestedData['id'] = $post->order_no;
                $nestedData['name'] = $post->FullName;
                $nestedData['id_no'] = $post->IDNumber;
                $nestedData['email'] = $post->email;
                $nestedData['mobile'] = $post->mobile;
                $nestedData['created_at'] = date('Y-m-d h:i A',strtotime($post->created_at));

                $nestedData['school'] = $post->SchoolName;
                $nestedData['gender'] = $post->gender;
                $nestedData['options'] = "&emsp;
                                          &emsp;<a data-href='{$show}' title='delete' href='#'  data-toggle='modal' data-target='#confirm-delete' class='btn btn-danger'><i class='entypo-cross'></i>حذف</a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function searchO(Request $request){
        DB::enableQueryLog();
        $query = App\Models\New_order::leftjoin('users','users.StudentId','new_orders.student_id')->leftjoin('schools','schools.Id','users.school');

        if($request->OrderId){
            $query->where('new_orders.order_no',$request->OrderId);
        }
        if($request->from){
            $query->whereDate('orders.created_at','>=',$request->from);
        }
        if($request->to){
            $query->whereDate('orders.created_at','<=',$request->to);
        }
        if($request->IDNumber){
            $query->where('users.IDNumber',$request->IDNumber);
        }
        if($request->SchoolId ){
            $query->where('users.school',$request->SchoolId);
        }
        if($request->FullName){
            $query->where('users.FullName','LIKE','%'.$request->FullName.'%');
        }
        if($request->mobile){
            $query->where('users.mobile',$request->mobile);
        }
        if($request->email){
            $query->where('users.email',$request->email);
        }

        $schools = DB::table('schools')->get();
        $items = $query->select('new_orders.*','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender',DB::RAW('schools.Name as SchoolName'))
            ->orderByDesc('new_orders.id')->groupby('new_orders.student_id')->get();

        return view('admin.orders_new_search',compact('items','schools'));
    }


    public function orders_unpaid_all(Request $request)
    {

        $columns = array(
            0=>'#',
            1 =>'id',
            2 =>'name',
            3=> 'id_no',
            4=> 'mobile',
            5=> 'email',
            6=>'created_at',
            7=>'course_no',
            8=> 'course_name',
            9=>'school',
            10=>'start_date',
            11=>'period',
            12=>'pay_status',
            13=>'ref_no',
            14=>'gender',
            15=>'options'
        );

        $totalData = DB::table('orders')->where('IsPaid',0)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')
            ->where('IsPaid',0);
            $posts=$posts->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))
                ->offset($start)
                ->limit($limit)
                ->orderBy('orders.OrderId','DESC')
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')
                ->where('IsPaid',0)
                ->where('users.StudentId','LIKE',"%{$search}%")
                ->orWhere('FullName', 'LIKE',"%{$search}%")
                ->orWhere('IDNumber', 'LIKE',"%{$search}%")
                ->orWhere('email', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))
                ->offset($start)
                ->limit($limit)
                //->orderBy($order,$dir)
                ->get();

            $totalFiltered =DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')
                ->where('users.StudentId','LIKE',"%{$search}%")
                ->orWhere('FullName', 'LIKE',"%{$search}%")
                ->orWhere('IDNumber', 'LIKE',"%{$search}%")
                ->orWhere('email', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  url('admin/orders/'.$post->OrderId.'/delete');
                $edit =  url('admin/orders/'.$post->OrderId.'/update');
                $nestedData['#']='<input type="checkbox" value="'.$post->StudentId.'" name="StudentId[]">';
                $nestedData['id'] = $post->StudentId;
                $nestedData['name'] = $post->FullName;
                $nestedData['id_no'] = $post->IDNumber;
                $nestedData['email'] = $post->email;
                $nestedData['mobile'] = $post->mobile;
                $nestedData['created_at'] = date('d-m-Y h:i',strtotime($post->created_at));
                $nestedData['course_no'] = $post->CourseRefNo;
                $nestedData['course_name'] = $post->CourseName;
                $nestedData['start_date'] = $post->CourseStartIn;
                $nestedData['period'] = $post->fromH .' : '.$post->toH;

                $nestedData['school'] = $post->SchoolName;
                $nestedData['gender'] = $post->gender;
                $nestedData['ref_no'] = '';
                $nestedData['pay_status'] = 'لا';
                $nestedData['options'] = "&emsp;<a href='{$edit}' title='edit' class='btn btn-default btn-sm btn-icon icon-left'><i class='entypo-pencil'></i>تعديل</a>
                                          &emsp;<a data-href='{$show}' title='delete' href='#'  data-toggle='modal' data-target='#confirm-delete' class='btn btn-danger'><i class='entypo-cross'></i>حذف</a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function search_orders_unpaid(Request $request)
    {
        DB::enableQueryLog();
        $query = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period');

        if($request->OrderId){
            $query->where('orders.OrderId',$request->OrderId);
        }
        if($request->from){
            $query->whereDate('orders.created_at','>=',$request->from);
        }
        if($request->to){
            $query->whereDate('orders.created_at','<=',$request->to);
        }
        if($request->IDNumber){
            $query->where('users.IDNumber',$request->IDNumber);
        }
        if($request->SchoolId ){
            $query->where('orders.SchoolId',$request->SchoolId);
        }
        if($request->FullName){
            $query->where('users.FullName','LIKE','%'.$request->FullName.'%');
        }
        if($request->mobile){
            $query->where('users.mobile',$request->mobile);
        }
        if($request->email){
            $query->where('users.email',$request->email);
        }
        if($request->courseref){
            $query->where('orders.CourseRefNo',$request->courseref);
        }
        if($request->order_ref){
            $query->where('orders.trans_no',$request->order_ref);
        }
        if($request->course_name){
            $query->where('course.CourseName','like','%'.$request->course_name.'%');
        }
        // $query->where('orders.type',1);
        $schools = DB::table('schools')->get();
        //$query->where('orders.deleted_at','!=','0000-00-00 00:00:00');
        //  $query->whereNULL('orders.deleted_at');
        $query->where('IsPaid',0);
        //$query->where('orders.deleted_at','0000-00-00 00:00:00');
        $items = $query->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))
            ->orderByDesc('OrderId')->get();
        //$query = DB::getQueryLog();

        return view('admin.orders_unpaid_search',compact('items','schools'));
    }

    public function orders_paid_all(Request $request)
    {

        $columns = array(
            0=>'#',
            1 =>'id',
            2 =>'name',
            3=> 'id_no',
            4=> 'mobile',
            5=> 'email',
            6=>'created_at',
            7=>'course_no',
            8=> 'course_name',
            9=>'school',
            10=>'start_date',
            11=>'period',
            12=>'pay_status',
            13=>'ref_no',
            14=>'gender',
            15=>'options'
        );

        $totalData = DB::table('orders')->where('IsPaid',1)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')
                ->where('IsPaid',1);
            $posts=$posts->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))
                ->offset($start)
                ->limit($limit)
                ->orderBy('orders.OrderId','DESC')
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')
                ->where('IsPaid',1)
                ->where('users.StudentId','LIKE',"%{$search}%")
                ->orWhere('FullName', 'LIKE',"%{$search}%")
                ->orWhere('IDNumber', 'LIKE',"%{$search}%")
                ->orWhere('email', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))
                ->offset($start)
                ->limit($limit)
                //->orderBy($order,$dir)
                ->get();

            $totalFiltered =DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')
                ->where('users.StudentId','LIKE',"%{$search}%")
                ->orWhere('FullName', 'LIKE',"%{$search}%")
                ->orWhere('IDNumber', 'LIKE',"%{$search}%")
                ->orWhere('email', 'LIKE',"%{$search}%")
                ->orWhere('mobile', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  url('admin/orders/'.$post->OrderId.'/delete');
                $edit =  url('admin/orders/'.$post->OrderId.'/update');
                $nestedData['#']='<input type="checkbox" value="'.$post->StudentId.'" name="StudentId[]">';
                $nestedData['id'] = $post->StudentId;
                $nestedData['name'] = $post->FullName;
                $nestedData['id_no'] = $post->IDNumber;
                $nestedData['email'] = $post->email;
                $nestedData['mobile'] = $post->mobile;
                $nestedData['created_at'] = date('d-m-Y h:i',strtotime($post->created_at));
                $nestedData['course_no'] = $post->CourseRefNo;
                $nestedData['course_name'] = $post->CourseName;
                $nestedData['start_date'] = $post->CourseStartIn;
                $nestedData['period'] = $post->fromH .' : '.$post->toH;

                $nestedData['school'] = $post->SchoolName;
                $nestedData['gender'] = $post->gender;
                $nestedData['ref_no'] = $post->trans_no;
                $nestedData['pay_status'] = 'نعم';
                $nestedData['options'] = "&emsp;<a href='{$edit}' title='edit' class='btn btn-default btn-sm btn-icon icon-left'><i class='entypo-pencil'></i>تعديل</a>
                                          &emsp;<a data-href='{$show}' title='delete' href='#'  data-toggle='modal' data-target='#confirm-delete' class='btn btn-danger'><i class='entypo-cross'></i>حذف</a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function search_orders_paid(Request $request)
    {
        DB::enableQueryLog();
        $query = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period');

        if($request->OrderId){
            $query->where('orders.OrderId',$request->OrderId);
        }
        if($request->from){
            $query->whereDate('orders.created_at','>=',$request->from);
        }
        if($request->to){
            $query->whereDate('orders.created_at','<=',$request->to);
        }
        if($request->IDNumber){
            $query->where('users.IDNumber',$request->IDNumber);
        }
        if($request->SchoolId ){
            $query->where('orders.SchoolId',$request->SchoolId);
        }
        if($request->FullName){
            $query->where('users.FullName','LIKE','%'.$request->FullName.'%');
        }
        if($request->mobile){
            $query->where('users.mobile',$request->mobile);
        }
        if($request->email){
            $query->where('users.email',$request->email);
        }
        if($request->courseref){
            $query->where('orders.CourseRefNo',$request->courseref);
        }
        if($request->order_ref){
            $query->where('orders.trans_no',$request->order_ref);
        }
        if($request->course_name){
            $query->where('course.CourseName','like','%'.$request->course_name.'%');
        }
        // $query->where('orders.type',1);
        $schools = DB::table('schools')->get();
        //$query->where('orders.deleted_at','!=','0000-00-00 00:00:00');
        //  $query->whereNULL('orders.deleted_at');
        $query->where('IsPaid',1);
        //$query->where('orders.deleted_at','0000-00-00 00:00:00');
        $items = $query->select('orders.*','course.CourseName','course.CourseStartIn','course.days','users.NationalityId','users.DoB','users.mobile','users.email','users.IDNumber','users.FullName','users.gender','periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'))
            ->orderByDesc('OrderId')->groupby('StudentId','CourseId')->get();
        //$query = DB::getQueryLog();
        return view('admin.orders_paid_search',compact('items','schools'));
    }




    public function orders_school()
    {
        return view('admin.courses.import_orders_school');
    }

    public function import_orders_school(Request $request){
        $messages = [
            'file.required' => 'من فضلك اختر الملف'
        ];
        $validation = Validator::make($request->all(),[
            'file'=> 'required',
        ],$messages);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else {
            if ($request->hasFile('file')) {
                $extension = File::extension($request->file->getClientOriginalName());
                if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                    $path = $request->file->getRealPath();
                    $data = Excel::load($path, function ($reader) {
                    })->get();

                    if (!empty($data) && $data->count()) {
                        $insert = [];
                        $stu=[];
                        $seats=[];
                        $newstu=[];
                        foreach ($data as $key => $value) {

                            $student = App\Customer::where('IDNumber',$value->idnumber)->first();

                            if (!empty($student)) {
                                $course = DB::table('course')->where('CourseRefNo', $value->courserefno)
                                    ->where('SchoolId', $student->school)->first();

                                $orders=DB::table('orders')->where('StudentId',$student->StudentId)
                                    ->where('CourseRefNo',$course->CourseRefNo)->first();
                                $orders_course=DB::table('orders')->where('CourseRefNo',$course->CourseRefNo)
                                    ->where('SchoolId', $student->school)->get();
                                if(count($orders_course) < $course->seats) {
                                    if (!empty($course) and empty($orders)) {

                                        $insert[] = [
                                            'OrderId' => $value->orderno,
                                            'StudentId' => $student->StudentId,
                                            'SchoolId' => $student->school,
                                            'CourseRefNo' => $course->CourseRefNo,
                                            'CourseId' => $course->id,
                                            'ApplicationDefId' => $course->applicationdef,
                                            'course_start' => $course->CourseStartIn,
                                            'course_end' => $course->CourseEndIn,
                                            'IsPaid' => 1
                                        ];

                                    } else {
                                        $stu[] = ['Studentid' => $student->StudentId];

                                    }
                                }else{
                                    $seats[]=['course' => $course->CourseRefNo];
                                }

                            }else {

                                if ($value->fullname !=null) {
                                    $school = DB::table('schools')->where('Name', $value->school)->first();

                                    $course = DB::table('course')->where('CourseRefNo', $value->courserefno)
                                        ->where('SchoolId', $school->Id)->first();

                                    $nat = DB::table('nationality')->where('Name', 'like', '%' . $value->nationality . '%')->first();
                                    $pass = $this->randomPassword();
                                    $nwst = DB::table('users')->insert([
                                        'FullName' => $value->fullname,
                                        'IDNumber' => $value->idnumber,
                                        'gender' => $value->gender,
                                        'DoB' => $value->dob,
                                        'mobile' => $value->mobile,
                                        'school' => $school->Id,
                                        'NationalityId' => $nat->Id,
                                        'password' => Hash::make($pass),

                                    ]);
                                    $msg = "برجاء استخدام رقم الهوية كأسم المستخدم وكلمة المرور الخاصة بك هى " . $pass;
                                     $this->send_sms($value->mobile,$msg);
                                    $stt = DB::table('users')->where('IDNumber', $value->idnumber)->first();
                                    $insert[] = [

                                        'StudentId' => $stt->StudentId,
                                        'SchoolId' => $school->Id,
                                        'CourseRefNo' => $value->courserefno,
                                        'CourseId' => $course->id,
                                        'ApplicationDefId' => $course->applicationdef,
                                        'course_start' => $course->CourseStartIn,
                                        'course_end' => $course->CourseEndIn,
                                        'trans_no' => $value->payrefno,
                                        'IsPaid' => 1
                                    ];


                                }
                            }
                        }
                        if(!empty($insert)){
                            DB::table('orders')->insert($insert);
                            /*$countr = DB::table('orders')::where('CourseId', $course->id)->count();
                            $no_seats = $course->seats;
                            if ($countr == $course->seats) {
                                $course->enable = 0;
                                $course->save();
                            }*/
                            Session::put('success',Lang::get('validation.Saved'));
                            return redirect()->back();
                        }elseif(!empty($orders)) {

                            return redirect()->back()->withErrors('فى تكرار فى البيانات ');
                        } elseif(!empty($seats)){

                            return redirect()->back()->withErrors('عدد الاماكن غير متاحة فى الدورة ');
                        }else{
                            return redirect()->back()->withErrors('الملف فارغ');
                        }


                    } else {
                        return redirect()->back()->withErrors('صيغة الملف غير مدعومة');
                    }
                }
            }
        }
    }

    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }


    public function order_report_new_1(Request $request,$i=1){


        $query = App\Models\New_order::leftjoin('users','users.StudentId','new_orders.student_id')->leftjoin('schools','schools.Id','users.school');

        if($request->from){
            $query->whereDate('new_orders.created_at','>=',$request->from);
        }
        if($request->to){
            $query->whereDate('new_orders.created_at','<=',$request->to);
        }


        $items = $query->select('new_orders.order_no','users.FullName','users.IDNumber','users.mobile','users.email','users.DoB','users.NationalityId','users.school','new_orders.created_at','users.gender',DB::RAW('schools.Name as SchoolName'))
            ->orderByDesc('new_orders.id')->groupby('new_orders.student_id')->get()->toArray();
         $count=Count($items);

        $halved = array_chunk($items, (int)round(count($items)/15));

        $import = Excel::create('getOnlinenew', function ($excel) use ($halved) {
            $excel->sheet('getOnlinenew', function ($sheet) use ($halved) {

                $sheet->loadView('excel.orders_new_test', ['items' => $halved[0]]);
            });
        })->store('xlsx', storage_path('app/public/excel'), true);
      // $i=1;
        $this->allValues2=$items;

        Session::forget('orders');
        return $this->putdata(1);

        //$import->export('xlsx');
    }


    public function putdata($i)
    {
        if(Session('orders')){
            $it=Session('orders');
        }else {
            $items = $this->allValues2;
            $count = Count($items);
            Session(['orders' => $items]);
            $it = Session('orders');
        }
//dd($it);
        $halved = array_chunk($it, (int)round(count($it)/15));
       if($i<= count($halved)-1) {
           while ($i <= count($halved) - 1) {

               $path = storage_path('app/public/excel/getOnlinenew.xlsx');

               foreach ($halved[$i] as $item):

                   $down = Excel::load($path, function ($doc) use ($item) {

                       $sheet = $doc->setActiveSheetIndex(0);
                       $last = $sheet->getHighestRow();
                       $lastt = $last + 1;

                       $sheet->setCellValue('A' . $lastt, $item['order_no']);
                       $sheet->setCellValue('B' . $lastt, $item['FullName']);
                       $sheet->setCellValue('C' . $lastt, $item['IDNumber']);
                       $sheet->setCellValue('D' . $lastt, $item['mobile']);
                       $sheet->setCellValue('E' . $lastt, $item['email']);
                       $sheet->setCellValue('F' . $lastt, $item['DoB']);
                       $sheet->setCellValue('G' . $lastt, 1);
                       $sheet->setCellValue('H' . $lastt, $item['NationalityId']);
                       $sheet->setCellValue('I' . $lastt, $item['school']);
                       $sheet->setCellValue('J' . $lastt, '');
                       $sheet->setCellValue('K' . $lastt, $item['created_at']);
                       $sheet->setCellValue('L' . $lastt, '');
                       $sheet->setCellValue('M' . $lastt, '');
                       $sheet->setCellValue('N' . $lastt, '');
                       $sheet->setCellValue('O' . $lastt, 1);
                   })->store('xlsx', storage_path('app/public/excel'), true);

               endforeach;

               // header("Refresh:0");
               $i++;
               return redirect('/admin/orders/putdata/' . $i);


           }
       }else{
           $path = storage_path('app/public/excel/getOnlinenew.xlsx');

           Excel::load($path, function ($doc) {

           })->download('xlsx');

    }

    }

    public function retry_pay($id)
    {
        $order_no=random_int(100000, 99999999);
        $ordercheck=DB::table('orders')->where('OrderId',$order_no)->first();
        if(empty($ordercheck))
        {
            DB::table('orders')->where('OrderId',$id)
                               ->update(['OrderId'=>$order_no,
                                         'old_no'=>$id]);
            return redirect('/payment_course/'.$order_no);
        }else{
            return redirect('/retry_pay/'.$id);
        }

    }


    public function sendemail($toemail,$fromemail,$con)
    {
        $fromemail='info@dallahdrivingschool.sa';
        $config = '{ 
                     "sender":{  
                              "name":"Dallah driving ",
                              "email":"'.$fromemail.'"
                           },
                           "to":[  
                              {  
                                 "email":"'.$toemail.'",
                                 "name":"Reciver Name"
                              }
                           ],
                           "subject":"Dallah driving",
                           "htmlContent":'.json_encode($con).'
                     }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.sendinblue.com/v3/smtp/email',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$config,
            CURLOPT_HTTPHEADER => array(
                'api-key:'.Config::get('app.sendblue_apikey'),
                'Accept: application/json',
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);

        curl_close($curl);


    }
//End Mai
}