<?php

namespace App\Http\Controllers;

use App\Application;
use App\Category;
use App\Models\Evaluation_time;
use Carbon\Carbon;
use Validator;
use App\Pages;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use App;
use Auth;
use Excel;
use Storage;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    ######### Evaluations time ############

    public function get_newPeriod(){
        $courses=DB::table('parent_courses')->get();
        return view('admin.videos.new',compact('courses'));
    }
    public function new(Request $request)
    {
        $messages = [
            'file.required' => 'من فضلك اختر الملف'
        ];
        $validation = Validator::make($request->all(),[
            'file'=> 'required',
        ],$messages);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            if($request->hasFile('file')){
                $extension = File::extension($request->file->getClientOriginalName());
                if ($extension == "mov" || $extension == "mp4") {
                    $photo = $request->file('file');
                    if(!empty($photo)){
                        $photo_name = str_random(5).time();
                        $photo_ext = $photo->getClientOriginalExtension();
                        $photo_fullname = $photo_name.'.'.$photo_ext;
                        $path = public_path().'/videos/';
                        $upload = $photo->move($path,$photo_fullname);
                    }

                    App\Models\Video::create(['course_id'=>$request->course_id,
                                              'link_video'=>$photo_fullname]);
                    Session::put('success',Lang::get('validation.Saved'));
                   return redirect()->back();

                }else {
                    return redirect()->back()->withErrors('صيغة الملف غير مدعومة');
                }
            }
        }
    }


    public function get_updatePeriod($id){
        $item = App\Models\Video::find($id);
        $courses=DB::table('parent_courses')->get();
        return view('admin.videos.update')->with(['item'=>$item,'courses'=>$courses]);
    }
    public function updatePeriod($id,Request $request)
    {

            if($request->hasFile('file')){
                $extension = File::extension($request->file->getClientOriginalName());
                if ($extension == "mov" || $extension == "mp4") {
                    $photo = $request->file('file');
                    if(!empty($photo)){
                        $photo_name = str_random(5).time();
                        $photo_ext = $photo->getClientOriginalExtension();
                        $photo_fullname = $photo_name.'.'.$photo_ext;
                        $path = public_path().'/videos/';
                        $upload = $photo->move($path,$photo_fullname);
                    }

                    App\Models\Video::where('id',$request->id)->update(['course_id'=>$request->course_id,
                        'link_video'=>$photo_fullname]);
                    Session::put('success',Lang::get('validation.Saved'));
                    return redirect()->back();

                }else {
                    return redirect()->back()->withErrors('صيغة الملف غير مدعومة');
                }
            }else{
                App\Models\Video::where('id',$request->id)->update(['course_id'=>$request->course_id,
                    ]);
                Session::put('success',Lang::get('validation.Saved'));
                return redirect()->back();

            }




    }

    public function videos(){
        $items = App\Models\Video::leftjoin('parent_courses','parent_courses.id','=','videos.course_id')
                                 ->select('videos.*','parent_courses.name')->paginate(100);
        return view('admin.videos.items')->with('items',$items);
    }

    //*-------------------delete -------------------*//
    public function destroy_Period($id){
        $cat = App\Models\Video::find($id);
        if($cat->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect()->back();
        }
    }

    public function active($id){
        $add = Evaluation_time::find($id);
        $add->id	= $id;
        $add->status	= 1;
        if($add->save()){
            Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();
        }
    }
    public function inactive($id){
        $add = Evaluation_time::find($id);
        $add->id	= $id;
        $add->status	= 0;
        if($add->save()){
            Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();
        }
    }


    public function orders()
    {
        $items=App\Models\Evaluation_reserve::leftjoin('evaluation_times','evaluation_reserves.time_id','=','evaluation_times.id')
                                              ->leftJoin('schools','schools.id','=','evaluation_times.school_id')
                                              ->leftjoin('users','users.StudentId','=','evaluation_reserves.student_id')
            ->select('evaluation_reserves.*','schools.Name','users.FullName','users.IDNumber','evaluation_times.day','evaluation_times.from_time','evaluation_times.to_time')->get();
        return view('admin.evaluation_time.reserves')->with('items',$items);

    }


    public function attend($id){
        $add = App\Models\Evaluation_reserve::find($id);
        $add->id	= $id;
        $add->attend	= 1;
        if($add->save()){
            Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();
        }
    }
    public function inattend($id){
        $add = App\Models\Evaluation_reserve::find($id);
        $add->id	= $id;
        $add->attend	= 0;
        if($add->save()){
            Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();
        }
    }
    public function delete_reserve($id){
        $cat = App\Models\Evaluation_reserve::find($id);
        if($cat->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect()->back();
        }
    }

    public function export_excel()
    {
        $items=App\Models\Evaluation_reserve::leftjoin('evaluation_times','evaluation_reserves.time_id','=','evaluation_times.id')
            ->leftJoin('schools','schools.id','=','evaluation_times.school_id')
            ->leftjoin('users','users.StudentId','=','evaluation_reserves.student_id')
            ->select('evaluation_reserves.*','schools.Name','users.FullName','users.IDNumber','evaluation_times.day','evaluation_times.from_time','evaluation_times.to_time')->get();

        $file = Excel::create('getOnlinestudent', function($excel) use($items) {
            $excel->sheet('getOnlinestudent', function($sheet) use($items) {

                $sheet->loadView('excel.evaluation', ['items' => $items]);
            });
        })->export('xlsx');

    }



    public function result(){
        return view('admin.evaluation_time.result');
    }
    public function import_result(Request $request)
    {
        $messages = [
            'file.required' => 'من فضلك اختر الملف'
        ];
        $validation = Validator::make($request->all(),[
            'file'=> 'required',
        ],$messages);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            if($request->hasFile('file')){
                $extension = File::extension($request->file->getClientOriginalName());
                if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                    $path = $request->file->getRealPath();
                    $data = Excel::load($path, function($reader) {
                    })->get();
                    if(!empty($data) && $data->count()){
                        $insert  = [];
                        foreach ($data as $key => $value) {
                            $user=App\User::where('IDNumber',$value->idnumber)->first();
                            $course=DB::table('course')->where('SchoolId',$value->schoolid)
                                                       ->where('CourseRefNo',$value->courserefno)->first();
                            if($value->ispaid=='1'){
                                $del_date="0000-00-00 00:00:00";
                            }else{
                                $del_date=date('Y-m-d H:i:s');
                            }
                            if(!empty($user)){
                                $us=$user->studentid;
                            }else{
                                $us='';
                            }
                            DB::table('orders')->insert(['SchoolId'=>$value->schoolid,
                                                         'StudentId'=>$us,
                                                          'ApplicationDefId'=>$value->applicationdefid,
                                                          'CourseRefNo'=>$value->courserefno,
                                                           'IsPaid'=>$value->ispaid,
                                                           'CourseId'=>$course->id,
                                                            'assign_grade'=>$value->assigngrade,
                                                            'deleted_at'=>$del_date]);

                        }


                        Session::put('success',Lang::get('validation.Saved'));
                        return redirect()->back();
                    }
                }else {
                    return redirect()->back()->withErrors('صيغة الملف غير مدعومة');
                }
            }
        }
    }
    ######### Evaluation times ############

////////////////front end ////////////

    public function evaluation(){
        $schools=DB::table('schools')->get();
        $userid = Auth::guard('doctor')->user()->StudentId;
         $user=App\User::where('StudentId',$userid)->first();
        $times=Evaluation_time::where('status',1)
                        ->where('day','>=',date('Y-m-d'))
                       ->where('slots_avaliable_'.$user->gender,'>','0')->get();
        if(App::getLocale() == 'en'){
            return view('site.en.user.evaluation',compact('times','schools'));
        }else{
            return view('site.user.evaluation',compact('times','schools'));
        }
    }

    public function get_time(Request  $request)
    {
        $id=$request->id;
        $userid = Auth::guard('doctor')->user()->StudentId;
        $user=App\User::where('StudentId',$userid)->first();
        $data=Evaluation_time::leftjoin('schools','schools.Id','=','evaluation_times.school_id')
            ->where('status',1)
            ->where('school_id',$id)
            ->where('day','>=',date('Y-m-d'))
            ->where('slots_avaliable_'.$user->gender,'>','0')->select('evaluation_times.id','schools.Name','from_time','to_time','day')->get();
        $sen['success'] = true;
        $sen['result'] = $data->toArray();
        return Response::json( $sen );
    }

    public function reserve($id)
    {
        $userid = Auth::guard('doctor')->user()->StudentId;
        $user=App\User::where('StudentId',$userid)->first();
        $reserve=App\Models\Evaluation_reserve::leftjoin('evaluation_times','evaluation_times.id','=','evaluation_reserves.time_id')
                                             ->where('student_id',$userid)
                                             ->where('attend',0)->select('evaluation_reserves.*','evaluation_times.day')->first();
        $time=Evaluation_time::where('id',$id)->first();
        if(empty($reserve))
        {
            App\Models\Evaluation_reserve::create(['time_id' => $id,
                'student_id' => $userid]);
            if($user->gender=='male') {
                $ti = $time->slots_avaliable_male;
                Evaluation_time::where('id', $id)->update(['slots_avaliable_male' => $ti - 1]);

            }else{
                Evaluation_time::where('id', $id)->update(['slots_avaliable_female' => $ti - 1]);

                $ti = $time->slots_avaliable_female;
            }


            Session::put('success', "تم الحجز بنجاح");
            return redirect()->back()->with('msg', 'تم الحجز بنجاح');
        } else if( $time->day > $reserve->day ) {
            App\Models\Evaluation_reserve::where('id',$reserve->id)->delete();

            App\Models\Evaluation_reserve::create(['time_id' => $id,
                'student_id' => $userid]);
            Evaluation_time::where('id', $id)->update(['slots_avliable' => $time->slots_avliable - 1]);

            Session::put('success', "تم الحجز بنجاح");
            return redirect()->back()->with('msg', 'تم الحجز بنجاح');
        }else{
            Session::put('error', "انت مسجل فى اختبار اخر");
            return redirect()->back()->with('msg', 'انت مسجل فى اختبار اخر');
        }

    }


}
