<?php

namespace App\Http\Controllers;

use App\Application;
use App\Categories;
use App\Category;
use App\Models\Evaluation_time;
use App\Photos;
use Carbon\Carbon;
use Validator;
use App\Pages;
use App\Customer;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use App;
use Auth;
use Excel;
use Storage;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class SchoolController extends Controller
{
    ######### Evaluations time ############


    public function getschools(){
        $items = DB::table('schools')->get();

        return view('admin.schools.items')->with('items',$items);
    }


    public function active($id){
        $add = DB::table('schools')->where('Id',$id)->update(['active'=>0]);

        Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();

    }
    public function inactive($id){
        $add = DB::table('schools')->where('Id',$id)->update(['active'=>1]);

            Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();

    }


    public function report()
    {
        $items=DB::table('schools')->join('users','users.school','=','schools.Id')
                                  ->select('schools.*',DB::raw('count(*) as total'))->groupby('school')->get();


        return view('admin.schools.report')->with('items',$items);

    }

    public function exportreport()
    {
        $items=DB::table('schools')->join('users','users.school','=','schools.Id')
            ->select('schools.*',DB::raw('count(*) as total'))->groupby('school')->get();


        Excel::create('schoolreport', function($excel) use($items) {
            $excel->sheet('schoolreport', function($sheet) use($items) {

                $sheet->loadView('excel.schoolreport', ['items' => $items]);
            });
        })->export('xlsx');

    }

    public function exportreport_drive()
    {
        $items=DB::table('schools')->join('users','users.school','=','schools.Id')
            ->select('schools.*',DB::raw('count(*) as total'))->groupby('school')->get();


        Excel::create('schoolreport', function($excel) use($items) {
            $excel->sheet('schoolreport', function($sheet) use($items) {

                $sheet->loadView('excel.schoolreport', ['items' => $items]);
            });
        })->store('xlsx', storage_path('app/public/excel'), true);

        Storage::disk('googledrive')->put('schoolreport.xlsx', file_get_contents(storage_path('app/public/excel/schoolreport.xlsx')));


    }

    public function view_update($id){
        $item = DB::table('schools')->where('Id',$id)->first();

        return view('admin.schools.edit')->with('item',$item);
    }
    public function update($id,Request $request)
    {
        $validation = Validator::make($request->all(),[
            'Name' 	=> 'required',

        ]);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            $update = DB::table('schools')->where('Id',$id)
                ->update(['Name'=>$request->input('Name')]);

            if($update){
                Session::put('success',Lang::get('validation.Saved'));
                return redirect()->back();
            }else{
                return redirect()->back()->withErrors(Lang::get('validation.error'));
            }
        }
    }

}
