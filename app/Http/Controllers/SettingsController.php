<?php

namespace App\Http\Controllers;

use Validator;
use App\settings;
use Auth;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{
	private function permations($name){
		$res = AdminController::permations($name);
		return $res;
	}
    // settings
	public function settings(){
		
		$set = settings::find(1);
		$data = array(
			'fBufferT' 	=> $set->fBufferT,
			'sBufferT' 	=> $set->sBufferT,
			'requestCost' => array(
				'minSpace' 	=> $set->minSpace,
				'PriceHr'  	=> $set->PriceHr,
				'time'		=> ['from' => (int)$set->timeFrom,'to' => (int)$set->timeTo],
				'average'  	=> $set->average
			),
			'contact' => array(
				'site' 	=> 'http://kazzeem.com/',
				'phone'  	=> $set->phone,
				'email'		=> 'info@kazzem.com',
				'address'  	=> ['name' => 'Dokki, Evaluationtime Doqi, Giza Governorate','lat' => 30.0412944,'lng' => 31.1855309]
			),
			'apps_versions'		=> array(
				'agent' => ['android' => ['current_version' => $set->ag_andro_curr,'minimum_version' => $set->ag_andro_min],'ios' => ['current_version' => 0,'minimum_version' => 0]],
				'customer' => ['android' => ['current_version' => $set->cus_andro_curr,'minimum_version' => $set->cus_andro_min],'ios' => ['current_version' => $set->cus_ios_curr,'minimum_version' => $set->cus_ios_min]]
			),
			'socket' => ['ip' => $set->socket_ip,'port' => (int)$set->socket_port]
		);
		
		$res = array(
					'status' => 'done',
					'code' 	=> 201,
					'data' => $data
				);
		
		return response()->json($res);
	}
	
	static function gsettings(){
		$settings = settings::find(1);
		return $settings;
	}
	
	public function get_settings(){
		
		dd(41);
		if(!$this->permations('settings')){
			return view('admin.permations');
		}
		
		$settings = settings::find(1);
		return view('admin.settings')->with('settings',$settings);
	}
	
	public function update_settings(Request $request){
		if(!$this->permations('settings')){
			return view('admin.permations');
		}
		$update = settings::find(1);
		$update->legalDocExpir		= $request->input('legalDocExpir');
		$update->companyIncentive 	= $request->input('companyIncentive');
		$update->fBufferT 			= $request->input('fBufferT');
		$update->sBufferT 			= $request->input('sBufferT');
		$update->deductionT 		= $request->input('deductionT');
		$update->deductionAmount	= $request->input('deductionAmount');
		$update->minSpace 			= $request->input('minSpace');
		$update->PriceHr 			= $request->input('PriceHr');
		$update->timeFrom 			= $request->input('timeFrom');
		$update->timeTo 			= $request->input('timeTo');
		$update->average 			= $request->input('average');
		$update->ag_andro_curr 			= $request->input('ag_andro_curr');
		$update->ag_andro_min 			= $request->input('ag_andro_min');
		$update->cus_andro_curr 			= $request->input('cus_andro_curr');
		$update->cus_andro_min 			= $request->input('cus_andro_min');
		$update->cus_ios_curr 			= $request->input('cus_ios_curr');
		$update->cus_ios_min 			= $request->input('cus_ios_min');

        if($update->save()){
                 Session::put('success',Lang::get('validation.Saved'));
            return redirect()->back();
        }else{
            return redirect()->back()->withErrors(Lang::get('validation.error'));
        }
	}

}
