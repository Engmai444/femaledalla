<?php

namespace App\Http\Controllers;

use Validator;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use App\Slider;
use App\Photos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class SliderController extends Controller
{
	private function permations($name){
		$res = AdminController::permations($name);
		return $res;
	}
	
    ########## slider ###############
	public function get_newpage(){
		if(!$this->permations('slider')){
			return view('admin.permations');
		}
		return view('admin.slider_new');
	}
	public function newpage(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'photo' => 'required|mimes:jpeg,jpg,png|max:10000',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = new Slider;
			
		    $add->title			= $request->input('title');
		    $add->content 		= $request->input('content');
			$add->title_ar		= $request->input('title_ar');
			$add->content_ar 	= $request->input('content_ar');
		
			$photo = $request->file('photo');
			if(!empty($photo)){
				$photo_name      = str_random(5).time();
				$photo_ext       = $photo->getClientOriginalExtension();
				$photo_fullname  = $photo_name.'.'.$photo_ext;
				$path            = public_path().'/photos/';
				$thumb           = Image::make($photo)->resize(1500,651)->save($path.$photo_fullname);
				$add->photo      = $photo_fullname;
			}
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	
	public function get_updatepage($id){
		if(!$this->permations('slider')){
			return view('admin.permations');
		}
		$page = Slider::find($id);
		return view('admin.slider_update')->with('page',$page);
	}
	public function updatepage($id,Request $request)
    {
		$validation = Validator::make($request->all(),[
			'photo' => 'mimes:jpeg,jpg,png|max:10000',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = Slider::find($id);
			
		    $add->title		= $request->input('title');
		    $add->content 	= $request->input('content');
			$add->title_ar	= $request->input('title_ar');
			$add->content_ar= $request->input('content_ar');
			
			$photo = $request->file('photo');
			if(!empty($photo)){
				$photo_name      = str_random(5).time();
				$photo_ext       = $photo->getClientOriginalExtension();
				$photo_fullname  = $photo_name.'.'.$photo_ext;
				$path            = public_path().'/photos/';
				$thumb           = Image::make($photo)->resize(1500,651)->save($path.$photo_fullname);
				$add->photo      = $photo_fullname;
			}
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	//*-------------------delete -------------------*//
	public function destroy_page($id){
		if(!$this->permations('slider')){
			return view('admin.permations');
		}
		$cat = Slider::find($id);
		if($cat->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}

	public function get_admin_pages(){
		if(!$this->permations('slider')){
			return view('admin.permations');
		}
		$pages = Slider::get();
		return view('admin.slider')->with('pages',$pages);
	}


	

	########### photos ################
	public function get_newphoto(){
		$albums = DB::table('photos_cat')->pluck('catname','id');
		return view('admin.photos_new',compact('albums'));
	}
	public function newphoto(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'parent_id' => 'required',
			'photo' => 'required|mimes:jpeg,jpg,png|max:10000',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = new Photos;
		    $add->parent_id			= $request->input('parent_id');
			$photo = $request->file('photo');
			if(!empty($photo)){
				$photo_name      = str_random(5).time();
				$photo_ext       = $photo->getClientOriginalExtension();
				$photo_fullname  = $photo_name.'.'.$photo_ext;
				$path            = public_path().'/photos/';
				$thumb           = Image::make($photo)->resize(750,476)->save($path.$photo_fullname);
				$add->photo      = $photo_fullname;
			}
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	
	//*-------------------delete -------------------*//
	public function destroy_photo($id){
		$cat = Photos::find($id);
		if($cat->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}

	public function get_admin_photos(){
		$pages = Photos::where('type','photo')->get();
		return view('admin.photos')->with('pages',$pages);
	}



	





	public function get_newvideo(){
		return view('admin.videos_new');
	}
	public function newvideo(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'photo' => 'required|mimes:jpeg,jpg,png|max:10000',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = new Photos;
		    $add->title		= $request->input('title');
			$add->title_en	= $request->input('title_en');
			$add->videolink= $request->input('videolink');
			$add->type= 'video';
			$photo = $request->file('photo');
			if(!empty($photo)){
				$photo_name      = str_random(5).time();
				$photo_ext       = $photo->getClientOriginalExtension();
				$photo_fullname  = $photo_name.'.'.$photo_ext;
				$path            = public_path().'/photos/';
				$thumb           = Image::make($photo)->resize(1500,651)->save($path.$photo_fullname);
				$add->photo      = $photo_fullname;
			}
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	
	public function get_updatevideo($id){
		$page = Photos::find($id);
		return view('admin.videos_update')->with('page',$page);
	}
	public function updatevideo($id,Request $request)
    {
		$validation = Validator::make($request->all(),[
			'photo' => 'mimes:jpeg,jpg,png|max:10000',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$add = Photos::find($id);
			
		    $add->title		= $request->input('title');
			$add->title_en	= $request->input('title_en');
			$add->videolink= $request->input('videolink');
			
			$photo = $request->file('photo');
			if(!empty($photo)){
				$photo_name      = str_random(5).time();
				$photo_ext       = $photo->getClientOriginalExtension();
				$photo_fullname  = $photo_name.'.'.$photo_ext;
				$path            = public_path().'/photos/';
				$thumb           = Image::make($photo)->resize(1500,651)->save($path.$photo_fullname);
				$add->photo      = $photo_fullname;
			}
	    	if($add->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }
	
	//*-------------------delete -------------------*//
	public function destroy_video($id){
		$cat = Photos::find($id);
		if($cat->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}

	public function get_admin_videos(){
		$pages = Photos::where('type','video')->get();
		return view('admin.videos')->with('pages',$pages);
	}

	

}
