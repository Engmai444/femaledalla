<?php

namespace App\Http\Controllers;

use App\Application;
use App\Category;
use App\Models\Evaluation_time;
use App\Models\Evaluation_period;
use Carbon\Carbon;
use Validator;
use App\Pages;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use App;
use Auth;
use Excel;
use Storage;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class Evaluation_periodController extends Controller
{

    ######### periods ############

    public function get_newPeriod(){
        return view('admin.eva_period.new');
    }
    public function newPeriod(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'fromH' => 'required',
            'toH' => 'required',
        ]);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            $add = new Evaluation_period;

            $add->fromH	= $request->input('fromH');
            $add->toH 	= $request->input('toH');
            if($add->save()){
                Session::put('success',Lang::get('validation.Saved'));
                return redirect()->back();
            }else{
                return redirect()->back()->withErrors(Lang::get('validation.error'));
            }
        }
    }


    public function get_updatePeriod($id){
        $item = Evaluation_period::find($id);
        return view('admin.eva_period.update')->with('item',$item);
    }
    public function updatePeriod($id,Request $request)
    {
        $validation = Validator::make($request->all(),[
            'fromH' => 'required',
            'toH' => 'required',
        ]);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            $add = Evaluation_period::find($id);
            $add->id	= $request->input('Id');
            $add->fromH	= $request->input('fromH');
            $add->toH 	= $request->input('toH');
            if($add->save()){
                Session::put('success',Lang::get('validation.Saved'));
                return redirect()->to('admin/eva_period');
            }else{
                return redirect()->back()->withErrors(Lang::get('validation.error'));
            }
        }
    }

    public function get_Period(){
        $items = Evaluation_period::paginate(100);
        return view('admin.eva_period.items')->with('items',$items);
    }

    //*-------------------delete -------------------*//
    public function destroy_Period($id){
        $cat = Evaluation_period::find($id);
        if($cat->delete()){
            Session::put('success',Lang::get('validation.deleted'));
            return redirect()->back();
        }
    }

    ######### periods ############
}
