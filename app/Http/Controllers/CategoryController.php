<?php

namespace App\Http\Controllers;
use Validator;
use App\Categories;
use App\Photos;
use Response;
use Redirect;
use Image;
use Session;
use Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

    public function view_new(){
        $cats = Categories::pluck('title','cat_id');
		return view('admin.category_add')->with('cats',$cats);
	}


    //
    public function store(Request $request)
    {
		$validation = Validator::make($request->all(),[
			'title' 	=> 'required',
			'content' 	=> 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$photo = $request->file('photo');
		if(!empty($photo)){
			$photo_name = str_random(5).time();
			$photo_ext = $photo->getClientOriginalExtension();
			$photo_fullname = $photo_name.'.'.$photo_ext;
			$path = public_path().'/photos/';
			//$upload = $photo->move($path,$photo_fullname);
			$thumb = Image::make($photo)->resize(590, 590)->save($path.$photo_fullname);
		}
		
			$add = new Categories;
			$add->title		    = $request->input('title');
		    $add->content 	    = $request->input('content');
			$add->short_desc 	= $request->input('short_desc');
			$add->short_desc_ar	= $request->input('short_desc_ar');
            $add->seo_title     = $request->input('seo_title');
            $add->seo_desc      = $request->input('seo_desc');
            $add->title_ar		= $request->input('title_ar');
            $add->content_ar 	= $request->input('content_ar');
            $add->seo_title_ar  = $request->input('seo_title_ar');
            $add->seo_desc_ar   = $request->input('seo_desc_ar');
			$add->video_link    = $request->input('video_link');
			$add->_status		= $request->input('_status');
            $add->_order   		= $request->input('_order');
			$add->lat   		= $request->input('lat');
			$add->lng   		= $request->input('lng');

			if(!empty($photo)){
				$add->photo 	= $photo_fullname;
			}
	    	if($add->save()){
				if(!empty($request->input('token'))){
                    DB::table('photos')->where('token', $request->input('token'))->update(['parent_id' => $add->id]);
                }
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }

    public function view_update($id){
        $item = Categories::find($id);
        $cats = Categories::pluck('title','cat_id');
      //  $cats[0] = 'main category';
	  $photos = Photos::where('parent_id',$item->cat_id)->get();
		return view('admin.category_update')->with('item',$item)->with('cats',$cats)->with('photos',$photos);
	}
    public function update($id,Request $request)
    {
		$validation = Validator::make($request->all(),[
			'title' 	=> 'required',
			'content' 	=> 'required',
		]);

		if($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}else{
			$photo = $request->file('photo');
		if(!empty($photo)){
			$photo_name = str_random(5).time();
			$photo_ext = $photo->getClientOriginalExtension();
			$photo_fullname = $photo_name.'.'.$photo_ext;
			$path = public_path().'/photos/';
			//$upload = $photo->move($path,$photo_fullname);
			$thumb = Image::make($photo)->resize(590, 590)->save($path.$photo_fullname);
		}
		
			$update = Categories::find($id);
			$update->title		    = $request->input('title');
		    $update->content 	    = $request->input('content');
            $update->seo_title      = $request->input('seo_title');
            $update->seo_desc       = $request->input('seo_desc');
            $update->title_ar		= $request->input('title_ar');
            $update->content_ar 	= $request->input('content_ar');
            $update->seo_title_ar   = $request->input('seo_title_ar');
            $update->seo_desc_ar    = $request->input('seo_desc_ar');
			$update->video_link    	= $request->input('video_link');
			$update->short_desc 	= $request->input('short_desc');
			$update->short_desc_ar	= $request->input('short_desc_ar');
			$update->_status		= $request->input('_status');
            $update->_order         = $request->input('_order');
			$update->lat   			= $request->input('lat');
			$update->lng   			= $request->input('lng');

			if(!empty($photo)){
				$update->photo 		= $photo_fullname;
			}
	    	if($update->save()){
                 Session::put('success',Lang::get('validation.Saved'));
                 return redirect()->back();
            }else{
                 return redirect()->back()->withErrors(Lang::get('validation.error'));
			}
		}
    }

    public function destroy($id){
		$item = Categories::find($id);
		if($item->delete()){
			Session::put('success',Lang::get('validation.deleted'));
			return redirect()->back();
		}
	}

	public function get_all(){
		$items = Categories::orderBy('_order','asc')->paginate(100);
		return view('admin.category')->with('items',$items);
	}

}
