<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Response;
use App\Admin;
use Illuminate\Http\Request;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest.admin',  ['except' => ['logout', 'getlogout', 'authenticate']]);
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }
	
	public function newuser()
    {
        return Admin::create([
            'name' => 'admin',
            'email' => 'admin@kazzeem.com',
            'password' => bcrypt('9364175'),
        ]);
    }
	
	public function authenticate(Request $request)
	{
		$validator = Validator::make($request->all(),
			[
				'email' => "required",
				'password' => 'required'

			]
		);
		if($validator->fails()){
			$errors = $validator->errors();
			return \Response::json([
				'status' => 'failed',
				'msg' => $errors->first()
			]);
		}
		$email = $request->input('email');
		$password = $request->input('password');
		if (Auth::guard('admins')->attempt(['email' => $email, 'password' => $password])) {
			// Authentication passed...
			return \Response::json([
				'status' => 'done',
				'msg' => 'Successfully Logins.',
			]);
		}
		else {
			return \Response::json([
				'status' => 'error',
				'msg' => 'Username and password is incorrect',
			]);
		}
	}
	
	
	public function getlogout()
	{
		Auth::guard('admins')->logout();
    return Redirect::to('admin/login');
	}
	

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admins');
    }
}
