<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\Hijri;

use App;
use Validator;
use Hash;
use DB;
use Mail;


class CustomerController extends Controller
{
	public function Nationality(){
		// print(\GeniusTS\HijriDate\Hijri::convertToGregorian(23, 10, 1440));
		// print('///');
		// print(\GeniusTS\HijriDate\Hijri::convertToGregorian(23, 10, 1441));
		// return;

		$data = DB::table('nationality')->select('Id','Name')->orderBy('_order','desc')->get();
		foreach($data as $item){
			$item->Name = trim($item->Name);
		}
		$res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $data
		);
		return response()->json($res);
	}
    public function signup(Request $request)
    {
		$lang = $request->header('Accept-Language');
		// if(isset()){
			// $lang=$request->header('Accept-Language');
		// }
		
		
        if(isset($lang) && $lang == 'en'){
            $message = [
                'FullName.required' => 'Name is Required',
                //'FullName.min' => 'برجاء ادخال الاسم ثلاثي',
                'email.email' => 'Email is not Correct' ,
                'email.required' => 'Email is Required ',
                'email.unique' => 'This Email is already Exist',				
				
                'mobile.required' => 'Mobile is Required',
                'mobile.between'=>'Max Mobile Length 10 Number',
                'password.required' => 'Password is Required',
                'Birthday.required' => 'BirthDate is Required',
                //'IDNumber.required' => 'رقم الهوية مطلوب',
                'IDNumber.between' => 'Max ID / Residence number Length 10 Number',
                'IDNumber.required' => 'ID / Residence number is Required',
				'NationalityId.required' => 'Nationality is Required',
                'IDNumber.unique' => 'The ID / Residence number is already registered. Recover the password by mail or contact the administration',
                'school.required' => 'School is Required',
            ];
			}else{
            $message = [
                'FullName.required' => 'الاسم مطلوب',
                //'FullName.min' => 'برجاء ادخال الاسم ثلاثي',
                'email.email' => 'بريد الالكتروني غير صحيح',
                'email.required' => 'ادخل البريد الالكترونى ',
                'email.unique' => 'البريد الالكترونى موجود من قبل',
                'gender.required' => 'ادخل الجنس ',
                'mobile.required' => 'رقم الجوال مطلوب',
                'mobile.between'=>'رقم الجوال لا يزيد عن 10 ارقام',
                'password.required' => 'ادخل كلمة المرور',
                'Birthday.required' => 'ادخل تاريخ الميلاد',
                // 'Birthday.date' => 'ادخل تاريخ ميلاد صحيح',
                //'IDNumber.required' => 'رقم الهوية مطلوب',
                'IDNumber.between' => 'رقم الهوية لا تزيد عن 10 ارقام',
                'IDNumber.required' => 'رقم الإقامة مطلوب',
				'NationalityId.required' => 'الجنسية مطلوبة',
                'IDNumber.unique' => 'رقم الهوية مسجل بالفعل قم باستعادة كلمة المرور عن طريق البريد او التواصل مع الادارة',
                'school.required' => 'ادخل المدرسة',

            ];
        }
        $validation = Validator::make($request->all(),[
           'FullName' 	=> 'required',
           'email' 	=> 'unique:users|email|min:5|max:30',
           'mobile' 	=> 'required|unique:users|between:1,10',
            'password' 	=> 'required|min:6|max:50',
            'Birthday' 	=> 'required|date',
            'IDNumber' 	=> 'required|unique:users|between:1,10',
            'gender' 	=> 'required',
            'school' 	=> 'required',
            
        ],$message);

        if($validation->fails()){
            $errors = $validation->errors();
            $res = array(
                'status' 	=> 'error',
                'code' 		=> 231,
                'message' 	=> $errors->first()
            );
        }else{
            $token  = str_random(150);
            $mobile = $request->input('mobile');

            if(intval($request->input('birthdayrequired')) != 1){
				if(isset($lang) && $lang == 'en'){
	                $res = array(
                    'status' 	=> 'error',
                    'code' 		=> 231,
                    'message' 	=> 'لابد من الموافقة على التعهد لاكمال التسجيل'
					);				
				}else{
                $res = array(
                    'status' 	=> 'error',
                    'code' 		=> 231,
                    'message' 	=> 'Conditions must be agreed to complete the registration'
					);					
				}
                return response()->json($res);
            }

			if(!$this->is_arabic($request->input('FullName'))){
				if(isset($lang) && $lang == 'en'){
					$res = array(
						'status' 	=> 'error',
						'code' 		=> 231,
						'message' 	=> 'Enter Full Name With Arabic'
					);						
				}else{
					$res = array(
						'status' 	=> 'error',
						'code' 		=> 231,
						'message' 	=> 'أدخل الاسم باللغة العربية'
					);					
				}
					return response()->json($res);
			}
            if($request->input('gender')=='female'){
                if(isset($lang) && $lang == 'en'){
                    $res = array(
                        'status' 	=> 'error',
                        'code' 		=> 231,
                        'message' 	=> 'Registeration for male only'
                    );
                }else{
                    $res = array(
                        'status' 	=> 'error',
                        'code' 		=> 231,
                        'message' 	=> 'التسجيل للذكور فقط '
                    );
                }
                return response()->json($res);
            }

			
			$d_type = $request->d_type;
			if(!isset($d_type) || $d_type!= '1'){
				$time=explode("-",$request->input('Birthday'));
				$day=$time['2'];
				$month=$time['1'];
				$year=$time['0'];			
				if(!checkdate($month,$day,$year)){
					if(isset($lang) && $lang == 'en'){
						$res = array(
							'status' 	=> 'error',
							'code' 		=> 231,
							'message' 	=> 'Please Enter BirthDate Correctly'
						);						
						
					}else{
						$res = array(
							'status' 	=> 'error',
							'code' 		=> 231,
							'message' 	=> 'من فضلك أدخل تاريخ الميلاد صحيح'
						);						
						
						
					}

					return response()->json($res);
				}				
				
			}
          /*  if($request->d_type == 'hj'){
                $DateConv=new Hijri;
                $format="YYYY-MM-DD";
                $datev="{$request->d_year}-{$request->d_month}-{$request->d_day}";
                $date =$DateConv->HijriToGregorian($datev,$format);
            }else{
                $date="{$request->d_year}-{$request->d_month}-{$request->d_day}";
            }*/

            $date = $request->input('Birthday');
       	$d_type=$request->d_type;
			if(isset($d_type) && $d_type== '1'){
				$bd = $request->input('Birthday');
				$time=explode("-",$bd);

				$day=$time['2'];
				$month=$time['1'];
				$year=$time['0'];

				$Date = \GeniusTS\HijriDate\Hijri::convertToGregorian($day, $month, $year);
				$date = date('Y-m-d', strtotime($Date. ' + 1 days'));


			}else{
				$d_type=0;
				$date = date('Y-m-d', strtotime($request->input('Birthday')));
			}
            $date1=date('Y/m/d',strtotime($date));
            // $dateinput="{$request->d_year}/{$request->d_month}/{$request->d_day}";
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da_st=date('Ymd',strtotime($date1));

            $date_sta="{$da_st}";
            $d_st =$DateConv->GregorianToHijri($date_sta,$format);
            $dh_start=date('Y/m/d',strtotime($d_st));


            $today = Carbon::today();
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da=date('Ymd',strtotime($today));

            $datev="{$da}";
            $d =$DateConv->GregorianToHijri($datev,$format);
            $dh_today=date('Y/m/d',strtotime($d));

            $startDate = Carbon::parse($dh_start);
            $endDate   = Carbon::parse($dh_today);
            $y = $startDate->diff($endDate)->format("%y");
            //$today = Carbon::today();
            //$y = $this->dateDifference($date,$today);

            /* if($request->d_type== '1'){
                 $DateConv=new Hijri;
                 $format="YYYYMMDD";
                 $date=date('Y/m/d',strtotime($date));
                 //$d =$DateConv->HijriToGregorian($datev,$format);
                 //		$date=date('Y-m-d',strtotime($d));

                 $today = Carbon::today();
                 $DateConv=new Hijri;
                 $format="YYYYMMDD";
                 $da=date('Ymd',strtotime($today));

                 $datev="{$da}";
                 $d =$DateConv->GregorianToHijri($datev,$format);
                 $dh_today=date('Y/m/d',strtotime($d));

                 $startDate = Carbon::parse($datev);
                 $endDate   = Carbon::parse($dh_today);
                 $y = $startDate->diff($endDate)->format("%y");


             }else{
                 $date=date('Y/m/d',strtotime($date));

                 $DateConv=new Hijri;
                 $format="YYYYMMDD";
                 $da_st=date('Ymd',strtotime($date));

                 $date_sta="{$da_st}";
                 $d_st =$DateConv->GregorianToHijri($date_sta,$format);
                 $dh_start=date('Y/m/d',strtotime($d_st));


                 $today = Carbon::today();
                 $DateConv=new Hijri;
                 $format="YYYYMMDD";
                 $da=date('Ymd',strtotime($today));

                 $datev="{$da}";
                 $d =$DateConv->GregorianToHijri($datev,$format);
                 $dh_today=date('Y/m/d',strtotime($d));

                 $startDate = Carbon::parse($dh_start);
                 $endDate   = Carbon::parse($dh_today);
                 $y = $startDate->diff($endDate)->format("%y");*/


            //}
            if(intval($y) < 17){
				if(isset($lang) && $lang == 'en'){
					$res = array(
						'status' 	=> 'error',
						'code' 		=> 231,
						'message' 	=> 'It is not possible to register for courses for those under the age of 17'
					);

				}else{
					$res = array(
						'status' 	=> 'error',
						'code' 		=> 231,
						'message' 	=> 'لا يمكن التسجيل فى الدورات لمن عمره أقل من 17 عام'
					);									
					
				}

                return response()->json($res);
            }

            if ($request->hasFile('img_national_id')) {

                $photo = $request->file('img_national_id');
                //$name = str_slug($image) . '.' . $image->getClientOriginalExtension();

                $photo_name = $request->input('IDNumber');
                $photo_ext = $photo->getClientOriginalExtension();


                if($photo->getClientOriginalExtension()!='jpg' and $photo->getClientOriginalExtension()!='jpeg' and $photo->getClientOriginalExtension()!='png') {
                    $res = array(
                        'status' => 'error',
                        'code' => 231,
                        'message' => 'من فضلك ارفع صورة الهوية بصيغة  (jpg,jpeg,png)'
                    );
                    return response()->json($res);
                }else{
                    $photo_fullname = $photo_name . '.' . $photo_ext;
                    $path = public_path().'/photos/thumb/' . $request->input('IDNumber').'/';
                    File::makeDirectory($path);
                    $thumb = Image::make($photo)->save($path . $photo_fullname);
                    Storage::disk($request->input('school'))->put($photo_fullname, file_get_contents(storage_path('../public/photos/thumb/'.$request->input('IDNumber').'/'.$photo_fullname )));

                }
            }else{
                $photo_fullname='';
            }

             
            
            $add = new Customer;
            $add->FullName	= $request->input('FullName');
            $add->IDNumber	= $request->input('IDNumber');
            $add->date_type 	=$d_type;
            if(!empty($date)){
            $add->DoB	    = $date;
            }
            $add->NationalityId	= $request->input('NationalityId');
            if(!empty($request->input('email'))){
                $add->email = $request->input('email');
            }
            $add->api_token = $token;
            $add->mobile 	= $mobile;
            $add->mobile_st = 1;
             $add->type_reg = 1;
            $add->password 	= Hash::make($request->input('password'));
            $add->gender	= $request->input('gender');
            $add->img_national_id=$photo_fullname;
            $add->school	= $request->input('school');
            if($add->save()){

                $data = [
                    'StudentId'      => $add->StudentId,
                    'FullName'      => $add->FullName,
                    'email'         => $add->email,
                    'mobile'        => $add->mobile,
                    'IDNumber'      => $add->IDNumber,
                    'DoB'           => $add->DoB,
                    'NationalityId' => $add->NationalityId,
                    'gender' => $add->gender,
                    'api_token'     => $add->api_token
                ];
				if(isset($lang) && $lang == 'en'){
						$res = array(
						'status' 	=> 'done',
						'code' 		=> 200,
						'message' 	=> 'successfully registered',
						'data'      => $data
					);				
				}else{
					$res = array(
						'status' 	=> 'done',
						'code' 		=> 200,
						'message' 	=> 'تم التسجيل بنجاح',
						'data'      => $data
					);					
				}


            }else{
				if(isset($lang) && $lang == 'en'){
						$res = array(
						'status' 	=> 'done',
						'code' 		=> 200,
						'message' 	=> 'Registration Error',
					);				
				}else{
					$res = array(
						'status' 	=> 'done',
						'code' 		=> 200,
						'message' 	=> 'خطأ فى التسجيل'
					);					
				}				

            }
        }
        return response()->json($res);
    }

    public function dateDifference($date_1 , $date_2 , $differenceFormat = '%y' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);

        return $interval->format($differenceFormat);

    }


    public function login(Request $request){

		$lang = $request->header('Accept-Language');
		if($lang == 'ar'){
			$message = [
				'IDNumber.required' => 'رقم الاقامة مطلوب',
				'password.required' => 'ادخل كلمة المرور',
			];			
		}else{
			$message = [
				'IDNumber.required' => 'ID Number is Required',
				'password.required' => 'Password is Required',
			];			
			
		}
        $validation = Validator::make($request->all(),[
            'IDNumber' 	=> 'required',
            'password' 	=> 'required',
        ],$message);

        if($validation->fails()){
            $errors = $validation->errors();
            $res = array(
                'status' 	=> 'error',
                'code' 		=> 231,
                'message' 	=> $errors->first()
            );
        }else{
            $customer = Customer::where('IDNumber',$request->IDNumber)->first();
            if (!empty($customer) && Hash::check($request->password, $customer->password)) {
                $customer->api_token = str_random(150);
                $customer->save();
                $data = [
                    'StudentId'     => $customer->StudentId,
                    'FullName'      => $customer->FullName,
                    'email'         => $customer->email,
                    'mobile'        => $customer->mobile,
                    'IDNumber'      => $customer->IDNumber,
                    'DoB'           => $customer->DoB,
                    'NationalityId' => $customer->NationalityId,
                    'api_token'     => $customer->api_token
                ];
                $res = array(
                    'status' 	=> 'done',
                    'code' 		=> 200,
                    'message' 	=> 'تم تسجيل دخولك بنجاح',
                    'data'      => $data
                );
            }else{
                $res = array(
                    'status' 	=> 'error',
                    'code' 		=> 231,
                    'message' 	=> 'بيانات الدخول غير صحيحة'
                );
            }
        }
        return response()->json($res);
    }

    public function profile(Request $request){
        $customer = Customer::where('StudentId',$request->user()->StudentId)->first();
        $orders= \Illuminate\Support\Facades\DB::table('orders')->where('StudentId',$request->user()->StudentId)
            ->where('deleted_at','0000-00-00 00:00:00')->first();

        if(empty($orders))
        {
            $edit_profile=1;
        }else{
            $edit_profile=0;
        }
        $data = [
            'StudentId'     => $customer->StudentId,
            'FullName'      => $customer->FullName,
            'email'         => $customer->email,
            'mobile'        => $customer->mobile,
            'IDNumber'      => $customer->IDNumber,
            'DoB'           => $customer->DoB,
            'NationalityId' => $customer->NationalityId,
            'edit_profile'  => $edit_profile
        ];
        $res = array(
            'status' 	=> 'done',
            'code' 		=> 200,
            'data'      => $data
        );
        return $res;
    }
	
	public function changepassword(Request $request){

		$validation = Validator::make($request->all(),[
			'password' => 'required',
			'newpassword' => 'required|different:password'
			
		]);

		if($validation->fails()){
			$errors = $validation->errors();
			$res = array(
					'status' 	=> 'error',
          			'code' 	=> 231,
					'message' 	=> $errors->first()
				);
		}else{
			$customer = Customer::find($request->user()->StudentId);
			if (!empty($customer) && Hash::check($request->password, $customer->password)) {
				$customer->password = Hash::make($request->newpassword);
			//	$customer->api_token = str_random(60);
				$customer->save();
				$res = array(
					'status' 	=> 'done',
					'code' 		=> 201,
					'message' 	=> 'تم تغيير كلمة المرور بنجاح .. شكراً لك'
				);
			}else{
				$res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> 'كلمة المرور القديمة غير صحيحة'
				);
			}
		}
		return response()->json($res);
	}
	
	public function resetpassword(Request $request){
		
		$email = $request->input('email');
	
		
		$customer = Customer::where('email',$email)->first();
		
		if (empty($customer->email)) {
			$res = array(
				'status' 	=> 'error',
        		'code' 		=> 231,
				'message' 	=> 'لا يوجد بريد الكتروني مسجل .. يرجي التواصل مع الادارة'
			);
		}else{
			$code = rand(1000, 9999);
			//$msg = "KAZZEEM Reset PW : \r\n http://kazzeem.com/app/pw?p={$customer->mobile}&c={$code}";
			$msg = "كودالتحقق : {$code}";
			$title = "استعادة كلمة المرور";
			$this->send_email($title,$customer->email,$msg);
			
			$customer->resetcode = $code;
			$customer->save();
			$res = array(
				'status' 	=> 'done',
				'code' 	=> 201,
				'message' 	=> 'تم ارسال كود التحقق على البريد الالكتروني الخاص بك',
				'verification' 	=> $code
			);

		}
		return response()->json($res);
	}
	
	public function send_email($title,$email,$msg){
		#send msg to email(1)
		Mail::send('emails.send', ['msg' => $msg], function ($message) use ($email,$title){
			$message->from('info@dallahdrivingschool.sa', 'شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ');
			$message->to($email)->subject($title);
		});
		if(Mail::failures()){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function checknewpass(Request $request){

		$validation = Validator::make($request->all(),[
			'email' 	=> 'required',
			'resetcode' => 'required',
		]);

		if($validation->fails()){
			 $errors = $validation->errors();
			$res = array(
					'status' 	=> 'error',
          			'code' 	=> 231,
					'message' 	=> $errors->first()
				);
		}else{
			$email = $request->input('email');
			$customer = Customer::where('email',$email)->where('resetcode',$request->resetcode)->first();
			if (!empty($customer)) {
				$res = array(
					'status' 	=> 'done',
					'code' 		=> 200,
					'message' 	=> 'كود التحقق صحيح',
				);
			}else{
				$res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> 'خطأ فى كود التحقق',
				);
			}
		}
		return response()->json($res);
	}
	
	public function changepass(Request $request){


		$validation = Validator::make($request->all(),[
			'newpassword' => 'required|min:6|max:50',
			//'newpassword_confirmation' => 'required',
			'email' 	=> 'required',
			'resetcode' => 'required',
		]);

		if($validation->fails()){
			$errors = $validation->errors();
			$res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> $errors->first()
				);
		}else{
			$email = $request->input('email');
			
			$customer = Customer::where('email',$email)->where('resetcode',$request->resetcode)->first();
			if (!empty($customer)) {
				$customer->password = Hash::make($request->newpassword);
				$customer->api_token = str_random(60);
				$customer->resetcode = '';
				$customer->save();
				$res = array(
					'status' 	=> 'done',
					'code' 		=> 200,
					'message' 	=>  'تم تغيير كلمة المرور بنجاح'
				);
			}else{
				$res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> 'كود التحقق غير صحيح'
				);
			}
		}
		return response()->json($res);
	}
	
	public function customerUpdate($userid,Request $request){
		$lang = $request->header('Accept-Language');
        $message = [
            'FullName.required' => 'الاسم مطلوب',
         //   'email.email' => 'بريد الالكتروني غير صحيح',
         //   'email.unique' => 'البريد الالكتروني مستخدم بالفعل',
            'mobile.required' => 'رقم الجوال مطلوب',
			'NationalityId.required' => 'الجنسية مطلوبة',
			'Birthday.required' => 'تاريخ الميلاد مطلوب'
        ];

        $validation = Validator::make($request->all(),[
			'FullName' => 'required|min:2|max:150',
			'mobile' 	=> 'required',
		//	'Birthday' 	=> 'required|date',
        ],$message);
        if ($validation->fails()) {
			$errors = $validation->errors();
			$res = array(
				'status' 	=> 'error',
				'code' 		=> 231,
				'message' 	=> $errors->first()
			);
        }else{
			if(!$this->is_arabic($request->input('FullName'))){
				if(isset($lang) && $lang == 'en'){
					$res = array(
						'status' 	=> 'error',
						'code' 		=> 231,
						'message' 	=> 'Enter Full Name With Arabic'
					);						
				}else{
					$res = array(
						'status' 	=> 'error',
						'code' 		=> 231,
						'message' 	=> 'أدخل الاسم باللغة العربية'
					);					
				}
				return response()->json($res);
			}			
			$add = Customer::find($userid);
			$add->FullName	= $request->input('FullName');
			$add->mobile 	= $request->input('mobile');
		//	$add->IDNumber 	= $request->input('IDNumber');
		//	$add->email 	= $request->input('email');
			$add->NationalityId 		= $request->input('NationalityId');
		//	$add->DoB 		= $request->input('DoB');



			$photo = $request->file('file');
			if(!empty($photo)){
				$photo_name = str_random(5).time();
				$photo_ext = $photo->getClientOriginalExtension();
				$photo_fullname = $photo_name.'.'.$photo_ext;
				$path = public_path().'/photos/';
				//$upload = $photo->move($path,$photo_fullname);
				$thumb = Image::make($photo)->resize(400, 400)->save($path.$photo_fullname);
				$add->avatar = $photo_fullname;
			}

			if(!empty($request->input('newpassword'))){
				$add->password = Hash::make($request->input('newpassword'));
			}


			if($add->save()){
				$res = array(
					'status' 	=> 'done',
					'code' 		=> 200,
					'message' 	=>  'تم تحديث بياناتك بنجاح'
				);
			}else{
				$res = array(
					'status' 	=> 'done',
					'code' 		=> 200,
					'message' 	=>  'تم تحديث بياناتك بنجاح'
				);
			}
        }
		return response()->json($res);
    }
    
    public function profileUpdate(Request $request){
        
        return $this->customerUpdate($request->user()->StudentId, $request);
    }
	
	
function uniord($u) {
    // i just copied this function fron the php.net comments, but it should work fine!
    $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
    $k1 = ord(substr($k, 0, 1));
    $k2 = ord(substr($k, 1, 1));
    return $k2 * 256 + $k1;
}
function is_arabic($str) {
    if(mb_detect_encoding($str) !== 'UTF-8') {
        $str = mb_convert_encoding($str,mb_detect_encoding($str),'UTF-8');
    }

    /*
    $str = str_split($str); <- this function is not mb safe, it splits by bytes, not characters. we cannot use it
    $str = preg_split('//u',$str); <- this function woulrd probably work fine but there was a bug reported in some php version so it pslits by bytes and not chars as well
    */
    preg_match_all('/.|\n/u', $str, $matches);
    $chars = $matches[0];
    $arabic_count = 0;
    $latin_count = 0;
    $total_count = 0;
    foreach($chars as $char) {
        //$pos = ord($char); we cant use that, its not binary safe 
        $pos = $this->uniord($char);
       // echo $char ." --> ".$pos.PHP_EOL;

        if($pos >= 1536 && $pos <= 1791) {
            $arabic_count++;
        } else if($pos > 123 && $pos < 123) {
            $latin_count++;
        }
        $total_count++;
    }
    if(($arabic_count/$total_count) > 0.6) {
        // 60% arabic chars, its probably arabic
        return true;
    }
    return false;
}


}
