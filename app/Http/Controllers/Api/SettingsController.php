<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pages;
use DB;
use Mail;
use Validator;
use App;
class SettingsController extends Controller
{
    //
	
	public function Page($id,Request $request){
        $lang = $request->header('Accept-Language');


        $page = Pages::find($id);
		$data['id']=$page->pageId;
		if($lang=='en') {
            $data['titla'] = $page->title;
            $content=strip_tags($page->content);
        }else{
            $data['titla'] = $page->title_ar;
            $content=strip_tags($page->content_ar);
        }

        $content2 = str_replace(array("\n", "\r","\t","&nbsp;"), '', $content);
        $data['content']=nl2br($content2);

        $res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $data
		);
		return response()->json($res);
	}
	
	public function branches(){
		$items = DB::table('branches')->orderBy('id','asc')->get();
		$res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $items
		);
		return response()->json($res);
	}
	
	public function gallery(){
		$items = DB::table('photos_cat')->orderBy('id','asc')->get();
		$items->map(function($item){
			
			$photos = DB::table('photos')->where('type','photo')->where('parent_id',$item->id)->get();
			foreach($photos as $photo){
				$photo->photo = url('/public/photos/'.$photo->photo);
			}
			$item->photos = $photos;
		});
		$res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $items
		);
		return response()->json($res);
	}
	
	public function faqs(){
		$items = DB::table('faqs')->get();
		
		$res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $items
		);
		return response()->json($res);
	}
	
	public function get_home_option(){
		$optios = DB::table('site_option')->get();
		foreach($optios as $item){
			$arr[$item->varname] = $item->value;
		}
		return $arr;
	}
	
	public function contact(Request $request)
	{

		$messages = [
			'name.required' => 'الاسم مطلوب',
			'msg.required' => 'اكتب استفسارك',
			'phone.required' => 'رقم التليفون مطولب',
			'email.required' => 'البريد الالكتروني مطلوب',
		];
		$validation = Validator::make($request->all(),[
			'name' 		=> 'required',
			'msg' 		=> 'required',
			'phone' 	=> 'required',
		],$messages);

		if($validation->fails()){
			$errors = $validation->errors();
			$res = array(
					'status' 	=> 'error',
					'code' 		=> 231,
					'message' 	=> $errors->first()
				);
		}else{
		    
    
			$options = $this->get_home_option();
	    	$data  = array(
	    		'name' 		=> $request->name,
				'email' 	=> $request->email,
				'mobile' 	=> $request->phone,
	    		'msg' 		=> $request->msg,
	    	);

			if (App::environment('production', 'staging'))
			{
				$sent = Mail::send('emails.contact', $data, function($message) use ($data,$options)
				{
					$message->from('info@dallahdrivingschool.sa' , $data['name']);
					$message->to($options['hm_bl4_contactform'], 'Admin')->subject('dalla Contact Form :'.$data['name']);
					$message->to('R.kh.alhassani@hotmail.com', 'Admin')->subject('dalla Contact Form :'.$data['name']);
				});	
			}

	    	if(count(Mail::failures()) ==  0){
				if($request->input('lang') == 'en'){
					$res = array(
						'status' => 'done',
						'message' 	=> 'sent successfuly'
					);
				}else{
					$res = array(
						'status' => 'done',
						'message' 	=> 'تم استقبال استفسارك وسيتم التواصل معك ..شكراً لك'
					);
				}
	    		
	    	}else{
	    		$res = array(
					'status' => 'error',
					'code' 		=> 231,
					'message' 	=> 'error'
				);
	    	}
			
	    }
		return response()->json($res);
			
	}

    public function get_schools(){
        $items = DB::table('schools')->orderBy('id','asc')->get();
        $res = array(
            'status' 	=> 'done',
            'code' 		=> 200,
            'data'      => $items
        );
        return response()->json($res);
    }

    public function slider(){
        $items = DB::table('slider')->orderBy('id','asc')->get();
        $res = array(
            'status' 	=> 'done',
            'code' 		=> 200,
            'path'=>url('/').'/public/photos/',
            'data'      => $items
        );
        return response()->json($res);
    }
}
