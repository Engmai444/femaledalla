<?php

namespace App\Http\Controllers\Api;

use App\Models\Evaluation_time;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use DB;
use App\Classes\Hijri;

use Carbon\Carbon;
use App\Course;
use App\Order;
use App\Models\Evaluation_reserve;
use App\Models\Evaluation_period;
use App\Models\New_order;

use App\Payment_log;


class CoursesController extends Controller
{
    //
	public function items(Request $request){

        
       // $periods = DB::table('periods')->get();
	   
	  
        $query = DB::table('course')->leftjoin('applicationdef','applicationdef.Id','course.applicationdef')->leftjoin('category','category.Id','course.category')->leftjoin('schools','schools.Id','course.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')->select('course.*','periods.fromH','periods.toH','applicationdef.AppDefName',DB::RAW('schools.Name schoolName'),DB::RAW('category.Name categoryName'));
       
		//$query->whereDate('course.CourseStartIn','>=',Carbon::today()->addDays(-2));
				$query->whereDate('course.CourseStartIn','>=',Carbon::today()->addDays(+1));

        
		$query->where('course.enable',1);
		 if($request->applicationdef != 0){

            $query->where('course.applicationdef',$request->applicationdef);
        }
       if($request->SchoolId != 0){
            $query->where('course.SchoolId',$request->SchoolId);
        }
        if($request->date_from and $request->date_to==NULL){
            $query->whereDate('course.CourseStartIn',$request->date_from.' 00:00:00');

        }

        if($request->date_to and $request->date_from==NULL){
            $query->whereDate('course.CourseEndIn',$request->date_to.' 00:00:00');
        }
        if($request->date_from and $request->date_to and $request->date_from!=NULL and $request->date_to!=NULL){

            $query->whereDate('course.CourseStartIn','>=',$request->date_from.' 00:00:00');
            $query->whereDate('course.CourseStartIn','<=',$request->date_to.' 00:00:00');

        }
        $courses = $query->get();
	   
	   
       /* $query = DB::table('course')->leftjoin('applicationdef','applicationdef.Id','course.applicationdef')->leftjoin('category','category.Id','course.category')->leftjoin('schools','schools.Id','course.SchoolId')->leftjoin('orders','orders.CourseId','course.id')->leftjoin('periods','periods.PeriodId','course.Period')->select('course.*','periods.fromH','periods.toH','applicationdef.AppDefName',DB::RAW('schools.Name schoolName'),DB::RAW('category.Name categoryName'),DB::RAW('count(orders.CourseId) booked'));
        $query->whereDate('course.CourseStartIn','>=',Carbon::today());
		if($request->applicationdef){
            $query->where('course.applicationdef',$request->applicationdef);
        }
        if($request->SchoolId){
            $query->where('course.SchoolId',$request->SchoolId);
        }

        if($request->date_from){
            $query->whereDate('course.CourseStartIn','>=',$request->date_from.' 00:00:00');
        }
        

        if($request->date_to){
            $query->whereDate('course.CourseStartIn','<=',$request->date_to.' 00:00:00');
        }
        $courses = $query->groupBy('course.id')->orderBy('course.id','desc')->get();*/

        
		$res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $courses
		);
		return response()->json($res);
        
    }
	
	public function applications(){
		$data = DB::table('applicationdef')->select('Id','AppDefName')->get();
		$res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $data
		);
		return response()->json($res);
        
	}
	
	public function schools(){
        $data = DB::table('schools')->select('Id','Name')->get();
		$res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $data
		);
		return response()->json($res);
	}
	
	public function checkUserSubs($userid,$date){
		$orders = DB::table('orders')->join('course','course.id','orders.CourseId')->where('orders.StudentId',$userid)->whereNULL('orders.deleted_at')->where('course.CourseEndIn','>',Carbon::today())->count();
		return $orders;
	}

    public function subsCours(Request $request){
       
       $userid = $request->user()->StudentId;
	  
	   $course = Course::find($request->courseid);

	   if(empty($course)){
			$res = array(
				'status' 	=> 'error',
				'code' 		=> 231,
				'message' 	=> 'الدورة غير موجودة'
			);
			return response()->json( $userid);
		}
	   
	  // $today = Carbon::today();
		//$y = $this->dateDifference($request->user()->DoB,$today);
        $date=$request->user()->DoB;

        $date_type=$request->user()->date_type;
       /* if($date_type==0) {
            $DateConv = new Hijri;
            $format = "YYYYMMDD";
            $da_st = date('Ymd', strtotime($date));

            $date_sta = "{$da_st}";
            $d_st = $DateConv->GregorianToHijri($date_sta, $format);
            $dh_start = date('Y/m/d', strtotime($d_st));


            $today = Carbon::today();
            $DateConv = new Hijri;
            $format = "YYYYMMDD";
            $da = date('Ymd', strtotime($today));

            $datev = "{$da}";
            $d = $DateConv->GregorianToHijri($datev, $format);
            $dh_today = date('Y/m/d', strtotime($d));

            $startDate = Carbon::parse($dh_start);
            $endDate = Carbon::parse($dh_today);
            $y = $startDate->diff($endDate)->format("%y");
        }else if($date_type==1) {
            $dh_start = $date;
            $today = Carbon::today();
            $DateConv = new Hijri;
            $format = "YYYYMMDD";
            $da = date('Ymd', strtotime($today));

            $datev = "{$da}";
            $d = $DateConv->GregorianToHijri($datev, $format);
            $dh_today = date('Y/m/d', strtotime($d));

            $startDate = Carbon::parse($dh_start);
            $endDate = Carbon::parse($dh_today);
            $y = $startDate->diff($endDate)->format("%y");
        }*/
        //$today = Carbon::today();
        //$y = $this->dateDifference($date,$today);
        $date1=date('Y/m/d',strtotime($date));
        // $dateinput="{$request->d_year}/{$request->d_month}/{$request->d_day}";
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da_st=date('Ymd',strtotime($date1));

        $date_sta="{$da_st}";
        $d_st =$DateConv->GregorianToHijri($date_sta,$format);
        $dh_start=date('Y/m/d',strtotime($d_st));


        $today = Carbon::today();
        $DateConv=new Hijri;
        $format="YYYYMMDD";
        $da=date('Ymd',strtotime($today));

        $datev="{$da}";
        $d =$DateConv->GregorianToHijri($datev,$format);
        $dh_today=date('Y/m/d',strtotime($d));

        $startDate = Carbon::parse($dh_start);
        $endDate   = Carbon::parse($dh_today);
        $y = $startDate->diff($endDate)->format("%y");
		// if(intval($y) < 18 && $course->applicationdef > 1){
			// $res = array(
				// 'status' 	=> 'error',
				// 'code' 		=> 231,
				// 'message' 	=> 'لا يمكن التسجيل في هذه الدورة لمن عمره اقل من 18 عام'
			// );
			// return response()->json($res);
		// }
 		if(intval($y) < 18 && $course->applicationdef != 5){
			$res = array(
				'status' 	=> 'error',
				'code' 		=> 231,
				'message' 	=> 'لا يمكن التسجيل في هذه الدورة لمن عمره اقل من 18 عام'
			);
			return response()->json($res);
		}
		
		if(intval($y) >= 18 && $course->applicationdef == 5){
			$res = array(
				'status' 	=> 'error',
				'code' 		=> 231,
				'message' 	=> 'لا يمكن التسجيل في هذه الدورة لمن عمره أكبر من 18 عام'
			);
			return response()->json($res);
		}      

       $order = DB::table('orders')->where('StudentId',$userid)->where('CourseId',$course->id)->whereNULL('orders.deleted_at')->first();

       if($order!= NULL){

        return response()->json(['
				status' 	=> 'error',
				'code' 		=> 231,
				'message'=>'لقد قمت بالاشتراك بالفعل فى هذه الدورة',
				'id'=>$order->OrderId
				]);
	   }
	   if($this->checkUserSubs($userid,$course->CourseStartIn) > 0){
			$res = array(
				'status' 	=> 'error',
				'code' 		=> 231,
				'message' 	=> 'لا يمكنك التسجيل فى دورة اخري او مدرسة اخري الا بعد الانتهاء من الدورة المشترك بيها حالياً'
			);
			return response()->json($res);
		}
        if($course->waiting_list=='0') {
            if (DB::table('orders')->where('CourseId', $course->id)->where('orders.IsPaid', '1')->count() >= $course->seats) {
                return response()->json(['message' => 'نعتذر لك ..نفذت المقاعد المتاحه للحجز, يمكنك معاودة المحاولة لاحقاً في الدورات القادمة']);
            }
        }else{
            if (DB::table('orders')->where('CourseId', $course->id)->count() >= $course->seats) {
                return response()->json(['message' => 'نعتذر لك ..نفذت المقاعد المتاحه للحجز, يمكنك معاودة المحاولة لاحقاً في الدورات القادمة']);
            }
        }
       if($course != NULL){
            $insert = new Order;
            $insert->StudentId = $userid;
            $insert->SchoolId = $course->SchoolId;
            $insert->CourseId = $course->id;
            $insert->CourseRefNo = $course->CourseRefNo;
            $insert->ApplicationDefId = $course->applicationdef;
           $insert->deleted_at = date('Y-m-d H:s:i');
           $insert->course_start = $course->CourseStartIn;
           $insert->course_end = $course->CourseEndIn;
           $insert->IsPaid = 0;
            if($insert->save()){
				$countr = DB::table('orders')->where('CourseId',$course->id)->whereNULL('orders.deleted_at')->count();
                $no_seats=$course->seats;
				if($countr == $course->seats){
					$course->enable = 0;
					$course->save();
				}
               // $course->seats=$no_seats-1;
               // $course->save();
                $now = time(); // or your date as well
                $your_date = strtotime($course->CourseStartIn);
                $datediff =  $your_date-$now;

                $dif= round($datediff / (60 * 60 * 24));
                if($course->waiting_list=='1')
                {
                    if($dif <= 5){
                        $payonline=1;
                    }else{
                        $payonline=0;
                    }

                }else{
                    $payonline=1;
                }
                return response()->json(['userid'=>$userid,'code'=> 200,'message'=>'تم اشتراكك بنجاح يجب السداد فور استلام الرابط خلال ٤٨ ساعه والا سوف يتم الغاء الاشتراك تلقائيا','id'=>$insert->OrderId,'payonline'=>$payonline]);
            }else{
                return response()->json([
				'status' 	=> 'error',
				'code' 		=> 231,
				'message'=>'خطأ فى الاشتراك']);
            }
       }
    }
	
	public function mycourses(Request $request){
        $data =DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('schools','schools.Id','orders.SchoolId')->where('orders.StudentId',$request->user()->StudentId)->where('orders.IsPaid','1')->select('orders.*','course.CourseName','course.CourseStartIn',DB::RAW('schools.Name as schoolName'))->get();

        $res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $data
		);
		return response()->json($res);
    }

    public function mycourse($id,Request $request){
        $course = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->leftjoin('users','users.StudentId','orders.StudentId')->leftjoin('schools','schools.Id','orders.SchoolId')->leftjoin('nationality','nationality.Id','users.NationalityId')->leftjoin('periods','periods.PeriodId','course.Period')->where('orders.StudentId',$request->user()->StudentId)->where('IsPaid','1')->where('orders.OrderId',$id)->select('orders.*','course.CourseName','course.CourseStartIn','course.days','periods.fromH','periods.toH','users.FullName','users.IDNumber','users.mobile',DB::RAW('nationality.Name as nationality'),DB::RAW('schools.Name as schoolName'))->first();
        $res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $course
		);
		return response()->json($res);
	}
	
	public function dateDifference($date_1 , $date_2 , $differenceFormat = '%y' )
	{
		$datetime1 = date_create($date_1);
		$datetime2 = date_create($date_2);
	   
		$interval = date_diff($datetime1, $datetime2);
	   
		return $interval->format($differenceFormat);
	   
	}
	
	public function news(Request $request){
        $data = DB::table('pages')->where('type_','news')->orderby('pageId','desc')->take(30)->get();
        $data->map(function($item){
            $item->created_at = date('Y-m-d',strtotime($item->created_at));
            $item->photo = ($item->photo == '') ? url('/public/photos/nophoto.png') : url('/public/photos/'.$item->photo);
        });
		$res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $data
		);
		return response()->json($res);
	}
	public function news_item($itemid){
        $item = DB::table('pages')->where('type_','news')->where('pageId',$itemid)->first();
        if($item){
            $item->created_at = date('Y-m-d',strtotime($item->created_at));
            $item->photo = ($item->photo == '') ? url('/public/photos/nophoto.png') : url('/public/photos/'.$item->photo);
        }
        
		$res = array(
			'status' 	=> 'done',
			'code' 		=> 200,
			'data'      => $item
		);
		return response()->json($res);
	}
		function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
    public function payment_course($id){
        $course = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->where('OrderId',$id)->select('course.*','orders.*')->first();

        return view('site.pay_fee_external')->with(['id'=>$id,'course'=>$course]);
    }
    function payment($id){
        $coursee = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->where('OrderId',$id)->select('course.*','orders.*')->first();
		date_default_timezone_set('Asia/Riyadh');
		$now = date('Y-m-d H:i:s');
		$prev_min = date('Y-m-d H:i:s', strtotime('-20 minute'));	
	
		$query = DB::table('course')->where('id',$coursee->CourseId)->first();
		$reserved_places =DB::table('orders')->where('orders.CourseId',$coursee->CourseId)->where('orders.IsPaid',1)->count();
		$msg = '';
		if( $query->seats - $reserved_places > 0){
			
			$msg = 'done';
			$cou=DB::table('orders')->join('payment_logs', function($join)
				{
				   $join->on('orders.OrderId','=','payment_logs.orderid');
					$join->on('orders.order_ref','=','payment_logs.order_ref');

				})->where('orders.CourseId',$coursee->CourseId)->where('orders.order_ref','!=',NULL)->groupby('StudentId')->get();
			// dd($cou);
			if($query->seats - $cou->count() == 0 || $query->seats - $cou->count() < 0){
				$last_registered=DB::table('orders')->join('payment_logs', function($join)
				{
				   $join->on('orders.OrderId','=','payment_logs.orderid');
					$join->on('orders.order_ref','=','payment_logs.order_ref');

				})->where('orders.CourseId',$coursee->CourseId)->where('orders.order_ref','!=',NULL)->where('orders.IsPaid',0)
					->groupby('StudentId')->whereBetween('payment_logs.created_at', [$prev_min, $now])->get();
				// dd($last_registered);
				if($last_registered->count() > 0){
					$msg = 'عدد الاماكن غير متاحه حاليا .. حاول لاحقا بعد 20 دقيقة';
				}else{
					$cou=DB::table('orders')->where('orders.CourseId',$coursee->CourseId)->where('orders.IsPaid',1)->count();
					if($query->seats - $cou == 0){
						$msg = 'عدد الاماكن غير متاحة';
					
					}
				}				
			}
		}else{
			$msg = 'عدد الاماكن غير متاحة';	
		}
		
		
		

		// dd($msg);
		if($msg != 'done'){
			return view('site.pay_fee_external')->with(['id'=>$id,'course'=>$coursee , 'msg'=>$msg]);
		}else {       				
			$userid=$coursee->StudentId;
			$params = array(
				'ivp_method'  => 'create',
				// 'ivp_store'   => '23827',
				// 'ivp_authkey' => '4m8CB@TWP4X#8QLS',
				'ivp_store'   => '22077',
				'ivp_authkey' => 'GBXg#bs95h^LPLpS',
				'ivp_cart'    => $coursee->OrderId,
				'ivp_test'    => '0',
				'ivp_amount'  => $coursee->price+$coursee->tax,
				//    'ivp_amount'  => '1',
				'ivp_currency'=> 'SAR',
				'ivp_desc'    => $coursee->CourseName,
				'return_auth' => 'https://dallahdrivingschool.sa/api/course/ispaid/'.$coursee->OrderId.'/'.$userid,
				'return_can'  => 'https://dallahdrivingschool.sa/api/course/pay_exit/',
				'return_decl' => 'https://dallahdrivingschool.sa/api/course/ispaid/'.$coursee->OrderId.'/'.$userid,

			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
			curl_setopt($ch, CURLOPT_POST, count($params));
			curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
			$results = curl_exec($ch);

			curl_close($ch);
			$results = json_decode($results,true);
			if(isset($results['error'])) {
				print_r('ERROR: '.$results['error']['note']);
			}else {
				$ref = trim($results['order']['ref']);
				session(['ref' => $ref]);
				$url = trim($results['order']['url']);

			if (empty($ref) || empty($url)) {
				print_r($results['error']['note']);

			}else {
				session(['ref' => $ref]);
						 date_default_timezone_set('Asia/Riyadh');

				DB::table('orders')->where('OrderId',$coursee->OrderId)->update(['order_ref'=>$ref]);
				$insert = new Payment_log;
				$insert->orderid = $id;
				$insert->url = $url;
				$insert->cart_id =$coursee->OrderId;
				$insert->order_ref = $ref;
			   $insert->created_at	 = date('Y-m-d H:i:s');
			   $insert->save();

				//print_r($results2);
				return redirect($url);

				// return $url;
			}
			}		
		}
		
		

	}
	function payment_online($id,$userid){
        $coursee = DB::table('orders')->leftjoin('course','course.id','orders.CourseId')->where('OrderId',$id)->select('course.*','orders.*')->first();
		date_default_timezone_set('Asia/Riyadh');
		$now = date('Y-m-d H:i:s');
		$prev_min = date('Y-m-d H:i:s', strtotime('-20 minute'));	
	
		$query = DB::table('course')->where('id',$coursee->CourseId)->first();
		$reserved_places =DB::table('orders')->where('orders.CourseId',$coursee->CourseId)->where('orders.IsPaid',1)->count();
		$msg = '';
		if( $query->seats - $reserved_places > 0){
			
			$msg = 'done';
			$cou=DB::table('orders')->join('payment_logs', function($join)
				{
				   $join->on('orders.OrderId','=','payment_logs.orderid');
					$join->on('orders.order_ref','=','payment_logs.order_ref');

				})->where('orders.CourseId',$coursee->CourseId)->where('orders.order_ref','!=',NULL)->groupby('StudentId')->get();
			// dd($cou);
			if($query->seats - $cou->count() == 0 || $query->seats - $cou->count() < 0){
				$last_registered=DB::table('orders')->join('payment_logs', function($join)
				{
				   $join->on('orders.OrderId','=','payment_logs.orderid');
					$join->on('orders.order_ref','=','payment_logs.order_ref');

				})->where('orders.CourseId',$coursee->CourseId)->where('orders.order_ref','!=',NULL)->where('orders.IsPaid',0)
					->groupby('StudentId')->whereBetween('payment_logs.created_at', [$prev_min, $now])->get();
				// dd($last_registered);
				if($last_registered->count() > 0){
					$msg = 'عدد الاماكن غير متاحه حاليا .. حاول لاحقا بعد 20 دقيقة';
				}else{
					$cou=DB::table('orders')->where('orders.CourseId',$coursee->CourseId)->where('orders.IsPaid',1)->count();
					if($query->seats - $cou == 0){
						$msg = 'عدد الاماكن غير متاحة';
					
					}
				}				
			}
		}else{
			$msg = 'عدد الاماكن غير متاحة';	
		}
		
		
		

		// dd($msg);
		if($msg != 'done'){
			return view('site.pay_fee_external')->with(['id'=>$id,'course'=>$coursee , 'msg'=>$msg]);
		}else {       				
			// $userid=$coursee->StudentId;
			$params = array(
				'ivp_method'  => 'create',
				// 'ivp_store'   => '23827',
				// 'ivp_authkey' => '4m8CB@TWP4X#8QLS',
				'ivp_store'   => '22077',
				'ivp_authkey' => 'GBXg#bs95h^LPLpS',
				'ivp_cart'    => $coursee->OrderId,
				'ivp_test'    => '0',
				'ivp_amount'  => $coursee->price+$coursee->tax,
				//    'ivp_amount'  => '1',
				'ivp_currency'=> 'SAR',
				'ivp_desc'    => $coursee->CourseName,
				'return_auth' => 'https://dallahdrivingschool.sa/api/course/ispaid/'.$coursee->OrderId.'/'.$userid,
				'return_can'  => 'https://dallahdrivingschool.sa/api/course/pay_exit/',
				'return_decl' => 'https://dallahdrivingschool.sa/api/course/ispaid/'.$coursee->OrderId.'/'.$userid,

			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
			curl_setopt($ch, CURLOPT_POST, count($params));
			curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
			$results = curl_exec($ch);

			curl_close($ch);
			$results = json_decode($results,true);
			if(isset($results['error'])) {
				print_r('ERROR: '.$results['error']['note']);
			}else {
				$ref = trim($results['order']['ref']);
				session(['ref' => $ref]);
				$url = trim($results['order']['url']);

			if (empty($ref) || empty($url)) {
				print_r($results['error']['note']);

			}else {
				session(['ref' => $ref]);
						 date_default_timezone_set('Asia/Riyadh');

				DB::table('orders')->where('OrderId',$coursee->OrderId)->update(['order_ref'=>$ref]);
				$insert = new Payment_log;
				$insert->orderid = $id;
				$insert->url = $url;
				$insert->cart_id =$coursee->OrderId;
				$insert->order_ref = $ref;
			   $insert->created_at	 = date('Y-m-d H:i:s');
			   $insert->save();

				//print_r($results2);
				return redirect($url);

				// return $url;
			}
			}
		}
	}
	
	function ispaid($id,$userid){

            //$ord=Order::where('OrderId',$id)->where('deleted_at','!=',NULL)->first();
            $ord = DB::table('orders')->where('OrderId', $id)->first();
		$payment_logs = DB::table('payment_logs')->where('orderid',$id)->get();
		if(!empty($payment_logs) && $payment_logs->count() > 0){
				foreach($payment_logs as $log){
					 $params2 = array(
						'ivp_method'  => 'check',

					'ivp_store'   => '22077',
					'ivp_authkey' => 'GBXg#bs95h^LPLpS',				
						'order_ref'   => $log->order_ref,
					);
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
					curl_setopt($ch, CURLOPT_POST, count($params2));
					curl_setopt($ch, CURLOPT_POSTFIELDS,$params2);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
					$results2 = curl_exec($ch);
					$results2 = json_decode($results2,true);
					// print('<pre>');
					// print_r($results2);exit;
					$code=$results2['order']['status']['code'];

					curl_close($ch);
					if($code=='3'){
						if($results2['order']['transaction']['status']=='A'){
						
							$order_ref=$results2['order']['transaction']['ref'];
							$transactionType=$results2['order']['transaction']['type'];
							$transactionCode=$results2['order']['transaction']['code'];
						   $return_order_ref ='';
						   $order = DB::table('orders')->where('OrderId',$id)->where('order_ref',$results2['order']['ref'])->first();
							if(isset($order->order_ref) && !empty($order->order_ref)){
								$return_order_ref  = $order->order_ref;
							}

						   $payment_logs = DB::table('payment_logs')->where('orderid',$id)->where('order_ref',$results2['order']['ref'])->first();
							if(isset($payment_logs->order_ref) && !empty($payment_logs->order_ref)){
								$return_order_ref  = $payment_logs->order_ref;
							}					


							if($return_order_ref != ''){
								DB::table('orders')
								->where('OrderId',$id)
								->update(
									[
										'order_ref'  => $return_order_ref,
										'IsPaid'        =>1,
										'trans_no'      =>$order_ref,
										'trans_paid'    =>date('Y-m-d H:s'),
										'deleted_at'   =>'',
										'type_insert'=>$r
									]
								);					
							}
								$course = DB::table('orders')->leftjoin('course', 'course.id', 'orders.CourseId')->leftjoin('users', 'users.StudentId', 'orders.StudentId')
								->leftjoin('schools', 'schools.Id', 'orders.SchoolId')->leftjoin('nationality', 'nationality.Id', 'users.NationalityId')
								->leftjoin('periods', 'periods.PeriodId', 'course.Period')->where('orders.StudentId', $userid)->where('orders.OrderId', $id)
								->select('orders.*', 'course.CourseName', 'course.CourseStartIn','course.price','course.tax', 'course.CourseRefNo', 'course.days', 'periods.fromH', 'periods.toH', 'users.FullName', 'users.IDNumber', 'users.mobile', 'users.email', DB::RAW('nationality.Name as nationality'), DB::RAW('schools.Name as schoolName'))->first();

								$price = $course->price + $course->tax;
								$optios = DB::table('site_option')->where('varname', 'hm_bl4_contactform')->first();

								$msg = "
							<h1><img src='http://pharaohsland.tours/dalla/resources/views/site/images/logo.png' /></h1>
							<h4>عملية دفع كورس </h4>
							اسم الكورس :{$course->CourseName}<br />
							رقم الكورس :{$course->CourseRefNo}<br />
							اسم المستخدم :{$course->FullName}<br />
							رقم المستخدم :{$course->mobile}<br/>
							رقم العملية :{$order_ref}<br/>
							المبلغ :{$price} 
							
						";

								#send msg to email(1)
								Mail::send('emails.send', ['msg' => $msg], function ($message) use ($course, $optios) {
									$message->from('no-reply@mail.com', 'شركة ﺩﻟﻪ ﻟتعليم قيادة اﻟﺴﻴﺎﺭاﺕ');
									$message->to($course->email)->subject(" عملية دفع كورس");
									$message->to($optios->value)->subject(" عملية دفع كورس");
								});


							$msg = "1";
							break;
						}else{
							$msg = "3";

						}
					}else{
						$msg = "2";
					}					
				}
		}		
  
        
     return redirect('https://dallahdrivingschool.sa/api/course/
     
     
     /'.$msg);
	    
	   
            }
            
            function pay_exit($msg){
                exit();
            }



    public function course_start(Request $request){
        $query = \Illuminate\Support\Facades\DB::table('course')->leftjoin('applicationdef','applicationdef.Id','course.applicationdef')->leftjoin('category','category.Id','course.category')->leftjoin('schools','schools.Id','course.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')->select('course.*',DB::RAW('category.Name categoryName'),'periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'));

        $query->whereDate('course.CourseStartIn','>=',Carbon::today()->addDays(+1));
        $query->where('course.type',2);
        $query->where('course.enable',1);
        $query->where('course.waiting_list',0);
        if(!empty($request->user()->StudentId)) {

            $date=$request->user()->DoB;
            $date_type=$request->user()->date_type;


            if($date_type==0) {
                $date=$request->user()->DoB;
            }else if($date_type==1) {
                $time=explode("-",$date);
                $day=$time['2'];

                $month=$time['1'];
                $year=$time['0'];

                $date=$request->user()->DoB;

            }

            $date1=date('Y/m/d',strtotime($date));
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da_st=date('Ymd',strtotime($date1));

            $date_sta="{$da_st}";
            $d_st =$DateConv->GregorianToHijri($date_sta,$format);
            $dh_start=date('Y/m/d',strtotime($d_st));


            $today = Carbon::today();
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da=date('Ymd',strtotime($today));

            $datev="{$da}";
            $d =$DateConv->GregorianToHijri($datev,$format);
            $dh_today=date('Y/m/d',strtotime($d));

            $startDate = Carbon::parse($dh_start);
            $endDate   = Carbon::parse($dh_today);
            $y = $startDate->diff($endDate)->format("%y");

            if (intval($y) < 18) {
                $query->where('applicationdef', '5');
            }else{
                $query->where('applicationdef', '!=', '5');
            }

            $school=$request->user()->school;
            $query->where('course.SchoolId',$school);

        }

        $courses = $query->get();

        $res = array(
            'status' 	=> 'done',
            'code' 		=> 200,
            'data'      => $courses
        );
        return response()->json($res);

    }
    public function course_start_wait(Request $request){
        $query = \Illuminate\Support\Facades\DB::table('course')->leftjoin('applicationdef','applicationdef.Id','course.applicationdef')->leftjoin('category','category.Id','course.category')->leftjoin('schools','schools.Id','course.SchoolId')->leftjoin('periods','periods.PeriodId','course.Period')->select('course.*',DB::RAW('category.Name categoryName'),'periods.fromH','periods.toH',DB::RAW('schools.Name as SchoolName'));

        $query->whereDate('course.CourseStartIn','>=',Carbon::today()->addDays(+1));
        $query->where('course.type',2);
        $query->where('course.enable',1);
        $query->where('course.waiting_list',1);
        if(!empty($request->user()->StudentId)) {

            $date=$request->user()->DoB;
            $date_type=$request->user()->date_type;


            if($date_type==0) {
                $date=$request->user()->DoB;
            }else if($date_type==1) {
                $time=explode("-",$date);
                $day=$time['2'];

                $month=$time['1'];
                $year=$time['0'];

                $date=$request->user()->DoB;

            }

            $date1=date('Y/m/d',strtotime($date));
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da_st=date('Ymd',strtotime($date1));

            $date_sta="{$da_st}";
            $d_st =$DateConv->GregorianToHijri($date_sta,$format);
            $dh_start=date('Y/m/d',strtotime($d_st));


            $today = Carbon::today();
            $DateConv=new Hijri;
            $format="YYYYMMDD";
            $da=date('Ymd',strtotime($today));

            $datev="{$da}";
            $d =$DateConv->GregorianToHijri($datev,$format);
            $dh_today=date('Y/m/d',strtotime($d));

            $startDate = Carbon::parse($dh_start);
            $endDate   = Carbon::parse($dh_today);
            $y = $startDate->diff($endDate)->format("%y");

            if (intval($y) < 18) {
                $query->where('applicationdef', '5');
            }else{
                $query->where('applicationdef', '!=', '5');
            }

            $school=$request->user()->school;
            $query->where('course.SchoolId',$school);

        }

        $courses = $query->get();

        $res = array(
            'status' 	=> 'done',
            'code' 		=> 200,
            'data'      => $courses
        );
        return response()->json($res);

    }
    public function get_time(Request  $request)
    {
        $day=date('Y-m-d',strtotime($request->id));
        $userid = $request->user()->StudentId;
        $school=$request->user()->school;
        $user=User::where('StudentId',$userid)->first();

        $times_school = Evaluation_time::where('school_id', $request->user()->school)
            ->where('status','1')
            ->get();
        $re=array();

        if(!empty($times_school)) {

            foreach ($times_school as $co):

                $counter = Evaluation_reserve::join('evaluation_periods', 'evaluation_periods.id', '=', 'evaluation_reserves.time_id')
                    ->Join('users', 'users.StudentID', '=', 'evaluation_reserves.student_id')
                    ->where('day', $day)
                    ->where('time_id', $co->period_id)
                    // ->groupBy('evaluation_reserves.time_id')
                    ->where('school', $request->user()->school)
                    ->get();
                $hour = Evaluation_time::where('period_id', $co->period_id)
                    ->where('school_id', $request->user()->school)
                    ->first();

                if (!empty($hour)) {
                    if ($user->gender == 'female') {
                        $ge = $hour->slots_female;
                    } else {
                        $ge = $hour->slots_male;
                    }

                    if (count($counter) < $ge) {
                        $today=date('Y-m-d');
                        if($day==$today) {
                            $ho = date('h A');
                            $hh = str_split($ho, strlen($ho) - 2);
                            $da = \Illuminate\Support\Facades\DB::table('evaluation_periods')->WhereNull('deleted_at')->where('id', $co->period_id)->first();

                            if (!empty($da)) {


                                $ho1 = $da->fromH;
                                $hours = str_split($ho1, strlen($ho1) - 2);
                                $format = $hours[1];

                                if ($format == 'AM' and $hh[1] == 'AM') {

                                    if ($hours[0] > $hh[0]) {
                                        $res = 'true';
                                        $re[] = $da;
                                    } else {
                                        $res = 'false';
                                    }
                                } else if ($format == 'AM' and $hh[1] == 'PM') {
                                    $res = 'false';

                                } else if ($format == 'PM' and $hh[1] == 'AM') {
                                    $res = 'true';
                                    $re[] = $da;
                                } else if ($format == 'PM' and $hh[1] == 'PM') {
                                    if ($hours[0] < $hh[0]) {
                                        $res = 'true';
                                        $re[] = $da;
                                    } else {
                                        $res = 'false';
                                    }
                                }

                            }

                        }else {

                            $data = DB::table('evaluation_periods')->WhereNull('deleted_at')->where('id', $co->period_id)->first();

                            $re[] = $data;
                        }
                    }
                }
            endforeach;

        }

        $times_schoolss = Evaluation_time::where('school_id', $request->user()->school)
            ->first();
        $sen['success'] = true;
        // $sen['result'] = $re->toArray();
        sort($re);
        $sen['result'] = $re;
        $sen['reserve'] = count($counter);
        $sen['count'] = $times_schoolss->slots_female;
        return response()->json($sen);
    }
    public function reserve(Request $request)
    {
        $userid = $request->user()->StudentId;
        $user=User::where('StudentId',$userid)->first();
        $reserve=Evaluation_reserve::leftjoin('evaluation_times','evaluation_times.id','=','evaluation_reserves.time_id')
            ->where('student_id',$userid)
            ->where('attend',0)->select('evaluation_reserves.*')->first();

        $hour=Evaluation_period::where('id',$request->time)->first();

        $ho=$hour->fromH;
        $hours=str_split($ho, strlen($ho) - 2);
        $format=$hours[1];
        if($format=='AM')
        {
            $fo="صباحا".$hours[0];
        }else{
            $fo="مساءا".$hours[0];
        }
        $time=Evaluation_time::where('period_id',$hour->id)
            ->where('school_id',$request->user()->school)->first();
        // dd($time);
        if(empty($reserve))
        {
            $day=date('Y-m-d',strtotime($request->day));
            Evaluation_reserve::create(['time_id' => $request->time,
                'day'=>$day,
                'student_id' => $userid]);
            if($user->gender=='male') {
                $ti = $time->slots_avaliable_male;
                Evaluation_time::where('id', $time->id)->update(['slots_avaliable_male' => $ti - 1]);

            }else{
                $ti = $time->slots_avaliable_female;
                Evaluation_time::where('id', $time->id)->update(['slots_avaliable_female' => $ti - 1]);

            }

            $msg="عزيزتى الطالبة ".$user->IDNumber." تم حجز موعد اختبار تحديد المستوى بنجاح وتاريخ الاختبار ".$request->day." الساعة ".$fo;
            $this->send_sms($user->mobile,$msg);
            $res = array(
                'status' 	=> 'done',
                'code' 		=> 200,
                'message'      =>  'تم الحجز بنجاح'
            );
            return response()->json($res);
        } else if( $request->day > $reserve->day ) {
            Evaluation_reserve::where('id',$reserve->id)->delete();
            Evaluation_reserve::create(['time_id' => $request->time,
                'day'=>$request->day,
                'student_id' => $userid]);
            if($user->gender=='male') {
                Evaluation_time::where('id', $time->id)->update(['slots_avaliable_male' => $time->slots_avliable_male - 1]);
            }else{
                Evaluation_time::where('id', $time->id)->update(['slots_avaliable_female' => $time->slots_avliable_female - 1]);

            }
            $msg="عزيزتى الطالبة ".$user->IDNumber." تم حجز موعد اختبار تحديد المستوى بنجاح وتاريخ الاختبار ".$request->day." الساعة ".$fo;
            $this->send_sms($user->mobile,$msg);
            $res = array(
                'status' 	=> 'done',
                'code' 		=> 200,
                'message'      =>  'تم الحجز بنجاح'
            );
            return response()->json($res);
        }else{
            $res = array(
                'status' 	=> 'error',
                'code' 		=> 231,
                'message'      =>  'انت مسجل فى موعد اختبار اخر'
            );
            return response()->json($res);
        }

    }
    public function send_sms($phone,$msg)
    {
        $phonenew=substr($phone,0,1);
        if($phonenew!='0')
        {
            $phone=$phone;
        }else{
            $phone=substr($phone,1);
        }
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://www.4jawaly.net/api/sendsms.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, 'username=dalla&password=565656&message='.$msg.'&numbers=966'.$phone.'&sender=DDC-DRIVING&unicode=utf8&Rmduplicated=1&return=json');


        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/x-www-form-urlencoded"
        ));

        $response = curl_exec($ch);

        curl_close($ch);

    }
    public function videos(Request $request){
        $studentid=$request->user()->StudentId;
        $today=date('Y-m-d');
        $orders= \Illuminate\Support\Facades\DB::table('orders')
            ->join('course','orders.CourseId','=','course.id')
            //->join('parent_courses','parent_courses.co_id','=','course.type')
            ->join('videos','videos.course_id','=','course.type')
            ->where('orders.StudentId',$studentid)
            ->where('orders.IsPaid','1')
            ->whereNULL('videos.deleted_at')
            ->where('orders.course_start','<=',$today)
            ->where('orders.course_end','>=',$today)
            ->get();

        $data['path']=url('/public/videos/').'/';
        $data['orders']=$orders;

        $res = array(
            'status' 	=> 'done',
            'code' 		=> 200,
            'data'      =>  $data
        );
        return response()->json($res);

    }
    public function get_home_option(){
        $optios = \Illuminate\Support\Facades\DB::table('site_option')->get();
        foreach($optios as $item){
            $arr[$item->varname] = $item->value;
        }
        return $arr;
    }
    public function testingpage(Request $request){
	    $lang = $request->header('Accept-Language');

        $options = $this->get_home_option();
        if($lang=='en')
        {
            $items = \Illuminate\Support\Facades\DB::table('questions')->select('id','name_en','photo')->get()->random(32);
            $items->map(function($item){
                $item->asw = DB::table('answers')->where('q_id',$item->id)->select('id','name_en','correct')->get();
            });
        }else{
            $items = \Illuminate\Support\Facades\DB::table('questions')->select('id','name','photo')->get()->random(32);
            $items->map(function($item){
                $item->asw = DB::table('answers')->where('q_id',$item->id)->select('id','name','correct')->get();
            });
        }


        $res = array(
            'status' 	=> 'done',
            'code' 		=> 200,
            'data'      =>  $items
        );
        return response()->json($res);

    }
    public function testingpage_lang(Request $request){
        $options = $this->get_home_option();
        $lang = $request->lang;
        if($lang=='ar'){
            $items = \Illuminate\Support\Facades\DB::table('questions')->select('id','name','photo')->get()->random(32);
            $items->map(function($item){
                $item->asw = DB::table('answers')->where('q_id',$item->id)->select('id','name','correct')->get();
            });

        }else {
            $items = \Illuminate\Support\Facades\DB::table('questions')->select('id', 'name_' . $lang, 'photo')->get()->random(32);
            $items->map(function ($item) use ($lang){
                $item->asw = DB::table('answers')->where('q_id', $item->id)->select('id', 'name_'.$lang , 'correct')->get();
            });

        }


        $res = array(
            'status' 	=> 'done',
            'code' 		=> 200,
            'data'      =>  $items
        );
        return response()->json($res);
    }

    public function checkAnswers(Request $request){
        foreach($request->input('q') as $k => $v){
            if(isset($request->input('sw')[$k])){
                $arr = [
                    'q' => $v,
                    'sw' => $request->input('sw')[$k],
                    'correct' => ($this->wrongAnswers($v) == $request->input('sw')[$k]) ? 1 : 0
                ];
            }else{
                $arr = [
                    'correct' =>  0
                ];
            }
        }
        return response()->json($arr);
    }
    private function wrongAnswers($id){
        $item = \Illuminate\Support\Facades\DB::table('answers')->where('q_id',$id)->where('correct',1)->first();
        return $item->id;
    }
    public function testing(Request $request){
        $total = 0;
        foreach($request->input('q') as $k => $v){
            if(isset($request->input('sw')[$k])){
                if($this->wrongAnswers($v) == $request->input('sw')[$k]){
                    $total += 1;
                }
                $arr[] = [
                    'q' => $v,
                    'sw' => $request->input('sw')[$k],
                    'correct' => ($this->wrongAnswers($v) == $request->input('sw')[$k]) ? 1 : 0
                ];
            }
        }

        if($request->input('lang') == 'en'){
            //$table = "Result : {$total} / 32";
            if($total < 24){
                $table = "Failure";
            }else{
                $table = "successful";
            }

        }else{
           // $table = "النتيجة : {$total} / 32";
            if($total < 24){
                $table = "راسب";
            }else{
                $table = "ناجح";
            }

        }

        return response()->json(['items'=>$table,'total' => $total.'/100','success'=>$total,'danger'=>(32 - $total)]);

    }


    public  function course_start_new(Request $request)
    {
        $student_id= $request->user()->StudentId;

        $add =New_order::where('student_id',$student_id)->first();
        if(!empty($add)){
            $msg='أنت مشترك من قبل';
            return response()->json(['msg'=>$msg]);
        }else{
            $user=User::where('StudentId',$student_id)->first();
            $school_name= \Illuminate\Support\Facades\DB::table('schools')->where('Id',$user->school)->first();
            $order_no=random_int(100000, 99999999);
            $order_same=New_order::where('order_no',$order_no)->first();
            if(empty($order_same)) {
                $msg = "تم الإشتراك فى قائمة الإنتظار بشركة دله لتعليم قيادة السيارات – " . $school_name->Name . " ورقم الاشتراك  " . $order_no." لعضوية رقم ".$user->IDNumber." وسيتم تحديد الموعد وارسال رساله بالموعد عند توفره";
                New_order::create(['student_id' => $student_id, 'order_no' => $order_no]);
                $this->send_sms($user->mobile, $msg);
            }else{
                $order_no=random_int(100000, 99999999);
                $msg = "تم الإشتراك فى قائمة الإنتظار بشركة دله لتعليم قيادة السيارات – " . $school_name->Name . " ورقم الاشتراك  " . $order_no." لعضوية رقم ".$user->IDNumber." وسيتم تحديد الموعد وارسال رساله بالموعد عند توفره";
                New_order::create(['student_id' => $student_id, 'order_no' => $order_no]);
                $this->send_sms($user->mobile, $msg);
            }

            $msg='تم الاشتراك بنجاح';
            return response()->json(['msg'=>$msg]);
        }
    }


    public function evaluation(Request $request){
        $schools= \Illuminate\Support\Facades\DB::table('schools')->get();
        $userid = $request->user()->StudentId;
        $user=User::where('StudentId',$userid)->first();
        $times=Evaluation_time::where('status',1)
            ->where('slots_'.$user->gender,'>','0')->get();
        $setting=DB::table('site_option')->where('varname','evaluation_time')->first();
        $setting_id=DB::table('site_option')->where('varname','id_setting')->first();
        $idno=$user->IDNumber;


        $counte = DB::table('evaluation_reserves')->join('evaluation_periods', 'evaluation_periods.id', '=', 'evaluation_reserves.time_id')
            ->Join('users','users.StudentID','=','evaluation_reserves.student_id')
            ->where('school', $request->user()->school)
            ->where('day','!=','0000-00-00')
            ->where('day','!=','1970-01-01')
            ->whereNULL('evaluation_reserves.deleted_at')
            ->groupBy('evaluation_reserves.day')
            ->select('school','day',DB::RAW('COUNT(evaluation_reserves.student_id) as booked'))->get();


        $count_time=Evaluation_time::where('school_id',$request->user()->school)
            ->select(DB::RAW('SUM(slots_female) as realf'),DB::RAW('SUM(slots_female) as realtm'))->first();

        if(!empty($count_time)) {
            if ($user->gender == 'female') {
                $real_time = $count_time->realf;
            } else {
                $real_time = $count_time->realtm;
            }
        }
        $days=array();
        if(!empty($counte))
        {
            foreach ($counte as $cou):
                if ((int)$cou->booked >= (int)$real_time) {
                    $days[] = date('Y-n-j',strtotime($cou->day));
                }

            endforeach;
        }

        $days='["'.implode('","',$days).'"]';

        return response()->json(['days'=>$days]);
    }

}