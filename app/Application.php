<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    //
	protected $table = 'applicationdef';
	
	protected $primaryKey = 'Id';
	
	public $timestamps = false;
}
