<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
class Order extends Model
{
    //
    use Notifiable;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'orders';
    protected $primaryKey = 'OrderId';
}
