<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    //
	protected $table = 'periods';
	
	protected $primaryKey = 'PeriodId';
	
	public $timestamps = false;
}
